FROM node:19 as base

ARG MODE=production
ENV NODE_ENV="$MODE"

WORKDIR /app

COPY . .

# RUN if [ "$MODE" = "development" ] ; then yarn install ; else yarn install --production ; fi
# For now dev deps are also used in production, to create the dist packages in
# the build step, so full install is needed

RUN yarn install --production=false

RUN yarn build

CMD yarn "$(if [ $NODE_ENV = 'development' ] ; then echo 'start:dev' ; else echo 'start'; fi)"