#!/bin/sh

':' // ; cat "$0" | node --input-type=module - $@ ; exit $?

import { execSync } from 'child_process';
import { readFileSync, writeFileSync, existsSync } from 'fs';
import { join, dirname } from 'path';

const {
    translationTargets: targets = {}
} = JSON.parse(readFileSync('./package.json'));

const argv = [...process.argv].slice(2);

const customTargets = argv.filter(arg => arg.startsWith('--target=')).map(arg => arg.slice(9));
const customLocales = argv.filter(arg => arg.startsWith('--locale=')).map(arg => arg.slice(9));

function isObject(o){
    return Object.prototype.toString.call(o) === '[object Object]';
}

function isString(o){
    return typeof o === 'string';
}

function isArray(o){
    return Array.isArray(o);
}

function translate(locale, source) {
    const result = execSync(`trans -b -t ${JSON.stringify(locale)} ${JSON.stringify(source)}`, { encoding: 'utf-8' });
    return result.trim();
}

function mergeTranslate(locale,target,source){
    for(const [key,value] of Object.entries(source)){
        if(isObject(value)){
            target[key] = mergeTranslate(locale,target[key] ?? {},value);
        }/*else if(isArray(value)){
            target[key] = value.map((sourceItem,index)=>mergeTranslate(locale,target?.[index] ?? {}));
        }*/else if(isString(value) && !(key in target)){
            target[key] = translate(locale,value);
        }
    }
    return target;
}

const cache = new Map();

function translateFile(targetLocale, source) {
    console.log(`translating ${source} to ${targetLocale}`);

    const sourceContent = cache.get(source) ?? JSON.parse(readFileSync(source, { encoding: 'utf-8' }));
    cache.set(source, sourceContent);
    const targetFile = join(dirname(source), `${targetLocale}.json`);

    let result;

    if (existsSync(targetFile)) {
        const currentTargetContent = JSON.parse(readFileSync(targetFile, { encoding: 'utf-8' }));
        result = mergeTranslate(targetLocale,currentTargetContent,sourceContent);
    } else {
        result = mergeTranslate(targetLocale,{},sourceContent);
    }

    writeFileSync(targetFile, JSON.stringify(result, undefined, 2));
}

function translateTarget(translateSources,targetLocales,allLocales){
    for (const source of translateSources) {
        const indexSource = [
            `/**
    * Auto generated import statements, do not modify this file manually
    */

    export {default as default} from './default.json';
    `
        ];
        for (const target of allLocales) {
            indexSource.push(`export {default as ${target}} from './${target}.json';`);
        }

        for (const locale of targetLocales) {
            translateFile(locale, source);
        }
        const outFileName = join(dirname(source), `index.ts`);
        console.log(`writing index module ${outFileName}`);
        writeFileSync(outFileName, indexSource.join('\n\r'));
    }
}

for(const [target,config] of Object.entries(targets)){
    if(customTargets.length && !customTargets.includes(target)) continue;
    const sources = execSync(`find packages -path "${config.source}"`,{encoding:'utf-8'}).split('\n').map(row=>row.trim()).filter(row => Boolean(row));
    const locales = customLocales.length ? customLocales : config.locales;
    translateTarget(sources,locales,config.locales);
}