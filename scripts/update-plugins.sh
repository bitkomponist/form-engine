#!/usr/bin/env bash

DASH_TO_CAMELCASE='BEGIN{FS="";RS="-";ORS=""} {$0=toupper(substr($0,1,1)) substr($0,2)} 1'

CORE_JS_PLUGINS_OUTPUT='packages/core/application/plugins.tsx'
CORE_JS_PLUGIN_MANIFEST='core.plugin.tsx'
CORE_SCSS_PLUGINS_OUTPUT='packages/core/application/plugins.scss'
CORE_SCSS_PLUGIN_MANIFEST='core.plugin.scss'

BUILDER_JS_PLUGINS_OUTPUT='packages/builder/application/plugins.tsx'
BUILDER_JS_PLUGIN_MANIFEST='builder.plugin.tsx'
BUILDER_SCSS_PLUGINS_OUTPUT='packages/builder/application/plugins.scss'
BUILDER_SCSS_PLUGIN_MANIFEST='builder.plugin.scss'

SERVER_JS_PLUGINS_OUTPUT='packages/server/application/plugins.ts'
SERVER_JS_PLUGIN_MANIFEST='server.plugin.ts'

# write core plugin js imports

touch "$CORE_JS_PLUGINS_OUTPUT"
cat > "$CORE_JS_PLUGINS_OUTPUT" <<EOL
/**
 * Auto generated import statements, do not modify this file manually
 */

EOL

for FILE in $(find packages -name "${CORE_JS_PLUGIN_MANIFEST}"); do
  JS_EXPORT_NAME=$(dirname $FILE | sed "s/\//-/g" | awk "${DASH_TO_CAMELCASE}")
  RELATIVE_PATH=$(echo $FILE | sed "s/^packages\//\.\.\/\.\.\//g")
  echo "export { default as ${JS_EXPORT_NAME} } from '${RELATIVE_PATH%.tsx}';" >> "$CORE_JS_PLUGINS_OUTPUT"
done

# write core plugin js imports

touch "$CORE_SCSS_PLUGINS_OUTPUT"
cat > "$CORE_SCSS_PLUGINS_OUTPUT" <<EOL
/**
 * Auto generated import statements, do not modify this file manually
 */

EOL

for FILE in $(find packages -name "${CORE_SCSS_PLUGIN_MANIFEST}"); do
  RELATIVE_PATH=$(echo $FILE | sed "s/^packages\//\.\.\/\.\.\//g")
  echo "@import '${RELATIVE_PATH}';" >> "$CORE_SCSS_PLUGINS_OUTPUT"
done

# write builder plugin js imports

touch "$BUILDER_JS_PLUGINS_OUTPUT"
cat > "$BUILDER_JS_PLUGINS_OUTPUT" <<EOL
/**
 * Auto generated import statements, do not modify this file manually
 */

EOL

for FILE in $(find packages -name "${BUILDER_JS_PLUGIN_MANIFEST}"); do
  JS_EXPORT_NAME=$(dirname $FILE | sed "s/\//-/g" | awk "${DASH_TO_CAMELCASE}")
  RELATIVE_PATH=$(echo $FILE | sed "s/^packages\//\.\.\/\.\.\//g")
  echo "export { default as ${JS_EXPORT_NAME} } from '${RELATIVE_PATH%.tsx}';" >> "$BUILDER_JS_PLUGINS_OUTPUT"
done

# write builder plugin js imports

touch "$BUILDER_SCSS_PLUGINS_OUTPUT"
cat > "$BUILDER_SCSS_PLUGINS_OUTPUT" <<EOL
/**
 * Auto generated import statements, do not modify this file manually
 */

EOL

for FILE in $(find packages -name "${BUILDER_SCSS_PLUGIN_MANIFEST}"); do
  RELATIVE_PATH=$(echo $FILE | sed "s/^packages\//\.\.\/\.\.\//g")
  echo "@import '${RELATIVE_PATH}';" >> "$BUILDER_SCSS_PLUGINS_OUTPUT"
done

# write server plugin js imports

touch "$SERVER_JS_PLUGINS_OUTPUT"
cat > "$SERVER_JS_PLUGINS_OUTPUT" <<EOL
/**
 * Auto generated import statements, do not modify this file manually
 */

EOL

for FILE in $(find packages -name "${SERVER_JS_PLUGIN_MANIFEST}"); do
  JS_EXPORT_NAME=$(dirname $FILE | sed "s/\//-/g" | awk "${DASH_TO_CAMELCASE}")
  RELATIVE_PATH=$(echo $FILE | sed "s/^packages\//\.\.\/\.\.\//g")
  echo "export { default as ${JS_EXPORT_NAME} } from '${RELATIVE_PATH%.ts}';" >> "$SERVER_JS_PLUGINS_OUTPUT"
done