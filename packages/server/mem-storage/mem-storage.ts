import {
    CrudEntity,
    CrudSchema,
} from '@form-engine/server-application/typings';
import { MemStorageAdapterSettings } from '.';
import { join, dirname } from 'path';
import { readFile, writeFile, existsSync, mkdirSync } from 'fs';
import { promisify } from 'util';

export default class MemStorage {
    static storage = new Map<CrudSchema, CrudEntity[]>();

    protected loaded = false;

    constructor(
        public schema: CrudSchema,
        public settings: MemStorageAdapterSettings
    ) {}

    get storagePath() {
        return join(this.settings.storagePath, `${this.schema.name}.json`);
    }

    get data() {
        if (!MemStorage.storage.get(this.schema)) {
            MemStorage.storage.set(this.schema, []);
        }
        return MemStorage.storage.get(this.schema) as CrudEntity[];
    }

    set data(data: CrudEntity[]) {
        MemStorage.storage.set(this.schema, data);
    }

    protected async ensureLoaded() {
        if (!this.loaded) {
            await this.read();
            this.loaded = true;
        }
    }

    async read() {
        if (!this.settings.persist) return;
        const { storagePath } = this;

        if (!existsSync(storagePath)) {
            return;
        }

        const text = await promisify(readFile)(storagePath, {
            encoding: 'utf8',
        });

        try {
            this.data = JSON.parse(text);
        } catch (e) {
            throw new Error(
                `mem storage ${storagePath} corrupted: ${(e as any)?.message}`
            );
        }
    }

    async write() {
        if (!this.settings.persist) return;
        const { storagePath } = this;

        if (!existsSync(storagePath)) {
            mkdirSync(dirname(storagePath), { recursive: true });
        }

        await promisify(writeFile)(
            storagePath,
            JSON.stringify(this.data, undefined, 2),
            { encoding: 'utf8' }
        );
    }

    async get() {
        await this.ensureLoaded();
        return this.data;
    }

    async set(data: CrudEntity[]) {
        this.data = data;
        return this.write();
    }

    async setItem(index: number, entity: CrudEntity) {
        await this.ensureLoaded();
        this.data[index] = entity;
        await this.write();
    }

    async splice(start: number, deleteCount?: number | undefined) {
        this.ensureLoaded();
        this.data.splice(start, deleteCount);
        await this.write();
    }

    async push(...entities: CrudEntity[]) {
        this.ensureLoaded();
        this.data = [...this.data, ...entities];
        await this.write();
    }
}
