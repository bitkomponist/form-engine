import {
    CrudStorageAdapter,
    CrudSchema,
} from '@form-engine/server-application/typings';
import { Model } from './model';
import { join } from 'path';
export interface MemStorageAdapterSettings {
    persist: boolean;
    storagePath: string;
}

export type MemStorageAdapterOptions = Partial<MemStorageAdapterSettings>;

export default function MemStorageAdapter(
    options: MemStorageAdapterOptions = {}
): CrudStorageAdapter {
    const settings: MemStorageAdapterSettings = {
        persist: true,
        storagePath: join(process.cwd(), '.storage'),
        ...options,
    };

    return async (schema: CrudSchema) => {
        const model = new Model(schema, settings);
        return Promise.resolve(model);
    };
}
