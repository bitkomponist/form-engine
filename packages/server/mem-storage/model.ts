import {
    CrudModel,
    CrudEntity,
    CrudSchema,
    CrudModelFindArgs,
    CrudModelFindResult,
    JSONValue,
    CrudSchemaVersioningSettings,
} from '@form-engine/server-application/typings';
import { MemStorageAdapterSettings } from '.';
import MemStorage from './mem-storage';
import sift from 'sift';
export class Model<
    T extends CrudSchema = CrudSchema,
    E extends CrudEntity<T> = CrudEntity<T>
> implements CrudModel<T, E>
{
    storage: MemStorage;

    constructor(public schema: T, public settings: MemStorageAdapterSettings) {
        this.storage = new MemStorage(schema, settings);
    }

    getSchemaFieldDefault(key: string) {
        const def = this.schema.fields[key];

        if (!def) throw new Error(`unknown field ${key}`);

        const type = typeof def.default;

        if (type === 'function') {
            return (def.default as any)();
        } else if (type === 'object') {
            return structuredClone(def.default);
        }

        return def.default;
    }

    updateEntity(original: E, update: Partial<E>) {
        const sanitizedUpdate = structuredClone(update) as Partial<E>;

        for (const [key, def] of Object.entries(this.schema.fields)) {
            if (def.createOnly || def.readOnly) {
                delete sanitizedUpdate[key];
            }
            if (def.forceUpdate) {
                (sanitizedUpdate as any)[key] = this.getSchemaFieldDefault(key);
            }
        }

        return structuredClone({ ...original, ...sanitizedUpdate }) as E;
    }

    ensureEntity(partialDto: Partial<E> = {}) {
        const defaultEntity = Object.fromEntries(
            Object.keys(this.schema.fields).map((key) => {
                return [key, this.getSchemaFieldDefault(key)];
            })
        );
        return structuredClone({ ...defaultEntity, ...partialDto }) as E;
    }

    createEntity(partialDto: Partial<E> = {}) {
        const dto = { ...partialDto };
        Object.entries(this.schema.fields).forEach(([key, def]) => {
            if (def.readOnly || def.forceDefault || def.updateOnly) {
                delete dto[key];
            }
        });
        return this.ensureEntity(dto);
    }

    pickEntityAttributes(dto: Partial<E>, attributes: Array<keyof E>) {
        const result = Object.fromEntries(
            Object.entries(dto).filter(([key]) => attributes.includes(key))
        );

        return result as Partial<E>;
    }
    async validateEntity(dto: Partial<E>) {
        for (const [key, def] of Object.entries(this.schema.fields)) {
            if (def.required && !(key in dto)) {
                throw new Error(`required field ${key} missing`);
            }
        }

        for (const [key, value] of Object.entries(dto)) {
            if (!(key in this.schema.fields)) {
                throw new Error(`invalid property ${key}`);
            }
            if (this.schema.fields[key].validate) {
                await this.schema.fields[key]?.validate?.(value);
            }
        }
    }
    async create(dto: E) {
        const writeDto = this.createEntity(dto);

        await this.storage.push(writeDto);

        return Promise.resolve(structuredClone(writeDto));
    }
    async retrieve(id: string): Promise<E | undefined> {
        const dto = (await this.storage.get()).find((e) => e.id === id);

        if (!dto) return Promise.resolve(undefined);

        return Promise.resolve(structuredClone(dto) as E);
    }
    async update(dto: E & Omit<Partial<E>, 'id'>): Promise<E> {
        const localDto = (await this.storage.get()).find(
            (e) => e.id === dto.id
        );

        if (!localDto) {
            throw new Error(`${dto.id} not found`);
        }

        const updatedEntity = this.updateEntity(localDto as E, dto);

        Object.assign(localDto, updatedEntity);

        await this.storage.write();

        return Promise.resolve(structuredClone(localDto) as E);
    }
    async delete(id: string) {
        const data = await this.storage.get();
        const index = data.findIndex((e) => e.id === id);

        if (index < 0) {
            throw new Error(`${id} not found`);
        }
        await this.storage.splice(index, 1);
        return Promise.resolve();
    }
    async find(
        findArgs: CrudModelFindArgs<T, E> = {}
    ): Promise<CrudModelFindResult<T, E>> {
        const storage = await this.storage.get();

        let allResults = storage;

        const { versioningSettings } = this;

        if (findArgs.includeVersioningHistory !== true && versioningSettings) {
            allResults = allResults.filter(
                (entity) => !entity[versioningSettings.versioningParentIdField]
            );
        }

        if (findArgs.filter) {
            allResults = allResults.filter(sift(findArgs.filter as any));
        }

        if (findArgs.sort) {
            const sortEntries = Object.entries(findArgs.sort);
            allResults.sort((a, b) => {
                for (const [key, direction] of sortEntries) {
                    if (this.schema.fields[key] && a[key] !== b[key]) {
                        let valueA: JSONValue | Date = a[key] ?? '';
                        let valueB: JSONValue | Date = b[key] ?? '';

                        if (this.schema.fields[key]?.type === Date) {
                            valueA = new Date(String(valueA));
                            valueB = new Date(String(valueB));
                        }

                        if (direction === 'asc') {
                            return valueA > valueB ? 1 : -1;
                        } else {
                            return valueA > valueB ? -1 : 1;
                        }
                    }
                }
                return 0;
            });
        }

        let startIndex = 0;

        if (findArgs.startAt) {
            startIndex = allResults.findIndex(
                ({ id }) => id === findArgs.startAt
            );
        }

        const { limit = 0 } = findArgs;

        let results = allResults.slice(
            startIndex,
            limit > 0 ? startIndex + limit : undefined
        );

        let lastKey: string | undefined = undefined;

        if (results.length < allResults.length) {
            lastKey = results.at(results.length - 1)?.id as string;
        }

        if (Array.isArray(findArgs.attributes)) {
            results = results.map((dto) =>
                this.pickEntityAttributes(
                    dto as Partial<E>,
                    findArgs.attributes as string[]
                )
            ) as E[];
        } else {
            results = results.map((dto) => structuredClone(dto)) as E[];
        }

        return Promise.resolve({
            results,
            lastKey,
            count: allResults.length,
        } as CrudModelFindResult<T, E>);
    }

    get versioningSettings() {
        if (!this.schema.versioning) return;

        const settings: CrudSchemaVersioningSettings = {
            versionDateField: 'createdAt',
            versioningParentIdField: 'versioningParentId',
            versionDiffField: 'versioningDiff',
            default: () => new Date().toISOString(),
        };

        if (typeof this.schema.versioning === 'object') {
            Object.assign(settings, this.schema.versioning);
        }

        return settings;
    }
}
