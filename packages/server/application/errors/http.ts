export class HttpError extends Error {
    public originalError?: Error;

    constructor(
        public message = 'Unknown http error',
        public statusCode = 500,
        public errorCode = 'SERVICE_ERROR'
    ) {
        super(message);
        Object.setPrototypeOf(this, new.target.prototype);
    }
}

export class BadRequestError extends HttpError {
    constructor(message = 'Bad request') {
        super(message, 400);
    }
}
export class UnauthorizedError extends HttpError {
    constructor(message = 'Unauthorized') {
        super(message, 401);
    }
}
export class ForbiddenError extends HttpError {
    constructor(message = 'Forbidden') {
        super(message, 403);
    }
}
export class NotFoundError extends HttpError {
    constructor(message = 'Not found') {
        super(message, 404);
    }
}
export class MethodNotAllowedError extends HttpError {
    constructor(message = 'Method not allowed') {
        super(message, 405);
    }
}
export class NotAcceptableError extends HttpError {
    constructor(message = 'Not acceptable') {
        super(message, 406);
    }
}
export class ProxyAuthenticationRequiredError extends HttpError {
    constructor(message = 'Proxy authentication required') {
        super(message, 407);
    }
}
export class RequestTimeoutError extends HttpError {
    constructor(message = 'Request timeout') {
        super(message, 408);
    }
}
export class ConflictError extends HttpError {
    constructor(message = 'Conflict') {
        super(message, 409);
    }
}
export class GoneError extends HttpError {
    constructor(message = 'Gone') {
        super(message, 410);
    }
}
export class LengthRequiredError extends HttpError {
    constructor(message = 'Length required') {
        super(message, 411);
    }
}
export class PreconditionFailedError extends HttpError {
    constructor(message = 'Precondition failed') {
        super(message, 412);
    }
}
export class PayloadTooLargeError extends HttpError {
    constructor(message = 'Payload too large') {
        super(message, 413);
    }
}
export class UriTooLongError extends HttpError {
    constructor(message = 'Uri too long') {
        super(message, 414);
    }
}
export class UnsupportedMediaTypeError extends HttpError {
    constructor(message = 'Unsupported media type') {
        super(message, 415);
    }
}
export class RequestedRangeNotSatisfiableError extends HttpError {
    constructor(message = 'Requested range not satisfiable') {
        super(message, 416);
    }
}
export class ExpectationFailedError extends HttpError {
    constructor(message = 'Expectation failed') {
        super(message, 417);
    }
}
export class MisdirectedRequestError extends HttpError {
    constructor(message = 'Misdirected request') {
        super(message, 421);
    }
}
export class UpgradeRequiredError extends HttpError {
    constructor(message = 'Upgrade required') {
        super(message, 426);
    }
}
export class PreconditionRequiredError extends HttpError {
    constructor(message = 'Precondition required') {
        super(message, 428);
    }
}
export class TooManyRequestsError extends HttpError {
    constructor(message = 'Too many requests') {
        super(message, 429);
    }
}
export class RequestHeaderFieldsTooLargeError extends HttpError {
    constructor(message = 'Request header fields too large') {
        super(message, 431);
    }
}
export class UnavailableForLegalReasonsError extends HttpError {
    constructor(message = 'Unavailable for legal reasons') {
        super(message, 451);
    }
}

export class InternalServerError extends HttpError {
    constructor(message = 'Internal server error') {
        super(message, 500);
    }
}

export class NotImplementedError extends HttpError {
    constructor(message = 'Not implemented') {
        super(message, 501);
    }
}

export class BadGatewayError extends HttpError {
    constructor(message = 'Bad gateway') {
        super(message, 502);
    }
}

export class ServiceUnavailableError extends HttpError {
    constructor(message = 'Service unavailable') {
        super(message, 503);
    }
}

export class GatewayTimeoutError extends HttpError {
    constructor(message = 'Gateway timeout') {
        super(message, 504);
    }
}
export class HttpVersionNotSupportedError extends HttpError {
    constructor(message = 'Http version not supported') {
        super(message, 505);
    }
}
export class VariantAlsoNegotiatesError extends HttpError {
    constructor(message = 'Variant also negotiates') {
        super(message, 506);
    }
}
export class InsufficiantStorageError extends HttpError {
    constructor(message = 'Insufficiant storage') {
        super(message, 507);
    }
}
export class LoopDetectedError extends HttpError {
    constructor(message = 'Loop detected') {
        super(message, 508);
    }
}
export class NotExtendedError extends HttpError {
    constructor(message = 'Not extended') {
        super(message, 510);
    }
}
export class NetworkAuthenticationRequiredError extends HttpError {
    constructor(message = 'Network authentication required') {
        super(message, 511);
    }
}
