import { Handler } from '../typings';
import Form from '../models/form';
import { NotFoundError } from '../errors/http';
import { FormDefinition } from '@form-engine/core-application/types/form';
import cors from 'cors';

export default (async function PostElementController({
    router,
    elementControllerBackendRoute,
}) {
    router.options(elementControllerBackendRoute, cors());
    router.post(elementControllerBackendRoute, cors(), (req, res, next) => {
        const {
            params: { formId, elementId },
            context,
        } = req;

        (async () => {
            const form = (await Form.retrieve(formId)) as
                | FormDefinition
                | undefined;

            if (!form) throw new NotFoundError('form not found');

            const definition = form.elements?.find(
                ({ id }) => id === elementId
            );

            if (!definition) throw new Error('element not found');

            if (!definition.type)
                throw new Error('incompatible element definition');

            const controllerApiName = `${definition.type}Controller`;

            const Type = Object.values(context.elementControllers).find(
                (t) => t.apiName === controllerApiName
            );

            if (!Type)
                throw new Error(
                    `unknown element controller type ${controllerApiName}`
                );

            const { session, data } = req.body || {};

            const controllerResult = await Type({
                form,
                definition,
                session,
                data,
            });

            res.json(controllerResult);
        })().catch(next);
    });
    return Promise.resolve();
} as Handler);
