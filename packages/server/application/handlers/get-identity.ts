import { Handler } from '../typings';

const GetIdentityHandler: Handler = async ({ router, apiPath }) => {
    router.get(`${apiPath}/identity`, (req, res) => {
        const { identity, identityType, identityError } = req;
        res.json({
            identity,
            identityType,
            identityError,
        });
    });

    return Promise.resolve();
};

export default GetIdentityHandler;
