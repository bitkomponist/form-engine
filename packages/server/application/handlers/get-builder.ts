import urlJoin from '@form-engine/core-util/url-join';
import { Handler } from '../typings';

const GetBuilderHandler: Handler = async (context) => {
    const {
        router,
        builderPath,
        wflBackendRoute,
        datasourceBackendRoute,
        elementControllerBackendRoute,
        apiPath,
        renderFormRoute,
    } = context;

    router.get([builderPath, `${builderPath}/*`], (req, res) => {
        const engineSettings = {
            wflBackendRoute: urlJoin(req.baseUrl, wflBackendRoute),
            datasourceBackendRoute: urlJoin(
                req.baseUrl,
                datasourceBackendRoute
            ),
            elementControllerBackendRoute: urlJoin(
                req.baseUrl,
                elementControllerBackendRoute
            ),
            Builder: {
                basename: urlJoin(req.basePath, builderPath),
                apiBaseUrl: urlJoin(req.baseUrl, apiPath),
                renderFormRoute: urlJoin(req.baseUrl, renderFormRoute),
            },
        };

        const script = `(function(g){g.FormEngine = Object.assign(g.FormEngine || {}, ${JSON.stringify(
            engineSettings
        )})})(window)`;

        res.render('builder', { context, request: req, script });
    });

    return Promise.resolve();
};

export default GetBuilderHandler;
