import { Handler } from '../typings';
import Form, {
    prefetchFormDatasources,
    omitFormCredentials,
    filterHiddenElements,
} from '../models/form';
import { FormDefinition } from '@form-engine/core-application/types/form';
import { BadRequestError, NotFoundError } from '../errors/http';
import urlJoin from '@form-engine/core-util/url-join';
import { RequestHandler } from 'express';
import cors from 'cors';

function isPlainObject<T = any>(obj: T): T is Record<string, unknown> {
    return Object.prototype.toString.call(obj) === '[object Object]';
}

function forcePlainObject(candidate: any, fallback = {}): any {
    return isPlainObject(candidate) ? candidate : fallback;
}

function initFormLoader(
    window: any,
    formUrl: string,
    engineSettings: any,
    assetsUrl: string
) {
    const target = window.document.currentScript;

    function loadResource(url: string, callback?: (e: Event) => void) {
        if (url.endsWith('.js')) {
            const script = document.createElement('script');
            script.async = true;
            if (callback) {
                script.onload = callback;
            }
            script.src = url;
            document.head.appendChild(script);
        } else if (url.endsWith('.css')) {
            const style = document.createElement('link');
            style.rel = 'stylesheet';
            style.type = 'text/css';
            style.href = url;
            document.head.appendChild(style);
        }
    }

    function isObject(any: any) {
        return any && typeof any === 'object' && !Array.isArray(any);
    }

    function mergeDeep(target: any, ...sources: any[]): any {
        if (!sources.length) return target;
        const source = sources.shift();
        if (isObject(target) && isObject(source)) {
            for (const key in source) {
                if (isObject(source[key])) {
                    if (!target[key]) Object.assign(target, { [key]: {} });
                    mergeDeep(target[key], source[key]);
                } else {
                    Object.assign(target, { [key]: source[key] });
                }
            }
        }
        return mergeDeep(target, ...sources);
    }

    async function initForm() {
        const node: any = target;

        if (!node) throw new Error('invalid invocation');

        const postData = {};

        if (node.dataset.session) {
            try {
                mergeDeep(postData, JSON.parse(node.dataset.session));
            } catch (e) {
                console &&
                    console.error(
                        'Custom Form Session is malformed / not valid json'
                    );
            }
        }
        let inlineSession = node.textContent && node.textContent.trim();
        if (inlineSession) {
            if (inlineSession.startsWith('(') && inlineSession.endsWith(')')) {
                inlineSession = inlineSession.slice(1, -1);
            }

            try {
                inlineSession = new DOMParser().parseFromString(
                    inlineSession,
                    'text/html'
                ).documentElement.textContent;
                mergeDeep(postData, JSON.parse(inlineSession));
            } catch (e) {
                console &&
                    console.error(
                        'Custom Inline Session malformed / not valid json'
                    );
            }
        }

        const request = await fetch(formUrl, {
            method: 'POST',
            headers: {
                accept: 'application/json',
                'content-type': 'application/json',
            },
            body: JSON.stringify(isObject(postData) ? postData : {}),
        });

        const data = await request.json();

        if (!isObject(data)) throw new Error('invalid form data response');

        window.FormEngine.initForm(node, data);
    }

    if (!window.FormEngine) {
        window.FormEngine = Object.assign(
            window.FormEngine || {},
            engineSettings
        );
        loadResource(`${assetsUrl}/form-engine/index.css`);
        loadResource(`${assetsUrl}/form-engine/index.js`, initForm);
    } else {
        initForm();
    }
}

export default (async function UseFormHandler({
    router,
    wflBackendRoute,
    datasourceBackendRoute,
    elementControllerBackendRoute,
    renderFormRoute,
    assetsPath,
}) {
    const handler: RequestHandler = (req, res, next) => {
        const queryLocals = { ...forcePlainObject(req.query) };

        const isEmbed = 'embed' in queryLocals;
        delete queryLocals.embed;

        const body: any = forcePlainObject(req.body);

        const {
            params: { formId },
            context,
        } = req;

        const engineSettings = {
            wflBackendRoute: urlJoin(req.baseUrl, wflBackendRoute),
            datasourceBackendRoute: urlJoin(
                req.baseUrl,
                datasourceBackendRoute
            ),
            elementControllerBackendRoute: urlJoin(
                req.baseUrl,
                elementControllerBackendRoute
            ),
        };

        if (isEmbed) {
            res.type('text/javascript').send(
                `(${initFormLoader.toString()})(window,"${urlJoin(
                    req.baseUrl,
                    renderFormRoute.replace('/:formId', `/${formId}`)
                )}",${JSON.stringify(engineSettings)},"${urlJoin(
                    req.baseUrl,
                    assetsPath
                )}")`
            );
            return;
        }

        const locale: string =
            queryLocals.lang ||
            queryLocals.language ||
            queryLocals.locale ||
            'default';

        delete queryLocals.lang;
        delete queryLocals.language;
        delete queryLocals.locale;

        const session = {
            locals: {
                ...queryLocals,
                locale,
                ...forcePlainObject(body?.locals),
            },
            request: {
                ...forcePlainObject(body?.request),
                headers: {
                    ...forcePlainObject(body?.request?.headers),
                    ...(req.headers ?? {}),
                },
            },
        };

        (async () => {
            let form = (await Form.retrieve(formId)) as
                | FormDefinition
                | undefined;
            if (!form) {
                const result = await Form.find({
                    filter: { slug: formId },
                    includeVersioningHistory: false,
                    limit: 1,
                });

                if (result.results.length) {
                    form = result.results[0] as FormDefinition;
                }
            }

            if (!form || !form.published)
                throw new NotFoundError('form not found');

            await prefetchFormDatasources(form, context.datasources, session);

            omitFormCredentials(form);

            filterHiddenElements(form);

            if (req.accepts('text/html')) {
                res.render('form', {
                    form,
                    session,
                    engineSettings,
                    context,
                    request: req,
                });
            } else if (req.accepts('application/json')) {
                res.json({
                    form,
                    session,
                });
            } else {
                throw new BadRequestError();
            }
        })().catch(next);
    };

    const corsHandler = cors({
        origin: true,
        allowedHeaders: ['Accept', 'Content-Type'],
    });

    router.options(renderFormRoute, corsHandler);
    router.get(renderFormRoute, corsHandler, handler);
    router.post(renderFormRoute, corsHandler, handler);

    return Promise.resolve();
} as Handler);
