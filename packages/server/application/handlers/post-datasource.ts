import { Handler } from '../typings';
import Form from '../models/form';
import { BadRequestError, NotFoundError } from '../errors/http';
import { FormDefinition } from '@form-engine/core-application/types/form';
import cors from 'cors';

export default (async function PostWorkflowLayer({
    router,
    datasourceBackendRoute,
}) {
    router.options(datasourceBackendRoute, cors());
    router.post(datasourceBackendRoute, cors(), (req, res, next) => {
        const {
            params: { formId, datasourceId },
            context,
        } = req;

        (async () => {
            const form = (await Form.retrieve(formId)) as
                | FormDefinition
                | undefined;

            if (!form) throw new NotFoundError('form not found');

            const definition = form.datasources?.find(
                ({ id }) => id === datasourceId
            );

            if (!definition) throw new Error('datasource not found');

            if (!definition.type)
                throw new BadRequestError('incompatible datasource definition');

            const Type = Object.values(context.datasources).find(
                (t) => t.apiName === definition.type
            );

            if (!Type)
                throw new BadRequestError(
                    `unknown datasource type ${definition.type}`
                );

            const { session } = req.body || {};

            if (session && typeof session !== 'object') {
                throw new BadRequestError(`invalid session`);
            }

            const datasourceResult = await Type({
                definition,
                form,
                session,
            });

            res.json(datasourceResult);
        })().catch(next);
    });
    return Promise.resolve();
} as Handler);
