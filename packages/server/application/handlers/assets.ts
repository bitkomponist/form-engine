import { Handler } from '../typings';
import express from 'express';

const AssetsHandler: Handler = async (context) => {
    const {
        router,
        assetsPath,
        builderClientDirectory,
        assetsDirectory,
        formEngineClientDirectory,
    } = context;

    router.use(`${assetsPath}/builder`, express.static(builderClientDirectory));
    router.use(
        `${assetsPath}/form-engine`,
        express.static(formEngineClientDirectory)
    );
    router.use(`${assetsPath}`, express.static(assetsDirectory));

    return Promise.resolve();
};

export default AssetsHandler;
