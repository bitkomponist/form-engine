import { Handler } from '../typings';
import Form from '../models/form';
import { NotFoundError } from '../errors/http';
import { FormDefinition } from '@form-engine/core-application/types/form';
import cors from 'cors';

export default (async function PostWorkflowLayer({ router, wflBackendRoute }) {
    router.options(wflBackendRoute, cors());
    router.post(wflBackendRoute, cors(), (req, res, next) => {
        const {
            params: { formId, wflId },
            context,
        } = req;

        (async () => {
            const form = (await Form.retrieve(formId)) as
                | FormDefinition
                | undefined;

            if (!form) throw new NotFoundError('form not found');

            const definition = form.workflowLayers?.find(
                ({ id }) => id === wflId
            );

            if (!definition) throw new Error('workflow layer not found');

            if (!definition.type)
                throw new Error('incompatible workflow layer definition');

            const Type = Object.values(context.workflowLayers).find(
                (t) => t.apiName === definition.type
            );

            if (!Type)
                throw new Error(
                    `unknown workflow layer type ${definition.type}`
                );

            const { session, data } = req.body || {};

            const layerResult = await Type({
                form,
                definition,
                session,
                data,
            });

            res.json(layerResult);
        })().catch(next);
    });
    return Promise.resolve();
} as Handler);
