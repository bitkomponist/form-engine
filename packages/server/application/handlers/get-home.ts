import { Handler } from '../typings';

const GetHomeHandler: Handler = async (context) => {
    const { router } = context;

    router.get('/', (req, res) => {
        res.render('home', { context, request: req });
    });

    return Promise.resolve();
};

export default GetHomeHandler;
