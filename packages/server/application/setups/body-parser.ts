import { Setup } from '../typings';
import bodyParser from 'body-parser';

export default (async function BodyParserSetup({ router }) {
    // expressApplication.use(bodyParser.json());
    router.use(bodyParser.json({ limit: '6mb' })); // lambda limit

    return Promise.resolve();
} as Setup);
