import { NotFoundError } from '../errors/http';
import { Setup } from '../typings';
import { Request, Response, NextFunction } from 'express';

const BodyParserSetup: Setup = async ({ app }) => {
    app.all('*', (_req, _res, next) => {
        next(new NotFoundError());
    });

    app.use((error: Error, req: Request, res: Response, next: NextFunction) => {
        const status =
            error instanceof Error && (error as any).statusCode
                ? (error as any).statusCode
                : 500;
        res.status(status);
        console.error(error);
        const isDev = process.env.NODE_ENV === 'development';
        res.format({
            'application/json': () => {
                res.json({
                    error: {
                        message: error?.message,
                        stack: isDev ? error?.stack : undefined,
                    },
                });
            },
            default: () => {
                res.render('error', {
                    error,
                    request: req,
                    context: req.context,
                    isDev,
                });
            },
        });

        next();
    });

    return Promise.resolve();
};

BodyParserSetup.order = 'PostRoutes';

export default BodyParserSetup;
