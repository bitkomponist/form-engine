import { Setup } from '../typings';
import identityMiddleware from '../middlewares/identity';

export default (async function BodyParserSetup({ router, identityOptions }) {
    router.use(identityMiddleware(identityOptions));

    return Promise.resolve();
} as Setup);
