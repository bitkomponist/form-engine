import { Setup } from '../typings';
import crudMiddleware from '../middlewares/crud';
import formatSlug from '@form-engine/core-util/format-slug';

export default (async function CrudHandlingSetup({
    router,
    storageAdapter,
    apiEndpoints,
    apiPath,
}) {
    for (const [_modelName, options] of Object.entries(apiEndpoints)) {
        const { model } = options;

        if (!model) {
            throw new Error('invalid api endpoint setup, missing model');
        }

        model.adapter = await storageAdapter(model.schema);

        const slug = formatSlug(model.schema.name);
        const path = `${apiPath}/${slug}`;
        router.use(
            path,
            crudMiddleware({
                model,
                ...options,
            })
        );
    }
} as Setup);
