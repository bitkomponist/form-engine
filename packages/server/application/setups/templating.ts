import { join } from 'path';
import { Setup } from '../typings';
import pug from 'pug';

export const DEFAULT_VIEW_DIRECTORY = join(process.cwd(), 'views');

export default (async function TemplatingSetup(context) {
    const { app, viewDirectory } = context;

    /* import manually so this gets bundled */
    app.engine('pug', (pug as any).__express);
    app.set('view engine', 'pug');
    app.set('views', viewDirectory ?? DEFAULT_VIEW_DIRECTORY);

    return Promise.resolve();
} as Setup);
