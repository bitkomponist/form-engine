import { Setup } from '../typings';
import morgan from 'morgan';

export default (async function LoggingSetup({ app }) {
    app.use(
        morgan('dev', {
            // skip:(_req,res) => res.statusCode < 400
        })
    );

    return Promise.resolve();
} as Setup);
