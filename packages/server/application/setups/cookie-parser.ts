import { Setup } from '../typings';
import cookieParser from 'cookie-parser';

export default (async function BodyParserSetup({ router }) {
    router.use(cookieParser());

    return Promise.resolve();
} as Setup);
