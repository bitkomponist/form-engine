export { default as TemplatingSetup } from './templating';
export { default as BodyParserSetup } from './body-parser';
export { default as CookieParserSetup } from './cookie-parser';
export { default as ErrorHandlingSetup } from './error-handling';
export { default as IdentityHandlingSetup } from './identity-handling';
export { default as CrudHandlingSetup } from './crud-handling';
export { default as LoggingSetup } from './logging';
