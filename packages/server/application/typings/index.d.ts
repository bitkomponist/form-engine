import type * as express from 'express';
import type morgan from 'morgan';
import type {
    DatasourceType,
    ElementControllerType,
    WorkflowLayerType,
} from '@form-engine/core-application/types/form';

export type JSONPrimitive = string | number | boolean | null;

export type JSONValue = JSONPrimitive | JSONObject | JSONArray;

export type JSONObject = { [member: string]: JSONValue };

export type JSONArray = Array<JSONValue>;

export type Handler = (context: ServerApplicationContext) => Promise<void>;

export type SetupOrder = 'PreRoutes' | 'PostRoutes';

export type Setup = ((context: ServerApplicationContext) => Promise<void>) & {
    order?: SetupOrder;
};

export interface ServerApplicationContext {
    app: express.Application;
    router: express.Router;
    pathPrefix: string;
    trustedOrigins: (string | RegExp)[];
    assetsPath: string;
    builderPath: string;
    wflBackendRoute: string;
    datasourceBackendRoute: string;
    elementControllerBackendRoute: string;
    renderFormRoute: string;
    assetsDirectory: string;
    viewDirectory?: string;
    builderClientDirectory: string;
    formEngineClientDirectory: string;
    handlers: { [key: string]: Handler };
    setups: { [key: string]: Setup };
    storageAdapterOptions?: Record<string, unknown>;
    storageAdapter: CrudStorageAdapter;
    apiPath: string;
    apiEndpoints: { [key: string]: Partial<CrudMiddlewareOptions> };
    datasources: { [key: string]: DatasourceType };
    workflowLayers: { [key: string]: WorkflowLayerType };
    elementControllers: { [key: string]: ElementControllerType };
    [key: string]: any;
}

export type ServerApplicationPlugin = Partial<ServerApplicationContext>;

declare global {
    namespace Express {
        interface Request {
            context: ServerApplicationContext;
            basePath: string;
            assetsBasePath: string;
        }
    }
}

export type IdentityType = 'basic' | 'bearer' | 'none';

export interface Identity {
    id: string;
    name: string;
    email: string;
    [key: string]: any;
}

export interface IdentityMiddlewareSettings {
    header: string;
    headerEnabled: boolean;
    cookie: string;
    cookieEnabled: boolean;
    failOnAttempt: boolean;
    staticIdentities?: { [credentials: string]: Identity };
    provider: (
        type: IdentityType,
        credentials: string,
        settings: IdentityMiddlewareSettings,
        context: ServerApplicationContext
    ) => Promise<Identity | undefined>;
}

export type IdentityMiddlewareOptions = Partial<IdentityMiddlewareSettings>;

declare global {
    namespace Express {
        interface Request {
            identity?: Identity;
            identityType: IdentityType;
            identityError?: Error;
        }
    }
}

declare interface ServerApplicationContext {
    identityOptions?: IdentityMiddlewareOptions;
}

export interface CrudSchemaField<T = JSONValue> {
    type: function;
    default?: T | (() => T);
    required?: boolean;
    validate?: (value?: T) => Promise<void>;
    schema?: CrudSchema | Array<CrudSchema | function>;
    forceDefault?: boolean;
    forceCreate?: boolean;
    forceUpdate?: boolean;
    readOnly?: boolean;
    createOnly?: boolean;
    [key: string]: any;
}

export interface CrudSchemaVersioningSettings {
    versioningParentIdField: string;
    versionDateField: string;
    versionDiffField: string;
    default: () => string;
}

export interface CrudSchema {
    name: string;
    versioning?: Partial<CrudSchemaVersioningSettings> | boolean;
    fields: {
        id: CrudSchemaField<JSONValue>;
        [key: string]: CrudSchemaField;
    };
}

export type CrudEntity<T extends CrudSchema = CrudSchema> = {
    [field in keyof T['fields']]: JSONValue;
};

export interface CrudModelFindArgs<
    T extends CrudSchema = CrudSchema,
    E extends CrudEntity<T> = CrudEntity<T>
> {
    filter?: Partial<{ [attr in keyof E]: JSONValue }>;
    startAt?: string;
    limit?: number;
    sort?: { [attr in keyof E]: 'asc' | 'desc' };
    attributes?: Array<keyof E>;
    includeVersioningHistory?: boolean;
}

export interface CrudModelFindResult<
    T extends CrudSchema = CrudSchema,
    E extends CrudEntity<T> = CrudEntity<T>
> {
    results: E[];
    lastKey?: string | number;
    count: number;
}

export interface CrudModel<
    T extends CrudSchema = CrudSchema,
    E extends CrudEntity<T> = CrudEntity<T>
> {
    schema: T;
    adapter?: CrudModel<T, E>;
    createEntity: (dto?: Partial<E>) => E;
    pickEntityAttributes: (dto: Partial<E>, attributes: string[]) => Partial<E>;
    validateEntity: (dto: Partial<E>) => Promise<void>;
    create: (dto: E) => Promise<E>;
    retrieve: (id: string) => Promise<E | undefined>;
    update: (dto: E) => Promise<E>;
    delete: (id: string) => Promise<void>;
    find: (
        findArgs?: CrudModelFindArgs<T, E>
    ) => Promise<CrudModelFindResult<T, E>>;
    readonly versioningSettings?: CrudSchemaVersioningSettings;
}

export type CrudStorageAdapter<
    T extends CrudSchema = CrudSchema,
    E extends CrudEntity<T> = CrudEntity<T>
> = (schema: T) => Promise<CrudModel<T, E>>;

export interface CrudHandlerDefinition {
    method: 'use' | 'get' | 'post' | 'put' | 'patch' | 'options' | 'delete';
    path?: string;
    handler: express.Handler;
    enabled: boolean;
    scope?: string;
    attributes?: string[];
}

export interface CrudMiddlewareSettings {
    model: CrudModel;
    scopeVerifier: (definition: CrudHandlerDefinition) => express.Handler;
    router: express.Router;
    scope?: string;
    logging: { enabled: boolean; format: string; options?: morgan.Options };
    handlers: {
        create: CrudHandlerDefinition;
        retrieve: CrudHandlerDefinition;
        update: CrudHandlerDefinition;
        list: CrudHandlerDefinition;
        delete: CrudHandlerDefinition;
    };
}

export interface CrudMiddlewareOptions
    extends Partial<Omit<CrudMiddlewareSettings, 'model'>>,
        CrudMiddlewareSettings {
    handlers?: Partial<CrudMiddlewareSettings['handlers']>;
}

declare global {
    namespace Express {
        interface Request {
            crudContext: {
                settings: CrudMiddlewareSettings;
                definition: CrudHandlerDefinition;
                model: CrudModel;
            };
        }
    }
}
