import { Handler, Request } from 'express';
import { BadRequestError, ForbiddenError } from '../errors/http';
import {
    ServerApplicationContext,
    Identity,
    IdentityMiddlewareOptions,
    IdentityMiddlewareSettings,
    IdentityType,
} from '../typings';

export async function DefaultIdentityProvider(
    type: IdentityType,
    credentials: string,
    settings: IdentityMiddlewareSettings,
    _context: ServerApplicationContext
): Promise<Identity | undefined> {
    if (type !== 'basic') return Promise.resolve();
    return Promise.resolve(settings.staticIdentities?.[credentials]);
}

export default function IdentityMiddleware(
    options: IdentityMiddlewareOptions = {}
): Handler {
    const settings: IdentityMiddlewareSettings = {
        header: 'authorization',
        headerEnabled: true,
        cookie: 'sessionId',
        cookieEnabled: false,
        provider: DefaultIdentityProvider,
        failOnAttempt: true,
        ...options,
    };

    return (req, _res, next) => {
        const props: Partial<Request> = {
            identity: undefined,
            identityType: 'none',
            identityError: undefined,
        };

        const invokeProvider = async (
            type: IdentityType,
            credentials: string
        ) => {
            if (!['basic', 'bearer'].includes(type)) {
                props.identityError = new BadRequestError(
                    'unsupported authorization method'
                );
            }
            props.identityType = type;
            try {
                const identity = await settings.provider(
                    type,
                    credentials,
                    settings,
                    req.context
                );
                if (identity) {
                    props.identity = identity;
                } else {
                    props.identityError = new ForbiddenError(
                        'invalid credentials'
                    );
                }
            } catch (reason) {
                props.identityError = new ForbiddenError(
                    reason instanceof Error ? reason.message : String(reason)
                );
            }
        };

        let providerResult;

        if (settings.header in req.headers) {
            const [authType = '', credentials = ''] =
                req.get(settings.header)?.split(/\s+/) ?? '';
            providerResult = invokeProvider(
                authType.toLowerCase() as IdentityType,
                credentials
            );
        } else if (settings.cookie in req.cookies) {
            providerResult = invokeProvider(
                'bearer',
                String(req.cookies[settings.cookie])
            );
        }

        if (!providerResult) {
            Object.assign(req, props);
            next();
        } else {
            providerResult
                .then(() => {
                    Object.assign(req, props);
                    next();
                })
                .catch((reason) => {
                    Object.assign(req, props);
                    if (settings.failOnAttempt) {
                        next(reason);
                    } else {
                        next();
                    }
                });
        }
    };
}
