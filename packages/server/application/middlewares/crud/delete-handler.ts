import { BadRequestError } from '../../errors/http';
import { CrudHandlerDefinition } from '../../typings';
export default {
    enabled: true,
    path: '/:id',
    method: 'delete',
    handler: (req, res, next) => {
        const { model } = req.crudContext;
        const { id } = req.params;
        (async () => {
            if (model.schema.fields.id.validate) {
                try {
                    await model.schema.fields.id.validate(id);
                } catch (e) {
                    throw new BadRequestError((e as Error).message);
                }
            }
            const result = await model.delete(id);
            res.json(result);
        })().catch(next);
    },
} as CrudHandlerDefinition;
