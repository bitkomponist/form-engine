import { BadRequestError } from '../../errors/http';
import { CrudEntity, CrudHandlerDefinition, JSONValue } from '../../typings';
import { diff } from 'deep-diff';
import { filterIgnoredHistoryDiffs } from '@form-engine/core-util/diff-util';

export const IGNORE_DIFF_PATHS: string[][] = [
    ['thumbnail'],
    ['createdAt'],
    ['updatedAt'],
];

export default {
    enabled: true,
    path: '/:id',
    method: 'patch',
    handler: (req, res, next) => {
        const { id } = req.params;
        (async () => {
            const { model, definition } = req.crudContext;
            let dto = { ...req.body, id };

            if (Array.isArray(definition.attributes)) {
                dto = model.pickEntityAttributes(dto, definition.attributes);
            }

            try {
                await model.validateEntity(dto);
            } catch (e) {
                throw new BadRequestError((e as Error).message);
            }

            const { versioningSettings } = model;

            let previousVersion: CrudEntity | undefined;

            if (versioningSettings) {
                previousVersion = await model.retrieve(id);
            }

            const result = await model.update(dto);

            if (result && previousVersion && versioningSettings) {
                const idGenerator = model.schema.fields.id.default;

                const diffResult = filterIgnoredHistoryDiffs(
                    diff(previousVersion, result),
                    IGNORE_DIFF_PATHS
                );

                if (diffResult) {
                    await model.create({
                        ...previousVersion,
                        [versioningSettings.versioningParentIdField]: id,
                        [versioningSettings.versionDateField]:
                            versioningSettings.default(),
                        [versioningSettings.versionDiffField]:
                            diffResult as unknown as JSONValue,
                        id:
                            (typeof idGenerator === 'function'
                                ? idGenerator()
                                : idGenerator) || '',
                    });
                }
            }

            if (Array.isArray(definition.attributes)) {
                res.json(
                    model.pickEntityAttributes(result, definition.attributes)
                );
            } else {
                res.json(result);
            }
        })().catch(next);
    },
} as CrudHandlerDefinition;
