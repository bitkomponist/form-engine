import { BadRequestError } from '../../errors/http';
import {
    CrudHandlerDefinition,
    CrudModelFindArgs,
    JSONObject,
    JSONValue,
} from '../../typings';

function isFindArgs(candidate: any): candidate is CrudModelFindArgs {
    // filter?:{[attr in keyof E]:JSONValue};
    // startAt?:string;
    // limit?:number;
    // attributes?:Array<keyof E>;
    // verbose?:boolean;

    if (typeof candidate !== 'object') return false;

    const checkPropType = (name: string, type: string) =>
        !(name in candidate) || typeof candidate[name] === type;

    if (!checkPropType('filter', 'object')) return false;
    if (
        !checkPropType('startAt', 'string') &&
        !checkPropType('startAt', 'number')
    )
        return false;
    if (!checkPropType('limit', 'number')) return false;
    if (!checkPropType('attributes', 'object')) return false;
    if (!checkPropType('sort', 'object')) return false;
    if (!checkPropType('includeVersioningHistory', 'boolean')) return false;
    if (!checkPropType('verbose', 'boolean')) return false;

    return true;
}

export default {
    enabled: true,
    path: '/',
    method: 'get',
    handler: (req, res, next) => {
        const rawParams = (req.query?.params ?? '{}') as string;
        let params: JSONObject;
        try {
            params = JSON.parse(rawParams);
            if (!isFindArgs(params)) {
                throw new Error('invalid params');
            }
        } catch (e) {
            throw new BadRequestError('invalid params');
        }

        const { verbose, ...findArgs } = params;

        const { model } = req.crudContext;

        (async () => {
            const result = await model.find(findArgs as CrudModelFindArgs);
            if (verbose === true) {
                res.json(result);
            } else {
                res.json(result.results);
            }
        })().catch(next);
    },
} as CrudHandlerDefinition;
