import { NotFoundError } from '../../errors/http';
import { CrudHandlerDefinition } from '../../typings';
export default {
    enabled: true,
    path: '/:id',
    method: 'get',
    handler: (req, res, next) => {
        const { model, definition } = req.crudContext;
        (async () => {
            const result = await model.retrieve(req.params.id);

            if (!result) {
                throw new NotFoundError(`${req.params.id} not found`);
            }

            if (Array.isArray(definition.attributes)) {
                res.json(
                    model.pickEntityAttributes(result, definition.attributes)
                );
            } else {
                res.json(result);
            }
        })().catch(next);
    },
} as CrudHandlerDefinition;
