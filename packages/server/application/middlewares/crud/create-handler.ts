import { BadRequestError } from '../../errors/http';
import { CrudHandlerDefinition } from '../../typings';
export default {
    enabled: true,
    path: '/',
    method: 'post',
    handler: (req, res, next) => {
        (async () => {
            const { model, definition } = req.crudContext;
            let dto = { ...req.body };

            if (Array.isArray(definition.attributes)) {
                dto = model.pickEntityAttributes(dto, definition.attributes);
            }

            dto = model.createEntity(dto);

            try {
                await model.validateEntity(dto);
            } catch (e) {
                throw new BadRequestError((e as Error).message);
            }

            const result = await model.create(dto);
            if (Array.isArray(definition.attributes)) {
                res.json(
                    model.pickEntityAttributes(result, definition.attributes)
                );
            } else {
                res.json(result);
            }
        })().catch(next);
    },
} as CrudHandlerDefinition;
