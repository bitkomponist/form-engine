import express, { Handler, NextFunction, Request, Response } from 'express';
import merge from 'lodash.merge';
import {
    CrudHandlerDefinition,
    CrudMiddlewareOptions,
    CrudMiddlewareSettings,
} from '../../typings';

import createHandler from './create-handler';
import retrieveHandler from './retrieve-handler';
import updateHandler from './update-handler';
import listHandler from './list-handler';
import deleteHandler from './delete-handler';
import { ForbiddenError, NotFoundError } from '../../errors/http';
import morgan from 'morgan';

export function DefaultScopeVerifier(definition: CrudHandlerDefinition) {
    return ((req, _res, next) => {
        if (!definition.scope) {
            next();
            return;
        }

        let identityScope: string[];
        if (Array.isArray(req.identity?.scope)) {
            identityScope = req.identity.scope.map((s: unknown) =>
                String(s ?? '').trim()
            );
        } else {
            identityScope = String(req.identity?.scope ?? '')
                .split(' ')
                .map((s) => s.trim());
        }

        if (!identityScope.includes(definition.scope)) {
            next(new ForbiddenError());
        } else {
            next();
        }
    }) as Handler;
}

export default function CrudMiddleware(
    options: CrudMiddlewareOptions
): Handler {
    const settings: CrudMiddlewareSettings = merge(
        {
            scopeVerifier: DefaultScopeVerifier,
            handlers: {
                create: createHandler,
                retrieve: retrieveHandler,
                update: updateHandler,
                list: listHandler,
                delete: deleteHandler,
            },
            router: express.Router({ mergeParams: true }),
            logging: {
                enabled: true,
                format: 'dev',
                options: {},
            },
        },
        options
    );

    const { router } = settings;

    if (settings.logging.enabled) {
        router.use(morgan(settings.logging.format, settings.logging.options));
    }

    function getPreHandler(definition: CrudHandlerDefinition) {
        return ((req, _res, next) => {
            req.crudContext = { settings, definition, model: settings.model };
            next();
        }) as Handler;
    }

    for (const [path, definition] of Object.entries(settings.handlers)) {
        if (!definition.enabled) continue;
        const method = definition.method;
        if (typeof router[method] !== 'function') {
            throw new Error(`unsupported crud handler method ${method}`);
        }

        router[method](
            definition.path ?? path,
            getPreHandler(definition),
            settings.scopeVerifier({
                ...definition,
                scope: definition.scope ?? settings.scope,
            }),
            definition.handler
        );
    }

    router.all('*', (_req, _res, next) => {
        next(new NotFoundError());
    });

    router.use(
        (error: Error, _req: Request, res: Response, next: NextFunction) => {
            const status =
                error instanceof Error && (error as any).statusCode
                    ? (error as any).statusCode
                    : 500;
            res.status(status).json({
                message: error?.message ?? 'internal error',
                errorCode: (error as any)?.errorCode,
                stack:
                    process.env.NODE_ENV === 'development'
                        ? error?.stack
                        : undefined,
            });
            next();
        }
    );

    return router;
}
