import express, { Handler } from 'express';
import * as setups from './setups';
import * as handlers from './handlers';
import { ServerApplicationContext } from './typings';
import merge from 'lodash.merge';
import MongoStorageAdapter from '@form-engine/mongo-storage';
import * as models from './models';
import * as plugins from './plugins';
import urlJoin from '@form-engine/core-util/url-join';
import { BadRequestError } from './errors/http';

export default async function FormEngineServerApplicationApplication(
    options: Partial<ServerApplicationContext> = {}
): Promise<ServerApplicationContext> {
    const context: ServerApplicationContext = merge(
        {
            trustedOrigins: [/^.*$/],
            pathPrefix: '',
            assetsPath: '/assets',
            builderPath: '/builder',
            renderFormRoute: '/form/:formId',
            wflBackendRoute: '/form/:formId/workflow/:wflId',
            datasourceBackendRoute: '/form/:formId/datasource/:datasourceId',
            elementControllerBackendRoute: '/form/:formId/element/:elementId',
            builderClientDirectory: 'dist/builder',
            formEngineClientDirectory: 'dist/form-engine',
            assetsDirectory: 'packages/server/application/assets',
            app: express(),
            router: express.Router({ mergeParams: true }),
            handlers,
            setups,
            storageAdapter: MongoStorageAdapter(
                options?.storageAdapterOptions ?? {}
            ),
            apiPath: '/api/v1',
            apiEndpoints: Object.fromEntries(
                Object.entries(models).map(([name, model]) => [name, { model }])
            ),
            datasources: {},
            workflowLayers: {},
            elementControllers: {},
        } as ServerApplicationContext,
        ...Object.values(plugins),
        options
    );

    const { app, router } = context;

    const setContext: Handler = (req, _res, next) => {
        req.context = context;
        next();
    };

    app.use(setContext);

    const useOrigin: Handler = (req, _res, next) => {
        const host = req.get('host');

        let trusted = false;
        const [origin = ''] = host?.split(':') ?? [];

        for (const trustedOrigin of context.trustedOrigins) {
            if (typeof trustedOrigin === 'string') {
                if (trustedOrigin === origin) {
                    trusted = true;
                    break;
                }
            } else if (trustedOrigin instanceof RegExp) {
                if (trustedOrigin.test(origin)) {
                    trusted = true;
                    break;
                }
            }
        }

        if (!host || !trusted) {
            next(new BadRequestError(`untrusted origin "${host}"`));
            return;
        }

        /* in case someone already set publicUrl on the request outside of this app, use it */
        const { publicUrl } = req as { publicUrl?: URL | string };

        const baseUrl = new URL(
            publicUrl ?? `${req.protocol}://${host}${context.pathPrefix}`
        );

        req.baseUrl = baseUrl.toString();
        req.basePath = baseUrl.pathname;
        req.assetsBasePath = urlJoin(req.basePath, context.assetsPath);
        next();
    };

    app.use(useOrigin);

    if (context.pathPrefix) {
        app.use(context.pathPrefix, router);
    } else {
        app.use(router);
    }

    const preRouteSetups = Object.values(context.setups).filter(
        ({ order }) => !order || order === 'PreRoutes'
    );
    for (const setup of preRouteSetups) {
        await setup(context);
    }

    for (const handler of Object.values(context.handlers)) {
        await handler(context);
    }

    const postRouteSetups = Object.values(context.setups).filter(
        ({ order }) => order === 'PostRoutes'
    );
    for (const setup of postRouteSetups) {
        await setup(context);
    }

    return Promise.resolve(context);
}
