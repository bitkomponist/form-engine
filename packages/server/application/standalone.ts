import { existsSync } from 'fs';
import { join } from 'path';
import ServerApplication from './index';
import merge from 'lodash.merge';
import { ServerApplicationContext } from './typings';

const {
    APPLICATION_PORT="8080",
    APPLICATION_PUBLIC_URL=`http://localhost:8080`,
    APPLICATION_VIEW_DIRECTORY=join(process.cwd(), 'packages/server/application/views'),
    APPLICATION_ADMIN_USERNAME="",
    APPLICATION_ADMIN_PASSWORD="",
    APPLICATION_ADMIN_EMAIL="admin@form-engine.com",
    APPLICATION_ADMIN_FIRSTNAME="Administrator",
    APPLICATION_ADMIN_LASTNAME="Administrator",
    APPLICATION_ADMIN_SCOPE="form-engine:crud",
    APPLICATION_ADMIN_ID="33ec7e43-eccc-40d8-9299-dfa18808ae76",
    DB_HOST='localhost',
    DB_USER='root',
    DB_PASSWORD='',
    DB_NAME='form-engine',
    DB_PORT='27017',
  } = process?.env ?? {};

export async function StandaloneServerApplicationApplication() {

    const config:Partial<ServerApplicationContext> = {
        publicUrl: new URL(APPLICATION_PUBLIC_URL),
        viewDirectory: APPLICATION_VIEW_DIRECTORY,
        apiEndpoints:{
            Form:{
                scope:'form-engine:crud'
            },
            SessionLog:{
                scope:'form-engine:crud'
            }
        },
        storageAdapterOptions: {
            connection: `mongodb://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}`,
            dbName: DB_NAME,
        },
        identityOptions:{
            staticIdentities:{
                ...(
                    APPLICATION_ADMIN_USERNAME && APPLICATION_ADMIN_PASSWORD ? {
                        [Buffer.from(`${APPLICATION_ADMIN_USERNAME}:${APPLICATION_ADMIN_PASSWORD}`).toString('base64')]:{
                            name:APPLICATION_ADMIN_USERNAME,
                            firstName:APPLICATION_ADMIN_FIRSTNAME,
                            lastName:APPLICATION_ADMIN_LASTNAME,
                            email: APPLICATION_ADMIN_EMAIL,
                            id: APPLICATION_ADMIN_ID,
                            scope: APPLICATION_ADMIN_SCOPE
                        }
                    } : {}
                )
            }
        }
    };

    const { APPLICATION_CONFIG = 'form-engine.config.js' } = process.env;
    const configPath =
        APPLICATION_CONFIG.charAt(0) === '/'
            ? APPLICATION_CONFIG
            : join(process.cwd(), APPLICATION_CONFIG);

    if (existsSync(configPath)) {
        const { default: localConfiguration = {} } = await import(configPath);
        merge(config, localConfiguration);
    }

    const context = await ServerApplication(config);

    context.app.listen(APPLICATION_PORT, () => {
        console.log(`listening ${context.publicUrl}`);
    });

    return context;
}

StandaloneServerApplicationApplication();
