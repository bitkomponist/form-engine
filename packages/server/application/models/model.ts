import {
    CrudModel,
    CrudEntity,
    CrudSchema,
    CrudModelFindArgs,
    CrudModelFindResult,
    CrudSchemaVersioningSettings,
} from '@form-engine/server-application/typings';

function isAdapter(adapter?: CrudModel): adapter is CrudModel {
    return Boolean(adapter) && typeof adapter === 'object';
}

export class Model<
    T extends CrudSchema = CrudSchema,
    E extends CrudEntity<T> = CrudEntity<T>
> implements CrudModel<T, E>
{
    constructor(public schema: T, public adapter?: CrudModel<T, E>) {}

    protected ensureAdapter(
        message = 'tried to invoke method without adapter'
    ) {
        if (!isAdapter(this.adapter as any)) {
            throw new Error(message);
        }
    }

    createEntity(partialDto: Partial<E> = {}) {
        this.ensureAdapter();
        return (this.adapter as CrudModel<T, E>).createEntity(partialDto);
    }

    pickEntityAttributes(dto: Partial<E>, attributes: string[]) {
        this.ensureAdapter();
        return (this.adapter as CrudModel<T, E>).pickEntityAttributes(
            dto,
            attributes
        );
    }
    async validateEntity(dto: Partial<E>) {
        this.ensureAdapter();
        return (this.adapter as CrudModel<T, E>).validateEntity(dto);
    }
    async create(dto: E) {
        this.ensureAdapter();
        return (this.adapter as CrudModel<T, E>).create(dto);
    }
    async retrieve(id: string): Promise<E | undefined> {
        this.ensureAdapter();
        return (this.adapter as CrudModel<T, E>).retrieve(id);
    }
    async update(dto: E & Omit<Partial<E>, 'id'>): Promise<E> {
        this.ensureAdapter();
        return (this.adapter as CrudModel<T, E>).update(dto);
    }
    async delete(id: string) {
        this.ensureAdapter();
        return (this.adapter as CrudModel<T, E>).delete(id);
    }
    async find(
        findArgs: CrudModelFindArgs<T, E> = {}
    ): Promise<CrudModelFindResult<T, E>> {
        this.ensureAdapter();
        return (this.adapter as CrudModel<T, E>).find(findArgs);
    }

    get versioningSettings() {
        return (this.adapter as CrudModel<T, E>).versioningSettings;
    }
}
