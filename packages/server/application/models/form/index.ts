import {
    DatasourceArgs,
    DatasourceDefinition,
    DatasourceType,
    FormDefinition,
    FormSession,
    ElementDefinition,
} from '@form-engine/core-application/types/form';
import { JSONObject } from '../../typings';
import { Model } from '../model';
import Schema from './schema';
import { getItemBranch } from '@form-engine/core-util/element-list';

const FormModel = new Model(Schema);

export default FormModel;

export function omitFormCredentials(
    form: FormDefinition,
    omitKeys = [
        'basicAuthUsername',
        'basicAuthPassword',
        'bearerToken',
        'bsoBasicAuthUsername',
        'bsoBasicAuthPassword',
        'bsoBearerToken',
    ]
) {
    omitKeys.forEach((omitKey) => delete form[omitKey]);

    ['datasources', 'workflowLayers', 'elements'].forEach((key) => {
        ((form as any)[key] as JSONObject[]).forEach((def) => {
            // eslint-disable-next-line no-param-reassign
            omitKeys.forEach((omitKey) => delete def[omitKey]);
        });
    });
}

export function filterHiddenElements(
    form: FormDefinition,
    propertyName: keyof ElementDefinition = 'hidden'
) {
    const filterIds: string[] = [];
    const { elements = [] } = form;

    for (const hiddenElement of elements.filter(
        (element) => element?.[propertyName] === true
    )) {
        const branch = getItemBranch(elements, hiddenElement);
        if (branch.length) {
            filterIds.push(...branch.map(({ id }) => id));
        }
    }

    form.elements = elements.filter(({ id }) => !filterIds.includes(id));
}

export function prefetchFormDatasources(
    form: FormDefinition,
    datasources: { [key: string]: DatasourceType },
    session?: FormSession
) {
    const datasourcesDefinitions = (form.datasources ||
        []) as DatasourceDefinition[];

    const operations = [];

    for (const definition of datasourcesDefinitions) {
        if (definition.type in datasources) {
            operations.push(
                prefetchFormDatasource(
                    { definition, form, session },
                    datasources
                ).then(($prefetchResult) => {
                    definition.$prefetchResult = $prefetchResult;
                })
            );
        }
    }

    return Promise.all(operations);
}

export async function prefetchFormDatasource(
    config: DatasourceArgs,
    datasources: { [key: string]: DatasourceType }
): Promise<any> {
    const { type } = config.definition;
    const datasource = datasources[type];
    if (datasource) {
        const result = await datasource(config);
        return result.data;
    }
}
