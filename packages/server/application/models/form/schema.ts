import { CrudSchema } from '@form-engine/server-application/typings';
import DefaultSchema from '../default-schema';

export const ElementSchema = {
    name: 'element',
    fields: {
        ...DefaultSchema.fields,
        name: {
            type: String,
            default: '',
        },
        type: {
            type: String,
            default: '',
        },
        parent: {
            type: String,
            default: '',
        },
        label: {
            type: String,
            default: '',
        },
    },
};

export const DatasourceSchema: CrudSchema = {
    name: 'datasource',
    fields: {
        ...DefaultSchema.fields,
        name: {
            type: String,
            default: '',
        },
        type: {
            type: String,
            default: '',
        },
    },
};

export const WorkflowLayerSchema: CrudSchema = {
    name: 'workflow-layer',
    fields: {
        ...DefaultSchema.fields,
        name: {
            type: String,
            default: '',
        },
        type: {
            type: String,
            default: '',
        },
    },
};

export const ConstantSchema: CrudSchema = {
    name: 'constant',
    fields: {
        ...DefaultSchema.fields,
        name: {
            type: String,
            default: '',
        },
        value: {
            type: String,
            default: '',
        },
        comment: {
            type: String,
            default: '',
        },
    },
};

export default {
    name: 'form',
    versioning: true,
    fields: {
        ...DefaultSchema.fields,
        versioningParentId: {
            type: String,
            default: null,
            createOnly: true,
        },
        versioningDiff: {
            type: Array,
            schema: [Object],
            default: null,
            createOnly: true,
        },
        slug: {
            type: String,
        },
        owner: {
            type: String,
            default: '',
        },
        name: {
            type: String,
            default: 'Untitled Form',
        },
        tags: {
            type: Array,
            schema: [String],
        },
        thumbnail: {
            type: String,
        },
        published: {
            type: Boolean,
            default: false,
        },
        datasources: {
            type: Array,
            schema: [DatasourceSchema],
        },
        elements: {
            type: Array,
            schema: [ElementSchema],
        },
        constants: {
            type: Array,
            schema: [ConstantSchema],
        },
        workflowLayers: {
            type: Array,
            schema: [WorkflowLayerSchema],
        },
        resources: {
            type: Array,
        },
        includeCoreResources: {
            type: Boolean,
            default: true,
        },
        includeBootstrapCdn: {
            type: Boolean,
            default: true,
        },
        bootstrapCdnUrl: {
            type: String,
        },
        stylesheet: {
            type: String,
        },
        showValidFeedback: {
            type: Boolean,
            default: true,
        },
        bsoEnvironment: {
            type: String,
        },
        bsoAuth: {
            type: String,
        },
        bsoBasicAuthUsername: {
            type: String,
        },
        bsoBasicAuthPassword: {
            type: String,
        },
        bsoBearerToken: {
            type: String,
        },
    },
} as CrudSchema;
