import { Model } from '../model';
import Schema from './schema';

const SessionLogModel = new Model(Schema);

export default SessionLogModel;
