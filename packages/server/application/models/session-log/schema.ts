import { CrudSchema } from '@form-engine/server-application/typings';
import DefaultSchema from '../default-schema';

export default {
    name: 'session-log',
    fields: {
        ...DefaultSchema.fields,
        formId: {
            type: String,
        },
        sessionId: {
            type: String,
        },
        value: {
            type: String,
        },
    },
} as CrudSchema;
