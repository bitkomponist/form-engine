import { CrudSchema } from '@form-engine/server-application/typings';
import { v4 as uuid } from 'uuid';

export default {
    name: 'default-schema',
    fields: {
        id: {
            type: String,
            default: uuid,
        },
        createdAt: {
            type: Date,
            default: () => new Date().toISOString(),
            forceCreate: true,
            readOnly: true,
        },
        updatedAt: {
            type: Date,
            default: () => new Date().toISOString(),
            forceCreate: true,
            forceUpdate: true,
            readOnly: true,
        },
    },
} as CrudSchema;
