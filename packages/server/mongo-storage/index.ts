import {
    CrudStorageAdapter,
    CrudSchema,
} from '@form-engine/server-application/typings';
import { Model } from './model';
import { MongoClient, Db } from 'mongodb';
export interface MongoStorageAdapterSettings {
    connection: string | MongoClient;
    dbName: string;
    clientOptions?: Record<string, unknown>;
    db?: Db;
    client?: MongoClient;
}

export type MongoStorageAdapterOptions = Omit<
    Partial<MongoStorageAdapterSettings>,
    'db' | 'client'
>;

export default function MongoStorageAdapter(
    options: MongoStorageAdapterOptions = {}
): CrudStorageAdapter {
    const settings: MongoStorageAdapterSettings = {
        connection: 'mongodb://localhost:27017',
        dbName: 'form-engine',
        ...options,
    };

    async function initClient() {
        const client =
            typeof settings.connection === 'string'
                ? new MongoClient(settings.connection, settings.clientOptions)
                : settings.connection;
        await client.connect();
        const db = client.db(settings.dbName);

        return {
            client,
            db,
        };
    }

    const storage = initClient();

    return async (schema: CrudSchema) => {
        const { db, client } = await storage;
        const model = new Model(schema, { ...settings, db, client });
        await model.setup();
        return model;
    };
}
