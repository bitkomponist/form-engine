import {
    CrudModel,
    CrudEntity,
    CrudSchema,
    CrudModelFindArgs,
    CrudModelFindResult,
    CrudSchemaVersioningSettings,
} from '@form-engine/server-application/typings';
import { MongoStorageAdapterSettings } from '.';
import {
    Collection,
    Filter,
    FindOptions,
    WithId,
    Sort,
    FindCursor,
} from 'mongodb';

export class Model<
    T extends CrudSchema = CrudSchema,
    E extends CrudEntity<T> = CrudEntity<T>
> implements CrudModel<T, E>
{
    collection: Collection;

    constructor(
        public schema: T,
        public settings: MongoStorageAdapterSettings
    ) {
        if (!this.settings.db) {
            throw new Error(`tried to create model without database`);
        }
        this.collection = this.settings.db.collection(this.schema.name);
    }

    async setup() {
        await this.collection.createIndex({ id: 1 }, { unique: true });
    }

    getSchemaFieldDefault(key: string) {
        const def = this.schema.fields[key];

        if (!def) throw new Error(`unknown field ${key}`);

        const type = typeof def.default;

        if (type === 'function') {
            return (def.default as any)();
        } else if (type === 'object') {
            return structuredClone(def.default);
        }

        return def.default;
    }

    updateEntity(original: E, update: Partial<E>) {
        const sanitizedUpdate = structuredClone(update) as Partial<E>;

        for (const [key, def] of Object.entries(this.schema.fields)) {
            if (def.createOnly || def.readOnly) {
                delete sanitizedUpdate[key];
            }
            if (def.forceUpdate) {
                (sanitizedUpdate as any)[key] = this.getSchemaFieldDefault(key);
            }
        }

        return structuredClone({ ...original, ...sanitizedUpdate }) as E;
    }

    ensureEntity(partialDto: Partial<E> = {}) {
        const defaultEntity = Object.fromEntries(
            Object.keys(this.schema.fields).map((key) => {
                return [key, this.getSchemaFieldDefault(key)];
            })
        );
        return structuredClone({ ...defaultEntity, ...partialDto }) as E;
    }

    createEntity(partialDto: Partial<E> = {}) {
        const dto = { ...partialDto };
        Object.entries(this.schema.fields).forEach(([key, def]) => {
            if (def.readOnly || def.forceDefault || def.updateOnly) {
                delete dto[key];
            }
        });
        return this.ensureEntity(dto);
    }

    pickEntityAttributes(dto: Partial<E>, attributes: Array<keyof E>) {
        const result = Object.fromEntries(
            Object.entries(dto).filter(([key]) => attributes.includes(key))
        );

        return result as Partial<E>;
    }
    async validateEntity(dto: Partial<E>) {
        for (const [key, def] of Object.entries(this.schema.fields)) {
            if (def.required && !(key in dto)) {
                throw new Error(`required field ${key} missing`);
            }
        }

        for (const [key, value] of Object.entries(dto)) {
            if (!(key in this.schema.fields)) {
                throw new Error(`invalid property ${key}`);
            }
            if (this.schema.fields[key].validate) {
                await this.schema.fields[key]?.validate?.(value);
            }
        }
    }
    async create(dto: E) {
        delete dto._id;

        const writeDto = this.createEntity(dto);

        await this.collection.insertOne(writeDto);

        return Promise.resolve(structuredClone(writeDto));
    }
    async retrieve(id: string): Promise<E | undefined> {
        const dto = await this.collection.findOne({ id });

        if (!dto) return Promise.resolve(undefined);

        delete (dto as any)._id;

        return Promise.resolve(structuredClone(dto) as unknown as E);
    }
    async update(dto: E & Omit<Partial<E>, 'id'>): Promise<E> {
        const update = await this.collection.updateOne(
            { id: dto.id },
            { $set: dto },
            { upsert: false }
        );

        if (!update.matchedCount) {
            throw new Error(`${dto.id} not found`);
        }

        const updatedEntity = await this.collection.findOne({ id: dto.id });

        if (!updatedEntity) {
            throw new Error(`${dto.id} not found`);
        }

        delete (updatedEntity as any)._id;

        return updatedEntity as unknown as E;
    }
    async delete(id: string) {
        const result = await this.collection.deleteOne({ id });

        if (!result.deletedCount) {
            throw new Error(`${id} not found`);
        }

        return Promise.resolve();
    }
    get versioningSettings() {
        if (!this.schema.versioning) return;

        const settings: CrudSchemaVersioningSettings = {
            versionDateField: 'createdAt',
            versioningParentIdField: 'versioningParentId',
            versionDiffField: 'versioningDiff',
            default: () => new Date().toISOString(),
        };

        if (typeof this.schema.versioning === 'object') {
            Object.assign(settings, this.schema.versioning);
        }

        return settings;
    }

    async find(
        findArgs: CrudModelFindArgs<T, E> = {}
    ): Promise<CrudModelFindResult<T, E>> {
        const conditions: Filter<Document>[] = [];
        const options: FindOptions = {};

        const { versioningSettings } = this;

        if (findArgs.includeVersioningHistory !== true && versioningSettings) {
            const { versioningParentIdField } = versioningSettings;

            conditions.push({
                $or: [
                    { [versioningParentIdField]: { $exists: false } },
                    { [versioningParentIdField]: { $eq: null } },
                    { [versioningParentIdField]: '' },
                ],
            });
        }

        if (findArgs.filter) {
            conditions.push(findArgs.filter as Filter);
        }

        if (findArgs.sort) {
            options.sort = Object.entries(findArgs.sort).map(
                ([key, direction]) => {
                    return [key, direction === 'asc' ? 1 : -1];
                }
            ) as Sort;
        }

        const startAt = Number(findArgs.startAt);

        if (startAt > 0) {
            options.skip = startAt;
        }

        const limit = Number(findArgs.limit);

        if (Number(limit) > 0) {
            options.limit = limit;
        }

        const filter: Filter = conditions.length ? { $and: conditions } : {};

        const count = await this.collection.countDocuments(filter);

        let cursor: FindCursor<WithId<E>> = this.collection.find(
            filter,
            options
        );

        if (Array.isArray(findArgs.attributes)) {
            cursor = cursor.project({
                ...Object.fromEntries(
                    findArgs.attributes.map((key) => [key, 1])
                ),
                _id: 0,
            });
        } else {
            cursor = cursor.project({ _id: 0 });
        }

        const results = await cursor.toArray();

        let lastKey: number | undefined = undefined;

        if (startAt + results.length < count) {
            lastKey = startAt + results.length;
        }

        for (const result of results) {
            delete (result as any)._id;
        }

        return {
            results: results as E[],
            lastKey,
            count,
        } as unknown as CrudModelFindResult<T, E>;
    }
}
