import {
    WorkflowLayerArgs,
    WorkflowLayerResponse,
    WorkflowLayerType,
} from '@form-engine/core-application/types/form';
import FormEngine from '@form-engine/core-application';

function FormEngineRedirectWfl({
    session,
    definition,
}: WorkflowLayerArgs): Promise<WorkflowLayerResponse> {
    if (definition.url) {
        window.open(
            definition.url,
            FormEngine.isBuilder ? '_blank' : definition.urlTarget ?? '_self'
        );
    }

    return Promise.resolve({
        ...session,
    });
}

FormEngineRedirectWfl.apiName = 'FormEngineRedirectWfl';

export default FormEngineRedirectWfl as WorkflowLayerType;
