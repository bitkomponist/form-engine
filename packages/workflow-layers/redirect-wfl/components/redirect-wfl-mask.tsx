import React from 'react';
import BuilderMaskGroup from '@form-engine/builder-mask/components/mask/group';
import BuilderMaskGroupWflBehaviour from '@form-engine/builder-mask/components/groups/wfl-behaviour';
import BuilderStringProperty from '@form-engine/builder-mask/components/properties/string';
import BuilderEnumProperty from '@form-engine/builder-mask/components/properties/enum';
import Wfl from '../workflow-layers/redirect';
import { i18n } from '@form-engine/builder-application';

export function BuilderRedirectWflSetup() {
    const { c } = i18n.prefix(`mask.${Wfl.apiName}.`);

    return (
        <>
            <BuilderMaskGroupWflBehaviour />
            <BuilderMaskGroup label={c('groupTitle')}>
                <BuilderStringProperty
                    propertyName="url"
                    type="url"
                    label={c('url')}
                />
                <BuilderEnumProperty
                    propertyName="urlTarget"
                    label={c('urlTarget')}
                    defaultValue="_self"
                    options={[
                        ['_self', 'Self'],
                        ['_blank', 'Blank'],
                        ['_parent', 'Parent'],
                        ['_top', 'Top'],
                    ]}
                />
            </BuilderMaskGroup>
        </>
    );
}

export default function BuilderRedirectWflMask() {
    return <BuilderRedirectWflSetup />;
}
