import { FormEnginePlugin } from '@form-engine/core-application';
import WorkflowLayer from './workflow-layers/redirect';
export default {
    workflowLayers: {
        [WorkflowLayer.apiName]: WorkflowLayer,
    },
} as FormEnginePlugin;
