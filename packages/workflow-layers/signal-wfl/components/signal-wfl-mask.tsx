import React from 'react';
import BuilderMaskGroupWflBehaviour from '@form-engine/builder-mask/components/groups/wfl-behaviour';
export function BuilderShowPageWflSetup() {
    return (
        <>
            <BuilderMaskGroupWflBehaviour />
        </>
    );
}

export default function BuilderShowPageWflMask() {
    return <BuilderShowPageWflSetup />;
}
