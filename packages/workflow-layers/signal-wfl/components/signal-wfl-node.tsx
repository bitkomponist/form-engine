import { BuilderWorkflowEditorLayerNode } from '@form-engine/builder-form-editor/components/workflow-editor/layer-node';
import React, { memo } from 'react';

export const BuilderSignalWflNode = memo(function BuilderSignalWflNode(
    props: React.ComponentProps<typeof BuilderWorkflowEditorLayerNode>
) {
    return (
        <BuilderWorkflowEditorLayerNode
            {...props}
            sources={{ success: {} }}
            targets={false}
        />
    );
});
