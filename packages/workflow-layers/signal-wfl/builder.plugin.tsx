import { default as Mask } from './components/signal-wfl-mask';
import { default as WorkflowLayer } from './workflow-layers/signal';
import { LightningChargeFill as Icon } from 'react-bootstrap-icons';
import { BuilderPlugin } from '@form-engine/builder-application';
import { BuilderSignalWflNode as Node } from './components/signal-wfl-node';
import * as translations from './builder-translations';

export default {
    translations,
    workflowLayers: {
        [WorkflowLayer.apiName]: {
            label: `${WorkflowLayer.apiName}.title`,
            description: `${WorkflowLayer.apiName}.description`,
            Icon,
            WorkflowLayer,
            Mask,
            Node,
        },
    },
} as BuilderPlugin;
