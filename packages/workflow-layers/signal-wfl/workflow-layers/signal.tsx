import {
    WorkflowLayerArgs,
    WorkflowLayerResponse,
    WorkflowLayerType,
} from '@form-engine/core-application/types/form';

function FormEngineSignalWfl({
    session,
    definition,
}: WorkflowLayerArgs): Promise<WorkflowLayerResponse> {
    return Promise.resolve({
        ...session,
    });
}

FormEngineSignalWfl.apiName = 'FormEngineSignalWfl';

export default FormEngineSignalWfl as WorkflowLayerType;
