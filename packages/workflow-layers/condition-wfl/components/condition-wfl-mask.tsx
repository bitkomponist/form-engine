import React from 'react';
import BuilderMaskGroupWflBehaviour from '@form-engine/builder-mask/components/groups/wfl-behaviour';
import BuilderMaskGroup from '@form-engine/builder-mask/components/mask/group';
import BuilderExpressionsProperty from '@form-engine/builder-mask/components/properties/expressions';
import { i18n } from '@form-engine/builder-application';
import Wfl from '../workflow-layers/condition';

export function BuilderShowPageWflSetup() {
    return (
        <>
            <BuilderMaskGroupWflBehaviour />
            <BuilderMaskGroup label={i18n.c(`mask.${Wfl.apiName}.groupTitle`)}>
                <BuilderExpressionsProperty />
            </BuilderMaskGroup>
        </>
    );
}

export default function BuilderShowPageWflMask() {
    return <BuilderShowPageWflSetup />;
}
