import { BuilderWorkflowEditorLayerNode } from '@form-engine/builder-form-editor/components/workflow-editor/layer-node';
import React, { memo } from 'react';
import { CheckCircleFill, XCircleFill } from 'react-bootstrap-icons';

export const BuilderConditionWflNode = memo(function BuilderConditionWflNode(
    props: React.ComponentProps<typeof BuilderWorkflowEditorLayerNode>
) {
    return (
        <BuilderWorkflowEditorLayerNode
            {...props}
            sources={{
                error: { icon: XCircleFill },
                success: { icon: CheckCircleFill },
            }}
        />
    );
});
