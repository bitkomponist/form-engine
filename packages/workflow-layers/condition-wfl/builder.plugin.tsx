import { default as Mask } from './components/condition-wfl-mask';
import { default as WorkflowLayer } from './workflow-layers/condition';
import { DiamondHalf as Icon } from 'react-bootstrap-icons';
import { BuilderPlugin } from '@form-engine/builder-application';
import { BuilderConditionWflNode as Node } from './components/condition-wfl-node';
import * as translations from './builder-translations';

export default {
    translations,
    workflowLayers: {
        [WorkflowLayer.apiName]: {
            label: `${WorkflowLayer.apiName}.title`,
            description: `${WorkflowLayer.apiName}.description`,
            Icon,
            WorkflowLayer,
            Mask,
            Node,
        },
    },
} as BuilderPlugin;
