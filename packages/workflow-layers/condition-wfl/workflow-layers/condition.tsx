import {
    WorkflowLayerArgs,
    WorkflowLayerResponse,
    WorkflowLayerType,
    ExpressionDefinition,
} from '@form-engine/core-application/types/form';
import { getFormSessionLocals } from '@form-engine/core-application/utilities/form-session';
import { evalExpressions } from '@form-engine/core-util/expressions';
async function FormEngineConditionWfl({
    session,
    definition,
    form,
}: WorkflowLayerArgs): Promise<WorkflowLayerResponse> {
    if (definition.expressions) {
        const locals = getFormSessionLocals(form, session);
        const result = Boolean(
            evalExpressions(
                definition.expressions as ExpressionDefinition[],
                locals
            )
        );

        if (!result) return Promise.reject(new Error('expression not met'));
    }

    return Promise.resolve({
        ...session,
    });
}

FormEngineConditionWfl.apiName = 'FormEngineConditionWfl';

export default FormEngineConditionWfl as WorkflowLayerType;
