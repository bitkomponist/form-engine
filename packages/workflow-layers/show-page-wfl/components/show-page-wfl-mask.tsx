import React from 'react';
import BuilderMaskGroup from '@form-engine/builder-mask/components/mask/group';
import BuilderMaskGroupWflBehaviour from '@form-engine/builder-mask/components/groups/wfl-behaviour';
import BuilderPageNameProperty from '@form-engine/builder-mask/components/properties/page-name';
import { i18n } from '@form-engine/builder-application';
import Wfl from '../workflow-layers/show-page';

export function BuilderShowPageWflSetup() {
    return (
        <>
            <BuilderMaskGroupWflBehaviour />
            <BuilderMaskGroup label={i18n.c(`mask.${Wfl.apiName}.groupTitle`)}>
                <BuilderPageNameProperty />
            </BuilderMaskGroup>
        </>
    );
}

export default function BuilderShowPageWflMask() {
    return <BuilderShowPageWflSetup />;
}
