import { FormEnginePlugin } from '@form-engine/core-application';
import WorkflowLayer from './workflow-layers/show-page';
export default {
    workflowLayers: {
        [WorkflowLayer.apiName]: WorkflowLayer,
    },
} as FormEnginePlugin;
