import { default as Mask } from './components/show-page-wfl-mask';
import { default as WorkflowLayer } from './workflow-layers/show-page';
import { BoxArrowInRight as Icon } from 'react-bootstrap-icons';
import { BuilderPlugin } from '@form-engine/builder-application';
import * as translations from './builder-translations';

export default {
    translations,
    workflowLayers: {
        [WorkflowLayer.apiName]: {
            label: `${WorkflowLayer.apiName}.title`,
            description: `${WorkflowLayer.apiName}.description`,
            Icon,
            WorkflowLayer,
            Mask,
        },
    },
} as BuilderPlugin;
