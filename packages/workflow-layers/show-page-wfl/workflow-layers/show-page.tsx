import {
    WorkflowLayerArgs,
    WorkflowLayerResponse,
    WorkflowLayerType,
} from '@form-engine/core-application/types/form';

function FormEngineShowPageWfl({
    session,
    definition,
}: WorkflowLayerArgs): Promise<WorkflowLayerResponse> {
    return Promise.resolve({
        ...session,
        currentPage: definition?.pageName || '',
    });
}

FormEngineShowPageWfl.apiName = 'FormEngineShowPageWfl';

export default FormEngineShowPageWfl as WorkflowLayerType;
