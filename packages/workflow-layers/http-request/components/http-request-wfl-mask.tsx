import { useMaskContext } from '@form-engine/builder-mask/hooks/use-mask';
import React from 'react';
import BuilderMaskGroup from '@form-engine/builder-mask/components/mask/group';
import BuilderMaskGroupWflBehaviour from '@form-engine/builder-mask/components/groups/wfl-behaviour';
import BuilderEnumProperty from '@form-engine/builder-mask/components/properties/enum';
import BuilderStringProperty from '@form-engine/builder-mask/components/properties/string';
import BuilderMaskGroupRequestPayload from '@form-engine/builder-mask/components/groups/request-payload';
import { formatVariableCase } from '@form-engine/core-util/format-case';
import Wfl from '../workflow-layers/client-http-request';
import { i18n } from '@form-engine/builder-application';
import BuilderAuthProperty from '@form-engine/builder-mask/components/properties/auth';

export function BuilderHttpRequestWflSetup() {
    const { state } = useMaskContext();

    const { c } = i18n.prefix(`mask.${Wfl.apiName}.`);

    return (
        <>
            <BuilderMaskGroupWflBehaviour />
            <BuilderMaskGroup label={c('requestGroupTitle')}>
                <BuilderStringProperty
                    compilable
                    propertyName="url"
                    label={c('url')}
                />
                <BuilderEnumProperty
                    propertyName="method"
                    label={c('method')}
                    options={['GET', 'POST', 'PUT', 'PATCH', 'DELETE']}
                />
                <BuilderEnumProperty
                    propertyName="responseType"
                    label={c('responseType')}
                    options={[
                        ['', 'TEXT'],
                        ['application/json', 'JSON'],
                    ]}
                />
                <BuilderStringProperty
                    propertyName="storeResultAs"
                    label={c('storeResultAs')}
                    placeholder={c('storeResultAsPlaceholder')}
                    formatter={formatVariableCase}
                />
            </BuilderMaskGroup>
            <BuilderMaskGroup label={c('authGroupTitle')}>
                <BuilderAuthProperty />
            </BuilderMaskGroup>

            {['POST', 'PUT', 'PATCH'].includes(state.method) && (
                <BuilderMaskGroupRequestPayload />
            )}
        </>
    );
}

export default function BuilderHttpRequestWflMask() {
    return <BuilderHttpRequestWflSetup />;
}
