import {
    WorkflowLayerArgs,
    WorkflowLayerResponse,
    WorkflowLayerType,
} from '@form-engine/core-application/types/form';
import fetchWflBackend from '@form-engine/core-application/workflow-layers/fetch-backend';

async function FormEngineHttpRequestWfl({
    form,
    session,
    definition,
}: WorkflowLayerArgs): Promise<WorkflowLayerResponse> {
    if (!form?.id)
        throw new Error(
            'tried to invoke workflow layer with incomplete form definition'
        );
    const sessionUpdate = await fetchWflBackend(
        form.id,
        definition.id,
        session
    );
    return sessionUpdate;
}

FormEngineHttpRequestWfl.apiName = 'FormEngineHttpRequestWfl';

export default FormEngineHttpRequestWfl as WorkflowLayerType;
