import {
    WorkflowLayerArgs,
    WorkflowLayerResponse,
    WorkflowLayerType,
} from '@form-engine/core-application/types/form';
import { compile } from '@form-engine/core-util/template';
import fetch from 'cross-fetch';
import { getFormSessionLocals } from '@form-engine/core-application/utilities/form-session';
import {
    compileAuthorizationHeader,
    AuthorizationInfoSettings,
    compileRequestBody,
    CompileRequestContentTypes,
} from '@form-engine/core-util/request';

async function FormEngineHttpRequestWfl({
    form,
    session,
    definition,
}: WorkflowLayerArgs): Promise<WorkflowLayerResponse> {
    const {
        auth = 'none',
        basicAuthUsername = '',
        basicAuthPassword = '',
        bearerToken = '',
        url = '',
        method = 'GET',
        contentType = 'text/plain',
        responseType = 'text/plain',
        body: bodyProp = '',
        storeResultAs,
        name,
        id,
    } = definition;

    // todo constants missing here
    const locals = getFormSessionLocals(form, session);

    const headers: any = {
        ...compileAuthorizationHeader({
            auth,
            basicAuthUsername,
            basicAuthPassword,
            bearerToken,
            locals,
        } as AuthorizationInfoSettings),
    };

    const hasBody = ['POST', 'PATCH', 'PUT'].includes(method);

    if (hasBody && contentType) {
        headers['content-type'] = contentType;
    }

    let result = '';
    let bodyResult;
    if (url) {
        if (hasBody) {
            bodyResult = compileRequestBody(
                contentType as CompileRequestContentTypes,
                bodyProp,
                locals
            );
        }

        const response = await fetch(compile(url, locals, ''), {
            method,
            body: bodyResult,
            headers,
        });

        if (response.status < 200 || response.status >= 400) {
            if (response.status < 200 || response.status >= 400) {
                let message = await response.text();
                try {
                    message = JSON.parse(message)?.message;
                } catch (e) {
                    //..
                }

                throw new Error(
                    JSON.stringify({ message, payload: bodyResult })
                );
            }
        }

        if (responseType === 'application/json') {
            result = await response.json();
        } else {
            result = await response.text();
        }
    }

    const localName = (storeResultAs as string) || `wfl_${name || id}`;
    const requestBodyLocalName = `wfl_${name || id}_body`;

    return {
        locals: { [localName]: result, [requestBodyLocalName]: bodyResult },
    };
}

FormEngineHttpRequestWfl.apiName = 'FormEngineHttpRequestWfl';
export default FormEngineHttpRequestWfl as WorkflowLayerType;
