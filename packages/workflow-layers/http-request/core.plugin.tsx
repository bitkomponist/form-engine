import { FormEnginePlugin } from '@form-engine/core-application';
import WorkflowLayer from './workflow-layers/client-http-request';
export default {
    workflowLayers: {
        [WorkflowLayer.apiName]: WorkflowLayer,
    },
} as FormEnginePlugin;
