import React from 'react';
import BuilderMaskGroupWflBehaviour from '@form-engine/builder-mask/components/groups/wfl-behaviour';

export function BuilderSessionLogWflSetup() {
    return (
        <>
            <BuilderMaskGroupWflBehaviour />
        </>
    );
}

export default function BuilderHttpRequestWflMask() {
    return <BuilderSessionLogWflSetup />;
}
