import { default as Mask } from './components/session-log-wfl-mask';
import { default as WorkflowLayer } from './workflow-layers/client-session-log';
import { Camera2 as Icon } from 'react-bootstrap-icons';
import SessionLogDefaultLogging from './presets/default-logging';
import { BuilderPlugin } from '@form-engine/builder-application';
import * as translations from './builder-translations';

export default {
    translations,
    workflowLayers: {
        [WorkflowLayer.apiName]: {
            label: `${WorkflowLayer.apiName}.title`,
            description: `${WorkflowLayer.apiName}.description`,
            Icon,
            WorkflowLayer,
            Mask,
        },
    },
    formPresets: {
        SessionLogDefaultLogging,
    },
} as BuilderPlugin;
