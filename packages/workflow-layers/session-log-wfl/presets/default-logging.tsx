import { FormPreset } from '@form-engine/builder-application/types/form-preset';
import { FormDefinition } from '@form-engine/core-application/types/form';
import uuidv4 from '@form-engine/core-util/uuid';
import FormEngineSessionLogWfl from '../workflow-layers/client-session-log';

export default {
    name: 'Default Logging',
    required: true,
    init(form: FormDefinition) {
        form?.workflowLayers?.push({
            id: uuidv4(),
            type: FormEngineSessionLogWfl.apiName,
            name: 'Log Session',
            enabled: true,
            events: ['form:submit@success'],
            nodeX: 250,
            nodeY: 100,
        });
    },
} as FormPreset;
