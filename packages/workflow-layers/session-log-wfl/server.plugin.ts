import { ServerApplicationPlugin } from '@form-engine/server-application/typings';
import WorkflowLayer from './workflow-layers/server-session-log';
export default {
    workflowLayers: {
        [WorkflowLayer.apiName]: WorkflowLayer,
    },
} as ServerApplicationPlugin;
