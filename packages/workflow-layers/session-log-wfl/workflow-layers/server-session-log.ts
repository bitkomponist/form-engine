import {
    WorkflowLayerArgs,
    WorkflowLayerResponse,
    WorkflowLayerType,
} from '@form-engine/core-application/types/form';
import SessionLog from '@form-engine/server-application/models/session-log';

async function FormEngineSessionLogWfl({
    form,
    session,
    definition,
}: WorkflowLayerArgs): WorkflowLayerResponse {
    const dto = {
        sessionId: session?.id,
        formId: form?.id,
        value: JSON.stringify({
            session,
            definition,
        }),
    };

    await SessionLog.create(dto);

    return {};
}

FormEngineSessionLogWfl.apiName = 'FormEngineSessionLogWfl';

export default FormEngineSessionLogWfl as WorkflowLayerType;
