import React, { useMemo } from 'react';
import { useFormContext } from '@form-engine/core-application/hooks/use-form';
import FormEngineElement from '@form-engine/core-application/components/element';
import FormEngineContainer from '@form-engine/core-application/components/container';
import useFormEngineElementState from '@form-engine/core-application/hooks/use-element';
import { useFormSessionContext } from '@form-engine/core-application/hooks/use-formsession';

export interface FormEnginePageElementProps {
    elementComponent?: React.ElementType;
    containerComponent?: React.ElementType;
    id: string;
    editMode?: boolean;
}

export default Object.assign(
    function FormEnginePageElement({
        elementComponent = FormEngineElement,
        containerComponent: Container = FormEngineContainer,
        id,
        editMode,
    }: FormEnginePageElementProps) {
        const {
            state: { elements = [] },
        } = useFormContext();
        const { state: element } = useFormEngineElementState(id);
        if (!element)
            throw new Error('tried to render page without definition');
        const {
            state: { currentPage = '' },
        } = useFormSessionContext();

        const childElementIds = useMemo(
            () =>
                elements
                    .filter((element) => element.parent === id)
                    .map((element) => element.id),
            [elements, id]
        );

        let isActive = true;

        if (!editMode) {
            if (!currentPage) {
                isActive = Boolean(element.isDefault);
            } else {
                isActive = currentPage === element.name;
            }
        }

        if (!isActive) return null;

        return (
            <Container
                id={id}
                className="fe-page"
                childElementIds={childElementIds}
                elementComponent={elementComponent}
                containerComponent={Container}
            />
        );
    },
    {
        apiName: 'FormEnginePageElement',
        isField: false,
        isContainer: true,
        enabled: true,
    }
);
