import React from 'react';
import BuilderElementMask, {
    BuilderElementMaskProps,
} from '@form-engine/builder-mask/components/mask';
import BuilderMaskGroup from '@form-engine/builder-mask/components/mask/group';
import BuilderMaskGroupConditions from '@form-engine/builder-mask/components/groups/conditions';
import BuilderBooleanProperty from '@form-engine/builder-mask/components/properties/boolean';
import BuilderMaskGroupAppearance from '@form-engine/builder-mask/components/groups/appearance';
import { Element } from '.';
import { i18n } from '@form-engine/builder-application';

export default function BuilderPageElementMask(props: BuilderElementMaskProps) {
    const { c } = i18n.prefix(`mask.${Element.apiName}.`);

    return (
        <BuilderElementMask id={Element.apiName} {...props}>
            <BuilderMaskGroupAppearance />
            <BuilderMaskGroup label={c('groupTitle')}>
                <BuilderBooleanProperty
                    propertyName="isDefault"
                    label={c('isDefault')}
                />
            </BuilderMaskGroup>
            <BuilderMaskGroupConditions />
        </BuilderElementMask>
    );
}
