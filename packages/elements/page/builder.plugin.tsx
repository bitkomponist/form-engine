import BuilderElement from './components/page-builder-element';
import DefaultPageElementPreset from './presets/default-page';
import * as translations from './builder-translations';

export default {
    translations,
    elements: {
        [BuilderElement.Element.apiName]: BuilderElement,
    },
    formPresets: {
        DefaultPageElementPreset,
    },
    maskConfigurations: {
        [BuilderElement.Element.apiName]: {
            edit: {
                permissions: {
                    name: 'write',
                    hidden: 'write',
                    width: 'write',
                    marginTop: 'write',
                    marginRight: 'write',
                    marginBottom: 'write',
                    marginLeft: 'write',
                    isDefault: 'write',
                    requiredExpressions: 'write',
                    hiddenExpressions: 'write',
                },
            },
            preview: {
                permissions: {
                    name: 'write',
                    hidden: 'write',
                    width: 'write',
                    marginTop: 'write',
                    marginRight: 'write',
                    marginBottom: 'write',
                    marginLeft: 'write',
                    isDefault: 'write',
                    requiredExpressions: 'write',
                    hiddenExpressions: 'write',
                },
            },
            translate: {
                permissions: {
                    name: 'read',
                    hidden: 'read',
                    width: 'read',
                    marginTop: 'read',
                    marginRight: 'read',
                    marginBottom: 'read',
                    marginLeft: 'read',
                    isDefault: 'read',
                    requiredExpressions: 'read',
                    hiddenExpressions: 'read',
                },
            },
        },
    },
};
