import { FormPreset } from '@form-engine/builder-application/types/form-preset';
import uuidv4 from '@form-engine/core-util/uuid';
import FormEnginePageElement from '../components/page-element';

export default {
    name: 'Default Page Element',
    required: true,
    init(form) {
        form?.elements?.push({
            id: uuidv4(),
            type: FormEnginePageElement.apiName,
            name: 'DefaultPage',
            label: 'Default Page',
            isDefault: true,
        });
    },
} as FormPreset;
