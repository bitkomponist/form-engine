import React, { useMemo } from 'react';
import { useFormEngineElement } from '@form-engine/core-application/hooks/use-element';
import useMarkdown from '@form-engine/core-application/hooks/use-markdown';
import { useFormSessionLocals } from '@form-engine/core-application/hooks/use-formsession';
import { compile } from '@form-engine/core-util/template';

export default Object.assign(
    function FormEngineMarkdownElement() {
        const { state } = useFormEngineElement();
        const { state: locals } = useFormSessionLocals();

        if (!state)
            throw new Error(
                'tried to render markdown element without definition'
            );

        const compileMarkdown = useMarkdown();

        const html = useMemo(() => {
            if (String(state.markdownBody).trim()) {
                return compile(
                    compileMarkdown(state.markdownBody ?? ''),
                    locals
                );
            } else {
                return '';
            }
        }, [state.markdownBody, compileMarkdown, locals]);

        return (
            <div
                className="fe-body-text"
                dangerouslySetInnerHTML={{ __html: html }}
            />
        );
    },
    { apiName: 'FormEngineMarkdownElement', isField: false, enabled: true }
);
