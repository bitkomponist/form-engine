import React from 'react';
import BuilderElementMask, {
    BuilderElementMaskProps,
} from '@form-engine/builder-mask/components/mask';
import BuilderMaskGroup from '@form-engine/builder-mask/components/mask/group';
import BuilderMaskGroupAppearance from '@form-engine/builder-mask/components/groups/appearance';
import BuilderMaskGroupConditions from '@form-engine/builder-mask/components/groups/conditions';
import BuilderMarkdownBodyProperty from '@form-engine/builder-mask/components/properties/markdown-body';
import { Element } from '.';
import { i18n } from '@form-engine/builder-application';

export default function BuilderMarkdownElementMask(
    props: BuilderElementMaskProps
) {
    return (
        <BuilderElementMask id={Element.apiName} {...props}>
            <BuilderMaskGroupAppearance />
            <BuilderMaskGroup label="Content">
                <BuilderMarkdownBodyProperty
                    label={i18n.c(`mask.${Element.apiName}.markdownBody`)}
                />
            </BuilderMaskGroup>
            <BuilderMaskGroupConditions />
        </BuilderElementMask>
    );
}
