import React from 'react';
import FormEngineInputElement from '@form-engine/element-input/components/input-element';
import { useFormEngineElement } from '@form-engine/core-application/hooks/use-element';
import { ElementTypeProps } from '@form-engine/core-application/types/form';

export default Object.assign(
    function FormEngineTextareaElement(props: ElementTypeProps) {
        const { state } = useFormEngineElement();

        if (!state)
            throw new Error('tried to render textarea without definition');

        return (
            <FormEngineInputElement
                {...props}
                as="textarea"
                rows={state.rows || '2'}
            />
        );
    },
    { apiName: 'FormEngineTextareaElement', isField: true, enabled: true }
);
