import { useMaskContext } from '@form-engine/builder-mask/hooks/use-mask';
import React from 'react';
import BuilderElementMask, {
    BuilderElementMaskProps,
} from '@form-engine/builder-mask/components/mask';
import BuilderMaskGroup from '@form-engine/builder-mask/components/mask/group';
import BuilderMaskGroupAppearance from '@form-engine/builder-mask/components/groups/appearance';
import BuilderMaskGroupConditions from '@form-engine/builder-mask/components/groups/conditions';
import BuilderMaskGroupValidation from '@form-engine/builder-mask/components/groups/validation';
import BuilderBooleanProperty from '@form-engine/builder-mask/components/properties/boolean';
import BuilderFloatingLabelProperty from '@form-engine/builder-mask/components/properties/floating-label';
import BuilderLabelProperty from '@form-engine/builder-mask/components/properties/label';
import BuilderPlaceholderProperty from '@form-engine/builder-mask/components/properties/placeholder';
import BuilderStringProperty from '@form-engine/builder-mask/components/properties/string';
import { Element } from '.';
import { i18n } from '@form-engine/builder-application';

export default function BuilderTextareaElementMask(
    props: BuilderElementMaskProps
) {
    const { c } = i18n.prefix(`mask.${Element.apiName}.`);

    return (
        <BuilderElementMask id={Element.apiName} {...props}>
            <BuilderMaskGroupAppearance />
            <BuilderMaskGroup label={c('groupTitle')}>
                <BuilderLabelProperty />
                <BuilderFloatingLabelProperty />
                <BuilderBooleanProperty
                    propertyName="showError"
                    label={c('showError')}
                />
                <BuilderPlaceholderProperty />
                <BuilderStringProperty
                    type="number"
                    step="1"
                    min="2"
                    max="20"
                    propertyName="rows"
                    label={c('rows')}
                    defaultValue="2"
                />
            </BuilderMaskGroup>
            <BuilderMaskGroupValidation />
            <BuilderMaskGroupConditions />
        </BuilderElementMask>
    );
}
