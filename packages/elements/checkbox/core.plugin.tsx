import { FormEnginePlugin } from '@form-engine/core-application';
import Element from './components/checkbox-element';

export default {
    elements: {
        [Element.apiName]: Element,
    },
} as FormEnginePlugin;
