import { useFormElementDatasourceData } from '@form-engine/core-application/hooks/use-datasource';
import { useFormContext } from '@form-engine/core-application/hooks/use-form';
import {
    useFormElementSession,
    useFormSessionLocals,
} from '@form-engine/core-application/hooks/use-formsession';
import useMarkdown from '@form-engine/core-application/hooks/use-markdown';
import { compile } from '@form-engine/core-util/template';
import React, { useMemo } from 'react';
import { useCallback } from 'react';
import { FormCheck, FormControl, FormGroup } from 'react-bootstrap';
import { useFormEngineElement } from '@form-engine/core-application/hooks/use-element';

export default Object.assign(
    function FormEngineCheckboxElement() {
        const {
            state: { showValidFeedback = true },
        } = useFormContext();
        const { state: element } = useFormEngineElement();
        if (!element)
            throw new Error('tried to render checkbox without definition');

        const [session, setSession] = useFormElementSession();
        const { options } = useFormElementDatasourceData();
        const currentValue = useMemo(() => {
            return Array.isArray(session?.value)
                ? session.value
                : String(session.value || '')
                      .trim()
                      .split(',');
        }, [session.value]);
        const compileMarkdown = useMarkdown();
        const { state: locals } = useFormSessionLocals();

        const handleChange = useCallback(
            (e: React.ChangeEvent) => {
                const target = e.target as HTMLInputElement;

                const getStringValue = (source: string[]) =>
                    source.filter((v) => Boolean(v)).join(',');

                if (target.checked && !currentValue.includes(target.value)) {
                    setSession({
                        ...session,
                        isTouched: true,
                        value: getStringValue([...currentValue, target.value]),
                    });
                } else if (
                    !target.checked &&
                    currentValue.includes(target.value)
                ) {
                    const copy = [...currentValue];
                    copy.splice(copy.indexOf(target.value), 1);
                    setSession({
                        ...session,
                        isTouched: true,
                        value: getStringValue(copy),
                    });
                }
            },
            [setSession, session, currentValue]
        );

        const controls = options.map((option, key) => {
            let label, value;
            if (Array.isArray(option)) {
                [value, label] = option;
            } else {
                label = value = option;
            }

            label = (
                <span
                    dangerouslySetInnerHTML={{ __html: compileMarkdown(label) }}
                />
            );

            value = compile(value || '', locals, '');

            const selected = currentValue.includes(value);

            return (
                <FormCheck
                    key={key}
                    inline={Boolean(element.inline)}
                    name={`${element.name}[${value}]`}
                    type="checkbox"
                    label={label}
                    checked={selected}
                    required={element.required}
                    isValid={
                        showValidFeedback && session.isTouched && !session.error
                    }
                    isInvalid={Boolean(session.isTouched && session.error)}
                    onChange={handleChange}
                    value={value}
                />
            );
        });

        const error = session.isTouched &&
            element.showError &&
            session.error && (
                <FormControl.Feedback type="invalid">
                    {session.error}
                </FormControl.Feedback>
            );

        return (
            <FormGroup className="fe-checkbox">
                <>
                    {element.label && (
                        <p className="form-label">{element.label}</p>
                    )}
                    {controls}
                    {error}
                </>
            </FormGroup>
        );
    },
    { apiName: 'FormEngineCheckboxElement', isField: true, enabled: true }
);
