import React from 'react';
import BuilderElementMask, {
    BuilderElementMaskProps,
} from '@form-engine/builder-mask/components/mask';
import BuilderMaskGroup from '@form-engine/builder-mask/components/mask/group';
import BuilderMaskGroupAppearance from '@form-engine/builder-mask/components/groups/appearance';
import BuilderLabelProperty from '@form-engine/builder-mask/components/properties/label';
import BuilderDataSourceProperty from '@form-engine/builder-mask/components/properties/data-source';
import BuilderMaskGroupValidation from '@form-engine/builder-mask/components/groups/validation';
import BuilderBooleanProperty from '@form-engine/builder-mask/components/properties/boolean';
import BuilderMaskGroupConditions from '@form-engine/builder-mask/components/groups/conditions';
import BuilderExpressionsProperty from '@form-engine/builder-mask/components/properties/expressions';
import { Element } from '.';
import { i18n } from '@form-engine/builder-application';

export default function BuilderCheckboxElementMask(
    props: BuilderElementMaskProps
) {
    const { c } = i18n.prefix(`mask.${Element.apiName}.`);

    return (
        <BuilderElementMask id={Element.apiName} {...props}>
            <BuilderMaskGroupAppearance />
            <BuilderMaskGroup label={c('groupTitle')}>
                <BuilderLabelProperty />
                <BuilderBooleanProperty
                    propertyName="inline"
                    label={c('inline')}
                />
                <BuilderDataSourceProperty label={c('datasource')} />
                <BuilderExpressionsProperty
                    label={c('datasourceExpressions')}
                    propertyName="datasourceExpressions"
                    variableOptions={['$i.']}
                />
            </BuilderMaskGroup>
            <BuilderMaskGroupValidation />
            <BuilderMaskGroupConditions />
        </BuilderElementMask>
    );
}
