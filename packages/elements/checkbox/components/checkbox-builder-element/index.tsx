import { default as DrawerButton } from './drawer-button';
import { default as Mask } from './mask';
import { default as Element } from '../checkbox-element';
import { ToggleOn as Icon } from 'react-bootstrap-icons';
import { BuilderElementDescriptor } from '@form-engine/builder-application/types/element';

const label = `${Element.apiName}.title`;
export { label, Element, DrawerButton, Mask, Icon };

export default {
    label,
    DrawerButton,
    Mask,
    Element,
    Icon,
} as BuilderElementDescriptor;
