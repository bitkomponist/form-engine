import { BuilderElementDescriptor } from '@form-engine/builder-application/types/element';

import { default as DrawerButton } from './drawer-button';
import { default as Mask } from './mask';
import { default as Element } from '../facette-select-element';
import { MenuButtonWideFill as Icon } from 'react-bootstrap-icons';

const label = `${Element.apiName}.title`;
export { label, Element, DrawerButton, Mask, Icon };

export default {
    label,
    DrawerButton,
    Mask,
    Element,
    Icon,
} as BuilderElementDescriptor;
