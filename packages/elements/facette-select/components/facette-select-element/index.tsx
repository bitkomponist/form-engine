import { useFormElementDatasourceData } from '@form-engine/core-application/hooks/use-datasource';
import { useFormEngineElement } from '@form-engine/core-application/hooks/use-element';
import { useFormElementSession } from '@form-engine/core-application/hooks/use-formsession';
import React, { useCallback, useMemo, useState } from 'react';
import { Row, Col } from 'react-bootstrap';

export interface FormEngineFacetteSelectOptionProps {
    label?: string;
    icon?: string;
    active?: boolean;
    onClick?: (e: React.MouseEvent) => void;
}

export interface FormEngineFacetteSelectOptionData {
    id?: string;
    label?: string;
    submenuLabel?: string;
    parent?: string;
    icon?: string;
}

export function FormEngineFacetteSelectOption({
    label,
    icon,
    active,
    onClick,
}: FormEngineFacetteSelectOptionProps) {
    return (
        <Col md={6}>
            <button
                className={`fe-facette-select-button ${active ? 'active' : ''}`}
                onClick={onClick}
            >
                {icon && <img src={icon} alt={label} />}
                {label && <span dangerouslySetInnerHTML={{ __html: label }} />}
            </button>
        </Col>
    );
}

export default Object.assign(
    function FormEngineFacetteSelectElement() {
        const { state: element } = useFormEngineElement();
        if (!element)
            throw new Error('tried to render checkbox without definition');
        const [session, setSession] = useFormElementSession();

        const handleChange = useCallback(
            (value: string) => {
                setSession({
                    ...session,
                    value,
                    isTouched: session.isTouched || session.value !== value,
                });
            },
            [setSession, session]
        );
        const { data = [] } = useFormElementDatasourceData();
        const options = useMemo(() => {
            return data;
        }, [data]);
        const [selection, setSelection] = useState<string[]>([]);
        const [menus, headings] = useMemo(() => {
            const path: string[] = [];
            const headings = [element?.label ?? ''];
            const menus = [null, ...selection].map((parent) => {
                const items = [];
                if (parent) {
                    path.push(parent);
                    const parentItem = options.find(
                        (item: any) => item.id === parent
                    );
                    parentItem && headings.push(parentItem?.submenuLabel ?? '');
                }
                for (const option of options) {
                    if (
                        (parent && option?.parent === parent) ||
                        (!parent && !option?.parent)
                    ) {
                        option.id &&
                            items.push({
                                ...option,
                                value: [...path, option.id],
                            });
                    }
                }
                return items;
            });
            return [menus, headings];
        }, [options, selection, element]);

        return (
            <>
                {menus.map((menuOptions, level) => {
                    return (
                        <div key={level}>
                            {headings[level] && <h6>{headings[level]}</h6>}
                            {menuOptions.length > 0 && (
                                <Row className="fe-facette-select-menu">
                                    {menuOptions.map((option, index) => (
                                        <FormEngineFacetteSelectOption
                                            key={index}
                                            {...option}
                                            active={
                                                option.id
                                                    ? selection.includes(
                                                          option.id
                                                      )
                                                    : false
                                            }
                                            onClick={(e) => {
                                                e.preventDefault();
                                                setSelection([...option.value]);
                                                const firstChild = options.find(
                                                    (item: any) =>
                                                        item.parent ===
                                                        option.id
                                                );
                                                if (firstChild) {
                                                    handleChange('');
                                                } else if (option.id) {
                                                    handleChange(option.id);
                                                }
                                            }}
                                        />
                                    ))}
                                </Row>
                            )}
                        </div>
                    );
                })}

                {session.isTouched && session.error && (
                    <p className="text-danger mb-3">{session.error}</p>
                )}
            </>
        );
    },
    {
        apiName: 'FormEngineFacetteSelectElement',
        isField: true,
        enabled: true,
    }
);
