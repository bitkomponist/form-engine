import { BuilderPlugin } from '@form-engine/builder-application';
import BuilderFacetteSelectElement from './components/facette-select-builder-element';
import * as translations from './builder-translations';

export default {
    translations,
    elements: {
        [BuilderFacetteSelectElement.Element.apiName]:
            BuilderFacetteSelectElement,
    },
    [BuilderFacetteSelectElement.Element.apiName]: {
        edit: {
            permissions: {
                name: 'write',
                hidden: 'write',
                width: 'write',
                marginTop: 'write',
                marginRight: 'write',
                marginBottom: 'write',
                marginLeft: 'write',
                label: 'write',
                datasource: 'write',
                required: 'write',
                requiredExpressions: 'write',
                hiddenExpressions: 'write',
            },
        },
        preview: {
            permissions: {
                name: 'write',
                hidden: 'write',
                width: 'write',
                marginTop: 'write',
                marginRight: 'write',
                marginBottom: 'write',
                marginLeft: 'write',
                label: 'write',
                datasource: 'write',
                required: 'write',
                requiredExpressions: 'write',
                hiddenExpressions: 'write',
            },
        },
        translate: {
            permissions: {
                name: 'read',
                hidden: 'read',
                width: 'read',
                marginTop: 'read',
                marginRight: 'read',
                marginBottom: 'read',
                marginLeft: 'read',
                label: 'write',
                datasource: 'write',
                required: 'read',
                requiredExpressions: 'read',
                hiddenExpressions: 'read',
            },
        },
    },
} as BuilderPlugin;
