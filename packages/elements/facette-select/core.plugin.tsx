import { FormEnginePlugin } from '@form-engine/core-application';
import FormEngineFacetteSelectElement from './components/facette-select-element';
export default {
    elements: {
        FormEngineFacetteSelectElement,
    },
} as FormEnginePlugin;
