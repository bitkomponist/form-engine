import React from 'react';
import BuilderElementMask, {
    BuilderElementMaskProps,
} from '@form-engine/builder-mask/components/mask';
import BuilderMaskGroupAppearance from '@form-engine/builder-mask/components/groups/appearance';
import BuilderMaskGroupConditions from '@form-engine/builder-mask/components/groups/conditions';
import { Element } from '.';
export default function BuilderContainerElementMask(
    props: BuilderElementMaskProps
) {
    return (
        <BuilderElementMask id={Element.apiName} {...props}>
            <BuilderMaskGroupAppearance />
            <BuilderMaskGroupConditions />
        </BuilderElementMask>
    );
}
