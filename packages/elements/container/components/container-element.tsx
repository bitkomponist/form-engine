import React, { useMemo } from 'react';
import { useFormContext } from '@form-engine/core-application/hooks/use-form';
import FormEngineElement from '@form-engine/core-application/components/element';
import FormEngineContainer from '@form-engine/core-application/components/container';

export interface FormEngineContainerElementProps {
    elementComponent?: React.ElementType;
    containerComponent?: React.ElementType;
    id: string;
}

export default Object.assign(
    function FormEngineContainerElement({
        elementComponent = FormEngineElement,
        containerComponent: Container = FormEngineContainer,
        id,
    }: FormEngineContainerElementProps) {
        const {
            state: { elements = [] },
        } = useFormContext();
        const childElementIds = useMemo(
            () =>
                elements
                    .filter((element) => element.parent === id)
                    .map((element) => element.id),
            [elements, id]
        );

        return (
            <Container
                id={id}
                childElementIds={childElementIds}
                elementComponent={elementComponent}
                containerComponent={Container}
            />
        );
    },
    {
        apiName: 'FormEngineContainerElement',
        isField: false,
        isContainer: true,
        enabled: true,
    }
);
