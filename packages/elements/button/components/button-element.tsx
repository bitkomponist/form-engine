import {
    useFormSessionContext,
    useParentFormPageSession,
} from '@form-engine/core-application/hooks/use-formsession';
import useWorkflow, {
    getWflEventName,
} from '@form-engine/core-application/hooks/use-workflow';
import React, { useMemo } from 'react';
import { Button } from 'react-bootstrap';
import { useFormEngineElement } from '@form-engine/core-application/hooks/use-element';

export const BUTTON_ACTIONS: { [key: string]: string } = {
    SUBMIT: 'submit',
    SHOW_PAGE: 'show-page',
    OPEN_URL: 'open-url',
    EMIT_EVENT: 'emit-event',
};

export const BUTTON_ACTIONS_LABELS: { [key: string]: string } = {
    SUBMIT: 'Submit',
    SHOW_PAGE: 'Show Page',
    OPEN_URL: 'Open Url',
    EMIT_EVENT: 'Emit Signal',
};

export default Object.assign(
    function FormEngineButtonElement() {
        const { state: elementDefinition } = useFormEngineElement();

        if (!elementDefinition)
            throw new Error('tried to render button without definition');

        const {
            action,
            label,
            isBlock,
            variant,
            size,
            url,
            urlTarget,
            pageName,
            eventName,
            requireValid = false,
        } = elementDefinition;

        const { state: session = {}, setItem: setSessionItem } =
            useFormSessionContext();
        const { valid: pageValid } = useParentFormPageSession();
        const { emit } = useWorkflow();

        const disabled = useMemo(() => {
            if (!requireValid) return false;
            return !pageValid;
        }, [requireValid, pageValid]);

        const actionProps: any = useMemo(() => {
            if (!action) return { type: 'submit' };

            switch (action) {
                case BUTTON_ACTIONS.SUBMIT:
                    return { type: 'submit' };
                case BUTTON_ACTIONS.OPEN_URL:
                    return {
                        as: 'a',
                        href: url ? url : undefined,
                        target: urlTarget ? urlTarget : undefined,
                    };
                case BUTTON_ACTIONS.SHOW_PAGE:
                    return {
                        onClick: () =>
                            pageName && setSessionItem('currentPage', pageName),
                    };
                case BUTTON_ACTIONS.EMIT_EVENT:
                    return {
                        onClick: () => emit && eventName && emit(eventName),
                    };
                default:
                    console.warn(`${action} not implemented`);
            }
        }, [action, url, pageName, session, setSessionItem, emit]);

        const { isSubmitting } = useWorkflow();

        const button = (
            <Button
                {...actionProps}
                {...{ variant, size }}
                disabled={disabled || isSubmitting}
                className="fe-button"
            >
                {label}
            </Button>
        );

        if (!isBlock) return button;

        return <div className="d-grid gap-2 fe-button">{button}</div>;
    },
    {
        apiName: 'FormEngineButtonElement',
        isField: false,
        enabled: true,
    }
);
