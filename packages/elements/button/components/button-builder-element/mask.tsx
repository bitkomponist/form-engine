import { BUTTON_ACTIONS, BUTTON_ACTIONS_LABELS } from '../button-element';
import React, { useMemo } from 'react';
import BuilderElementMask, {
    BuilderElementMaskProps,
} from '@form-engine/builder-mask/components/mask';
import BuilderMaskGroup from '@form-engine/builder-mask/components/mask/group';
import BuilderBooleanProperty from '@form-engine/builder-mask/components/properties/boolean';
import BuilderEnumProperty from '@form-engine/builder-mask/components/properties/enum';
import BuilderMaskGroupAppearance from '@form-engine/builder-mask/components/groups/appearance';
import BuilderLabelProperty from '@form-engine/builder-mask/components/properties/label';
import { useMaskContext } from '@form-engine/builder-mask/hooks/use-mask';
import BuilderStringProperty from '@form-engine/builder-mask/components/properties/string';
import BuilderPageNameProperty from '@form-engine/builder-mask/components/properties/page-name';
import BuilderMaskGroupConditions from '@form-engine/builder-mask/components/groups/conditions';
import { useWorkflowLayers } from '@form-engine/core-application/hooks/use-wfl';
import { Element } from '.';
import { i18n } from '@form-engine/builder-application';

const variants = [
    'primary',
    'secondary',
    'success',
    'danger',
    'warning',
    'info',
    'dark',
    'light',
    'link',
    'outline-primary',
    'outline-secondary',
    'outline-success',
    'outline-danger',
    'outline-warning',
    'outline-info',
    'outline-dark',
    'outline-light',
].map((key) => [key, key]);

const ACTION_OPTIONS = Object.keys(BUTTON_ACTIONS).map((key) => [
    BUTTON_ACTIONS[key],
    BUTTON_ACTIONS_LABELS[key] as string,
]);

const SIGNAL_TYPE_NAME = 'FormEngineSignalWfl';

export default function BuilderButtonElementMask(
    props: BuilderElementMaskProps
) {
    const { state } = useMaskContext();
    const { state: layers } = useWorkflowLayers();
    const signalOptions = useMemo(() => {
        return layers
            .filter(({ type }) => type === SIGNAL_TYPE_NAME)
            .map((signal) => [`${signal.id}@success`, signal.name]);
    }, [layers]);

    const { c } = i18n.prefix(`mask.${Element.apiName}.`);

    return (
        <BuilderElementMask id={Element.apiName} {...props}>
            <BuilderMaskGroupAppearance />
            <BuilderMaskGroup label="Button">
                <BuilderLabelProperty />
                <BuilderBooleanProperty
                    propertyName="isBlock"
                    label={c('isBlock')}
                />
                <BuilderEnumProperty
                    propertyName="variant"
                    label={c('variant')}
                    defaultValue="primary"
                    options={variants}
                />
                <BuilderBooleanProperty
                    propertyName="requireValid"
                    label={c('requireValid')}
                />
                <BuilderEnumProperty
                    propertyName="action"
                    label={c('action')}
                    defaultValue={BUTTON_ACTIONS.SUBMIT}
                    options={ACTION_OPTIONS}
                />
                {state.action === BUTTON_ACTIONS.EMIT_EVENT && (
                    <BuilderEnumProperty
                        propertyName="eventName"
                        label={c('eventName')}
                        options={signalOptions}
                    />
                )}
                {state.action === BUTTON_ACTIONS.SHOW_PAGE && (
                    <BuilderPageNameProperty label="Target Page" />
                )}
                {state.action === BUTTON_ACTIONS.OPEN_URL && (
                    <>
                        <BuilderStringProperty
                            propertyName="url"
                            type="url"
                            label={c('url')}
                        />
                        <BuilderEnumProperty
                            propertyName="urlTarget"
                            label={c('urlTarget')}
                            options={[
                                ['', 'Self'],
                                ['_blank', 'Blank'],
                                ['_parent', 'Parent'],
                                ['_top', 'Top'],
                            ]}
                        />
                    </>
                )}
            </BuilderMaskGroup>
            <BuilderMaskGroupConditions />
        </BuilderElementMask>
    );
}
