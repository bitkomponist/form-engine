import { BuilderPlugin } from '@form-engine/builder-application';
import BuilderElement from './components/navigation-builder-element';
import * as translations from './builder-translations';

export default {
    translations,
    elements: {
        [BuilderElement.Element.apiName]: BuilderElement,
    },
    [BuilderElement.Element.apiName]: {
        edit: {
            permissions: {
                name: 'write',
                hidden: 'write',
                width: 'write',
                marginTop: 'write',
                marginRight: 'write',
                marginBottom: 'write',
                marginLeft: 'write',
                datasource: 'write',
                datasourceExpressions: 'write',
                hiddenExpressions: 'write',
            },
        },
        preview: {
            permissions: {
                name: 'write',
                hidden: 'write',
                width: 'write',
                marginTop: 'write',
                marginRight: 'write',
                marginBottom: 'write',
                marginLeft: 'write',
                datasource: 'write',
                datasourceExpressions: 'write',
                hiddenExpressions: 'write',
            },
        },
        translate: {
            permissions: {
                name: 'read',
                hidden: 'read',
                width: 'read',
                marginTop: 'read',
                marginRight: 'read',
                marginBottom: 'read',
                marginLeft: 'read',
                datasource: 'write',
                datasourceExpressions: 'write',
                hiddenExpressions: 'read',
            },
        },
    },
} as BuilderPlugin;
