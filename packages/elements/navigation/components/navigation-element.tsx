import { useFormElementDatasourceData } from '@form-engine/core-application/hooks/use-datasource';

import {
    useCurrentPage,
    useFormSessionLocals,
} from '@form-engine/core-application/hooks/use-formsession';

import { compile } from '@form-engine/core-util/template';
import React from 'react';

import { FormGroup, Nav } from 'react-bootstrap';

export default Object.assign(
    function FormEngineNavigationElement() {
        const { options } = useFormElementDatasourceData();
        const { state: locals } = useFormSessionLocals();
        const [currentPage = '', setCurrentPage] = useCurrentPage();

        const controls = options.map((option, key) => {
            let label: string, value: string;
            if (Array.isArray(option)) {
                [value, label] = option;
            } else {
                label = value = option;
            }

            value = compile(value || '', locals, '');

            return (
                <Nav.Item key={key}>
                    <Nav.Link
                        eventKey={value}
                        onClick={(e: React.MouseEvent) => {
                            e.preventDefault();
                            setCurrentPage(value);
                        }}
                    >
                        {label}
                    </Nav.Link>
                </Nav.Item>
            );
        });

        return (
            <FormGroup className="fe-navigation">
                <Nav variant="tabs" activeKey={currentPage}>
                    {controls}
                </Nav>
            </FormGroup>
        );
    },
    { apiName: 'FormEngineNavigationElement', isField: false, enabled: true }
);
