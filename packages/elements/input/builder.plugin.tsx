import { BuilderPlugin } from '@form-engine/builder-application';
import BuilderElement from './components/input-builder-element';
import * as translations from './builder-translations';

export default {
    translations,
    elements: {
        [BuilderElement.Element.apiName]: BuilderElement,
    },
    maskConfigurations: {
        [BuilderElement.Element.apiName]: {
            edit: {
                permissions: {
                    name: 'write',
                    hidden: 'write',
                    width: 'write',
                    marginTop: 'write',
                    marginRight: 'write',
                    marginBottom: 'write',
                    marginLeft: 'write',
                    label: 'write',
                    floatingLabel: 'write',
                    showError: 'write',
                    placeholder: 'write',
                    inputType: 'write',
                    autoComplete: 'write',
                    required: 'write',
                    validationContentType: 'write',
                    validateContentTypeRegex: 'write',
                    validateContentTypeMessage: 'write',
                    validationMinLength: 'write',
                    validationMaxLength: 'write',
                    validationValueOf: 'write',
                    requiredExpressions: 'write',
                    hiddenExpressions: 'write',
                },
            },
            preview: {
                permissions: {
                    name: 'write',
                    hidden: 'write',
                    width: 'write',
                    marginTop: 'write',
                    marginRight: 'write',
                    marginBottom: 'write',
                    marginLeft: 'write',
                    label: 'write',
                    floatingLabel: 'write',
                    showError: 'write',
                    placeholder: 'write',
                    inputType: 'write',
                    autoComplete: 'write',
                    required: 'write',
                    validationContentType: 'write',
                    validateContentTypeRegex: 'write',
                    validateContentTypeMessage: 'write',
                    validationMinLength: 'write',
                    validationMaxLength: 'write',
                    validationValueOf: 'write',
                    requiredExpressions: 'write',
                    hiddenExpressions: 'write',
                },
            },
            translate: {
                permissions: {
                    name: 'read',
                    hidden: 'read',
                    width: 'read',
                    marginTop: 'read',
                    marginRight: 'read',
                    marginBottom: 'read',
                    marginLeft: 'read',
                    label: 'write',
                    floatingLabel: 'read',
                    showError: 'read',
                    placeholder: 'write',
                    inputType: 'read',
                    autoComplete: 'read',
                    required: 'read',
                    validationContentType: 'read',
                    validateContentTypeRegex: 'read',
                    validateContentTypeMessage: 'write',
                    validationMinLength: 'read',
                    validationMaxLength: 'read',
                    validationValueOf: 'read',
                    requiredExpressions: 'read',
                    hiddenExpressions: 'read',
                },
            },
        },
    },
} as BuilderPlugin;
