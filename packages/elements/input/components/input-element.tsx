import { useFormContext } from '@form-engine/core-application/hooks/use-form';
import { useFormElementSession } from '@form-engine/core-application/hooks/use-formsession';
import React from 'react';
import { useCallback } from 'react';
import {
    FloatingLabel,
    FormControl,
    FormGroup,
    FormLabel,
    ProgressBar,
} from 'react-bootstrap';
import { useFormEngineElement } from '@form-engine/core-application/hooks/use-element';
import { ElementTypeProps } from '@form-engine/core-application/types/form';

export type FormEngineInputElementProps = ElementTypeProps &
    React.ComponentProps<typeof FormControl>;

export default Object.assign(
    function FormEngineInputElement({
        editMode = false,
        ...additionalPropsProp
    }: FormEngineInputElementProps) {
        const {
            state: { showValidFeedback = true },
        } = useFormContext();
        const { state: element } = useFormEngineElement();
        if (!element)
            throw new Error('tried to render input without definition');
        const [session, setSession] = useFormElementSession();

        const handleChange = useCallback(
            (e: React.ChangeEvent) => {
                const value = (e.target as HTMLInputElement).value;
                setSession({ ...session, value });
            },
            [setSession, session]
        );
        const handleTouched = useCallback(() => {
            setSession({ ...session, isTouched: true });
        }, [setSession, session]);

        let additionalProps: React.ComponentProps<typeof FormControl> = {
            ...additionalPropsProp,
        };

        delete additionalProps.containerComponent;
        delete additionalProps.elementComponent;
        delete additionalProps.source;
        delete additionalProps.parent;
        delete additionalProps.id;
        delete additionalProps.prepend;
        delete additionalProps.append;

        if (editMode) {
            additionalProps = {
                ...additionalProps,
                autoComplete: 'off',
                autoCorrect: 'off',
                autoCapitalize: 'none',
                spellCheck: 'false',
            };
        }

        const control = (
            <FormControl
                id={element.id}
                name={element.name}
                placeholder={element.placeholder || ''}
                type={element.inputType || 'text'}
                required={element.required}
                isValid={
                    showValidFeedback &&
                    session.isTouched &&
                    !session.error &&
                    !session.loading
                }
                isInvalid={Boolean(session.isTouched && session.error)}
                onChange={handleChange}
                onBlur={handleTouched}
                value={session.value}
                autoComplete={element.autoComplete || 'on'}
                {...additionalProps}
            />
        );

        const error = session.isTouched &&
            element.showError &&
            session.error && (
                <FormControl.Feedback type="invalid" tooltip={false}>
                    {session.error}
                </FormControl.Feedback>
            );

        const loader = session.loading !== undefined &&
            session.loading !== false && (
                <ProgressBar
                    className="mt-1"
                    style={{ height: '5px' }}
                    animated
                    striped
                    now={
                        typeof session.loading === 'number'
                            ? session.loading
                            : 100
                    }
                />
            );

        return (
            <FormGroup className="fe-input">
                <>
                    {element.label && element.floatingLabel ? (
                        <FloatingLabel
                            label={element.label}
                            controlId={element.id}
                        >
                            <>
                                <control.type
                                    {...control.props}
                                    id={undefined}
                                />
                                {error}
                            </>
                        </FloatingLabel>
                    ) : (
                        <>
                            {element.label && (
                                <FormLabel htmlFor={element.id}>
                                    {element.label}
                                </FormLabel>
                            )}
                            {control}
                            {error}
                        </>
                    )}
                    {loader}
                </>
            </FormGroup>
        );
    },
    { apiName: 'FormEngineInputElement', isField: true, enabled: true }
);
