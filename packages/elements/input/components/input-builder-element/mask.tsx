import { useMaskContext } from '@form-engine/builder-mask/hooks/use-mask';
import React from 'react';
import BuilderElementMask, {
    BuilderElementMaskProps,
} from '@form-engine/builder-mask/components/mask';
import BuilderMaskGroup from '@form-engine/builder-mask/components/mask/group';
import BuilderMaskGroupAppearance from '@form-engine/builder-mask/components/groups/appearance';
import BuilderMaskGroupConditions from '@form-engine/builder-mask/components/groups/conditions';
import BuilderMaskGroupValidation from '@form-engine/builder-mask/components/groups/validation';
import BuilderBooleanProperty from '@form-engine/builder-mask/components/properties/boolean';
import BuilderEnumProperty from '@form-engine/builder-mask/components/properties/enum';
import BuilderFloatingLabelProperty from '@form-engine/builder-mask/components/properties/floating-label';
import BuilderInputTypeProperty from '@form-engine/builder-mask/components/properties/input-type';
import BuilderLabelProperty from '@form-engine/builder-mask/components/properties/label';
import BuilderPlaceholderProperty from '@form-engine/builder-mask/components/properties/placeholder';
import { formatTitleCase } from '@form-engine/core-util/format-case';
import { Element } from '.';
import { i18n } from '@form-engine/builder-application';
/** @see https://raw.githubusercontent.com/mdn/content/main/files/en-us/web/html/attributes/autocomplete/index.md */
const AUTOCOMPLETE_OPTIONS = [
    ['on'],
    ['off'],
    ['name'],
    ['honorific-prefix'],
    ['given-name'],
    ['additional-name'],
    ['family-name'],
    ['honorific-suffix'],
    ['nickname'],
    ['email'],
    ['username'],
    ['new-password'],
    ['current-password'],
    ['one-time-code'],
    ['organization-title'],
    ['organization'],
    ['address-line1'],
    ['address-line2'],
    ['address-line3'],
    ['address-level4'],
    ['address-level3'],
    ['address-level2'],
    ['address-level1'],
    ['country'],
    ['country-name'],
    ['postal-code'],
    ['cc-name'],
    ['cc-given-name'],
    ['cc-additional-name'],
    ['cc-family-name'],
    ['cc-number'],
    ['cc-exp'],
    ['cc-exp-month'],
    ['cc-exp-year'],
    ['cc-csc'],
    ['cc-type'],
    ['transaction-currency'],
    ['transaction-amount'],
    ['language'],
    ['bday'],
    ['bday-day'],
    ['bday-month'],
    ['bday-year'],
    ['sex'],
    ['tel'],
    ['tel-country-code'],
    ['tel-national'],
    ['tel-area-code'],
    ['tel-local'],
    ['tel-extension'],
    ['impp'],
    ['url'],
    ['photo'],
].map((item) => [item[0], formatTitleCase(item[0])]);

export default function BuilderInputElementMask(
    props: BuilderElementMaskProps
) {
    const { c } = i18n.prefix(`mask.${Element.apiName}.`);
    return (
        <BuilderElementMask id={Element.apiName} {...props}>
            <BuilderMaskGroupAppearance />
            <BuilderMaskGroup label={c('groupTitle')}>
                <BuilderLabelProperty />
                <BuilderFloatingLabelProperty />
                <BuilderBooleanProperty
                    propertyName="showError"
                    label={c('showError')}
                />
                <BuilderPlaceholderProperty />
                <BuilderInputTypeProperty />
                <BuilderEnumProperty
                    propertyName="autoComplete"
                    label={c('autoComplete')}
                    options={AUTOCOMPLETE_OPTIONS}
                />
            </BuilderMaskGroup>
            <BuilderMaskGroupValidation />
            <BuilderMaskGroupConditions />
        </BuilderElementMask>
    );
}
