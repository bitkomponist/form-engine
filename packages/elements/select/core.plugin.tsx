import { FormEnginePlugin } from '@form-engine/core-application';
import Element from './components/select-element';

export default {
    elements: {
        [Element.apiName]: Element,
    },
} as FormEnginePlugin;
