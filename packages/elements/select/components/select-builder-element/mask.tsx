import React from 'react';
import BuilderElementMask, {
    BuilderElementMaskProps,
} from '@form-engine/builder-mask/components/mask';
import BuilderMaskGroup from '@form-engine/builder-mask/components/mask/group';
import BuilderBooleanProperty from '@form-engine/builder-mask/components/properties/boolean';
import BuilderMaskGroupAppearance from '@form-engine/builder-mask/components/groups/appearance';
import BuilderFloatingLabelProperty from '@form-engine/builder-mask/components/properties/floating-label';
import BuilderLabelProperty from '@form-engine/builder-mask/components/properties/label';

import BuilderDataSourceProperty from '@form-engine/builder-mask/components/properties/data-source';
import BuilderMaskGroupValidation from '@form-engine/builder-mask/components/groups/validation';
import BuilderMaskGroupConditions from '@form-engine/builder-mask/components/groups/conditions';
import BuilderExpressionsProperty from '@form-engine/builder-mask/components/properties/expressions';
import { Element } from '.';
import { useMaskContext } from '@form-engine/builder-mask/hooks/use-mask';
import BuilderStringProperty from '@form-engine/builder-mask/components/properties/string';
import { i18n } from '@form-engine/builder-application';

export default function BuilderSelectElementMask(
    props: BuilderElementMaskProps
) {
    const { state } = useMaskContext();

    const { c } = i18n.prefix(`mask.${Element.apiName}.`);

    return (
        <BuilderElementMask id={Element.apiName} {...props}>
            <BuilderMaskGroupAppearance />
            <BuilderMaskGroup label={c('groupTitle')}>
                <BuilderLabelProperty />
                <BuilderFloatingLabelProperty />
                <BuilderBooleanProperty
                    propertyName="showError"
                    label={c('showError')}
                />
                <BuilderBooleanProperty
                    propertyName="showDefaultOption"
                    label={c('showDefaultOption')}
                />
                {state.showDefaultOption && (
                    <BuilderStringProperty
                        propertyName="defaultOptionLabel"
                        label={c('defaultOptionLabel')}
                        placeholder={c('defaultOptionLabelPlaceholder')}
                    />
                )}
                <BuilderDataSourceProperty label={c('datasource')} />
                <BuilderExpressionsProperty
                    label={c('datasourceExpressions')}
                    propertyName="datasourceExpressions"
                    variableOptions={['$i.']}
                />
            </BuilderMaskGroup>
            <BuilderMaskGroupValidation />
            <BuilderMaskGroupConditions />
        </BuilderElementMask>
    );
}
