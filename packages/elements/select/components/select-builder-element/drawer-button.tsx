import React from 'react';
import BuilderElementsDrawerButton, {
    BuilderElementsDrawerButtonProps,
} from '@form-engine/builder-form-editor/components/elements-drawer/button';
import { Element, Icon, label } from './index';
import { i18n } from '@form-engine/builder-application';

const ctor = (overrides: any = {}) => ({
    id: undefined,
    type: Element.apiName,
    ...overrides,
});

export default function BuilderSelectElementDrawerButton(
    props: BuilderElementsDrawerButtonProps
) {
    return (
        <BuilderElementsDrawerButton
            {...props}
            constructor={ctor}
            dragLabel={i18n.c(label)}
        >
            <Icon /> <i18n.Node id={label} />
        </BuilderElementsDrawerButton>
    );
}
