import { useFormElementDatasourceData } from '@form-engine/core-application/hooks/use-datasource';
import { useFormContext } from '@form-engine/core-application/hooks/use-form';
import {
    useFormElementSession,
    useFormSessionLocals,
} from '@form-engine/core-application/hooks/use-formsession';
import { compile } from '@form-engine/core-util/template';
import React from 'react';
import { useCallback } from 'react';
import {
    FloatingLabel,
    FormControl,
    FormGroup,
    FormLabel,
    FormSelect,
} from 'react-bootstrap';
import { useFormEngineElement } from '@form-engine/core-application/hooks/use-element';
import FormEngine from '@form-engine/core-application';

export default Object.assign(
    function FormEngineSelectElement() {
        const {
            state: { showValidFeedback = true },
        } = useFormContext();

        const { state: element } = useFormEngineElement();
        if (!element)
            throw new Error('tried to render select without definition');
        const {
            name = '',
            label = '',
            floatingLabel = false,
            showDefaultOption = false,
            defaultOptionLabel = FormEngine.i18n.c(
                'select-element.default-option'
            ),
            required = false,
        } = element;

        const { state: locals } = useFormSessionLocals();

        const [session, setSession] = useFormElementSession();
        const handleChange = useCallback(
            (e: React.ChangeEvent) => {
                const value = (e.target as HTMLSelectElement).value;
                setSession({ ...session, isTouched: true, value });
            },
            [setSession, session]
        );
        const { options, loading } = useFormElementDatasourceData();

        const control = (
            <FormSelect
                id={element.id}
                name={name}
                disabled={loading}
                value={session.value}
                onChange={handleChange}
                required={required}
                isValid={
                    showValidFeedback && session.isTouched && !session.error
                }
                isInvalid={Boolean(session.isTouched && session.error)}
            >
                <>
                    {showDefaultOption && (
                        <option value="">{defaultOptionLabel}</option>
                    )}

                    {options.map((option, key) => {
                        let label, value;
                        if (Array.isArray(option)) {
                            [value, label] = option;
                        } else {
                            label = value = option;
                        }

                        value = compile(value || '', locals, '');

                        return (
                            <option key={key} value={value}>
                                {label || value}
                            </option>
                        );
                    })}
                </>
            </FormSelect>
        );

        const error = session.isTouched &&
            element.showError &&
            session.error && (
                <FormControl.Feedback type="invalid" tooltip={false}>
                    {session.error}
                </FormControl.Feedback>
            );

        return (
            <FormGroup>
                <>
                    {label && floatingLabel ? (
                        <FloatingLabel label={label} controlId={element.id}>
                            <>
                                <control.type
                                    {...control.props}
                                    id={undefined}
                                />
                                {error}
                            </>
                        </FloatingLabel>
                    ) : (
                        <>
                            {label && (
                                <FormLabel htmlFor={element.id}>
                                    {label}
                                </FormLabel>
                            )}
                            {control}
                            {error}
                        </>
                    )}
                </>
            </FormGroup>
        );
    },
    { apiName: 'FormEngineSelectElement', isField: true, enabled: true }
);
