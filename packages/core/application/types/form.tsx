import type { Diff } from 'deep-diff';

export type FormResourceDefinition = { url?: string } | string[] | string;
export interface FormElementSessionState {
    value: string;
    isTouched: boolean;
    isHidden: boolean;
    error: string | null;
    [key: string]: unknown;
}

export interface FormDatasourceResult {
    data: any;
    error?: Error;
    options?: Array<[string, string]>;
    constants?: any;
}
export interface FormSession {
    locals?: any;
    constants?: any;
    currentPage?: string;
    elements?: { [id: string]: FormElementSessionState };
    datasources?: { [id: string]: FormDatasourceResult };
    errors?: { [id: string]: Error };
    editMode?: string;
    [key: string]: any;
}

export type FormCollectionItem = { id: string; [prop: string]: unknown };

export type FormCollection = FormCollectionItem[];

export interface ElementDatasourceConfiguration {
    mode?: 'static';
    staticOptions?: DatasourceResponseOptions;
    datasourceId?: string;
}

export interface AuthInfo {
    auth?: 'basic' | 'bearer';
    basicAuthUsername?: string;
    basicAuthPassword?: string;
    bearerToken?: string;
}

export type ElementGridOptions =
    | '0'
    | '1'
    | '2'
    | '3'
    | '4'
    | '5'
    | '6'
    | '7'
    | '8'
    | '9'
    | '10'
    | '11'
    | '12';

export interface ExpressionDefinition {
    leftOperand: string;
    rightOperand: string;
    operator:
        | 'equal'
        | 'not-equal'
        | 'greater'
        | 'greater-equal'
        | 'lesser'
        | 'lesser-equal'
        | 'in'
        | 'contains';
    disjunctor?: 'and' | 'or';
}
export interface ElementDefinition extends FormCollectionItem, AuthInfo {
    id: string;
    type: string;
    parent?: string;
    name?: string;
    label?: string;
    datasource?: ElementDatasourceConfiguration;
    datasourceExpressions?: ExpressionDefinition[];
    hidden?: boolean;
    hiddenExpressions?: ExpressionDefinition[];
    required?: boolean;
    requiredExpressions?: ExpressionDefinition[];
    requiredMessage?: string;
    validationMinLength?: string;
    validationMinLengthMessage?: string;
    validationMaxLength?: string;
    validationMaxLengthMessage?: string;
    validationContentType?: string;
    validateContentTypeMessage?: string;
    validateContentTypeRegex?: string;
    validateContentTypeDateBefore?: string;
    validateContentTypeDateAfter?: string;
    minDate?: string;
    maxDate?: string;
    width?: ElementGridOptions;
    marginTop?: ElementGridOptions;
    marginRight?: ElementGridOptions;
    marginBottom?: ElementGridOptions;
    marginLeft?: ElementGridOptions;
    action?: string;
    eventName?: string;
    isBlock?: boolean;
    variant?: string;
    size?: 'sm' | 'lg';
    url?: string;
    urlTarget?: string;
    pageName?: string;
    requireValid?: boolean;
    inline?: boolean;
    floatingLabel?: boolean;
    placeholder?: string;
    inputType?: string;
    autoComplete?: string;
    markdownBody?: string;
    isDefault?: boolean;
    showDefaultOptions?: boolean;
    defaultOptionLabel?: string;
    rows?: string;
    $locales?: {
        [key: string]: Partial<ElementDefinition>;
    };
    [option: string]: unknown;
}
export interface WorkflowLayerDefinition extends FormCollectionItem, AuthInfo {
    id: string;
    name: string;
    nodeX: number;
    nodeY: number;
    type: string;
    /** @deprecated */
    trigger?: 'event' | 'submit' | 'init';
    /** @deprecated */
    emitErrorEvent?: boolean;
    /** @deprecated */
    errorEventName?: string;
    /** @deprecated */
    emitSuccessEvent?: boolean;
    /** @deprecated */
    successEventName?: string;
    /** @deprecated */
    eventName?: string;
    events?: string[];
    enabled?: boolean;
    pageName?: string;
    url?: string;
    urlTarget?: string;
    method?: string;
    contentType?: string;
    body?: string;
    source?: string;
    sourceType?: string;
    [option: string]: unknown;
}
export interface DatasourceDefinition extends FormCollectionItem, AuthInfo {
    id: string;
    type: string;
    name?: string;
    url?: string;
    method?: string;
    contentType?: string;
    body?: string;
    source?: string;
    sourceType?: string;
    labelColumn?: string;
    valueColumn?: string;
    csvOptions?: any;
    resultPath?: string;
    jwtSource?: string;
    jwtOutput?: string;
    $prefetchResult?: any;
    [option: string]: unknown;
}
export interface ConstantDefinition extends FormCollectionItem {
    id: string;
    value: string;
    notes: string;
}

export interface FormDefinition {
    id?: string;
    createdAt?: string;
    updatedAt?: string;
    versioningParentId?: string;
    versioningDiff?: Diff<FormDefinition>[];
    name?: string;
    owner?: string;
    slug?: string;
    tags?: string[];
    thumbnail?: string;
    published?: boolean;
    elements?: ElementDefinition[];
    datasources?: DatasourceDefinition[];
    workflowLayers?: WorkflowLayerDefinition[];
    constants?: ConstantDefinition[];
    resources?: FormResourceDefinition[];
    includeCoreResources?: boolean;
    includeBootstrapCdn?: boolean;
    bootstrapCdnUrl?: string;
    stylesheet?: string;
    showValidFeedback?: boolean;
    [key: string]: unknown;
}

export type FormDefinitionCollections = Pick<
    FormDefinition,
    'elements' | 'datasources' | 'workflowLayers' | 'constants'
>;

export type ElementMetaProps = {
    apiName: string;
    isField?: boolean;
    isContainer?: boolean;
    enabled?: boolean;
};

export interface ElementTypeProps extends React.HTMLAttributes<HTMLElement> {
    id: string;
    as?: React.ElementType;
    elementComponent?: React.FunctionComponent<ElementTypeProps>;
    containerComponent?: React.FunctionComponent<ElementTypeProps>;
    prepend?: JSX.Element;
    append?: JSX.Element;
    editMode?: boolean;
    parent?: string;
    source?: ElementDefinition;
    [key: string]: unknown;
}

export type ElementType = React.FunctionComponent<ElementTypeProps> &
    ElementMetaProps;

export type WorkflowLayerMetaProps = {
    apiName: string;
};

export type WorkflowLayerArgs = {
    form: FormDefinition;
    session: FormSession;
    definition: WorkflowLayerDefinition;
    data?: any;
};
export type WorkflowLayerResponse = Partial<FormSession>;
export type WorkflowLayerType = ((
    args: WorkflowLayerArgs
) => Promise<WorkflowLayerResponse>) &
    WorkflowLayerMetaProps;

export type DatasourceMetaProps = {
    apiName: string;
    isGlobal?: boolean;
};
export interface DatasourceArgs {
    definition: DatasourceDefinition;
    session?: FormSession;
    form: FormDefinition;
}

export type DatasourceResponseOptions = string[][];
export interface DatasourceResponse {
    data: any;
    error?: any;
    options: DatasourceResponseOptions;
}
export type DatasourceType = ((
    args: DatasourceArgs
) => Promise<DatasourceResponse>) &
    DatasourceMetaProps;

export interface LogDefinition {
    id?: string;
    createdAt?: string;
    updatedAt?: string;
    form?: FormDefinition;
    formId?: string;
    value?: string;
}

export type ElementControllerMetaProps = {
    apiName: string;
};

export type ElementControllerArgs = {
    form: FormDefinition;
    session: FormSession;
    definition: ElementDefinition;
    data?: any;
};
export type ElementControllerResponse = any;
export type ElementControllerType = ((
    args: ElementControllerArgs
) => Promise<ElementControllerResponse>) &
    ElementControllerMetaProps;

export type ValidationType = (
    element: ElementDefinition,
    value: string,
    session: FormSession
) => string | null;
