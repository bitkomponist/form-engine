import React from 'react';
import { createRoot, Root } from 'react-dom/client';
import FormEngineEmbeddedApplication, {
    FormEngineEmbeddedApplicationProps,
} from './components/embedded-application';

// workaround to only import stylesheets with parcel, not with storybook
// import(/* webpackIgnore: true */'./style/theme.scss');
import './style/theme.scss';

import * as translations from './core-translations';
import * as plugins from './plugins';
import * as validations from './validation/validations';

import { registerImportedPluginModules } from '@form-engine/core-util/register-plugin';
import {
    DatasourceType,
    ElementType,
    WorkflowLayerType,
    ValidationType,
} from './types/form';
import { I18n, I18nState } from './utilities/i18n';

export interface IFormEngine {
    wflBackendRoute: string;
    elementControllerBackendRoute: string;
    datasourceBackendRoute: string;
    autoload: boolean;
    selector: string;
    isBuilder: boolean;
    initDocument: (doc?: Document) => Array<Root | null>;
    initFormFromConstructorNode: (constructorNode: Element) => Root | null;
    initForm: (
        siblingNode: Element,
        options?: FormEngineEmbeddedApplicationProps
    ) => Root;
    destroyForm: (root: Root) => void;
    getFieldElements: () => { [elementTypeId: string]: ElementType };
    getContainerElements: () => { [elementTypeId: string]: ElementType };
    elements: {
        [elementTypeId: string]: ElementType;
    };
    datasources: {
        [datasourceTypeId: string]: DatasourceType;
    };
    workflowLayers: {
        [workflowLayerTypeId: string]: WorkflowLayerType;
    };
    validations: {
        [validationTypeId: string]: ValidationType;
    };
    translations: I18nState['data'];
    i18n: I18n;
    [prop: string]: any;
}

export type FormEnginePlugin = Partial<IFormEngine>;

const prevFormEngine: Partial<IFormEngine> = (window as any).FormEngine ?? {};

const FormEngine: IFormEngine = {
    wflBackendRoute: `/form/:formId/workflow/:wflId`,
    datasourceBackendRoute: `/form/:formId/datasource/:datasourceId`,
    elementControllerBackendRoute: `/form/:formId/element/:elementId`,
    autoload: true,
    selector: 'script[type="application/form-engine-configuration"]',
    isBuilder: false,
    initDocument(doc = document) {
        return Array.from(doc.querySelectorAll(FormEngine.selector)).map(
            (node) => FormEngine.initFormFromConstructorNode(node)
        );
    },
    initFormFromConstructorNode(constructorNode) {
        let options = {};
        try {
            options = JSON.parse(constructorNode.textContent ?? '');
        } catch (e) {
            console.error(e);
            return null;
        }

        return FormEngine.initForm(
            constructorNode,
            options as FormEngineEmbeddedApplicationProps
        );
    },
    initForm(siblingNode, options) {
        const rootElement = document.createElement('div');
        rootElement.className = 'fe-root';
        siblingNode?.parentNode?.insertBefore(
            rootElement,
            siblingNode.nextSibling
        );

        if (!options?.form) {
            throw new Error(
                `invalid form constructor, missing form definition`
            );
        }

        const root = createRoot(rootElement);

        root.render(
            <React.StrictMode>
                <FormEngineEmbeddedApplication {...options} />
            </React.StrictMode>
        );

        return root;
    },
    destroyForm(root) {
        root.unmount();
    },
    getFieldElements() {
        return Object.fromEntries(
            Object.entries(FormEngine?.elements ?? {}).filter((entry) =>
                Boolean(entry[1].isField)
            )
        );
    },
    getContainerElements() {
        return Object.fromEntries(
            Object.entries(FormEngine?.elements ?? {}).filter((entry) =>
                Boolean(entry[1].isContainer)
            )
        );
    },
    ...(prevFormEngine ?? {}),
    elements: {
        ...(prevFormEngine?.elements ?? {}),
    },
    datasources: {
        ...(prevFormEngine?.datasources ?? {}),
    },
    workflowLayers: {
        ...(prevFormEngine?.workflowLayers ?? {}),
    },
    validations: {
        ...validations,
        ...(prevFormEngine?.validations ?? {}),
    },
    translations: {
        ...translations,
        ...(prevFormEngine?.translations ?? {}),
    },
    i18n: null as any,
};

(window as any).FormEngine = FormEngine;

registerImportedPluginModules(FormEngine, plugins);

FormEngine.i18n = new I18n({ data: FormEngine.translations });

export const i18n = FormEngine.i18n;

export default FormEngine;

window.addEventListener('DOMContentLoaded', () => {
    if (FormEngine.autoload) {
        FormEngine.initDocument();
    }
});
