import { isJson } from '@form-engine/core-util/is-json';
import FormEngine from '..';
import { FormSession } from '../types/form';

export default async function fetchWflBackend(
    formId: string,
    wflId: string,
    session: Partial<FormSession>,
    data?: any,
    options: RequestInit = {}
) {
    const path = FormEngine.wflBackendRoute
        .replace(':formId', formId)
        .replace(':wflId', wflId);

    const response = await fetch(path, {
        method: 'POST',
        ...options,
        body: JSON.stringify({ session, data }),
        headers: {
            'content-type': 'application/json',
            accept: 'application/json',
            ...(options.headers || {}),
        },
    });

    if (response.status < 200 || response.status >= 400) {
        const message = await response.text();
        let error;
        if (message.startsWith('{')) {
            try {
                const errorData = JSON.parse(message);
                if (String(errorData?.error?.message).startsWith('{')) {
                    errorData.error = JSON.parse(errorData.error.message);
                }
                error = new Error(errorData?.error?.message);
                Object.assign(error, errorData?.error ?? {});
                FormEngine.__currentError = error;
            } catch (e) {
                error = new Error(message);
                FormEngine.__currentError = error;
            }
        }

        throw error;
    }

    const result = await response.json();

    return (result || {}) as Partial<FormSession>;
}
