import React, { useMemo } from 'react';
import { FormResourceDefinition } from '../types/form';
import { useFormContext } from './use-form';

function getResourceUrl(resource: FormResourceDefinition) {
    if (Array.isArray(resource)) return resource[0] || '';
    if (typeof resource === 'object') return resource?.url || '';
    return String(resource);
}

/** @todo fetch version from package.js */
export const DEFAULT_BOOTSTRAP_CDN_URL =
    'https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css';

export default function useFormResources() {
    const {
        state: {
            resources: formResources = [],
            includeCoreResources = true,
            includeBootstrapCdn = true,
            bootstrapCdnUrl = '',
        },
    } = useFormContext();

    return useMemo(() => {
        let resources = formResources || [];

        if (!Array.isArray(resources) && typeof resources === 'object') {
            resources = Object.entries(resources);
        }

        if (includeBootstrapCdn) {
            resources = [
                bootstrapCdnUrl?.trim() || DEFAULT_BOOTSTRAP_CDN_URL,
                ...resources,
            ];
        }

        const coreResources = Array.from(
            document.querySelectorAll('head link[rel="stylesheet"]')
        ).map((node) => {
            return [(node as HTMLLinkElement).href];
        });

        return [...(includeCoreResources ? coreResources : []), ...resources]
            .map((resource) => getResourceUrl(resource))
            .filter((url, index, self) => self.indexOf(url) === index)
            .map((url, index) => {
                if (url.endsWith('.js')) {
                    return <script key={index} src={url} />;
                }
                if (url.endsWith('.css')) {
                    return (
                        <link
                            key={index}
                            type="text/css"
                            rel="stylesheet"
                            href={url}
                        />
                    );
                }
                return null;
            })
            .filter((node) => node !== null);
    }, [formResources, includeCoreResources]);
}
