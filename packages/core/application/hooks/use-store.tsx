import { useEffect, useState } from 'react';
import isEqual from 'lodash.isequal';

export interface StoreData {
    [prop: string]: any;
}

export type StoreListener = (eventData?: any) => void;

export interface StoreReference<T extends StoreData> {
    state: T;
    previousState?: T;
    set: (value: Partial<T> | null, eventData?: any) => T;
    setItem: (key: keyof T, value: any, eventData?: any) => T;
    unsetItem: (key: keyof T, eventData?: any) => T;
    clearAll: (eventData?: any) => T;
    eventData: any;
}

export class Store<
    T extends StoreData,
    R extends StoreReference<T> = StoreReference<T>
> {
    initialState: T;

    state: T;

    previousState?: T;

    listeners: StoreListener[];

    constructor(initialState: T) {
        this.initialState = initialState;
        this.state = { ...initialState };
        this.listeners = [];
    }

    set = (value: Partial<T> | null, eventData?: any) => {
        this.previousState = this.state;
        if (!value) {
            this.state = { ...this.initialState };
        } else {
            this.state = { ...this.initialState, ...value };
        }
        this.dispatch(eventData);
        return this.state;
    };

    setItem = (key: keyof T, value: any, eventData?: any) => {
        this.previousState = this.state;
        this.state = { ...this.state, [key]: value };
        this.dispatch(eventData);
        return this.state;
    };

    unsetItem = (key: keyof T, eventData?: any) => {
        this.previousState = this.state;
        const copy = { ...this.state };
        delete copy[key];
        this.state = copy;
        this.dispatch(eventData);
        return this.state;
    };

    clearAll = (eventData?: any) => {
        this.previousState = this.state;
        this.state = { ...this.initialState };
        this.dispatch(eventData);
        return this.state;
    };

    getReference(eventData: any = {}) {
        const { state, previousState, set, setItem, unsetItem, clearAll } =
            this;
        return {
            state,
            previousState,
            set,
            setItem,
            unsetItem,
            clearAll,
            eventData,
        } as R;
    }

    subscribe(listener: StoreListener) {
        this.listeners.push(listener);
        return this;
    }

    unsubscribe(listener: StoreListener) {
        const index = this.listeners.indexOf(listener);

        if (index < 0) return this;

        this.listeners.splice(index, 1);

        return this;
    }

    dispatch(eventData: any = {}) {
        if (isEqual(this.state, this.previousState)) return this;

        for (const listener of this.listeners) {
            listener(eventData);
        }

        return this;
    }
}

export function useStore<
    T extends StoreData = StoreData,
    S extends Store<T> = Store<T>
>(initialState: T, Implementation?: new (...args: any[]) => S) {
    const [storeReference, setStoreReference] =
        useState<StoreReference<T> | null>(null);
    useEffect(() => {
        const Type = Implementation ?? Store;
        const store = new Type<T>(initialState);
        const handleChange = (eventData?: any) => {
            setStoreReference(store.getReference(eventData));
        };
        store.subscribe(handleChange);
        handleChange();
        return () => {
            store.unsubscribe(handleChange);
        };
    }, [setStoreReference, initialState, Implementation]);

    return storeReference;
}

export function useStoreEffect<T extends StoreData = StoreData>(
    store: StoreReference<T>,
    callback: (store: StoreReference<T>) => void,
    additionalDependencies: any[] = []
) {
    const [dirty, setDirty] = useState(false);

    useEffect(() => {
        setDirty(true);
    }, [store, setDirty]);

    useEffect(() => {
        if (!dirty || !callback) {
            return;
        }
        setDirty(false);
        callback(store);
    }, [store, dirty, callback, setDirty, ...additionalDependencies]);
}
