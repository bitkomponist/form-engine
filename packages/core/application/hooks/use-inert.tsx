import { useEffect, useState } from 'react';
import 'wicg-inert';

export function useInert(
    ref: React.MutableRefObject<HTMLDivElement | HTMLFormElement | undefined>,
    initialValue = false
) {
    const [isInert, setIsInert] = useState(initialValue);

    useEffect(() => {
        if (ref.current) {
            if (isInert) {
                ref.current.setAttribute('inert', '');
            } else {
                ref.current.removeAttribute('inert');
            }
        }
        return () => {
            ref.current?.removeAttribute('inert');
        };
    }, [isInert]);

    return [isInert, setIsInert] as ReturnType<typeof useState<boolean>>;
}
