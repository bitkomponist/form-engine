import React, { createContext, useCallback, useContext, useMemo } from 'react';
import fetchElementControllerBackend from '../elements/fetch-backend';
import { ElementDefinition } from '../types/form';
import { useFormContext, useFormElement } from './use-form';
import { FormSessionContext, useFormSessionContext } from './use-formsession';

const FormEngineElementContext = createContext<string | undefined>(undefined);

export interface FormEngineElementContextProviderProps {
    id?: string;
    children: JSX.Element | ((id?: string) => JSX.Element);
}

export function FormEngineElementProvider({
    id,
    children,
}: FormEngineElementContextProviderProps) {
    if (!id) return null;

    return (
        <FormEngineElementContext.Provider value={id}>
            {typeof children === 'function' ? children(id) : children}
        </FormEngineElementContext.Provider>
    );
}

export function useFormEngineElementId() {
    const id = useContext(FormEngineElementContext);

    if (!id) throw new Error('tried to use form element without provider');

    return id;
}

/**
 * Gets localized element definition
 * when a directly matched locale is available, use it otherwise
 * split language tag into parts and check if at least the base language
 * is available. e.g. use 'en' for 'en-US' if 'en-US' is not present
 * @param element - source
 * @param locale - target locale
 * @returns localized element definition
 */
export function getLocalizedElementDefinition(
    element: ElementDefinition,
    locale: string
): ElementDefinition {
    let elementLocale: Partial<ElementDefinition> = {};

    if (element.$locales?.[locale]) {
        elementLocale = element.$locales[locale];
    } else if (locale.includes('-')) {
        const [fallbackLocale] = locale.split('-');
        if (element.$locales?.[fallbackLocale]) {
            elementLocale = element.$locales[fallbackLocale];
        }
    }

    return {
        ...element,
        ...elementLocale,
    };
}

export default function useFormEngineElementState(id?: string) {
    const element = useFormElement(id);
    const session = useContext(FormSessionContext);
    const localizedElement = useMemo(() => {
        const { locale = 'default' } = session?.state?.locals ?? {};

        if (
            !locale ||
            locale === 'default' ||
            !element?.state ||
            !element?.state?.$locales
        ) {
            return element;
        }

        return {
            ...element,
            state: {
                ...element.state,
                ...getLocalizedElementDefinition(element.state, locale),
            },
        };
    }, [element?.state, session?.state?.locals?.locale]);

    return localizedElement;
}

export function useFormEngineElement() {
    const id = useFormEngineElementId();
    return useFormEngineElementState(id);
}

export function useFormEngineElementController() {
    const { state: element } = useFormEngineElement();
    const { state: form } = useFormContext();
    const { state: session } = useFormSessionContext();

    if (!element || !form || !form?.id) {
        throw new Error(`tried to use element controller without provider`);
    }

    return useCallback(
        (data?: any) => {
            return fetchElementControllerBackend(
                form.id as string,
                element.id,
                session,
                data
            );
        },
        [element?.type, form?.id, session]
    );
}
