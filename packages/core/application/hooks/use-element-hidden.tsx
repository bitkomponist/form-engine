import { evalExpressions } from '@form-engine/core-util/expressions';
import { useMemo } from 'react';
import useFormEngineElementState, {
    useFormEngineElementId,
} from './use-element';
import { useFormSessionLocals } from './use-formsession';

export function useFormEngineElementHiddenState(id?: string) {
    const { state: element } = useFormEngineElementState(id);
    const { state: locals } = useFormSessionLocals();

    return useMemo(() => {
        if (!element || element.hidden || !element.hiddenExpressions?.length)
            return Boolean(element?.hidden);

        return Boolean(evalExpressions(element.hiddenExpressions, locals));
    }, [locals, element?.id, element?.hidden, element?.hiddenExpressions]);
}

export function useFormEngineElementHidden() {
    return useFormEngineElementHiddenState(useFormEngineElementId());
}
