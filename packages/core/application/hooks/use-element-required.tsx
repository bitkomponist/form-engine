import { evalExpressions } from '@form-engine/core-util/expressions';
import { useMemo } from 'react';
import useFormEngineElementState, {
    useFormEngineElementId,
} from './use-element';
import { useFormSessionLocals } from './use-formsession';

export function useFormEngineElementHiddenState(id?: string) {
    const { state: element } = useFormEngineElementState(id);
    const { state: locals } = useFormSessionLocals();

    return useMemo(() => {
        if (
            !element ||
            element.required ||
            !element.requiredExpressions?.length
        )
            return Boolean(element?.required);

        return Boolean(evalExpressions(element.requiredExpressions, locals));
    }, [locals, element?.id, element?.required, element?.requiredExpressions]);
}

export function useFormEngineElementHidden() {
    return useFormEngineElementHiddenState(useFormEngineElementId());
}
