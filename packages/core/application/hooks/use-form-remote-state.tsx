import React, { useEffect, useState } from 'react';
import { FormEngineRemote } from '../components/engine';

export default function useFormRemoteState(
    ref?: React.MutableRefObject<FormEngineRemote | undefined>
) {
    const [remoteState, setRemoteState] = useState<FormEngineRemote>();

    useEffect(() => {
        if (!ref || !ref.current) {
            setRemoteState(undefined);
            return;
        }
        let mounted = true;

        const listener = (state?: FormEngineRemote) => {
            if (!mounted) return;
            if (state) {
                setRemoteState({ ...state });
            } else {
                setRemoteState(undefined);
            }
        };

        ref?.current?.subscribe(listener);

        listener(ref?.current);

        return () => {
            mounted = false;
            setRemoteState(undefined);
            if (ref && ref.current) {
                ref.current.unsubscribe(listener);
            }
        };
    }, [ref?.current, setRemoteState]);

    return remoteState;
}
