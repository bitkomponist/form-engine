import React, { createContext, useContext } from 'react';
import { useCallback } from 'react';
import { useMemo } from 'react';
import {
    getLocalizedElementDefinition,
    useFormEngineElementId,
} from './use-element';
import {
    useFormContext,
    useFormElements,
    useFormEngineElementParentFormPage,
    useFormEngineElementStateChildren,
} from './use-form';
import { StoreReference, useStore } from './use-store';
import validate from '../validation';
import { useEffect } from 'react';
import { FormElementSessionState, FormSession } from '../types/form';
import {
    getFormSessionLocals,
    getFormSessionValues,
} from '../utilities/form-session';

import FormEngine from '../index';

/** @todo define 'FormEnginePageElement' as global constant */

export type FormSessionContextValue = StoreReference<FormSession> | null;

export const FormSessionContext = createContext<FormSessionContextValue>(null);

export interface FormSessionProviderProps {
    value: FormSession;
    children:
        | JSX.Element
        | ((store: StoreReference<FormSession>) => JSX.Element);
}

export function FormSessionI18nUpdater() {
    const { state: session } = useFormSessionContext();

    useEffect(() => {
        FormEngine.i18n.locale = session?.locals?.locale ?? 'default';
    }, [session?.locals?.locale]);

    return null;
}

export function FormSessionProvider({
    value: initialState,
    children,
}: FormSessionProviderProps) {
    const store = useStore(initialState);

    useFormSessionValidation(store);

    if (!store) return null;

    // expose current store value for debugging
    FormEngine.__currentSession = store?.state;

    return (
        <FormSessionContext.Provider value={store}>
            <FormSessionI18nUpdater />
            {typeof children === 'function' ? children(store) : children}
        </FormSessionContext.Provider>
    );
}

export function useFormSessionContext() {
    const context = useContext(FormSessionContext);

    if (!context) {
        throw new Error('tried to use form session without provider');
    }

    return context;
}

export function useCurrentPage(): [string, (pageName?: string) => void] {
    const {
        state: { elements = [] },
    } = useFormContext();

    const {
        state: { currentPage = '' },
        setItem,
    } = useFormSessionContext();

    const state = useMemo(() => {
        let value = currentPage;

        if (!currentPage) {
            const defaultPageElement = elements.find(
                ({ type, isDefault }) =>
                    type === 'FormEnginePageElement' && isDefault
            );
            value = defaultPageElement?.name || '';
        }

        return value;
    }, [currentPage, elements, setItem]);

    const setState = useCallback(
        (pageName?: string) => {
            setItem('currentPage', pageName);
        },
        [setItem]
    );

    return [state, setState];
}

export function FormElementSessionStateStruct(
    item: Partial<FormElementSessionState> = {}
) {
    return {
        value: '',
        isTouched: false,
        isHidden: false,
        error: null,
        ...item,
    };
}

export function useFormElementSessionState(
    id?: string
): [
    Partial<FormElementSessionState>,
    (value: Partial<FormElementSessionState>) => void
] {
    const { state: session = {}, setItem } = useFormSessionContext();

    const state = useMemo(() => {
        return FormElementSessionStateStruct(id ? session?.elements?.[id] : {});
    }, [id, id && session?.elements?.[id]]);

    const setter = useCallback(
        (value: Partial<FormElementSessionState>) => {
            if (!id) return;
            setItem('elements', {
                ...(session?.elements || {}),
                [id]: FormElementSessionStateStruct({
                    ...(session?.elements?.[id] || {}),
                    ...value,
                }),
            });
        },
        [setItem, id, session?.elements, id && session?.elements?.[id]]
    );

    return [state, setter];
}

export function useFormElementSession() {
    return useFormElementSessionState(useFormEngineElementId());
}

export function useParentFormPageSession() {
    const { id } = useFormEngineElementParentFormPage() || {};
    const children = useFormEngineElementStateChildren(id);
    const { state: session = {} } = useFormSessionContext();
    return useMemo(() => {
        const errors: { [id: string]: string } = {};
        let valid = true;
        const elements = Object.fromEntries(
            children.map(({ id }) => {
                const item = FormElementSessionStateStruct(
                    session?.elements?.[id]
                );
                if (item.error) {
                    errors[id] = item.error;
                    valid = false;
                }
                return [id, item];
            })
        );

        return { elements, valid, errors };
    }, [children, session]);
}

export function useFormSessionValidation(
    sessionStore: FormSessionContextValue
) {
    const { state: elements = [] } = useFormElements();
    const { state: session, setItem } = sessionStore || {};
    const { state: form } = useFormContext();
    useEffect(() => {
        if (!elements.length || !setItem) return;
        const elementsUpdate: { [id: string]: FormElementSessionState } = {};
        let needsUpdate = false;

        const validationSession = {
            ...(session ?? {}),
            locals: {
                ...(session?.locals ?? {}),
                values: {
                    ...(session?.locals?.values ?? {}),
                    ...getFormSessionValues(form, session ?? {}),
                },
            },
        };

        elements.forEach((element) => {
            const { id } = element;
            const item: FormElementSessionState = FormElementSessionStateStruct(
                validationSession?.elements?.[id]
            );
            const localizedElement = getLocalizedElementDefinition(
                element,
                validationSession?.locals?.locale ?? 'default'
            );
            const error = validate(
                localizedElement,
                item.value || '',
                validationSession
            );
            if (item.error !== error) {
                elementsUpdate[id] = { ...item, error };
                needsUpdate = true;
            }
        });

        if (needsUpdate) {
            setItem('elements', {
                ...(session?.elements || {}),
                ...elementsUpdate,
            });
        }
    }, [elements, session?.elements, session, setItem]);
}

export function useFormSessionDatasources() {
    const session = useFormSessionContext();

    if (!session) {
        throw new Error(`no form session available`);
    }

    const {
        state: { datasources = {} },
        setItem: setSessionItem,
    } = session;

    const set = useCallback(
        (value: any) => setSessionItem('datasources', value),
        [setSessionItem]
    );

    const setItem = useCallback(
        (key: string, value: unknown) => {
            if (!setSessionItem) return;
            set({
                ...(datasources || {}),
                [key]: value,
            });
        },
        [set, datasources]
    );

    return { state: datasources, setItem, set };
}

export function useFormSessionLocals() {
    const { state: session, setItem: setSessionItem } =
        useFormSessionContext() || { state: {}, setItem: null };
    const { state: form } = useFormContext();

    const state = useMemo(() => {
        return getFormSessionLocals(form, session);
    }, [session]);

    const setItem = useCallback(
        (key: string, value: unknown) => {
            if (!setSessionItem) return;
            setSessionItem('locals', {
                ...(session?.locals || {}),
                [key]: value,
            });
        },
        [setSessionItem, session?.locals]
    );

    return { state, setItem };
}

export function useFormSessionValues() {
    const { state: session } = useFormSessionContext();
    const { state: form } = useFormContext();
    return useMemo(() => {
        return getFormSessionValues(form, session);
    }, [session, form?.elements]);
}

export function useFormSessionErrors() {
    const { state, setItem } = useFormSessionContext();

    const set = useCallback(
        (errors?: { [id: string]: Error }) => setItem('errors', errors),
        [setItem]
    );

    return {
        state: state?.errors ?? {},
        clear: useCallback(() => {
            set(undefined);
        }, [setItem]),
        addError: useCallback(
            (id: string, error: Error) =>
                set({ ...(state?.errors ?? {}), [id]: error }),
            [set, state]
        ),
        set,
    };
}
