import { useCallback, useMemo, useState } from 'react';
import { useEffect } from 'react';
import { useFormEngineElement } from './use-element';
import { useFormContext, useFormDatasource } from './use-form';
import FormEngine from '../index';
import useLoadQueue from './use-loadqueue';
import {
    useFormSessionContext,
    useFormSessionDatasources,
    useFormSessionErrors,
    useFormSessionLocals,
} from './use-formsession';
import {
    DatasourceResponseOptions,
    ElementDatasourceConfiguration,
    ExpressionDefinition,
    FormDatasourceResult,
} from '../types/form';
import { evalExpressions } from '@form-engine/core-util/expressions';

export default function useDatasource(id?: string) {
    return useFormDatasource(id);
}

export function useInvalidateDatasourceData() {
    const { state: sessionDatasources, set: setSessionDatasources } =
        useFormSessionDatasources();

    return useCallback(
        (id?: string) => {
            if (id) {
                const copy = { ...sessionDatasources };
                delete copy[id];
                setSessionDatasources(copy);
            } else {
                setSessionDatasources({});
            }
        },
        [sessionDatasources, setSessionDatasources]
    );
}

export function useDatasourceData(id?: string) {
    const { state: datasource } = useDatasource(id);
    const { state: form } = useFormContext();
    const { state: session } = useFormSessionContext();
    const { state: errors, addError } = useFormSessionErrors();
    const { state: sessionDatasources, setItem: setSessionDatasourcesItem } =
        useFormSessionDatasources();
    const result =
        id && sessionDatasources[id]
            ? sessionDatasources[id]
            : ({} as FormDatasourceResult);
    const errorId = `datasources:${id}`;
    const currentError = errors[errorId];
    const loading = Boolean(id && !(id in sessionDatasources) && !currentError);

    useEffect(() => {
        if (currentError || !datasource || !id || sessionDatasources[id]) {
            return;
        }

        let mounted = true;

        const { type = '' } = datasource;

        if (!FormEngine?.datasources?.[type]) return;

        FormEngine.datasources[type]({ definition: datasource, session, form })
            .then((result) => {
                if (!mounted) return;
                setSessionDatasourcesItem(id, result);
            })
            .catch((e) => {
                if (!mounted) return;
                addError(errorId, e);
            });

        return () => {
            mounted = false;
        };
    }, [
        id,
        datasource,
        session,
        sessionDatasources,
        setSessionDatasourcesItem,
        addError,
        errorId,
        currentError,
    ]);

    return { error: null, options: [], data: [], ...(result || {}), loading };
}

export function useConfigDatasourceData(
    datasourceConfig?: ElementDatasourceConfiguration,
    expressions?: ExpressionDefinition[]
) {
    const isStatic = datasourceConfig?.mode === 'static';

    const datasourceResult = useDatasourceData(datasourceConfig?.datasourceId);

    const staticResult = useMemo(() => {
        let data: any = [];

        if (isStatic && Array.isArray(datasourceConfig?.staticOptions)) {
            data = [...datasourceConfig.staticOptions];
        }

        return {
            loading: false,
            error: null,
            data,
            options: [...data] as DatasourceResponseOptions,
        };
    }, [isStatic, datasourceConfig?.staticOptions]);

    const result = isStatic ? staticResult : datasourceResult;

    const { enqueue, dequeue } = useLoadQueue();

    useEffect(() => {
        if (!datasourceConfig?.datasourceId) return;
        if (result.loading) {
            enqueue(datasourceConfig.datasourceId);
        } else {
            dequeue(datasourceConfig.datasourceId);
        }
    }, [result.loading, enqueue, dequeue, datasourceConfig?.datasourceId]);

    const { state: locals } = useFormSessionLocals();

    const filteredResult = useMemo(() => {
        if (!expressions?.length) return result;

        const { data } = result;
        const isArrayResult = Array.isArray(data);

        return {
            ...result,
            options: result.options?.filter(($option, $index) => {
                const iterator = {
                    ...(isArrayResult ? data[$index] ?? {} : {}),
                    $option,
                    $index,
                };
                return evalExpressions(expressions, {
                    ...locals,
                    $i: iterator,
                });
            }),
        };
    }, [result, expressions, locals]);

    return filteredResult;
}

export function useFormElementDatasourceData() {
    const { datasource, datasourceExpressions } =
        useFormEngineElement().state ?? {};
    return useConfigDatasourceData(datasource, datasourceExpressions);
}
