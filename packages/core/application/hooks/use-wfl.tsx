import { useFormWorkflowLayers, useFormWorkflowLayer } from './use-form';

export function useWorkflowLayers() {
    return useFormWorkflowLayers();
}

export default function useWorkflowLayer(id?: string) {
    return useFormWorkflowLayer(id);
}
