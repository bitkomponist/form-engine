import { useCallback } from 'react';
import { Converter } from 'showdown';

const converter = new Converter({
    tables: true,
    simplifiedAutoLink: true,
    strikethrough: true,
    tasklists: true,
});

export function useMarkdownConverter() {
    return useCallback((message: string) => {
        return converter.makeHtml(message);
    }, []);
}

export default function useMarkdown() {
    return useCallback((source: string) => {
        return converter.makeHtml(source);
    }, []);
}
