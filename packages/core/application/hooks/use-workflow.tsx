import React, { useCallback, useMemo, useState } from 'react';
import {
    useFormSessionContext,
    useFormSessionErrors,
    useFormSessionValues,
} from './use-formsession';
import { useWorkflowLayers } from './use-wfl';
import FormEngine from '../index';
import { useFormContext } from './use-form';
import useLoadQueue from './use-loadqueue';
import { useEffect } from 'react';
import { createContext } from 'react';
import { StoreReference, useStore } from './use-store';
import { useContext } from 'react';
import {
    FormDefinition,
    FormSession,
    WorkflowLayerDefinition,
} from '../types/form';

export function getWflEventName(
    layerOrId: WorkflowLayerDefinition | string,
    eventName?: string
) {
    let id: string;
    if (typeof layerOrId === 'object') {
        if (!layerOrId?.id) {
            throw new Error('layer definition missing required id');
        }
        id = String(layerOrId.id);
    } else {
        id = String(layerOrId);
    }

    if (!eventName) return id;

    return `${id}@${eventName}`;
}
export interface WorkflowState {
    current?: Promise<any>;
    currentEvent?: string;
    initialized?: boolean; // starting initial async work
    submitted?: boolean; // form completed
    ready?: boolean; // stopped initial async work
}

export type WorkflowContextValue = StoreReference<WorkflowState> | null;

export const WorkflowContext = createContext<WorkflowContextValue>(null);

interface WorkflowContextProviderProps {
    value?: WorkflowState;
    children:
        | React.ReactNode
        | ((store: StoreReference<WorkflowState>) => JSX.Element);
}

export function WorkflowProvider({
    value,
    children,
}: WorkflowContextProviderProps) {
    const [initialState] = useState(value || {});
    const store = useStore(initialState);
    const { state: session, set: setSession } = useFormSessionContext();
    const { addError } = useFormSessionErrors();
    const [loading, setLoading] = useState(false);
    const { enqueue, dequeue, queue } = useLoadQueue();

    useEffect(() => {
        if (!store?.state?.current) return;

        let mounted = true;
        setLoading(true);

        store?.state?.current
            .then((sessionUpdate) => {
                if (!mounted) {
                    console?.warn?.('workflow aborted');
                    return;
                }
                const {
                    state: { currentEvent },
                    setItem,
                } = store;
                if (currentEvent === getWflEventName('form:init', 'success')) {
                    setItem('initialized', true);
                }
                if (currentEvent === getWflEventName('form:ready', 'success')) {
                    setItem('ready', true);
                }
                if (
                    currentEvent === getWflEventName('form:submit', 'success')
                ) {
                    setItem('submitted', true);
                }
                setItem('current', null);
                setItem('currentEvent', null);
                setSession(sessionUpdate);
                setLoading(false);
            })
            .catch((e) => {
                console?.error?.(e);
                if (!mounted) return;
                setLoading(false);
                addError(
                    `workflow:${store?.state?.currentEvent ?? 'global'}`,
                    e
                );
            });

        return () => {
            mounted = false;
        };
    }, [
        store,
        store?.state?.current,
        store?.state?.currentEvent,
        store?.setItem,
        setSession,
        setLoading,
        // cant use addError here, because addError triggers unmount for its session dependency, we purposely throw away session updates that occured during the workflow
    ]);

    useEffect(() => {
        if (loading && !queue.includes('workflow')) {
            enqueue('workflow');
        } else if (!loading && queue.includes('workflow')) {
            dequeue('workflow');
        }
    }, [loading, enqueue, dequeue, queue]);

    if (!store || !session) return null;
    return (
        <WorkflowContext.Provider value={store}>
            {typeof children === 'function' ? children(store) : children}
        </WorkflowContext.Provider>
    );
}

function executeLayer(
    form: FormDefinition,
    layerDefinition: WorkflowLayerDefinition,
    intermediateSession: Partial<FormSession>,
    data?: any
): Promise<Partial<FormSession>> {
    const { type } = layerDefinition;
    const LayerType = FormEngine?.workflowLayers?.[type];
    if (!LayerType)
        return Promise.reject(new Error(`unknown WFL type ${type}`));
    let result;
    try {
        result = LayerType({
            session: intermediateSession,
            form,
            definition: layerDefinition,
            data,
        });
    } catch (e) {
        return Promise.reject(e);
    }

    if (!(result instanceof Promise)) {
        result = Promise.resolve(result);
    }

    return result;
}

function emitWorkflowEvent(
    form: FormDefinition,
    formWfls: WorkflowLayerDefinition[],
    intermediateSession: Partial<FormSession>,
    eventName: string,
    eventData?: any
): Promise<Partial<FormSession>> {
    const targets = (formWfls || []).filter(({ events, enabled }) => {
        return enabled && Array.isArray(events) && events.includes(eventName);
    });

    if (!targets.length) return Promise.resolve({ ...intermediateSession });

    async function emitTargets() {
        let sessionUpdate: Partial<FormSession> = { ...intermediateSession };
        for (const target of targets) {
            let result;
            let error;
            try {
                result = await executeLayer(
                    form,
                    target,
                    sessionUpdate,
                    eventData
                );
            } catch (e) {
                error = e;
            }

            if (error) {
                if (target.id) {
                    sessionUpdate = await emitWorkflowEvent(
                        form,
                        formWfls,
                        sessionUpdate,
                        getWflEventName(target, 'error'),
                        error
                    );
                }
            } else {
                if (result) {
                    sessionUpdate = {
                        ...sessionUpdate,
                        ...result,
                        locals: {
                            ...(sessionUpdate?.locals || {}),
                            ...(result?.locals || {}),
                        },
                    };
                }
                if (target.id) {
                    sessionUpdate = await emitWorkflowEvent(
                        form,
                        formWfls,
                        sessionUpdate,
                        getWflEventName(target, 'success')
                    );
                }
            }
        }

        return sessionUpdate;
    }

    return emitTargets();
}

export default function useWorkflow(disabled = false) {
    const context = useContext(WorkflowContext);

    if (!context) {
        throw new Error('tried to use workflow without provider');
    }

    const {
        state: workflow,
        setItem: setWorkflowItem,
        set: setWorkflow,
        clearAll: clearWorkflow,
    } = context;
    const { current, initialized, submitted, ready: isReady } = workflow;
    const { state: session } = useFormSessionContext();
    const values = useFormSessionValues();
    const wflIntermediateSession = useMemo(() => {
        return {
            ...session,
            locals: {
                ...(session?.locals || {}),
                values: {
                    ...(values || {}),
                    ...(session?.locals?.values || {}),
                },
            },
        } as FormSession;
    }, [session, values]);
    const { state: form } = useFormContext();
    const { state: formWfls } = useWorkflowLayers();
    const { queue: loadQueue = [] } = useLoadQueue();

    const emit = useCallback(
        (
            eventName: string,
            eventData?: any,
            intermediateSession: Partial<FormSession> = wflIntermediateSession
        ) => {
            if (current) {
                throw new Error(
                    `could not emit ${eventName}, workflow already processing`
                );
            }
            if (disabled) {
                return Promise.resolve({ ...intermediateSession });
            }

            const promise = emitWorkflowEvent(
                form,
                formWfls,
                intermediateSession,
                eventName,
                eventData
            );

            setWorkflowItem('currentEvent', eventName);
            setWorkflowItem('current', promise);

            return promise;
        },
        [
            disabled,
            formWfls,
            form,
            wflIntermediateSession,
            setWorkflowItem,
            current,
            values,
        ]
    );

    const submit = useCallback(
        (intermediateSession?: Partial<FormSession>) =>
            emit(
                getWflEventName('form:submit', 'success'),
                null,
                intermediateSession
            ),
        [emit]
    );

    const init = useCallback(
        (intermediateSession?: Partial<FormSession>) =>
            emit(
                getWflEventName('form:init', 'success'),
                null,
                intermediateSession
            ),
        [emit]
    );

    const ready = useCallback(
        (intermediateSession?: Partial<FormSession>) =>
            emit(
                getWflEventName('form:ready', 'success'),
                null,
                intermediateSession
            ),
        [emit]
    );

    const loading =
        loadQueue.includes('workflow') || Boolean(workflow?.current);

    const isSubmitting =
        workflow?.currentEvent === getWflEventName('form:submit', 'success') &&
        loading;
    const isInitializing =
        workflow?.currentEvent === getWflEventName('form:init', 'success') &&
        loading;

    return {
        state: workflow,
        set: setWorkflow,
        setItem: setWorkflowItem,
        clearAll: clearWorkflow,
        submitted,
        initialized,
        loading,
        emit,
        submit,
        init,
        isReady: Boolean(isReady),
        ready,
        isSubmitting,
        isInitializing,
    };
}

export type WorkflowController = ReturnType<typeof useWorkflow>;
