import { getMemoryStorageNamespace } from '@form-engine/core-util/memory-storage';
import { compile } from '@form-engine/core-util/template';
import React, {
    createContext,
    useContext,
    useCallback,
    useMemo,
    useEffect,
} from 'react';
import {
    ConstantDefinition,
    DatasourceDefinition,
    ElementDefinition,
    FormCollectionItem,
    FormDefinition,
    FormDefinitionCollections,
    WorkflowLayerDefinition,
} from '../types/form';
import { useFormEngineElementId } from './use-element';
import { StoreReference, useStore } from './use-store';

/** @todo define 'FormEnginePageElement' as global constant */

export const FormOfflineStorage = getMemoryStorageNamespace('form');

export type FormContextValue = StoreReference<FormDefinition> | null;

export const FormContext = createContext<FormContextValue>(null);

interface FormContextProviderProps {
    value: FormDefinition;
    children:
        | JSX.Element
        | ((store: StoreReference<FormDefinition>) => JSX.Element);
}

export interface FormStoreReferenceProviderProps {
    value?: StoreReference<FormDefinition> | null;
    children:
        | JSX.Element
        | ((store: StoreReference<FormDefinition>) => JSX.Element);
}

export function FormStoreReferenceProvider({
    value,
    children,
}: FormStoreReferenceProviderProps) {
    if (!value) return null;
    return (
        <FormContext.Provider value={value}>
            {typeof children === 'function' ? children(value) : children}
        </FormContext.Provider>
    );
}

export function FormProvider({
    value: initialState,
    children,
}: FormContextProviderProps) {
    const store = useStore<FormDefinition>(initialState);
    return (
        <FormStoreReferenceProvider value={store}>
            {children}
        </FormStoreReferenceProvider>
    );
}

export function useFormContext() {
    const value = useContext(FormContext);

    if (!value) {
        throw new Error('tried to use form context without provider');
    }

    return value;
}

export function useFormCollection<
    T extends FormCollectionItem = FormCollectionItem
>(propName: keyof FormDefinitionCollections) {
    const { state, setItem } = useFormContext();
    const collection: T[] = (state?.[propName] || []) as unknown as T[];

    const setCollectionItem = useCallback(
        (index: number, value: T) => {
            if (index < 0) return;
            const copy = [...collection];
            copy[index] = value;
            setItem(propName, copy);
        },
        [collection, setItem, propName]
    );

    const unsetCollectionItem = useCallback(
        (index: number) => {
            if (index < 0 || !collection.length) return;
            const omit = [index];
            const copy = [];
            for (let i = 0; i < collection.length; ++i) {
                if (!omit.includes(i)) {
                    copy.push(collection[i]);
                }
            }
            setItem(propName, copy);
        },
        [propName, collection, setItem]
    );

    const setCollection = useCallback(
        (value: T[]) => {
            setItem(propName, value);
        },
        [setItem, propName]
    );

    return {
        state: collection,
        set: setCollection,
        setItem: setCollectionItem,
        unsetItem: unsetCollectionItem,
    };
}

export function useFormCollectionItem<
    T extends FormCollectionItem = FormCollectionItem
>(propName: keyof FormDefinitionCollections, id?: string) {
    const { state, setItem } = useFormContext();
    const collection: T[] = (state?.[propName] || []) as unknown as T[];

    const index = useMemo(
        () =>
            id && collection.length
                ? collection.findIndex((element) => element.id === id)
                : -1,
        [collection, id]
    );

    const traverse = useCallback(
        (fn: (child: T, index: number) => void) => {
            if (!id || !collection.length) return;
            const visited: string[] = [];
            function t(parent: string) {
                for (let i = 0; i < collection.length; ++i) {
                    if (
                        !visited.includes(collection[i].id) &&
                        collection[i].parent === parent
                    ) {
                        visited.push(collection[i].id);
                        fn(collection[i], i);
                        t(collection[i].id);
                    }
                }
            }
            t(id);
        },
        [collection, id]
    );

    const unsetCollectionItem = useCallback(() => {
        if (index < 0 || !collection.length) return;
        const omit = [index];
        traverse((_child, childIndex) => {
            omit.push(childIndex);
        });
        const copy = [];
        for (let i = 0; i < collection.length; ++i) {
            if (!omit.includes(i)) {
                copy.push(collection[i]);
            }
        }
        setItem(propName, copy);
    }, [id, index, propName, collection, setItem, traverse]);

    const setCollectionItem = useCallback(
        (value: Partial<T> | null) => {
            if (index < 0 || !collection.length) return;

            if (!value) {
                unsetCollectionItem();
            } else {
                const copy = [...collection];
                copy[index] = value as T;
                setItem(propName, copy);
            }
        },
        [collection, index, setItem, propName, unsetCollectionItem]
    );

    const setCollectionItemProperty = useCallback(
        (itemPropertyName: keyof T, value: any) => {
            if (index < 0 || !collection.length || !collection[index]) return;

            if (value === undefined) {
                const itemUpdate = { ...collection[index] };
                delete itemUpdate[itemPropertyName];
                setCollectionItem(itemUpdate);
            } else if (collection[index][itemPropertyName] !== value) {
                const itemUpdate = {
                    ...collection[index],
                    [itemPropertyName]: value,
                };
                setCollectionItem(itemUpdate);
            }
        },
        [collection, index, setCollectionItem, propName, unsetCollectionItem]
    );

    return {
        index,
        state: index > -1 ? collection[index] : undefined,
        setItem: setCollectionItem,
        unsetItem: unsetCollectionItem,
        setItemProperty: setCollectionItemProperty,
        traverse,
    };
}

export function useFormElements() {
    return useFormCollection<ElementDefinition>('elements');
}
export function useFormElement(id?: string) {
    return useFormCollectionItem<ElementDefinition>('elements', id);
}
export function useFormDatasources() {
    return useFormCollection<DatasourceDefinition>('datasources');
}
export function useFormDatasource(id?: string) {
    return useFormCollectionItem<DatasourceDefinition>('datasources', id);
}
export function useFormWorkflowLayers() {
    return useFormCollection<WorkflowLayerDefinition>('workflowLayers');
}
export function useFormWorkflowLayer(id?: string) {
    return useFormCollectionItem<WorkflowLayerDefinition>('workflowLayers', id);
}
export function useFormConstants() {
    const { state = [], ...api } =
        useFormCollection<ConstantDefinition>('constants');

    const constants = useMemo(() => {
        const result: any = {};

        state.forEach((def) => {
            const key = (def.id || '').trim();
            if (!key) return;
            result[key] = compile(def.value || '', result, '');
        });

        return result;
    }, [JSON.stringify(state)]);

    return {
        ...api,
        constants,
        state,
    };
}

export function useFormEngineElementStateParentFormPage(id?: string) {
    const { state: elements = [] } = useFormElements();

    return useMemo(() => {
        if (!id) return null;

        function traverse(targetId: string): ElementDefinition | null {
            for (const element of elements) {
                if (element.id === targetId) {
                    if (element.type === 'FormEnginePageElement') {
                        return element;
                    } else if (element.parent) {
                        return traverse(element.parent);
                    } else {
                        return null;
                    }
                }
            }

            return null;
        }

        return traverse(id);
    }, [id, elements]);
}

export function useFormEngineElementParentFormPage() {
    return useFormEngineElementStateParentFormPage(useFormEngineElementId());
}

export function useFormEngineElementStateChildren(id?: string, deep = true) {
    const { state: elements = [] } = useFormElements();

    return useMemo(() => {
        if (!id) return [];

        function traverse(parentId: string, result: ElementDefinition[] = []) {
            for (const element of elements) {
                if (element.parent === parentId) {
                    result.push(element);
                    if (deep) traverse(element.id, result);
                }
            }
            return result;
        }

        return traverse(id);
    }, [id, elements, deep]);
}

export function useFormEngineElementChildren(deep = true) {
    return useFormEngineElementStateChildren(useFormEngineElementId(), deep);
}
