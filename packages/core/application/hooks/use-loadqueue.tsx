import React, { createContext, useContext, useState } from 'react';
import { useCallback } from 'react';
import { useMemo } from 'react';
import { StoreReference, useStore } from './use-store';

export interface LoadQueueDefinition {
    queue?: string[];
}

export type LoadQueueContextValue = StoreReference<LoadQueueDefinition> | null;

const LoadQueueContext = createContext<LoadQueueContextValue>(null);

function useLoadQueueWithContext(store: LoadQueueContextValue) {
    const { state, setItem } = store || {};

    const { queue = [] } = state || {};

    const loading = useMemo(() => Boolean(queue.length > 0), [queue]);
    const enqueue = useCallback(
        (identifier: string) => {
            if (!setItem || queue.includes(identifier)) return;
            setItem('queue', [...queue, identifier]);
        },
        [queue, setItem]
    );
    const dequeue = useCallback(
        (identifier: string) => {
            if (!setItem) return;
            const index = queue.indexOf(identifier);
            if (index < 0) return;
            const copy = [...queue];
            copy.splice(index, 1);
            setItem('queue', copy);
        },
        [queue, setItem]
    );
    return {
        queue,
        loading,
        enqueue,
        dequeue,
    };
}

export interface LoadQueueProviderProps {
    value?: LoadQueueDefinition;
    children:
        | JSX.Element
        | ((api: ReturnType<typeof useLoadQueueWithContext>) => JSX.Element);
}

export function LoadQueueProvider({ value, children }: LoadQueueProviderProps) {
    const [initialValue] = useState(value ?? { queue: [] });
    const store = useStore(initialValue);
    const api = useLoadQueueWithContext(store);
    if (!store) return null;
    return (
        <LoadQueueContext.Provider value={store}>
            {typeof children === 'function' ? children(api) : children}
        </LoadQueueContext.Provider>
    );
}

export default function useLoadQueue() {
    return useLoadQueueWithContext(useContext(LoadQueueContext));
}
