import { PropertyPath } from 'lodash';
import { Store, StoreData, StoreReference } from '../hooks/use-store';
import get from 'lodash.get';
import { compile } from '@form-engine/core-util/template';

export interface I18nState extends StoreData {
    defaultLocale: string;
    locale: string;
    data: { [locale: string]: { [key: string]: string } };
}

export interface I18nReference<T extends StoreData = I18nState>
    extends StoreReference<T> {
    t: (path: PropertyPath) => string;
    c: (path: PropertyPath, locals?: any) => string;
    fallback: (
        source: string | undefined,
        fallbackPath: PropertyPath,
        locals?: any
    ) => string;
    Node: (props: { id: PropertyPath; [local: string]: any }) => string;
    prefix: (
        prefix: string
    ) => Pick<I18nReference<T>, 't' | 'c' | 'fallback' | 'prefix' | 'Node'>;
}

export class I18n extends Store<I18nState, I18nReference> {
    constructor(initialState?: Partial<I18nState>) {
        super({
            defaultLocale: 'default',
            locale: 'default',
            ...(initialState ?? {}),
            data: {
                ...(initialState?.data ?? {}),
            },
        });
    }

    get locale() {
        return this.state?.locale;
    }

    set locale(locale: string) {
        this.setItem('locale', locale);
    }

    get defaultLocale() {
        return this.state?.defaultLocale;
    }

    set defaultLocale(locale: string) {
        this.setItem('defaultLocale', locale);
    }

    getReference(eventData: any = {}): I18nReference<I18nState> {
        const {
            state,
            previousState,
            set,
            setItem,
            unsetItem,
            clearAll,
            t,
            c,
            fallback,
            prefix,
            Node,
        } = this;
        return {
            state,
            previousState,
            set,
            setItem,
            unsetItem,
            clearAll,
            eventData,
            t,
            c,
            fallback,
            prefix,
            Node,
        };
    }

    t = (path: PropertyPath) => {
        const { locale, defaultLocale, data } = this.state;
        let result = get(data?.[locale] ?? {}, path);

        if (result !== undefined) return result;

        if (locale.includes('-')) {
            const [fallbackLocale] = locale.split('-');
            result = get(data?.[fallbackLocale] ?? {}, path);
            if (result !== undefined) return result;
        }

        result = get(data?.[defaultLocale] ?? {}, path);
        if (result !== undefined) return result;

        return '';
    };

    c = (path: PropertyPath, locals?: any) => {
        const result = this.t(path);
        if (!locals) return result;
        return compile(result, locals);
    };

    fallback = (
        source: string | undefined,
        fallbackPath: PropertyPath,
        locals?: any
    ) => {
        const sourceString = String(source ?? '').trim();
        const result = sourceString ? sourceString : this.t(fallbackPath);
        if (!locals) return result;
        return compile(result, locals);
    };

    Node = ({
        id: path,
        ...locals
    }: {
        id: PropertyPath;
        [local: string]: any;
    }) => {
        return this.c(path, locals);
    };

    prefix = (prefix: string) => {
        function withPrefix(path: PropertyPath): PropertyPath {
            if (Array.isArray(path)) return [prefix, ...path];
            return `${prefix}${String(path)}`;
        }
        return {
            t: (...params: Parameters<(typeof I18n.prototype)['t']>) =>
                this.t(withPrefix(params[0])),
            c: (...params: Parameters<(typeof I18n.prototype)['c']>) =>
                this.c(withPrefix(params.shift()), ...(params as any[])),
            fallback: (
                ...params: Parameters<(typeof I18n.prototype)['fallback']>
            ) =>
                this.fallback(withPrefix(params.shift()), ...(params as any[])),
            Node: (props: Parameters<(typeof I18n.prototype)['Node']>[0]) =>
                this.Node({ ...(props ?? {}), id: withPrefix(props.id) }),
            prefix: (prefix: PropertyPath) =>
                this.prefix(withPrefix(prefix) as string),
        };
    };
}
