import { FormDefinition, FormSession } from '../types/form';
import { compile } from '@form-engine/core-util/template';

export function getFormConstants(form: FormDefinition) {
    const result: any = {};

    ((form.constants as any[]) || []).forEach((def) => {
        const key = String(def.id || '').trim();
        if (!key) return;
        result[key] = compile(String(def.value || ''), result, '');
    });

    return result;
}

export function getFormSessionValues(
    form: FormDefinition,
    session: FormSession
) {
    const { elements = [] } = form;
    const values: { [name: string]: string } = {};
    Object.entries(session?.elements || {}).forEach(([id, { value = '' }]) => {
        const element = elements.find((el) => el.id === id);
        if (!element || !element.name) return;
        values[element.name] = value;
    });
    return values;
}

export function getFormSessionLocals(
    form: FormDefinition,
    session: FormSession
) {
    const constants = getFormConstants(form);
    const { locals, request, datasources } = session;
    const values = getFormSessionValues(form, session);
    const datasourceConstants: any = {};
    for (const datasourceResult of Object.values(datasources || {})) {
        if (datasourceResult.constants) {
            Object.assign(datasourceConstants, datasourceResult.constants);
        }
    }
    return {
        ...(locals || {}),
        ...(constants || {}),
        ...datasourceConstants,
        values,
        request: request || {},
    };
}
