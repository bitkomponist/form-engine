import React, { Fragment, useMemo, useRef, useState } from 'react';
import { Form } from 'react-bootstrap';
import {
    FormProvider,
    useFormContext,
    useFormDatasources,
} from '../hooks/use-form';
import FormEngineElement from './element';
import FormEngineContainer from './container';
import {
    FormSessionProvider,
    useFormSessionContext,
} from '../hooks/use-formsession';
import useWorkflow, {
    WorkflowController,
    WorkflowProvider,
    WorkflowState,
} from '../hooks/use-workflow';
import { useEffect } from 'react';
import { useCallback } from 'react';

import ShadowPortal from './shadow-portal';
import useFormResources from '../hooks/use-form-resources';
import { ElementTypeProps, FormDefinition, FormSession } from '../types/form';
import { StoreReference } from '../hooks/use-store';
import { useDatasourceData } from '../hooks/use-datasource';
import FormEngineConfig from '..';
import FormEngineErrorAlert from './error-alert';
import useLoadQueue from '../hooks/use-loadqueue';
import FormEngineSpinnerBoundary from './engine-spinner-boundary';
import { useInert } from '../hooks/use-inert';

interface FormElementProps extends React.HTMLAttributes<HTMLFormElement> {
    [prop: string]: any;
}

export type FormEngineRemoteListener = (formRemote?: FormEngineRemote) => void;
export interface FormEngineRemote {
    form: StoreReference<FormDefinition>;
    session: StoreReference<FormSession>;
    workflow: WorkflowController;
    reset: () => void;
    $listeners: FormEngineRemoteListener[];
    subscribe: (listener: FormEngineRemoteListener) => void;
    unsubscribe: (listener: FormEngineRemoteListener) => void;
    emit: (remote?: FormEngineRemote) => void;
}

export function FormEngineGlobalDatasource({
    id,
    onComplete,
}: {
    id?: string;
    onComplete?: () => void;
}) {
    const { loading } = useDatasourceData(id);
    const { enqueue, dequeue, queue } = useLoadQueue();
    const [loaded, setLoaded] = useState(false);

    useEffect(() => {
        if (!id) return;
        if (loading) {
            enqueue(id);
        } else {
            dequeue(id);
        }
    }, [loading, id, queue]);

    useEffect(() => {
        if (!loading && !loaded) {
            setLoaded(true);
            onComplete?.();
        }
    }, [loading, loaded, setLoaded, onComplete]);

    return null;
}

export function FormEngineGlobalDatasourceLoader() {
    const { state: datasources } = useFormDatasources();
    const { ready: emitReady, initialized } = useWorkflow();
    // track datasources that have been loaded
    const [loaded, setLoaded] = useState<string[]>([]);
    // track if ready was emitted
    const [isReady, setIsReady] = useState(false);
    // force to remount loaders when form reset function is used
    const [forceRenderKey, setForceRenderKey] = useState(0);

    const globalDatasources = useMemo(
        () =>
            datasources.filter(
                (def) => FormEngineConfig.datasources[def.type]?.isGlobal
            ),
        [datasources]
    );

    useEffect(() => {
        // reset load process when form is reinitialized (form reset function was used)
        if (initialized) return;
        setIsReady(false);
        setForceRenderKey((c) => c + 1);
        setLoaded([]);
    }, [initialized, setIsReady, setForceRenderKey]);

    useEffect(() => {
        // if not already emitted, emit ready workflow when all global datasources are loaded
        if (
            initialized &&
            !isReady &&
            globalDatasources.length === loaded.length
        ) {
            setIsReady(true);
            emitReady();
        }
    }, [
        globalDatasources,
        loaded,
        isReady,
        setIsReady,
        emitReady,
        initialized,
    ]);

    return (
        <>
            {globalDatasources.map(({ id }) => (
                <FormEngineGlobalDatasource
                    onComplete={() => setLoaded([...loaded, id])}
                    key={`${forceRenderKey}:${id}`}
                    id={id}
                />
            ))}
        </>
    );
}

export interface FormEngineRendererProps
    extends Omit<React.HTMLAttributes<HTMLElement>, 'onChange'> {
    elementComponent?: React.FunctionComponent<ElementTypeProps>;
    containerComponent?: React.FunctionComponent<ElementTypeProps>;
    formComponent?: React.FunctionComponent<FormElementProps>;
    children?: JSX.Element;
    editMode?: boolean | string;
    remote?: React.MutableRefObject<FormEngineRemote | undefined>;
    onReset?: () => void;
}

function FormEngineRenderer({
    elementComponent: ElementComponent = FormEngineElement,
    containerComponent = FormEngineContainer,
    formComponent: FormComponent = Form,
    children,
    remote,
    onSubmit,
    onReset,
    ...props
}: FormEngineRendererProps) {
    const customResources = props.editMode !== 'edit';
    const editable =
        props.editMode === 'edit' || props.editMode === 'translate';
    delete props.editMode;
    const formContext = useFormContext();
    const sessionContext = useFormSessionContext();
    const workflowContext = useWorkflow(editable);
    const { state } = formContext;
    const {
        submit: emitSubmit,
        init: emitInit,
        initialized,
        loading,
        isSubmitting,
        isInitializing,
        isReady,
    } = workflowContext;

    const ref = useRef<HTMLFormElement>();

    const [_inert, setIsInert] = useInert(ref, loading);

    useEffect(() => {
        setIsInert(loading);
    }, [loading, setIsInert]);

    useEffect(() => {
        if (editable) return;
        if (!loading && !initialized) emitInit();
    }, [editable, loading, initialized]);

    const handleSubmit = useCallback(
        (e: React.FormEvent<HTMLElement>) => {
            e.preventDefault();
            if (editable || isSubmitting) return;
            onSubmit?.(e);
            emitSubmit();
        },
        [emitSubmit, onSubmit, editable, isSubmitting]
    );

    const reset = useCallback(() => {
        workflowContext.clearAll();
        emitInit(sessionContext.clearAll());
        onReset?.();
    }, [sessionContext, workflowContext, emitInit, onReset]);

    useEffect(() => {
        if (typeof remote !== 'object') return;

        // inherit listeners of previous forms of the same remote ref
        const $listeners: FormEngineRemoteListener[] = [
            ...(remote?.current?.$listeners ?? []),
        ];
        remote.current = {
            $listeners,
            subscribe: (listener: FormEngineRemoteListener) => {
                if (!$listeners.includes(listener)) {
                    $listeners.push(listener);
                }
            },
            unsubscribe: (listener: FormEngineRemoteListener) => {
                const index = $listeners.indexOf(listener);
                if (index > -1) {
                    $listeners.splice(index, 1);
                }
            },
            emit: (value?: FormEngineRemote) => {
                $listeners.forEach((l) => l(value));
            },
            form: formContext,
            session: sessionContext,
            workflow: workflowContext,
            reset,
        };

        return () => {
            remote?.current?.emit(undefined);
        };
    }, [remote]);

    useEffect(() => {
        if (typeof remote !== 'object' || !remote.current) return;

        Object.assign(remote.current, {
            form: formContext,
            session: sessionContext,
            workflow: workflowContext,
            reset,
        });

        remote.current.emit(remote?.current);
    }, [remote?.current, formContext, sessionContext, workflowContext, reset]);

    const resources = useFormResources();

    const Wrap = customResources ? ShadowPortal : Fragment;

    const errors = Object.entries(sessionContext?.state?.errors ?? {});

    const showSpinner =
        !editable && (isSubmitting || isInitializing || !isReady);

    return (
        <Wrap>
            {customResources && resources}
            {customResources && (
                <style
                    type="text/css"
                    dangerouslySetInnerHTML={{
                        __html: state?.stylesheet || '',
                    }}
                />
            )}

            <FormEngineGlobalDatasourceLoader />
            {errors.map(([id, error]) => {
                return <FormEngineErrorAlert key={id} error={error} />;
            })}
            <FormComponent
                {...props}
                className={`fe ${editable ? 'feb' : ''} ${
                    initialized || editable ? 'is-initialized' : ''
                } ${loading ? 'is-loading' : ''} ${props.className || ''}`}
                onSubmit={handleSubmit}
                ref={ref}
            >
                <FormEngineSpinnerBoundary bar={false} show={showSpinner} />
                <div
                    className="fe-elements"
                    style={{ display: errors.length ? 'none' : undefined }}
                >
                    {(state?.elements || [])
                        .filter((element) => !element.parent)
                        .map((element) => (
                            <ElementComponent
                                key={element.id}
                                id={element.id}
                                elementComponent={ElementComponent}
                                containerComponent={containerComponent}
                            />
                        ))}
                    {children}
                </div>
            </FormComponent>
        </Wrap>
    );
}

export interface FormEngineProps extends FormEngineRendererProps {
    value?: FormSession;
    workflow?: WorkflowState;
}

export default function FormEngine({
    value: session,
    workflow,
    ...props
}: FormEngineProps) {
    const [initialSession] = useState<FormSession>(session || {});
    return (
        <FormSessionProvider value={initialSession}>
            <WorkflowProvider value={workflow}>
                <FormEngineRenderer {...props} />
            </WorkflowProvider>
        </FormSessionProvider>
    );
}

export interface FormEngineStateProviderProps {
    form: FormDefinition;
    session: FormSession;
    workflow: WorkflowState;
    children: React.ReactNode;
}

export function FormEngineStateProvider({
    form: formValue,
    session: initialSession,
    workflow: initialWorkflow,
    children,
}: FormEngineStateProviderProps) {
    return (
        <FormProvider value={formValue}>
            <FormSessionProvider value={initialSession}>
                <WorkflowProvider value={initialWorkflow}>
                    {children}
                </WorkflowProvider>
            </FormSessionProvider>
        </FormProvider>
    );
}
