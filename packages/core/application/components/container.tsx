import React from 'react';
import { ElementTypeProps } from '../types/form';
import FormEngineElement from './element';

export interface FormEngineContainerProps extends ElementTypeProps {
    childElementIds?: string[];
}

export default function FormEngineContainer({
    elementComponent: ElementComponent = FormEngineElement,
    containerComponent,
    childElementIds = [],
    prepend,
    append,
    as: Component = 'div',
    id,
    ...props
}: FormEngineContainerProps): JSX.Element | null {
    return (
        <Component
            {...props}
            className={`fe-container ${props.className || ''}`}
        >
            {prepend}
            {childElementIds.map((childId) => (
                <ElementComponent
                    key={childId}
                    id={childId}
                    parent={id}
                    containerComponent={containerComponent}
                    elementComponent={ElementComponent}
                />
            ))}
            {append}
        </Component>
    );
}
