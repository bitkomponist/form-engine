import React from 'react';
import FormEngineAlert from './alert';
import { CloudLightningRainFill } from 'react-bootstrap-icons';

export default function FormEngineErrorAlert({ error }: { error: Error }) {
    return (
        <FormEngineAlert
            variant="danger-outline"
            title={error?.message}
            aside={<CloudLightningRainFill />}
        >
            {process.env.NODE_ENV === 'development' && (
                <pre>{error?.stack}</pre>
            )}
        </FormEngineAlert>
    );
}
