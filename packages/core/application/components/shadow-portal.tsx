import React from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import { useRef } from 'react';
import { createPortal } from 'react-dom';

class FebShadowElement extends HTMLDivElement {}

customElements.define('fe-shadow', FebShadowElement, { extends: 'div' });

export interface ShadowPortalProps extends React.HTMLAttributes<HTMLElement> {
    as?: React.ElementType;
    children?: React.ReactNode;
    resetStyling?: boolean;
}

export default function ShadowPortal({
    as: Component = 'div',
    children,
    resetStyling = false,
    ...props
}: ShadowPortalProps): JSX.Element {
    const root = useRef<Element>();
    const [shadowRoot, setShadowRoot] = useState<ShadowRoot | null>(null);

    useEffect(() => {
        const { current: domRoot } = root;

        if (!domRoot) return;

        const shadowElement = document.createElement('feb-shadow');
        shadowElement.attachShadow({ mode: 'open' });

        domRoot.appendChild(shadowElement);

        setShadowRoot(shadowElement.shadowRoot);

        if (resetStyling) {
            const reset = document.createElement('style');
            reset.textContent = `
        :host {
          all: initial;
        }
      `;
            shadowElement?.shadowRoot?.appendChild(reset);
        }

        return () => {
            domRoot.removeChild(shadowElement);
        };
    }, [setShadowRoot, resetStyling]);

    return (
        <Component ref={root} {...props}>
            {shadowRoot && children && createPortal(children, shadowRoot)}
        </Component>
    );
}
