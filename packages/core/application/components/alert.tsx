import React from 'react';

const VARIANTS = {
    primary: ['bg-primary bg-opacity-50', 'text-primary', 'text-white'],
    secondary: ['bg-secondary bg-opacity-50', 'text-secondary', 'text-white'],
    success: ['bg-success bg-opacity-50', 'text-success', 'text-white'],
    danger: ['bg-danger bg-opacity-50', 'text-danger', 'text-white'],
    warning: ['bg-warning bg-opacity-50', 'text-warning', 'text-white'],
    info: ['bg-info bg-opacity-50', 'text-info', 'text-white'],
    light: ['bg-light bg-opacity-50', 'text-dark', 'text-dark'],
    dark: ['bg-dark bg-opacity-50', 'text-white', 'text-white'],
    white: ['bg-white bg-opacity-50', 'text-dark', 'text-dark'],
    transparent: ['bg-transparent', 'text-body', 'text-body'],
    'primary-outline': [
        'bg-light bg-opacity-75 border border-primary',
        'text-primary',
        'text-body',
    ],
    'secondary-outline': [
        'bg-light bg-opacity-75 border border-secondary',
        'text-secondary',
        'text-body',
    ],
    'success-outline': [
        'bg-light bg-opacity-75 border border-success',
        'text-success',
        'text-body',
    ],
    'danger-outline': [
        'bg-light bg-opacity-75 border border-danger',
        'text-danger',
        'text-body',
    ],
    'warning-outline': [
        'bg-light bg-opacity-75 border border-warning',
        'text-warning',
        'text-body',
    ],
    'info-outline': [
        'bg-light bg-opacity-75 border border-info',
        'text-info',
        'text-body',
    ],
    'light-outline': [
        'bg-light bg-opacity-75 border border-light',
        'text-dark',
        'text-dark',
    ],
    'dark-outline': [
        'bg-light bg-opacity-75 border border-dark',
        'text-body',
        'text-body',
    ],
    'white-outline': [
        'bg-light bg-opacity-75 border border-white',
        'text-dark',
        'text-dark',
    ],
    'transparent-outline': [
        'bg-transparent border border-light',
        'text-body',
        'text-body',
    ],
};

export interface FormEngineAlertProps
    extends Omit<React.HTMLAttributes<HTMLElement>, 'title'> {
    variant?: keyof typeof VARIANTS;
    title?: React.ReactNode;
    children?: React.ReactNode;
    className?: string;
    as?: React.ElementType;
    titleAs?: React.ElementType;
    aside?: React.ReactNode;
}

export default function FormEngineAlert({
    variant = 'primary',
    title,
    children,
    className,
    as: Component = 'div',
    titleAs: TitleComponent = 'h6',
    aside,
    ...props
}: FormEngineAlertProps) {
    return (
        <Component
            {...props}
            className={`fe-alert d-flex flex-row ${VARIANTS[variant][0]} ${
                className ?? ''
            }`}
        >
            {aside && (
                <div
                    className={`fe-alert__aside p-4 pe-2 ${VARIANTS[variant][1]}`}
                >
                    {aside}
                </div>
            )}
            <div className="fe-alert__content p-4 ps-2 flex-grow-1">
                {title && (
                    <TitleComponent className={`mb-4 ${VARIANTS[variant][1]}`}>
                        {title}
                    </TitleComponent>
                )}
                {children && (
                    <div className={`${VARIANTS[variant][2]}`}>{children}</div>
                )}
            </div>
        </Component>
    );
}
