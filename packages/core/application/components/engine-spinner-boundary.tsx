import React from 'react';
import { Spinner } from 'react-bootstrap';

export function FormEngineSpinner({
    children,
    ...props
}: React.ComponentProps<typeof Spinner>) {
    return (
        <Spinner
            role="status"
            variant="primary"
            {...props}
            className={`fe-spinner ${props.className ?? ''}`}
            animation={props.animation || 'border'}
        >
            {children}
            <span className="visually-hidden">Loading...</span>
        </Spinner>
    );
}
export function FormEngineSpinnerBar({
    className,
    ...props
}: React.HTMLAttributes<HTMLElement>) {
    return <div className={`fe-spinner-bar ${className}`} {...props} />;
}

export interface FormEngineSpinnerBoundaryProps
    extends React.HTMLAttributes<HTMLElement> {
    show?: boolean;
    spinner?: React.ElementType | false;
    bar?: React.ElementType | false;
    children?: React.ReactNode;
    prepend?: React.ReactNode;
    append?: React.ReactNode;
}

export default function FormEngineSpinnerBoundary({
    children,
    show = false,
    spinner: SpinnerComponent = FormEngineSpinner,
    bar: BarComponent = FormEngineSpinnerBar,
    className = '',
    prepend,
    append,
    ...props
}: FormEngineSpinnerBoundaryProps) {
    return (
        <>
            {prepend}
            <div
                className={`fe-spinner-boundary ${
                    show
                        ? `fe-spinner-boundary--show`
                        : `fe-spinner-boundary--hide`
                } ${className}`}
                {...props}
            >
                {BarComponent && <BarComponent />}
                {SpinnerComponent && <SpinnerComponent />}
                {children}
            </div>
            {append}
        </>
    );
}
