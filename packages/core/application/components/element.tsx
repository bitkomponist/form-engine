import React from 'react';
import useFormEngineElementState, {
    FormEngineElementProvider,
} from '../hooks/use-element';
import { useFormEngineElementHiddenState } from '../hooks/use-element-hidden';
import FormEngine from '../index';
import { ElementTypeProps } from '../types/form';

export enum CONDITION_TYPES {
    EQUAL = 'equal',
    NOT_EQUAL = 'not-equal',
    GREATER = 'greater',
    GREATER_EQUAL = 'greater-equal',
    LESSER = 'lesser',
    LESSER_EQUAL = 'lesser-equal',
}

export default function FormEngineElement({
    id,
    as: Component = 'div',
    elementComponent,
    containerComponent,
    prepend,
    append,
    ...props
}: ElementTypeProps): JSX.Element | null {
    const { state: elementDefinition } = useFormEngineElementState(id);

    const isHidden = useFormEngineElementHiddenState(id);

    if (!elementDefinition) return null;

    const {
        type = 'FormEngineTextElement',
        width = '12',
        marginLeft = '0',
        marginRight = '0',
        marginTop = '0',
        marginBottom = '0',
    } = elementDefinition;

    const editMode = 'editMode' in props && props.editMode !== false;
    delete props.editMode;

    if (!editMode && isHidden) return null;

    const Type = FormEngine.elements[type];

    if (!Type) return null;

    const classNames = ['fe-element', `fe-width-${width}`];

    if (marginLeft && marginLeft !== '0') {
        classNames.push(`fe-ms-${marginLeft}`);
    }

    if (marginRight && marginRight !== '0') {
        classNames.push(`fe-me-${marginRight}`);
    }

    if (marginBottom && marginBottom !== '0') {
        classNames.push(`fe-mb-${marginBottom}`);
    }

    if (marginTop && marginTop !== '0') {
        classNames.push(`fe-mt-${marginTop}`);
    }

    if (props.className) classNames.push(props.className);

    // clean props for htmlelement targets
    if (typeof Component === 'string') {
        delete props.editMode;
        delete props.parent;
        delete props.source;
    }

    return (
        <FormEngineElementProvider id={id}>
            <Component {...props} className={classNames.join(' ')}>
                {prepend}
                <Type
                    id={id}
                    containerComponent={containerComponent}
                    elementComponent={elementComponent}
                    editMode={editMode}
                />
                {append}
            </Component>
        </FormEngineElementProvider>
    );
}
