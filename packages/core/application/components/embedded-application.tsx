import React from 'react';
import FormEngine from './engine';
import { FormProvider } from '../hooks/use-form';
import { FormDefinition, FormSession } from '../types/form';

export interface FormEngineEmbeddedApplicationProps {
    form: FormDefinition;
    session?: FormSession;
}

export default function FormEngineEmbeddedApplication({
    form,
    session,
}: FormEngineEmbeddedApplicationProps): JSX.Element {
    return (
        <FormProvider value={form}>
            <FormEngine value={session || {}} />
        </FormProvider>
    );
}
