import { evalExpressions } from '@form-engine/core-util/expressions';
import FormEngine from '..';
import { ElementDefinition, FormSession, ValidationType } from '../types/form';

export default (function validate(
    element: ElementDefinition,
    value: string,
    session: FormSession
) {
    const validationFunctions = Object.values(FormEngine.validations);

    for (const validation of validationFunctions) {
        const error: ReturnType<ValidationType> = validation(
            element,
            value ?? '',
            session
        );
        if (error !== null) return error;
    }

    let required = element.required;

    if (!required && element.requiredExpressions?.length) {
        required = evalExpressions(
            element.requiredExpressions,
            session.locals ?? {}
        );
    }

    if (
        required &&
        String(value || '') === '' &&
        !session?.elements?.[element.id]?.loading
    ) {
        return FormEngine.i18n.fallback(
            element.requiredMessage,
            'validation.required',
            {
                label: element.label || element.name,
            }
        );
    }

    return null;
} as ValidationType);
