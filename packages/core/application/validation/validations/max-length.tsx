import { ValidationType } from '../../types/form';
import FormEngine from '../..';

export default (function FormEngineValidateMaxLength(element, value) {
    const max = parseInt(element.validationMaxLength || '0', 10);
    if (!max || isNaN(max)) return null;

    if (value !== '' && value.length > max) {
        return FormEngine.i18n.fallback(
            element.validationMaxLengthMessage,
            'validation.maxLength',
            {
                label: element.label || element.name,
                max,
            }
        );
    }

    return null;
} as ValidationType);
