import { compile } from '@form-engine/core-util/template';
import { ValidationType } from '../../../types/form';
import FormEngine from '../../..';

/** @todo needs a more robust implementation */
export default (function FormEngineValidateContentTypeDatetime(
    element,
    value,
    session
) {
    const date = Date.parse(value);

    if (value === '') return null;

    if (value !== '' && (isNaN(date) || !value.includes(':')))
        return FormEngine.i18n.fallback(
            element.validateContentTypeMessage,
            'validation.contentTypeDatetime',
            {
                label: element.label || element.name,
            }
        );

    if (element.validateContentTypeDateBefore) {
        const beforeDate = Date.parse(
            compile(element.validateContentTypeDateBefore, session?.locals)
        );
        if (!isNaN(beforeDate) && beforeDate > date) {
            return FormEngine.i18n.fallback(
                element.validateContentTypeMessage,
                'validation.contentTypeDatetimeBefore',
                {
                    label: element.label || element.name,
                    date,
                    beforeDate,
                }
            );
        }
    }

    if (element.validateContentTypeDateAfter) {
        const afterDate = Date.parse(
            compile(element.validateContentTypeDateAfter, session?.locals)
        );
        if (!isNaN(afterDate) && afterDate < date) {
            return FormEngine.i18n.fallback(
                element.validateContentTypeMessage,
                'validation.contentTypeDatetimeAfter',
                {
                    label: element.label || element.name,
                    date,
                    afterDate,
                }
            );
        }
    }

    return null;
} as ValidationType);
