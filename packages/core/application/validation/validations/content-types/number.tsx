import { ValidationType } from '../../../types/form';
import FormEngine from '../../..';

export const REGEX = /^[+-]?([0-9]*[.])?[0-9]+$/;

export default (function FormEngineValidateContentTypeNumber(element, value) {
    if (value !== '' && !REGEX.test(value))
        return FormEngine.i18n.fallback(
            element.validateContentTypeMessage,
            'validation.contentTypeNumber',
            {
                label: element.label || element.name,
            }
        );

    return null;
} as ValidationType);
