import { ValidationType } from '../../../types/form';
import FormEngine from '../../..';

export const REGEX = /^\d+$/;

export default (function FormEngineValidateContentTypeInteger(element, value) {
    if (value !== '' && !REGEX.test(value))
        return FormEngine.i18n.fallback(
            element.validateContentTypeMessage,
            'validation.contentTypeInteger',
            {
                label: element.label || element.name,
            }
        );

    return null;
} as ValidationType);
