import { ValidationType } from '../../../types/form';
import FormEngine from '../../..';

export default (function FormEngineValidateContentTypeText(element, value) {
    if (typeof value !== 'string')
        return FormEngine.i18n.fallback(
            element.validateContentTypeMessage,
            'validation.contentTypeText',
            {
                label: element.label || element.name,
            }
        );

    return null;
} as ValidationType);
