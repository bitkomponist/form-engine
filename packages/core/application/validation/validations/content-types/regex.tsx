import { ValidationType } from '../../../types/form';
import FormEngine from '../../..';

export default (function FormEngineValidateContentTypeRegex(element, value) {
    const { validateContentTypeRegex: pattern = false } = element;

    if (!pattern) return null;
    let regexp;
    try {
        regexp = new RegExp(pattern);
    } catch (e) {
        return null;
    }

    if (value !== '' && !regexp.test(value))
        return FormEngine.i18n.fallback(
            element.validateContentTypeMessage,
            'validation.contentTypeRegex',
            {
                label: element.label || element.name,
            }
        );

    return null;
} as ValidationType);
