import { ValidationType } from '../../../types/form';
import FormEngine from '../../..';

export const REGEX =
    /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;

export default (function FormEngineValidateContentTypeEmail(element, value) {
    if (value !== '' && !REGEX.test(String(value || '')))
        return FormEngine.i18n.fallback(
            element.validateContentTypeMessage,
            'validation.contentTypeEmail',
            {
                label: element.label || element.name,
            }
        );

    return null;
} as ValidationType);
