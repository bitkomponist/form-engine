import { ValidationType } from '../../../types/form';
import FormEngine from '../../..';

export const REGEX = /^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$/;

export default (function FormEngineValidateContentTypeTime(element, value) {
    if (value !== '' && !REGEX.test(value))
        return FormEngine.i18n.fallback(
            element.validateContentTypeMessage,
            'validation.contentTypeTime',
            {
                label: element.label || element.name,
            }
        );

    return null;
} as ValidationType);
