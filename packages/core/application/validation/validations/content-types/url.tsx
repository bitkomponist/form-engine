import { ValidationType } from '../../../types/form';
import FormEngine from '../../..';

export const REGEX =
    /^(https?:\/\/)?(www\.)?[-a-zA-Z0-9@:%._+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_+.~#?&//=]*)$/;

export default (function FormEngineValidateContentTypeUrl(element, value) {
    if (value !== '' && !REGEX.test(value))
        return FormEngine.i18n.fallback(
            element.validateContentTypeMessage,
            'validation.contentTypeUrl',
            {
                label: element.label || element.name,
            }
        );

    return null;
} as ValidationType);
