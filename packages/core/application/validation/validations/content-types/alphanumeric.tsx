import { ValidationType } from '../../../types/form';
import FormEngine from '../../..';

export const REGEX = /^[\p{L}\p{N}]*$/u;

export default (function FormEngineValidateContentTypeAlphanumeric(
    element,
    value,
    session
) {
    if (value !== '' && !REGEX.test(value))
        return FormEngine.i18n.fallback(
            element.validateContentTypeMessage,
            'validation.contentTypeAlphanumeric',
            {
                label: element.label || element.name,
            }
        );

    return null;
} as ValidationType);
