import { compile } from '@form-engine/core-util/template';
import { ValidationType } from '../../../types/form';
import FormEngine from '../../..';

export const DEFAULT_MESSAGE = '{{label}} needs to be a date';

export default (function FormEngineValidateContentTypeDate(
    element,
    value,
    session
) {
    const date = Date.parse(value);

    if (value === '') return null;

    if (isNaN(date))
        return FormEngine.i18n.fallback(
            element.validateContentTypeMessage,
            'validation.contentTypeDate',
            {
                label: element.label || element.name,
            }
        );

    if (element.validateContentTypeDateBefore) {
        const beforeDate = Date.parse(
            compile(element.validateContentTypeDateBefore, session?.locals)
        );
        if (!isNaN(beforeDate) && beforeDate < date) {
            return FormEngine.i18n.fallback(
                element.validateContentTypeMessage,
                'validation.contentTypeDateBefore',
                {
                    label: element.label || element.name,
                    date,
                    beforeDate,
                }
            );
        }
    }

    if (element.validateContentTypeDateAfter) {
        const afterDate = Date.parse(
            compile(element.validateContentTypeDateAfter, session?.locals)
        );
        if (!isNaN(afterDate) && afterDate > date) {
            return FormEngine.i18n.fallback(
                element.validateContentTypeMessage,
                'validation.contentTypeDateAfter',
                {
                    label: element.label || element.name,
                    date,
                    afterDate,
                }
            );
        }
    }

    return null;
} as ValidationType);
