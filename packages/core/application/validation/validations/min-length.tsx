import { ValidationType } from '../../types/form';
import FormEngine from '../..';

export default (function FormEngineValidateMinLength(element, value) {
    const min = parseInt(element.validationMinLength || '0', 10);
    if (!min || isNaN(min)) return null;

    if (value !== '' && value.length < min) {
        return FormEngine.i18n.fallback(
            element.validationMinLengthMessage,
            'validation.minLength',
            {
                label: element.label || element.name,
                min,
            }
        );
    }

    return null;
} as ValidationType);
