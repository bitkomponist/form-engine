import { ValidationType } from '../../types/form';
import * as contentTypes from './content-types';

const contentTypeMap = contentTypes as { [key: string]: ValidationType };

export default (function FormEngineValidateContentType(
    element,
    value,
    session
) {
    const {
        validationContentType: type = 'FormEngineValidateContentTypeText',
    } = element;

    if (!type || !contentTypeMap[type]) return null;

    return contentTypeMap[type](element, value, session);
} as ValidationType);
