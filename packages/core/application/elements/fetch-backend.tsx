import FormEngine from '..';
import { FormSession } from '../types/form';

export default async function fetchElementControllerBackend(
    formId: string,
    elementId: string,
    session: Partial<FormSession>,
    data?: any,
    options: RequestInit = {}
) {
    const path = FormEngine.elementControllerBackendRoute
        .replace(':formId', formId)
        .replace(':elementId', elementId);

    const response = await fetch(path, {
        method: 'POST',
        ...options,
        body: JSON.stringify({ session, data }),
        headers: {
            'content-type': 'application/json',
            accept: 'application/json',
            ...(options.headers || {}),
        },
    });

    if (response.status < 200 || response.status >= 400) {
        const message = await response.text();
        let error;
        if (message.startsWith('{')) {
            try {
                const errorData = JSON.parse(message);
                error = new Error(errorData?.error?.message);
                Object.assign(error, errorData?.error ?? {});
            } catch (e) {
                error = new Error(message);
            }
        }

        throw error;
    }

    const result = await response.json();

    return (result || {}) as Partial<FormSession>;
}
