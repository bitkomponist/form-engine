export default function urlJoin(...segments: string[]) {
    return segments
        .map((segment, index) => {
            let sanitizedSegment = segment;

            if (segment.endsWith('/') && index !== segments.length - 1) {
                sanitizedSegment = segment.slice(0, -1);
            }

            if (segment.startsWith('/') && index !== 0) {
                sanitizedSegment = segment.slice(1);
            }

            return sanitizedSegment;
        })
        .join('/');
}
