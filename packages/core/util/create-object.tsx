import { useMemo } from 'react';
import merge from 'lodash.merge';
import { compile } from './template';

export function createObject<T>(defaults: T, ...overrides: Partial<T>[]): T {
    return merge({}, defaults, ...overrides);
}

export function useObjectDefaults<T>(defaults: T, ...overrides: Partial<T>[]) {
    return useMemo(
        () => createObject(defaults, ...overrides),
        [defaults, ...overrides]
    );
}

export interface CompileableObjectDefinitionEntry {
    key: string;
    value?: string;
    type: 'json' | 'string' | 'boolean' | 'number' | 'raw-string';
}

export type CompileableObjectDefinition = CompileableObjectDefinitionEntry[];

export function compileObject(
    objectDefinition: CompileableObjectDefinition,
    locals?: any
) {
    const result = {} as any;

    for (const { key, value, type } of objectDefinition) {
        const compiled =
            typeof value === 'string' ? compile(value, locals, '') : '';
        const sanitized = String(compiled ?? '').trim();
        switch (type) {
            case 'json':
                try {
                    result[key] = JSON.parse(sanitized);
                } catch (e) {
                    console?.warn(`could not parse ${key}`);
                }
                break;
            case 'string':
                if (sanitized !== '') {
                    result[key] = compiled;
                }
                break;
            case 'raw-string':
                result[key] = compiled;
                break;
            case 'boolean':
                try {
                    result[key] = Boolean(JSON.parse(sanitized));
                } catch (e) {
                    result[key] = false;
                }
                break;
            case 'number':
                result[key] = Number(sanitized);
                if (isNaN(result[key])) {
                    result[key] = 0;
                }
                break;
            default:
                throw new Error(`unknown type ${type}`);
        }
    }

    return result;
}
