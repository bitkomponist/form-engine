import get from 'lodash.get';
import {
    formatTitleCase,
    formatCamelCase,
    formatPascalCase,
} from './format-case';

const expressionsRegex = /\{\{([^}]+)\}\}/g;
const expressionRegex = /\{\{([^}]+)\}\}/;
const spaceSplitRegex = /(?:[^\s"]+|"[^"]*")+/g;
const pipeSplitRegex = /(?:[^\>"]+|"[^"]*")+/g;
const isNumberRegex = /^(\d+(?:\.\d+)?)$/;
export function isTemplate(source: string) {
    return typeof source === 'string' && expressionRegex.test(source);
}

function isValidDate(d: any) {
    return d instanceof Date && !isNaN(Number(d));
}

export type TemplateHelperMap = { [key: string]: (...args: any[]) => string };

export const TemplateDateMacros = {
    now: () => new Date(),
    yesterday: () => {
        const date = new Date();
        date.setDate(date.getDate() - 1);
        return date;
    },
    tomorrow: () => {
        const date = new Date();
        date.setDate(date.getDate() + 1);
        return date;
    },
} as const;

function parseDateSource(source: any) {
    return source in TemplateDateMacros
        ? TemplateDateMacros[source as keyof typeof TemplateDateMacros]()
        : new Date(
              String(source).match(isNumberRegex) ? Number(source) : source
          );
}

export const DefaultTemplateHelpers: TemplateHelperMap = {
    uppercase: (_locals: any, source: string) => String(source).toUpperCase(),
    lowercase: (_locals: any, source: string) => String(source).toLowerCase(),
    titlecase: (_locals: any, source: string) =>
        formatTitleCase(String(source)),
    camelcase: (_locals: any, source: string) =>
        formatCamelCase(String(source)),
    pascalcase: (_locals: any, source: string) =>
        formatPascalCase(String(source)),
    number: (
        _locals: any,
        source: string,
        locale = 'en',
        maximumFractionDigits = 2
    ) => {
        try {
            return new Intl.NumberFormat(locale, {
                maximumFractionDigits,
            }).format(Number(source));
        } catch (e) {
            console?.warn(`invalid expression ${source}`);
            return source;
        }
    },
    round: (_locals: any, source: string, locale = 'en') => {
        try {
            return new Intl.NumberFormat(locale).format(
                Math.round(Number(source))
            );
        } catch (e) {
            console?.warn(`invalid expression ${source}`);
            return source;
        }
    },
    floor: (_locals: any, source: string, locale = 'en') => {
        try {
            return new Intl.NumberFormat(locale).format(
                Math.floor(Number(source))
            );
        } catch (e) {
            console?.warn(`invalid expression ${source}`);
            return source;
        }
    },
    ceil: (_locals: any, source: string, locale = 'en') => {
        try {
            return new Intl.NumberFormat(locale).format(
                Math.ceil(Number(source))
            );
        } catch (e) {
            console?.warn(`invalid expression ${source}`);
            return source;
        }
    },
    currency: (
        _locals: any,
        source: string,
        locale = 'en',
        currency = 'EUR'
    ) => {
        try {
            return new Intl.NumberFormat(locale, {
                style: 'currency',
                currency,
            }).format(Number(source));
        } catch (e) {
            console?.warn(`invalid expression ${source}`);
            return source;
        }
    },
    date: (
        _locals: any,
        source: string,
        locale = 'en',
        dateStyle = 'medium'
    ) => {
        const date = parseDateSource(source);
        if (!isValidDate(date)) return source;
        try {
            return new Intl.DateTimeFormat(locale, { dateStyle }).format(date);
        } catch (e) {
            console?.warn(`invalid expression ${source}`);
            return source;
        }
    },
    datetime: (
        _locals: any,
        source: string,
        locale = 'en',
        dateStyle = 'medium',
        timeStyle = 'short'
    ) => {
        const date = parseDateSource(source);
        if (!isValidDate(date)) return source;
        try {
            return new Intl.DateTimeFormat(locale, {
                dateStyle,
                timeStyle,
            }).format(date);
        } catch (e) {
            console?.warn(`invalid expression ${source}`);
            return source;
        }
    },
    time: (
        _locals: any,
        source: string,
        locale = 'en',
        timeStyle = 'short'
    ) => {
        const date = parseDateSource(source);
        if (!isValidDate(date)) return source;
        try {
            return new Intl.DateTimeFormat(locale, { timeStyle }).format(date);
        } catch (e) {
            console?.warn(`invalid expression ${source}`);
            return source;
        }
    },
    join: (_locals: any, delimiter = '', ...parts: string[]) =>
        parts.join(delimiter),
    concat: (_locals: any, ...parts: string[]) => parts.join(''),
    get: (locals: any, varname = '', ...segments: string[]) =>
        get(locals, [varname, ...segments].join(''), get(locals, varname, '')),
    getPathCamel: (locals: any, varname = '', ...segments: string[]) =>
        get(
            locals,
            formatCamelCase([varname, ...segments].join(' ')),
            get(locals, varname, '')
        ),
    getPathPascal: (locals: any, varname = '', ...segments: string[]) =>
        get(
            locals,
            formatPascalCase([varname, ...segments].join(' ')),
            get(locals, varname, '')
        ),
    json: (
        _locals: any,
        value = '',
        space: string | number | undefined = undefined
    ) => JSON.stringify(value ?? null, undefined, space),
    jsonString: (
        _locals: any,
        value = '',
        space: string | number | undefined = undefined
    ) => {
        const result = JSON.stringify(value ?? null, undefined, space);

        if (result.startsWith('"') && result.endsWith('"'))
            return result.slice(1, -1);

        return result;
    },
    reverse: (_locals: any, varname = '') =>
        String(varname ?? '')
            .split('')
            .reverse()
            .join(''),
    echo: (_locals: any, string: any) => String(string ?? ''),
    condition: (
        locals: any,
        input: string,
        thenArg?: string,
        elseArg?: string
    ) => {
        if (!input)
            return elseArg !== undefined ? get(locals, elseArg, elseArg) : '';

        let compare = false;
        try {
            compare = Boolean(JSON.parse(get(locals, input, input)));
        } catch (e) {
            compare = Boolean(String(input).trim());
        }
        if (compare) {
            return thenArg !== undefined ? get(locals, thenArg, thenArg) : '';
        }
        return elseArg !== undefined ? get(locals, elseArg, elseArg) : '';
    },
};

function compileMatch(
    expression: string,
    locals: any = {},
    defaultValue?: string,
    helpers: TemplateHelperMap = DefaultTemplateHelpers,
    pipeResult?: any
) {
    let parts = Array.from(expression.match(spaceSplitRegex) ?? []);

    if (!parts.length) {
        if (pipeResult !== undefined) {
            return get(locals, pipeResult, defaultValue);
        } else {
            return expression;
        }
    }

    if (pipeResult !== undefined) {
        const [helper, ...args] = parts;
        parts = [helper, JSON.stringify(pipeResult), ...args];
    }

    if (parts.length === 1) return get(locals, parts[0], defaultValue);

    if (parts[0] in helpers) {
        const [helperName, ...args] = parts;
        const helperArgs = args.map((part) => {
            const arg = part.trim();
            if (arg.startsWith('"') && arg.endsWith('"')) {
                return arg.slice(1, -1);
            } else if (arg.match(isNumberRegex)) {
                return Number(arg);
            }
            return get(locals, arg, defaultValue);
        });

        return helpers[helperName as keyof typeof helpers](
            locals,
            ...helperArgs
        );
    }

    return get(locals, parts.join(' '), defaultValue);
}

function compilePipes(
    expression: string,
    locals: any = {},
    defaultValue?: string,
    helpers: TemplateHelperMap = DefaultTemplateHelpers
) {
    const parts = Array.from(expression.match(pipeSplitRegex) ?? []).map(
        (part) => part.trim()
    );

    if (!parts.length) return expression;

    if (parts.length === 1)
        return compileMatch(parts[0], locals, defaultValue, helpers);

    let intermediateResult;

    for (const expression of parts) {
        intermediateResult = compileMatch(
            expression,
            locals,
            defaultValue,
            helpers,
            intermediateResult
        );
    }

    return intermediateResult;
}

export function compile(
    source: string,
    locals: any = {},
    defaultValue?: string,
    helpers: TemplateHelperMap = DefaultTemplateHelpers
) {
    const enhancedLocals = {
        ...locals,
        $this: locals,
    };

    return source.replace(expressionsRegex, (match) =>
        compilePipes(
            match.slice(2, -2).trim(),
            enhancedLocals,
            defaultValue,
            helpers
        )
    );
}
