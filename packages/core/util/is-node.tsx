export default function isNode() {
    return Boolean(process?.versions?.node);
}
