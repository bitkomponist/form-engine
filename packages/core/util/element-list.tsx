import uuidv4 from './uuid';

interface ElementListItem {
    id: string;
    parent?: string;
    [prop: string]: any;
}

function isListWithItems<T extends ElementListItem = ElementListItem>(
    items: readonly T[]
) {
    return Array.isArray(items) && items.length > 0;
}

function resolveItemId<T extends ElementListItem = ElementListItem>(
    idOrItem?: string | T
) {
    return typeof idOrItem === 'string' ? idOrItem : idOrItem?.id;
}

function parentSort<T extends ElementListItem = ElementListItem>(
    items: readonly T[]
) {
    const result: T[] = [];
    function traverse(parent?: string) {
        for (const item of items) {
            if (
                (parent && item?.parent === parent) ||
                (!parent && !item?.parent)
            ) {
                result.push(item);
                item?.id && traverse(item.id);
            }
        }
    }

    traverse();
    return result;
}

export function getItemIndexById<T extends ElementListItem = ElementListItem>(
    items: readonly T[],
    id?: string
) {
    if (!id || !isListWithItems(items)) return -1;
    return items.findIndex((item) => item.id === id);
}

export function getItemById<T extends ElementListItem = ElementListItem>(
    items: readonly T[],
    id?: string
) {
    if (!id || !isListWithItems(items)) return;
    return items.find((item) => item.id === id);
}

export function insertItemBefore<T extends ElementListItem = ElementListItem>(
    items: readonly T[],
    target: T,
    before?: string | T
) {
    if (!isListWithItems(items)) return [{ ...target }];
    const beforeId = resolveItemId(before);
    const copy = [...items];
    if (beforeId === target.id) return parentSort(copy);
    let index = getItemIndexById(copy, beforeId);
    if (index < 0) index = 0;
    copy.splice(index, 0, { ...target });
    return parentSort(copy);
}

export function insertItemAfter<T extends ElementListItem = ElementListItem>(
    items: readonly T[],
    target: T,
    after?: string | T
) {
    if (!isListWithItems(items)) return [{ ...target }];
    const afterId = resolveItemId(after);

    const copy = [...items];
    if (afterId === target.id) return parentSort(copy);
    let index = getItemIndexById(copy, afterId) + 1;

    if (index < 1 || index > copy.length) index = copy.length;

    copy.splice(index, 0, { ...target });
    return parentSort(copy);
}

export function removeItem<T extends ElementListItem = ElementListItem>(
    items: readonly T[],
    target: T
) {
    if (!isListWithItems(items)) return [];

    const copy = [...items];
    if (!target) return parentSort(copy);

    const index = getItemIndexById(copy, resolveItemId(target));

    if (index < 0) return parentSort(copy);

    copy.splice(index, 1);

    return parentSort(removeOrphans(copy));
}

export function addItem<T extends ElementListItem = ElementListItem>(
    items: readonly T[],
    target: T
) {
    if (!isListWithItems(items)) {
        if (target) {
            return [target];
        } else {
            return [];
        }
    }

    if (!target) return parentSort(items);

    return parentSort([...items, target]);
}

export function getItemParents<T extends ElementListItem = ElementListItem>(
    items: readonly T[],
    target?: T | string
) {
    const index = getItemIndexById(items, resolveItemId(target));
    if (index < 0 || !isListWithItems(items)) return [];

    const parents: T[] = [];
    let current: T | null = items[index];
    while (current?.parent) {
        const parentIndex: number = getItemIndexById(items, current.parent);
        if (parentIndex > -1) {
            const parent: T = items[parentIndex];
            parents.push(parent);
            current = parent;
        } else {
            current = null;
        }
    }
    return parents;
}

export function removeOrphans<T extends ElementListItem = ElementListItem>(
    items: readonly T[]
) {
    if (!isListWithItems(items)) return [];

    const copy = [...items];

    const idLookup = items.map(({ id }) => id);

    function removeBranch(target: T) {
        const index = copy.indexOf(target);
        if (index < 0) return;
        copy.splice(index, 1);
        for (const child of getItemChildren(copy, target)) {
            removeBranch(child);
        }
    }

    for (const item of items) {
        if (item.parent && !idLookup.includes(item.parent)) {
            removeBranch(item);
        }
    }

    return copy;
}

export function hasParentItem<T extends ElementListItem = ElementListItem>(
    items: readonly T[],
    parent: string | T,
    child?: string | T
) {
    const parentId = resolveItemId<T>(parent);
    return (
        getItemParents<T>(items, child).findIndex(({ id }) => id === parentId) >
        -1
    );
}

export function relocateItem<T extends ElementListItem = ElementListItem>(
    insert: (items: readonly T[], target: T, destination?: string | T) => T[],
    items: readonly T[],
    source: T,
    destination?: string | T
) {
    // guard relocation from items inside of them selves
    if (
        !source ||
        hasParentItem<T>(items, source, destination /*|| source.parent*/) ||
        source.id === source.parent
    )
        return [...(items || [])];
    const sourceId = resolveItemId(source);
    const destinationId = resolveItemId(destination);
    const copy = [...items];
    const index = getItemIndexById(items, sourceId);
    if (index > -1) {
        copy.splice(index, 1);
    }
    return insert(copy, source, destinationId);
}

export function relocateItemBefore<T extends ElementListItem = ElementListItem>(
    items: readonly T[],
    source: T,
    destination?: string | T
) {
    return relocateItem(insertItemBefore, items, source, destination);
}

export function relocateItemAfter<T extends ElementListItem = ElementListItem>(
    items: readonly T[],
    source: T,
    destination?: string | T
) {
    return relocateItem(insertItemAfter, items, source, destination);
}

export function getItemChildren<T extends ElementListItem = ElementListItem>(
    items: readonly T[],
    source?: string | T
) {
    if (!isListWithItems(items)) return [];
    const sourceId = resolveItemId(source);

    return items.filter((item) => item.parent === sourceId);
}

export function getItemBranch<T extends ElementListItem = ElementListItem>(
    items: readonly T[],
    source: string | T
) {
    if (!isListWithItems(items)) return [];
    const sourceId = resolveItemId(source);
    const item =
        typeof source === 'object' && items.includes(source)
            ? source
            : getItemById(items, sourceId);
    if (!item) return [];

    let branch = [item];
    getItemChildren(items, sourceId).forEach((child) => {
        const children = getItemBranch(items, child);
        branch = [...branch, ...children];
    });

    return branch;
}

export function cloneItemBranch<T extends ElementListItem = ElementListItem>(
    items: readonly T[],
    source: string | T,
    generateId: () => string = uuidv4
) {
    const idMap: { [itemId: string]: string } = {};
    const sourceId = resolveItemId(source);
    // careful: assumes getItemBranch result to be sorted parent first
    return getItemBranch<T>(items, source).map((item) => {
        const id = generateId();
        idMap[item.id] = id;
        const clonedParentId =
            item?.parent && item?.parent in idMap
                ? idMap[item.parent]
                : undefined;
        const result = { ...item };
        result.id = id;
        // use original parent if item is root
        if (item.id === sourceId) {
            result.parent = item.parent;
        } else if (clonedParentId) {
            result.parent = clonedParentId;
        }

        return result;
    });
}
