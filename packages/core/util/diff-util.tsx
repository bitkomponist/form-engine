import { Diff } from 'deep-diff';

export function pathEquals(a: string[], b: string[]) {
    if (a.length !== b.length) return false;
    for (let i = 0, l = a.length; i < l; ++i) {
        if (a[i] !== b[i]) return false;
    }
    return true;
}

export function isIgnoredPath(path: string[], ignoredPaths: string[][]) {
    for (let i = 0, l = ignoredPaths.length; i < l; ++i) {
        if (pathEquals(path, ignoredPaths[i])) return true;
    }

    return false;
}

export function filterIgnoredHistoryDiffs(
    diffs?: Diff<any>[],
    ignorePaths: string[][] = []
) {
    if (!diffs) return undefined;

    const filtered = diffs.filter(
        (diff) => !diff.path || !isIgnoredPath(diff.path, ignorePaths)
    );

    return filtered.length ? filtered : undefined;
}
