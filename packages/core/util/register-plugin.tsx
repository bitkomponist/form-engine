import merge from 'lodash.merge';

export function registerPlugin(target: any, definition: any) {
    // window.FormEngine = merge(window.FormEngine || {}, definition);
    return merge(target || {}, definition);
}

export function registerImportedPluginModules(
    target: any,
    moduleDictionary: any,
    modifier?: (input: any) => any
) {
    let result: any;
    Object.values(moduleDictionary).forEach((child) => {
        result = registerPlugin(target, modifier ? modifier(child) : child);
    });

    return result;
}
