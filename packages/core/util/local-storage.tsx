export function getStorage(key: string, fallback: any = null): any {
    const raw = localStorage.getItem(key);
    if (raw) return JSON.parse(raw);
    return fallback;
}

export function setStorage(key: string, value: any) {
    localStorage.setItem(key, JSON.stringify(value));
}

export function getStorageNamespace(namespace: string) {
    const getKeys = () => {
        const prefix = `${namespace}/`;
        return Object.keys(localStorage)
            .filter((key) => key.startsWith(prefix))
            .map((key) => key.slice(prefix.length));
    };
    const getItem = (key: string, fallback: any = null) => {
        return getStorage(`${namespace}/${key}`, fallback);
    };

    return {
        getKeys,
        getAll() {
            const result: { [namespaceItem: string]: any } = {};
            getKeys().forEach((key) => {
                result[key] = getItem(key);
            });
            return result;
        },
        getItem,
        setItem(key: string, value: any) {
            return setStorage(`${namespace}/${key}`, value);
        },
        unsetItem(key: string) {
            localStorage.removeItem(`${namespace}/${key}`);
        },
    };
}

export type LocalStorageNamespace = ReturnType<typeof getStorageNamespace>;
