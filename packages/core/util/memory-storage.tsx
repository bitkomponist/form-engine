const storage: Record<string, string | undefined> = {};

export function getMemoryStorage(key: string, fallback: any = null): any {
    const raw = storage[key];
    if (raw) return JSON.parse(raw);
    return fallback;
}

export function setMemoryStorage(key: string, value: any) {
    storage[key] = JSON.stringify(value);
}

export function getMemoryStorageNamespace(namespace: string) {
    const getKeys = () => {
        const prefix = `${namespace}/`;
        return Object.keys(storage)
            .filter((key) => key.startsWith(prefix))
            .map((key) => key.slice(prefix.length));
    };
    const getItem = (key: string, fallback: any = null) => {
        return getMemoryStorage(`${namespace}/${key}`, fallback);
    };

    return {
        getKeys,
        getAll() {
            const result: { [namespaceItem: string]: any } = {};
            getKeys().forEach((key) => {
                result[key] = getItem(key);
            });
            return result;
        },
        getItem,
        setItem(key: string, value: any) {
            return setMemoryStorage(`${namespace}/${key}`, value);
        },
        unsetItem(key: string) {
            delete storage[`${namespace}/${key}`];
        },
    };
}

export type MemoryStorageNamespace = ReturnType<
    typeof getMemoryStorageNamespace
>;
