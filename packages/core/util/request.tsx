import { AuthInfo } from '@form-engine/core-application/types/form';
import { compileObject } from './create-object';
import { compile } from './template';

export type AuthType = 'basic' | 'bearer';

export interface AuthorizationInfoSettings extends AuthInfo {
    locals?: any;
}

export function compileAuthorizationInfo(settings: AuthorizationInfoSettings) {
    const {
        auth,
        basicAuthUsername = '{{defaultBasicAuthUsername}}',
        basicAuthPassword = '{{defaultBasicAuthPassword}}',
        bearerToken = '{{request.headers.authorization}}',
        locals = {},
    } = settings;

    const result: {
        auth?: AuthType;
        accessToken?: string;
        username?: string;
        password?: string;
        authorization?: string;
    } = {
        auth,
    };

    if (auth === 'basic') {
        const username = (result.username = compile(
            basicAuthUsername,
            locals,
            ''
        ));
        const password = (result.password = compile(
            basicAuthPassword,
            locals,
            ''
        ));
        result.authorization = `Basic ${Buffer.from(
            `${username}:${password}`
        ).toString('base64')}`;
    } else if (auth === 'bearer') {
        const token = (result.accessToken = compile(bearerToken, locals, ''));

        result.authorization = token.toLowerCase().startsWith('bearer ')
            ? token
            : `Bearer ${token}`;
    } else if (auth && auth !== 'none') {
        throw new Error(`unknown auth method ${auth}`);
    }

    return result;
}

export function compileAuthorizationHeader(
    settings: AuthorizationInfoSettings
) {
    const { authorization } = compileAuthorizationInfo(settings);

    if (!authorization) return {};

    return { authorization };
}

export type CompileRequestContentTypes =
    | 'text/plain'
    | 'text/csv'
    | 'application/json'
    | 'application/x-www-form-urlencoded';

export function compileRequestBody(
    contentType: CompileRequestContentTypes,
    data: any,
    locals: any
): string | FormData | undefined {
    let body: string | FormData | undefined;

    if (contentType === 'application/json') {
        if (typeof data === 'string') {
            body = compile(data, locals, '');
        } else if (typeof data === 'object') {
            body = JSON.stringify(compileObject(data, locals));
        }
    } else if (contentType === 'application/x-www-form-urlencoded') {
        if (typeof data === 'string') {
            body = encodeURI(compile(data, locals, ''));
        } else if (typeof data === 'object') {
            const bodyData = compileObject(data, locals);
            body = new FormData();
            Object.entries(bodyData).forEach(([key, value]) => {
                (body as FormData).append(
                    key,
                    typeof value === 'string' ? value : JSON.stringify(value)
                );
            });
        }
    } else if (typeof data === 'string') {
        body = data;
    }

    return body;
}
