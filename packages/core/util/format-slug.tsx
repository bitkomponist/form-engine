const slugPatterns: { [pattern: string]: string } = {
    '\u00fc': 'ue',
    '\u00e4': 'ae',
    '\u00f6': 'oe',
    '\u00df': 'ss',
    ' ': '+',
};

const formatRegexp = new RegExp(Object.keys(slugPatterns).join('|'), 'gi');

export default function formatSlug(source: string) {
    let result = String(source || '')
        .toLowerCase()
        .trim();
    result = result.replace(formatRegexp, (matched) => slugPatterns[matched]);
    result = result.replace(/[^a-zA-Z0-9+-_]/g, '');
    return result;
}
