export type Optional<T> = T | undefined;
export type Nullable<T> = T | null;
export type WithRequired<T, K extends keyof T> = Required<Pick<T, K>> &
    Omit<Partial<T>, K>;
