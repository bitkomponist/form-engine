export type SortType =
    | 'String'
    | 'Number'
    | 'Date'
    | ((a: unknown, b: unknown) => number);

export function createObjectSort<T = any>(
    sortType: SortType,
    propertyName: string,
    order: 'asc' | 'desc' = 'asc'
): (a: T, b: T) => number {
    if (sortType === 'String') {
        return (a, b) => {
            const comp = String((a as any)?.[propertyName] ?? '').localeCompare(
                String((b as any)?.[propertyName] ?? '')
            );
            return order === 'asc' ? comp : comp * -1;
        };
    } else if (sortType === 'Number') {
        return (a, b) => {
            const numberA = Number((a as any)?.[propertyName]);
            const numberB = Number((b as any)?.[propertyName]);
            const comp = numberA - numberB;
            return order === 'asc' ? comp : comp * -1;
        };
    } else if (sortType === 'Date') {
        return (a, b) => {
            const dateA = (a as any)?.[propertyName];
            const dateB = (b as any)?.[propertyName];
            const comp = Date.parse(String(dateA)) - Date.parse(String(dateB));
            return order === 'asc' ? comp : comp * -1;
        };
    } else if (typeof sortType === 'function') {
        return (a, b) => {
            const valueA = (a as any)?.[propertyName];
            const valueB = (b as any)?.[propertyName];
            const comp = sortType(valueA, valueB);
            return order === 'asc' ? comp : comp * -1;
        };
    }
    throw new Error(`unsupported sort type ${sortType}`);
}
