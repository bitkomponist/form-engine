import { ExpressionDefinition } from '@form-engine/core-application/types/form';
import { compile } from './template';

const parseNumber = (value: unknown) => {
    let num: number;

    if (value instanceof Date) {
        num = value.getTime();
    } else if (typeof value === 'number') {
        num = value;
    } else {
        num = parseFloat(String(value).trim());
    }

    if (isNaN(num)) return 0;

    return num;
};

const compareUnknowns = (a: unknown, b: unknown) => {
    const compA = typeof a === 'string' ? a.trim() : JSON.stringify(a) ?? '';
    const compB = typeof b === 'string' ? b.trim() : JSON.stringify(b) ?? '';
    return compA === compB;
};

export const comparisonOperations: {
    [operation: string]: (a: unknown, b: unknown) => boolean;
} = {
    equal: (a, b) => compareUnknowns(a, b),
    'not-equal': (a, b) => !compareUnknowns(a, b),
    greater: (a, b) => parseNumber(a) > parseNumber(b),
    'greater-equal': (a, b) => parseNumber(a) >= parseNumber(b),
    lesser: (a, b) => parseNumber(a) < parseNumber(b),
    'lesser-equal': (a, b) => parseNumber(a) <= parseNumber(b),
    // 'in':(a,b) => parseFloat(String(a)) < parseFloat(String(b)), todo
    // 'contains':(a,b) => parseFloat(String(a)) < parseFloat(String(b)), todo
};

export function evalExpression(
    expression: ExpressionDefinition,
    locals: any
): boolean {
    const { operator } = expression;

    if (!comparisonOperations[operator]) return false;

    const leftOperandValue = expression?.leftOperand
        ? compile(expression.leftOperand, locals, '')
        : '';
    const rightOperandValue = expression?.rightOperand
        ? compile(expression.rightOperand, locals, '')
        : '';

    const result = comparisonOperations[expression?.operator](
        leftOperandValue,
        rightOperandValue
    );

    return result;
}

export function evalExpressions(
    expressions: ExpressionDefinition[],
    locals: any
) {
    let result: boolean | undefined;
    let prevDisjunctor: string | undefined;
    for (const expression of expressions) {
        if (result === undefined) {
            result = evalExpression(expression, locals);
        } else if (!prevDisjunctor || prevDisjunctor === 'and') {
            result = result && evalExpression(expression, locals);
        } else if (prevDisjunctor === 'or') {
            result = result || evalExpression(expression, locals);
        }
        prevDisjunctor = expression.disjunctor;
    }
    return result;
}
