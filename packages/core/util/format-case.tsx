const replacePatterns: { [pattern: string]: string } = {
    ü: 'ue',
    ä: 'ae',
    ö: 'oe',
    ß: 'ss',
    Ü: 'Ue',
    Ä: 'Ae',
    Ö: 'Oe',
};

const formatRegexp = new RegExp(Object.keys(replacePatterns).join('|'), 'gi');

export function formatPascalCase(source: string) {
    let result = String(source || '').trim();
    result = result.replace(
        formatRegexp,
        (matched) => replacePatterns[matched]
    );
    result = result
        .split(/\s+/)
        .map((word) => {
            if (word.length < 2) return word.toUpperCase();
            return `${word.slice(0, 1).toUpperCase()}${word.slice(1)}`;
        })
        .join('');
    result = result.replace(/[^a-zA-Z0-9+-_$]/g, '');
    return result;
}

export const formatResourceNameCase = formatPascalCase;

export function formatCamelCase(source: string) {
    const result = formatResourceNameCase(source);
    return `${result.slice(0, 1).toLowerCase()}${result.slice(1)}`;
}

export const formatVariableCase = formatCamelCase;

export function formatTitleCase(source: string) {
    let result = String(source || '').trim();

    result = result
        .split(/[\s-]+/)
        .map((word) => {
            if (word.length < 2) return word.toUpperCase();
            return `${word.slice(0, 1).toUpperCase()}${word.slice(1)}`;
        })
        .join(' ');

    return result;
}
