import { ServerApplicationPlugin } from '@form-engine/server-application/typings';
import WorkflowLayer from './workflow-layers/server-cxp-api-request';
export default {
    workflowLayers: {
        [WorkflowLayer.apiName]: WorkflowLayer,
    },
} as ServerApplicationPlugin;
