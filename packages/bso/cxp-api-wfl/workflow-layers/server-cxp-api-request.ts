import {
    WorkflowLayerArgs,
    WorkflowLayerResponse,
    WorkflowLayerType,
} from '@form-engine/core-application/types/form';
import { compile } from '@form-engine/core-util/template';
import fetch from 'cross-fetch';
import { getFormSessionLocals } from '@form-engine/core-application/utilities/form-session';
import {
    AuthorizationInfoSettings,
    compileRequestBody,
    CompileRequestContentTypes,
} from '@form-engine/core-util/request';
import { Server } from '@form-engine/bso-cxp-api-client';
import { compileCxpAuthHeader } from '@form-engine/bso-core/utilities/compile-auth-header';

export const DEFAULT_SERVER = Server.Sandbox;

async function BsoServerCxpApiRequestWfl({
    form,
    session,
    definition,
}: WorkflowLayerArgs): Promise<WorkflowLayerResponse> {
    const {
        auth = 'none',
        basicAuthUsername = '',
        basicAuthPassword = '',
        bearerToken = '',
        url: path = '',
        method = 'GET',
        contentType = 'application/json',
        responseType = 'application/json',
        body = '',
        storeResultAs,
        name,
        id,
    } = definition;

    const locals = getFormSessionLocals(form, session ?? {});

    const bsoEnvironment: keyof typeof Server | undefined = (form as any)
        .bsoEnvironment;

    const server = bsoEnvironment ? Server[bsoEnvironment] : DEFAULT_SERVER;

    const headers: any = {
        ...compileCxpAuthHeader(
            {
                auth,
                basicAuthUsername,
                basicAuthPassword,
                bearerToken,
                locals,
            } as AuthorizationInfoSettings,
            form
        ),
    };

    const hasBody = ['POST', 'PATCH', 'PUT'].includes(method);

    if (hasBody && contentType) {
        headers['content-type'] = contentType;
    }

    let result = '';
    let bodyResult;

    if (path && server) {
        let compiledPath = compile(path, locals, '').trim();
        if (compiledPath.startsWith('/')) {
            compiledPath = compiledPath.slice(1);
        }

        if (hasBody) {
            bodyResult = compileRequestBody(
                contentType as CompileRequestContentTypes,
                body,
                locals
            );
        }

        const url = `${server}/${compiledPath}`;

        const response = await fetch(url, {
            method,
            body: bodyResult,
            headers,
        });

        if (response.status < 200 || response.status >= 400) {
            let message = await response.text();
            try {
                message = JSON.parse(message)?.message;
            } catch (e) {
                //..
            }

            throw new Error(JSON.stringify({ message, payload: bodyResult }));
        }

        if (responseType === 'application/json') {
            result = await response.json();
        } else {
            result = await response.text();
        }
    }

    const localName = (storeResultAs as string) || `wfl_${name || id}`;
    const requestBodyLocalName = `wfl_${name || id}_body`;

    return {
        locals: { [localName]: result, [requestBodyLocalName]: bodyResult },
    };
}

BsoServerCxpApiRequestWfl.apiName = 'BsoCxpApiRequestWfl';
export default BsoServerCxpApiRequestWfl as WorkflowLayerType;
