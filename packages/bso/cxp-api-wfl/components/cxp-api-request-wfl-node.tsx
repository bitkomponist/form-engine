import { BuilderWorkflowEditorLayerNode } from '@form-engine/builder-form-editor/components/workflow-editor/layer-node';
import React, { memo } from 'react';
import { CheckCircleFill, ExclamationDiamondFill } from 'react-bootstrap-icons';

export const BsoCxpApiRequestWflNode = memo(function BsoCxpApiRequestWflNode(
    props: React.ComponentProps<typeof BuilderWorkflowEditorLayerNode>
) {
    return (
        <BuilderWorkflowEditorLayerNode
            {...props}
            sources={{
                error: { icon: ExclamationDiamondFill },
                success: { icon: CheckCircleFill },
            }}
        />
    );
});
