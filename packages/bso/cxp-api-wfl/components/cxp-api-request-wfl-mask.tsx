import { useMaskContext } from '@form-engine/builder-mask/hooks/use-mask';
import React from 'react';
import BuilderMaskGroup from '@form-engine/builder-mask/components/mask/group';
import BuilderMaskGroupWflBehaviour from '@form-engine/builder-mask/components/groups/wfl-behaviour';
import BuilderEnumProperty from '@form-engine/builder-mask/components/properties/enum';
import BuilderStringProperty from '@form-engine/builder-mask/components/properties/string';
import { formatVariableCase } from '@form-engine/core-util/format-case';
import BuilderMaskGroupRequestPayload from '@form-engine/builder-mask/components/groups/request-payload';
import BuilderAuthProperty from '@form-engine/builder-mask/components/properties/auth';
import Wfl from '../workflow-layers/client-cxp-api-request';
import { i18n } from '@form-engine/builder-application';

export function BsoCxpApiRequestWflSetup() {
    const { state } = useMaskContext();

    const { c } = i18n.prefix(`mask.${Wfl.apiName}.`);

    return (
        <>
            <BuilderMaskGroupWflBehaviour />
            <BuilderMaskGroup label={c('requestGroupTitle')}>
                <BuilderStringProperty
                    compilable
                    propertyName="url"
                    label={c('url')}
                />
                <BuilderEnumProperty
                    propertyName="method"
                    label={c('method')}
                    options={['GET', 'POST', 'PUT', 'PATCH', 'DELETE']}
                />
                <BuilderStringProperty
                    propertyName="storeResultAs"
                    label={c('storeResultAs')}
                    placeholder={c('storeResultAsPlaceholder')}
                    formatter={formatVariableCase}
                />
            </BuilderMaskGroup>
            <BuilderMaskGroup label={c('authGroupTitle')}>
                <BuilderAuthProperty />
            </BuilderMaskGroup>

            {['POST', 'PUT', 'PATCH'].includes(state.method) && (
                <BuilderMaskGroupRequestPayload forceContentType="application/json" />
            )}
        </>
    );
}

export default function BsoCxpApiRequestWflMask() {
    return <BsoCxpApiRequestWflSetup />;
}
