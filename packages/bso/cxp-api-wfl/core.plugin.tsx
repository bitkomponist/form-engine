import { FormEnginePlugin } from '@form-engine/core-application';
import WorkflowLayer from './workflow-layers/client-cxp-api-request';
export default {
    workflowLayers: {
        [WorkflowLayer.apiName]: WorkflowLayer,
    },
} as FormEnginePlugin;
