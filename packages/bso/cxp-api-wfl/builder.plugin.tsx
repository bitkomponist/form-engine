import { default as Mask } from './components/cxp-api-request-wfl-mask';
import { default as WorkflowLayer } from './workflow-layers/client-cxp-api-request';
import { Server as Icon } from 'react-bootstrap-icons';
import { BuilderPlugin } from '@form-engine/builder-application';
import { BsoCxpApiRequestWflNode as Node } from './components/cxp-api-request-wfl-node';
import * as translations from './builder-translations';

export default {
    translations,
    workflowLayers: {
        [WorkflowLayer.apiName]: {
            label: `${WorkflowLayer.apiName}.title`,
            description: `${WorkflowLayer.apiName}.description`,
            Icon,
            WorkflowLayer,
            Mask,
            Node,
        },
    },
} as BuilderPlugin;
