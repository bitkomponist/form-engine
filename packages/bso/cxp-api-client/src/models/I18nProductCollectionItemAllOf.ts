/* tslint:disable */
/* eslint-disable */
/**
 * API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import type { Product2 } from './Product2';
import {
    Product2FromJSON,
    Product2FromJSONTyped,
    Product2ToJSON,
} from './Product2';

/**
 *
 * @export
 * @interface I18nProductCollectionItemAllOf
 */
export interface I18nProductCollectionItemAllOf {
    /**
     *
     * @type {Product2}
     * @memberof I18nProductCollectionItemAllOf
     */
    product?: Product2;
}

/**
 * Check if a given object implements the I18nProductCollectionItemAllOf interface.
 */
export function instanceOfI18nProductCollectionItemAllOf(
    value: object
): boolean {
    let isInstance = true;

    return isInstance;
}

export function I18nProductCollectionItemAllOfFromJSON(
    json: any
): I18nProductCollectionItemAllOf {
    return I18nProductCollectionItemAllOfFromJSONTyped(json, false);
}

export function I18nProductCollectionItemAllOfFromJSONTyped(
    json: any,
    ignoreDiscriminator: boolean
): I18nProductCollectionItemAllOf {
    if (json === undefined || json === null) {
        return json;
    }
    return {
        product: !exists(json, 'product')
            ? undefined
            : Product2FromJSON(json['product']),
    };
}

export function I18nProductCollectionItemAllOfToJSON(
    value?: I18nProductCollectionItemAllOf | null
): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        product: Product2ToJSON(value.product),
    };
}
