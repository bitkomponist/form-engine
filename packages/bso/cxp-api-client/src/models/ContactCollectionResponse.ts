/* tslint:disable */
/* eslint-disable */
/**
 * API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import type { Contact } from './Contact';
import {
    ContactFromJSON,
    ContactFromJSONTyped,
    ContactToJSON,
} from './Contact';

/**
 *
 * @export
 * @interface ContactCollectionResponse
 */
export interface ContactCollectionResponse {
    /**
     * wether the request was successfully executed
     * @type {boolean}
     * @memberof ContactCollectionResponse
     */
    success?: boolean;
    /**
     * parsed reflection of parameters that were used in this request
     * @type {object}
     * @memberof ContactCollectionResponse
     */
    input?: object;
    /**
     *
     * @type {Array<Contact>}
     * @memberof ContactCollectionResponse
     */
    response?: Array<Contact>;
}

/**
 * Check if a given object implements the ContactCollectionResponse interface.
 */
export function instanceOfContactCollectionResponse(value: object): boolean {
    let isInstance = true;

    return isInstance;
}

export function ContactCollectionResponseFromJSON(
    json: any
): ContactCollectionResponse {
    return ContactCollectionResponseFromJSONTyped(json, false);
}

export function ContactCollectionResponseFromJSONTyped(
    json: any,
    ignoreDiscriminator: boolean
): ContactCollectionResponse {
    if (json === undefined || json === null) {
        return json;
    }
    return {
        success: !exists(json, 'success') ? undefined : json['success'],
        input: !exists(json, 'input') ? undefined : json['input'],
        response: !exists(json, 'response')
            ? undefined
            : (json['response'] as Array<any>).map(ContactFromJSON),
    };
}

export function ContactCollectionResponseToJSON(
    value?: ContactCollectionResponse | null
): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        success: value.success,
        input: value.input,
        response:
            value.response === undefined
                ? undefined
                : (value.response as Array<any>).map(ContactToJSON),
    };
}
