/* tslint:disable */
/* eslint-disable */
/**
 * API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import type { Account } from './Account';
import {
    AccountFromJSON,
    AccountFromJSONTyped,
    AccountToJSON,
} from './Account';

/**
 *
 * @export
 * @interface AccountResponse
 */
export interface AccountResponse {
    /**
     * wether the request was successfully executed
     * @type {boolean}
     * @memberof AccountResponse
     */
    success?: boolean;
    /**
     * parsed reflection of parameters that were used in this request
     * @type {object}
     * @memberof AccountResponse
     */
    input?: object;
    /**
     *
     * @type {Account}
     * @memberof AccountResponse
     */
    response?: Account;
}

/**
 * Check if a given object implements the AccountResponse interface.
 */
export function instanceOfAccountResponse(value: object): boolean {
    let isInstance = true;

    return isInstance;
}

export function AccountResponseFromJSON(json: any): AccountResponse {
    return AccountResponseFromJSONTyped(json, false);
}

export function AccountResponseFromJSONTyped(
    json: any,
    ignoreDiscriminator: boolean
): AccountResponse {
    if (json === undefined || json === null) {
        return json;
    }
    return {
        success: !exists(json, 'success') ? undefined : json['success'],
        input: !exists(json, 'input') ? undefined : json['input'],
        response: !exists(json, 'response')
            ? undefined
            : AccountFromJSON(json['response']),
    };
}

export function AccountResponseToJSON(value?: AccountResponse | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        success: value.success,
        input: value.input,
        response: AccountToJSON(value.response),
    };
}
