/* tslint:disable */
/* eslint-disable */
/**
 * API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import type { CollectionItem } from './CollectionItem';
import {
    CollectionItemFromJSON,
    CollectionItemFromJSONTyped,
    CollectionItemToJSON,
} from './CollectionItem';
import type { CollectionItemCollectionResponse } from './CollectionItemCollectionResponse';
import {
    CollectionItemCollectionResponseFromJSON,
    CollectionItemCollectionResponseFromJSONTyped,
    CollectionItemCollectionResponseToJSON,
} from './CollectionItemCollectionResponse';
import type { FindAsset200ResponseAllOf } from './FindAsset200ResponseAllOf';
import {
    FindAsset200ResponseAllOfFromJSON,
    FindAsset200ResponseAllOfFromJSONTyped,
    FindAsset200ResponseAllOfToJSON,
} from './FindAsset200ResponseAllOf';

/**
 *
 * @export
 * @interface FindCollectionItem200Response
 */
export interface FindCollectionItem200Response {
    /**
     * wether the request was successfully executed
     * @type {boolean}
     * @memberof FindCollectionItem200Response
     */
    success?: boolean;
    /**
     * parsed reflection of parameters that were used in this request
     * @type {object}
     * @memberof FindCollectionItem200Response
     */
    input?: object;
    /**
     *
     * @type {Array<CollectionItem>}
     * @memberof FindCollectionItem200Response
     */
    response?: Array<CollectionItem>;
    /**
     * the total number of objects queryable by the current search input
     * @type {number}
     * @memberof FindCollectionItem200Response
     */
    count?: number;
}

/**
 * Check if a given object implements the FindCollectionItem200Response interface.
 */
export function instanceOfFindCollectionItem200Response(
    value: object
): boolean {
    let isInstance = true;

    return isInstance;
}

export function FindCollectionItem200ResponseFromJSON(
    json: any
): FindCollectionItem200Response {
    return FindCollectionItem200ResponseFromJSONTyped(json, false);
}

export function FindCollectionItem200ResponseFromJSONTyped(
    json: any,
    ignoreDiscriminator: boolean
): FindCollectionItem200Response {
    if (json === undefined || json === null) {
        return json;
    }
    return {
        success: !exists(json, 'success') ? undefined : json['success'],
        input: !exists(json, 'input') ? undefined : json['input'],
        response: !exists(json, 'response')
            ? undefined
            : (json['response'] as Array<any>).map(CollectionItemFromJSON),
        count: !exists(json, 'count') ? undefined : json['count'],
    };
}

export function FindCollectionItem200ResponseToJSON(
    value?: FindCollectionItem200Response | null
): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        success: value.success,
        input: value.input,
        response:
            value.response === undefined
                ? undefined
                : (value.response as Array<any>).map(CollectionItemToJSON),
        count: value.count,
    };
}
