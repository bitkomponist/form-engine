/* tslint:disable */
/* eslint-disable */
/**
 * API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import type { ExternalFile } from './ExternalFile';
import {
    ExternalFileFromJSON,
    ExternalFileFromJSONTyped,
    ExternalFileToJSON,
} from './ExternalFile';

/**
 *
 * @export
 * @interface ExternalFileResponse
 */
export interface ExternalFileResponse {
    /**
     * wether the request was successfully executed
     * @type {boolean}
     * @memberof ExternalFileResponse
     */
    success?: boolean;
    /**
     * parsed reflection of parameters that were used in this request
     * @type {object}
     * @memberof ExternalFileResponse
     */
    input?: object;
    /**
     *
     * @type {ExternalFile}
     * @memberof ExternalFileResponse
     */
    response?: ExternalFile;
}

/**
 * Check if a given object implements the ExternalFileResponse interface.
 */
export function instanceOfExternalFileResponse(value: object): boolean {
    let isInstance = true;

    return isInstance;
}

export function ExternalFileResponseFromJSON(json: any): ExternalFileResponse {
    return ExternalFileResponseFromJSONTyped(json, false);
}

export function ExternalFileResponseFromJSONTyped(
    json: any,
    ignoreDiscriminator: boolean
): ExternalFileResponse {
    if (json === undefined || json === null) {
        return json;
    }
    return {
        success: !exists(json, 'success') ? undefined : json['success'],
        input: !exists(json, 'input') ? undefined : json['input'],
        response: !exists(json, 'response')
            ? undefined
            : ExternalFileFromJSON(json['response']),
    };
}

export function ExternalFileResponseToJSON(
    value?: ExternalFileResponse | null
): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        success: value.success,
        input: value.input,
        response: ExternalFileToJSON(value.response),
    };
}
