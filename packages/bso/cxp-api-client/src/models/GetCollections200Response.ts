/* tslint:disable */
/* eslint-disable */
/**
 * API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import type { GetCollections200ResponseResponseInner } from './GetCollections200ResponseResponseInner';
import {
    GetCollections200ResponseResponseInnerFromJSON,
    GetCollections200ResponseResponseInnerFromJSONTyped,
    GetCollections200ResponseResponseInnerToJSON,
} from './GetCollections200ResponseResponseInner';

/**
 *
 * @export
 * @interface GetCollections200Response
 */
export interface GetCollections200Response {
    /**
     *
     * @type {boolean}
     * @memberof GetCollections200Response
     */
    success?: boolean;
    /**
     * available contexts
     * @type {Array<GetCollections200ResponseResponseInner>}
     * @memberof GetCollections200Response
     */
    response?: Array<GetCollections200ResponseResponseInner>;
}

/**
 * Check if a given object implements the GetCollections200Response interface.
 */
export function instanceOfGetCollections200Response(value: object): boolean {
    let isInstance = true;

    return isInstance;
}

export function GetCollections200ResponseFromJSON(
    json: any
): GetCollections200Response {
    return GetCollections200ResponseFromJSONTyped(json, false);
}

export function GetCollections200ResponseFromJSONTyped(
    json: any,
    ignoreDiscriminator: boolean
): GetCollections200Response {
    if (json === undefined || json === null) {
        return json;
    }
    return {
        success: !exists(json, 'success') ? undefined : json['success'],
        response: !exists(json, 'response')
            ? undefined
            : (json['response'] as Array<any>).map(
                  GetCollections200ResponseResponseInnerFromJSON
              ),
    };
}

export function GetCollections200ResponseToJSON(
    value?: GetCollections200Response | null
): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        success: value.success,
        response:
            value.response === undefined
                ? undefined
                : (value.response as Array<any>).map(
                      GetCollections200ResponseResponseInnerToJSON
                  ),
    };
}
