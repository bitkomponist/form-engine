export enum Server {
    Sandbox = 'https://dogw9252yb.execute-api.eu-central-1.amazonaws.com/dev',
    Dev = 'https://crm-sf-api.dev.connected-biking.cloud',
    Stage = 'https://crm-sf-api.stage.connected-biking.cloud',
    Qa = 'https://crm-sf-api.qa.connected-biking.cloud',
    Prod = 'https://crm-sf-api.prod.connected-biking.cloud',
}
