/* tslint:disable */
/* eslint-disable */
/**
 * API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import * as runtime from '../runtime';
import type { CaseErrorResponse, CaseResponse } from '../models';
import {
    CaseErrorResponseFromJSON,
    CaseErrorResponseToJSON,
    CaseResponseFromJSON,
    CaseResponseToJSON,
} from '../models';

export interface GetIdentityCasesRequest {
    limit?: number;
    offset?: number;
    sort?: string;
    search?: string;
    populate?: string;
}

/**
 *
 */
export class CasesApi extends runtime.BaseAPI {
    /**
     * Retrieve current identities cases data
     */
    async getIdentityCasesRaw(
        requestParameters: GetIdentityCasesRequest,
        initOverrides?: RequestInit | runtime.InitOverrideFunction
    ): Promise<runtime.ApiResponse<CaseResponse>> {
        const queryParameters: any = {};

        if (requestParameters.limit !== undefined) {
            queryParameters['limit'] = requestParameters.limit;
        }

        if (requestParameters.offset !== undefined) {
            queryParameters['offset'] = requestParameters.offset;
        }

        if (requestParameters.sort !== undefined) {
            queryParameters['sort'] = requestParameters.sort;
        }

        if (requestParameters.search !== undefined) {
            queryParameters['search'] = requestParameters.search;
        }

        if (requestParameters.populate !== undefined) {
            queryParameters['populate'] = requestParameters.populate;
        }

        const headerParameters: runtime.HTTPHeaders = {};

        if (
            this.configuration &&
            (this.configuration.username !== undefined ||
                this.configuration.password !== undefined)
        ) {
            headerParameters['Authorization'] =
                'Basic ' +
                btoa(
                    this.configuration.username +
                        ':' +
                        this.configuration.password
                );
        }
        if (this.configuration && this.configuration.accessToken) {
            // oauth required
            headerParameters['Authorization'] =
                await this.configuration.accessToken('ciam', []);
        }

        const response = await this.request(
            {
                path: `/action/identity-cases`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            },
            initOverrides
        );

        return new runtime.JSONApiResponse(response, (jsonValue) =>
            CaseResponseFromJSON(jsonValue)
        );
    }

    /**
     * Retrieve current identities cases data
     */
    async getIdentityCases(
        requestParameters: GetIdentityCasesRequest = {},
        initOverrides?: RequestInit | runtime.InitOverrideFunction
    ): Promise<CaseResponse> {
        const response = await this.getIdentityCasesRaw(
            requestParameters,
            initOverrides
        );
        return await response.value();
    }
}
