/* tslint:disable */
/* eslint-disable */
/**
 * API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import * as runtime from '../runtime';
import type { ClearCache200Response, WarmCache200Response } from '../models';
import {
    ClearCache200ResponseFromJSON,
    ClearCache200ResponseToJSON,
    WarmCache200ResponseFromJSON,
    WarmCache200ResponseToJSON,
} from '../models';

export interface ClearCacheRequest {
    tags?: string;
}

/**
 *
 */
export class CacheApi extends runtime.BaseAPI {
    /**
     * Clear api cache for various caching tags, or drop the whole cache alltogether (omit tags query parameter)
     * Clear api cache
     */
    async clearCacheRaw(
        requestParameters: ClearCacheRequest,
        initOverrides?: RequestInit | runtime.InitOverrideFunction
    ): Promise<runtime.ApiResponse<ClearCache200Response>> {
        const queryParameters: any = {};

        if (requestParameters.tags !== undefined) {
            queryParameters['tags'] = requestParameters.tags;
        }

        const headerParameters: runtime.HTTPHeaders = {};

        const response = await this.request(
            {
                path: `/action/clear-cache`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            },
            initOverrides
        );

        return new runtime.JSONApiResponse(response, (jsonValue) =>
            ClearCache200ResponseFromJSON(jsonValue)
        );
    }

    /**
     * Clear api cache for various caching tags, or drop the whole cache alltogether (omit tags query parameter)
     * Clear api cache
     */
    async clearCache(
        requestParameters: ClearCacheRequest = {},
        initOverrides?: RequestInit | runtime.InitOverrideFunction
    ): Promise<ClearCache200Response> {
        const response = await this.clearCacheRaw(
            requestParameters,
            initOverrides
        );
        return await response.value();
    }

    /**
     * Warm up api cache so costly operations dont have to be made during regular requests
     * Warm api cache
     */
    async warmCacheRaw(
        initOverrides?: RequestInit | runtime.InitOverrideFunction
    ): Promise<runtime.ApiResponse<WarmCache200Response>> {
        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        const response = await this.request(
            {
                path: `/action/warm-cache`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            },
            initOverrides
        );

        return new runtime.JSONApiResponse(response, (jsonValue) =>
            WarmCache200ResponseFromJSON(jsonValue)
        );
    }

    /**
     * Warm up api cache so costly operations dont have to be made during regular requests
     * Warm api cache
     */
    async warmCache(
        initOverrides?: RequestInit | runtime.InitOverrideFunction
    ): Promise<WarmCache200Response> {
        const response = await this.warmCacheRaw(initOverrides);
        return await response.value();
    }
}
