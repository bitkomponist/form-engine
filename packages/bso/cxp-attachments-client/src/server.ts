export enum Server {
    Sandbox = 'https://p9p3lf0b0l.execute-api.eu-central-1.amazonaws.com/dev/attachments',
    Dev = 'https://crm-sf-attachments.dev.connected-biking.cloud/attachments',
    Stage = 'https://crm-sf-attachments.stage.connected-biking.cloud/attachments',
    Qa = 'https://crm-sf-attachments.qa.connected-biking.cloud/attachments',
    Prod = 'https://crm-sf-attachments.prod.connected-biking.cloud/attachments',
}
