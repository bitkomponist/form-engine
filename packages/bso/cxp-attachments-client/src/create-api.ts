import { Server } from './server';
import {
    BaseAPI,
    Configuration,
    FetchAPI,
    ConfigurationParameters,
} from './runtime';
import fetch from 'cross-fetch';

export interface CreateApiOptions {
    server?: Server;
    auth?: 'basic' | 'bearer';
    username?: string;
    password?: string;
    accessToken?: string;
    fetchApi?: FetchAPI;
    config?: ConfigurationParameters;
}

export function createApi<T extends BaseAPI>(
    type: new (config?: Configuration) => T,
    options: CreateApiOptions = {}
): T {
    const {
        server: basePath = Server.Sandbox,
        auth,
        username = '',
        password = '',
        accessToken = '',
        fetchApi = fetch,
    } = options;

    const config: ConfigurationParameters = {
        fetchApi,
        basePath,
        ...(options?.config ?? {}),
        headers: {
            accept: 'application/json',
            ...(options?.config?.headers ?? {}),
        },
    };

    if (auth === 'basic') {
        config.username = username;
        config.password = password;
    } else if (auth === 'bearer') {
        config.accessToken = accessToken;
    }

    return new type(new Configuration(config));
}
