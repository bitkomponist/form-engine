import fetchDatasourceBackend from '@form-engine/core-application/datasources/fetch-backend';
import { DatasourceType } from '@form-engine/core-application/types/form';
import FormEngineStaticDatasource, {
    SourceType,
} from '@form-engine/datasource-static/datasources/static-datasource';

const BsoClientCxpApiDatasource: DatasourceType = async ({
    form,
    session,
    definition,
}) => {
    const { $prefetchResult, ...staticOptions } = definition;

    Object.assign(staticOptions, {
        sourceType: SourceType.Json,
    });

    if ($prefetchResult !== undefined) {
        return FormEngineStaticDatasource({
            definition: {
                ...staticOptions,
                source: $prefetchResult,
            },
            form,
            session,
        });
    }

    if (!form?.id)
        throw new Error(
            'tried to invoke datasource with incomplete form definition'
        );

    const { data: source } = await fetchDatasourceBackend(
        form.id,
        definition.id,
        session ?? {}
    );

    return FormEngineStaticDatasource({
        definition: {
            ...staticOptions,
            source,
        },
        form,
        session,
    });
};

BsoClientCxpApiDatasource.apiName = 'BsoCxpApiDatasource';

export default BsoClientCxpApiDatasource as DatasourceType;
