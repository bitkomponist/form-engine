import {
    DatasourceArgs,
    DatasourceType,
} from '@form-engine/core-application/types/form';
import { getFormSessionLocals } from '@form-engine/core-application/utilities/form-session';
import {
    AuthorizationInfoSettings,
    compileRequestBody,
    CompileRequestContentTypes,
} from '@form-engine/core-util/request';
import { Server } from '@form-engine/bso-cxp-api-client/dist';
import { compile } from '@form-engine/core-util/template';
import { HttpError } from '@form-engine/server-application/errors/http';
import { compileCxpAuthHeader } from '@form-engine/bso-core/utilities/compile-auth-header';
import fetch from 'cross-fetch';

export const DEFAULT_SERVER = Server.Sandbox;

async function BsoServerCxpApiDatasource({
    form,
    session,
    definition,
}: DatasourceArgs) {
    const {
        auth = 'none',
        basicAuthUsername = '',
        basicAuthPassword = '',
        bearerToken = '',
        url: path = '',
        method = 'GET',
        contentType = 'application/json',
        body = '',
    } = definition;

    const locals = getFormSessionLocals(form, session ?? {});

    const bsoEnvironment: keyof typeof Server | undefined = (form as any)
        .bsoEnvironment;

    const server = bsoEnvironment ? Server[bsoEnvironment] : DEFAULT_SERVER;

    const headers: any = {
        ...compileCxpAuthHeader(
            {
                auth,
                basicAuthUsername,
                basicAuthPassword,
                bearerToken,
                locals,
            } as AuthorizationInfoSettings,
            form
        ),
    };

    const hasBody = ['POST', 'PATCH', 'PUT'].includes(method);

    if (hasBody && contentType) {
        headers['content-type'] = contentType;
    }

    let result = '';
    if (path && server) {
        let compiledPath = compile(path, locals, '').trim();
        if (compiledPath.startsWith('/')) {
            compiledPath = compiledPath.slice(1);
        }

        const url = `${server}/${compiledPath}`;

        const response = await fetch(url, {
            method,
            body: hasBody
                ? compileRequestBody(
                      contentType as CompileRequestContentTypes,
                      body,
                      locals
                  )
                : undefined,
            headers,
        });

        if (response.status < 200 || response.status >= 400) {
            const message = await response.text();
            throw new HttpError(message, response.status);
        }

        result = await response.text();
    }

    return {
        data: result,
    };
}

BsoServerCxpApiDatasource.apiName = 'BsoCxpApiDatasource';

export default BsoServerCxpApiDatasource as DatasourceType;
