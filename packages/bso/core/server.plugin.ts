import { ServerApplicationPlugin } from '@form-engine/server-application/typings';
export default {
    apiEndpoints: {
        Form: {
            model: {
                schema: {
                    fields: {
                        bsoEnvironment: {
                            type: String,
                        },
                    },
                },
            },
        },
    },
} as unknown as ServerApplicationPlugin;
