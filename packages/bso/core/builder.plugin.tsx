import BsoFormMaskExtension from './components/form-mask-extension';
import { BuilderPlugin } from '@form-engine/builder-application';
import * as translations from './builder-translations';

export default {
    translations,
    componentExtensions: {
        BuilderFormMask: {
            BsoFormMaskExtension: {
                placement: 'after',
                component: BsoFormMaskExtension,
            },
        },
    },
    maskConfigurations: {
        BuilderFormMask: {
            edit: {
                permissions: {
                    bsoEnvironment: 'write',
                    bsoAuth: 'write',
                    bsoBasicAuthUsername: 'write',
                    bsoBasicAuthPassword: 'write',
                    bsoBearerToken: 'write',
                },
            },
            preview: {
                permissions: {
                    bsoEnvironment: 'write',
                    bsoAuth: 'write',
                    bsoBasicAuthUsername: 'write',
                    bsoBasicAuthPassword: 'write',
                    bsoBearerToken: 'write',
                },
            },
            translate: {
                permissions: {
                    bsoEnvironment: 'read',
                    bsoAuth: 'read',
                    bsoBasicAuthUsername: 'read',
                    bsoBasicAuthPassword: 'read',
                    bsoBearerToken: 'read',
                },
            },
        },
    },
} as BuilderPlugin;
