import { FormDefinition } from '@form-engine/core-application/types/form';
import {
    AuthorizationInfoSettings,
    compileAuthorizationHeader,
} from '@form-engine/core-util/request';

export function compileCxpAuthHeader(
    settings?: AuthorizationInfoSettings,
    form?: FormDefinition
) {
    const authSettings = { ...settings };

    // if no auth is provided in the settings, try using globally defined auth of
    // the project
    if (!authSettings.auth && form) {
        const {
            bsoAuth: auth,
            bsoBasicAuthUsername: basicAuthUsername,
            bsoBasicAuthPassword: basicAuthPassword,
            bsoBearerToken: bearerToken,
        } = form;

        Object.assign(authSettings, {
            auth,
            basicAuthUsername,
            basicAuthPassword,
            bearerToken,
        });
    }

    return compileAuthorizationHeader(authSettings);
}
