import { HttpError } from '@form-engine/server-application/errors/http';

export default async function convertResponseError<T extends Error>(error: T) {
    const { response } = error as any;
    const httpError = new HttpError(error.message, 500);
    httpError.stack = error.stack;
    if (!response) return httpError;
    try {
        const { message = '', stack = [] } = await response.json();
        Object.assign(httpError, {
            message,
            stack: Array.isArray(stack) ? stack.join('\n\r') : stack,
        });
        return httpError;
    } catch (_e) {
        console.log(_e);
        return httpError;
    }
}
