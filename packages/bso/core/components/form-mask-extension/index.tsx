import React from 'react';
import BuilderMaskGroup from '@form-engine/builder-mask/components/mask/group';
import BuilderEnumProperty from '@form-engine/builder-mask/components/properties/enum';
import { useMaskContext } from '@form-engine/builder-mask/hooks/use-mask';
import BuilderStringProperty from '@form-engine/builder-mask/components/properties/string';
import { i18n } from '@form-engine/builder-application';

export default function BsoFormMaskExtension() {
    const { state } = useMaskContext();

    const { c } = i18n.prefix('mask.BsoCore.');

    return (
        <BuilderMaskGroup label={c('formGroupTitle')}>
            <BuilderEnumProperty
                propertyName="bsoEnvironment"
                label={c('bsoEnvironment')}
                options={[
                    ['', 'Sandbox'],
                    ['Dev'],
                    ['Stage'],
                    ['Qa'],
                    ['Prod'],
                ]}
            />

            <BuilderEnumProperty
                propertyName="bsoAuth"
                label={c('bsoAuth')}
                options={[['', 'none'], ['basic'], ['bearer']]}
            />
            {state.bsoAuth === 'basic' && (
                <>
                    <BuilderStringProperty
                        compilable
                        propertyName="bsoBasicAuthUsername"
                        label={c('bsoBasicAuthUsername')}
                    />
                    <BuilderStringProperty
                        compilable
                        propertyName="bsoBasicAuthPassword"
                        label={c('bsoBasicAuthPassword')}
                        type="password"
                    />
                </>
            )}
            {state.bsoAuth === 'bearer' && (
                <>
                    <BuilderStringProperty
                        compilable
                        propertyName="bsoBearerToken"
                        label={c('bsoBearerToken')}
                    />
                </>
            )}
        </BuilderMaskGroup>
    );
}
