import { FormEnginePlugin } from '@form-engine/core-application';
import Datasource from './datasources/client-bso-diagnostic-report-datasource';
export default {
    datasources: {
        [Datasource.apiName]: Datasource,
    },
} as FormEnginePlugin;
