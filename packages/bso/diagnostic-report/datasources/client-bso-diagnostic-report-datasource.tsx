import fetchDatasourceBackend from '@form-engine/core-application/datasources/fetch-backend';
import { DatasourceType } from '@form-engine/core-application/types/form';
import FormEngineStaticDatasource, {
    SourceType,
    FormEngineStaticDatasourceArgs,
} from '@form-engine/datasource-static/datasources/static-datasource';

interface BsoDiagnosticReportDatasourceArgs
    extends FormEngineStaticDatasourceArgs {
    bsoTransactionIdPath?: string;
}

async function BsoClientDiagnosticReportDatasource({
    form,
    session,
    definition,
}: BsoDiagnosticReportDatasourceArgs) {
    const { $prefetchResult, ...staticOptions } = definition;

    Object.assign(staticOptions, {
        sourceType: SourceType.Json,
        valueColumn: '{{$i.partNumber}}',
        labelColumn: '{{$i.label}}',
        resultPath: 'response.products',
        constantsPath: 'bso.diagnosticReport',
    });

    if ($prefetchResult !== undefined) {
        return FormEngineStaticDatasource({
            definition: {
                ...staticOptions,
                source: $prefetchResult,
            },
            form,
            session,
        });
    }

    if (!form?.id)
        throw new Error(
            'tried to invoke datasource with incomplete form definition'
        );

    let source = '';

    // skip loading when editing
    if (!['translate', 'edit'].includes(session?.editMode ?? '')) {
        const { data } = await fetchDatasourceBackend(
            form.id,
            definition.id,
            session ?? {}
        );
        source = data;
    }

    return FormEngineStaticDatasource({
        definition: {
            ...staticOptions,
            source,
        },
        form,
        session,
    });
}

export default Object.assign(BsoClientDiagnosticReportDatasource, {
    apiName: 'BsoDiagnosticReportDatasource',
    isGlobal: true,
}) as DatasourceType;
