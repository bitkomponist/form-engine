import {
    DatasourceArgs,
    DatasourceDefinition,
    DatasourceType,
} from '@form-engine/core-application/types/form';
import { getFormSessionLocals } from '@form-engine/core-application/utilities/form-session';
import { SourceType } from '@form-engine/datasource-static/datasources/static-datasource';
import get from 'lodash.get';
import {
    AuthorizationInfoSettings,
    compileAuthorizationInfo,
} from '@form-engine/core-util/request';
import {
    DiagnosticReportApi,
    createApi,
    Server,
} from '@form-engine/bso-cxp-api-client/dist';
import convertResponseError from '@form-engine/bso-core/utilities/convert-response-error';

export const DEFAULT_TRANSACTION_ID_PATH = 'transactionId';

export const DEFAULT_SERVER = Server.Sandbox;

interface BsoDiagnosticReportDatasourceArgs extends DatasourceArgs {
    definition: {
        bsoTransactionIdPath?: string;
    } & DatasourceDefinition;
}

async function BsoServerDiagnosticReportDatasource({
    form,
    session,
    definition,
}: BsoDiagnosticReportDatasourceArgs) {
    const { bsoTransactionIdPath, ...staticOptions } = definition;

    const {
        bsoAuth: auth = 'none',
        bsoBasicAuthUsername: basicAuthUsername = '',
        bsoBasicAuthPassword: basicAuthPassword = '',
        bsoBearerToken: bearerToken = '',
    } = form;

    Object.assign(staticOptions, {
        sourceType: SourceType.Json,
        valueColumn: '{{$i.partKey}}',
        labelColumn: '{{$i.label}}',
        resultPath: 'response.products',
    });

    const locals = getFormSessionLocals(form, session ?? {});

    const bsoEnvironment: keyof typeof Server | undefined = (form as any)
        .bsoEnvironment;
    const server = bsoEnvironment ? Server[bsoEnvironment] : DEFAULT_SERVER;

    const transactionId = get(
        locals,
        bsoTransactionIdPath || DEFAULT_TRANSACTION_ID_PATH
    );

    let source = '';

    if (server && transactionId) {
        const diagnosticReport = createApi(DiagnosticReportApi, {
            server,
            ...compileAuthorizationInfo({
                auth,
                basicAuthUsername,
                basicAuthPassword,
                bearerToken,
                locals,
            } as AuthorizationInfoSettings),
        });

        try {
            const result = await diagnosticReport.getCreateCaseFormState({
                transactionId,
            });

            if (!result.success) {
                throw new Error(result?.response || (result as any)?.message);
            }

            source = JSON.stringify(result);
        } catch (error) {
            throw await convertResponseError(error as Error);
        }
    }

    return {
        data: source,
    };
}

BsoServerDiagnosticReportDatasource.apiName = 'BsoDiagnosticReportDatasource';

export default BsoServerDiagnosticReportDatasource as DatasourceType;
