import { useFormEditorContext } from '@form-engine/builder-form-editor/hooks/use-form-editor';
import { useMaskContext } from '@form-engine/builder-mask/hooks/use-mask';
import React from 'react';
import { useCallback } from 'react';
import { Col, Row } from 'react-bootstrap';
import BuilderDatasourceDataTable from '@form-engine/builder-application/components/datasource-data-table';
import BuilderDrawerTabs from '@form-engine/builder-application/components/drawer-tabs';
import BuilderMaskGroup from '@form-engine/builder-mask/components/mask/group';
import BuilderStringProperty from '@form-engine/builder-mask/components/properties/string';
import BuilderNameProperty from '@form-engine/builder-mask/components/properties/name';

export function BuilderBsoDiagnosticReportDatasourceSetup() {
    const { state } = useMaskContext();

    return (
        <>
            <BuilderMaskGroup>
                <BuilderNameProperty />
                <BuilderStringProperty
                    propertyName="description"
                    label="Description"
                />
                <BuilderStringProperty
                    propertyName="bsoTransactionIdPath"
                    label="Transaction Id Path"
                    defaultValue="transactionId"
                />
            </BuilderMaskGroup>
        </>
    );
}

export default function BuilderBsoDiagnosticReportDatasourceMask({
    id,
}: {
    id: string;
}) {
    const { state: editor = {}, setItem: setEditorItem } =
        useFormEditorContext();
    const { tab = 'setup' } = (editor.datasources || {})[id] || {};
    const setTab = useCallback(
        (tab: string) => {
            const { datasources = {} } = editor;
            const { [id]: datasource = {} } = datasources;
            setEditorItem('datasources', {
                ...datasources,
                [id]: { ...datasource, tab },
            });
        },
        [editor, setEditorItem, id]
    );

    return (
        <>
            <BuilderDrawerTabs
                activeKey={tab}
                items={{ setup: 'Setup', data: 'Browse' }}
                onChange={setTab}
            />
            <div className="feb-view">
                {tab === 'setup' && (
                    <div className="feb-view-inner">
                        <Row>
                            <Col xl={{ span: 8, offset: 2 }}>
                                <BuilderBsoDiagnosticReportDatasourceSetup />
                            </Col>
                        </Row>
                    </div>
                )}
                {tab === 'data' && (
                    <div className="feb-view-inner p-0">
                        <BuilderDatasourceDataTable id={id} />
                    </div>
                )}
            </div>
        </>
    );
}
