import { ServerApplicationPlugin } from '@form-engine/server-application/typings';
import Datasource from './datasources/server-bso-diagnostic-report-datasource';
export default {
    datasources: {
        [Datasource.apiName]: Datasource,
    },
} as ServerApplicationPlugin;
