import { default as Mask } from './components/bso-diagnostic-report-datasource-mask';
import { default as Datasource } from './datasources/client-bso-diagnostic-report-datasource';
import { HddStack as Icon } from 'react-bootstrap-icons';
import { BuilderPlugin } from '@form-engine/builder-application';
import * as translations from './builder-translations';

export default {
    translations,
    datasources: {
        [Datasource.apiName]: {
            label: `${Datasource.apiName}.title`,
            description: `${Datasource.apiName}.description`,
            exposesOptions: true,
            Icon,
            Datasource,
            Mask,
        },
    },
} as BuilderPlugin;
