import { FormEnginePlugin } from '@form-engine/core-application';
import Datasource from './datasources/client-collection-api-datasource';
export default {
    datasources: {
        [Datasource.apiName]: Datasource,
    },
} as FormEnginePlugin;
