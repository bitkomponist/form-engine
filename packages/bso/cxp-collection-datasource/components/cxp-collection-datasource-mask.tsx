import { useFormEditorContext } from '@form-engine/builder-form-editor/hooks/use-form-editor';
import React from 'react';
import { useCallback } from 'react';
import { Col, Row } from 'react-bootstrap';
import BuilderDatasourceDataTable from '@form-engine/builder-application/components/datasource-data-table';
import BuilderDrawerTabs from '@form-engine/builder-application/components/drawer-tabs';
import BuilderMaskGroup from '@form-engine/builder-mask/components/mask/group';
import BuilderStringProperty from '@form-engine/builder-mask/components/properties/string';
import BuilderNameProperty from '@form-engine/builder-mask/components/properties/name';
import Datasource from '../datasources/client-collection-api-datasource';
import { i18n } from '@form-engine/builder-application';
import BuilderAuthProperty from '@form-engine/builder-mask/components/properties/auth';

export function BsoCxpCollectionDatasourceSetup() {
    const { c } = i18n.prefix(`mask.${Datasource.apiName}.`);

    return (
        <>
            <BuilderMaskGroup>
                <BuilderNameProperty />
                <BuilderStringProperty
                    propertyName="description"
                    label={c('description')}
                />
                <BuilderStringProperty
                    compilable
                    propertyName="bsoContext"
                    label={c('bsoContext')}
                    placeholder={c('bsoContextPlaceholder')}
                />
            </BuilderMaskGroup>
            <BuilderMaskGroup label={c('authGroupTitle')}>
                <BuilderAuthProperty />
            </BuilderMaskGroup>
        </>
    );
}

export default function BsoCxpCollectionDatasourceMask({ id }: { id: string }) {
    const { c } = i18n.prefix(`mask.${Datasource.apiName}.`);
    const { state: editor = {}, setItem: setEditorItem } =
        useFormEditorContext();
    const { tab = 'setup' } = (editor.datasources || {})[id] || {};
    const setTab = useCallback(
        (tab: string) => {
            const { datasources = {} } = editor;
            const { [id]: datasource = {} } = datasources;
            setEditorItem('datasources', {
                ...datasources,
                [id]: { ...datasource, tab },
            });
        },
        [editor, setEditorItem, id]
    );

    return (
        <>
            <BuilderDrawerTabs
                activeKey={tab}
                items={{ setup: c('setup'), data: c('data') }}
                onChange={setTab}
            />
            <div className="feb-view">
                {tab === 'setup' && (
                    <div className="feb-view-inner">
                        <Row>
                            <Col xl={{ span: 8, offset: 2 }}>
                                <BsoCxpCollectionDatasourceSetup />
                            </Col>
                        </Row>
                    </div>
                )}
                {tab === 'data' && (
                    <div className="feb-view-inner p-0">
                        <BuilderDatasourceDataTable id={id} />
                    </div>
                )}
            </div>
        </>
    );
}
