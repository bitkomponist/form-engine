import {
    DatasourceArgs,
    DatasourceType,
} from '@form-engine/core-application/types/form';
import { getFormSessionLocals } from '@form-engine/core-application/utilities/form-session';
import { AuthorizationInfoSettings } from '@form-engine/core-util/request';
import { Server } from '@form-engine/bso-cxp-api-client/dist';
import { HttpError } from '@form-engine/server-application/errors/http';
import { compileCxpAuthHeader } from '@form-engine/bso-core/utilities/compile-auth-header';
import fetch from 'cross-fetch';

export const DEFAULT_SERVER = Server.Sandbox;

async function BsoServerCxpCollectionDatasource({
    form,
    session,
    definition,
}: DatasourceArgs) {
    const {
        auth = '',
        basicAuthUsername = '',
        basicAuthPassword = '',
        bearerToken = '',
        bsoContext,
    } = definition;

    const locals = getFormSessionLocals(form, session ?? {});

    const { locale = 'default' } = locals;

    const bsoEnvironment: keyof typeof Server | undefined = (form as any)
        .bsoEnvironment;

    const server = bsoEnvironment ? Server[bsoEnvironment] : DEFAULT_SERVER;

    const headers: any = {
        ...compileCxpAuthHeader(
            {
                auth,
                basicAuthUsername,
                basicAuthPassword,
                bearerToken,
                locals,
            } as AuthorizationInfoSettings,
            form
        ),
        accept: 'application/json',
    };

    let result = '';

    if (server && bsoContext) {
        const url = `${server}/action/collections/${encodeURIComponent(
            bsoContext as string
        )}/${encodeURIComponent(locale as string)}`;

        const response = await fetch(url, {
            method: 'GET',
            headers,
        });

        if (response.status < 200 || response.status >= 400) {
            const message = await response.text();
            throw new HttpError(message, response.status);
        }

        result = await response.text();
    }

    return {
        data: result,
    };
}

BsoServerCxpCollectionDatasource.apiName = 'BsoCxpCollectionDatasource';

export default BsoServerCxpCollectionDatasource as DatasourceType;
