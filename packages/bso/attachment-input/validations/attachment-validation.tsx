import FormEngine from '@form-engine/core-application';
import { ValidationType } from '@form-engine/core-application/types/form';

const BsoAttachmentValidation: ValidationType = (element, value, session) => {
    const uploadSession: any = session.elements?.[element.id] ?? {};
    const files: FileList | undefined = uploadSession?.currentFileList;
    const { c } = FormEngine.i18n;
    const label = element?.label ?? element?.name;

    if (!files) return null;

    if (element.validationMaxAttachments) {
        const max = Number(element.validationMaxAttachments);
        if (files.length > max)
            return c('validation.maxAttachments', { label, max });
    }

    if (element.validationMaxAttachments) {
        const min = Number(element.validationMinAttachments);
        if (files.length < min)
            return c('validation.minAttachments', {
                label,
                min: element.validationMinAttachments,
            });
    }

    if (element.validationMaxAttachmentSize) {
        const max = Number(element.validationMaxAttachmentSize) * 1024 * 1024;

        for (const file of files) {
            if (file.size > max) {
                return c('validation.maxAttachmentSize', {
                    label,
                    max: element.validationMaxAttachmentSize,
                });
            }
        }
    }

    return null;
};

export default BsoAttachmentValidation;
