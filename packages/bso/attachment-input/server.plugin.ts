import { ServerApplicationPlugin } from '@form-engine/server-application/typings';
import Controller from './controllers/attachments-element-controller';
export default {
    elementControllers: {
        [Controller.apiName]: Controller,
    },
} as ServerApplicationPlugin;
