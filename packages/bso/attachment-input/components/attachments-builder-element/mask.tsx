import React from 'react';
import BuilderElementMask, {
    BuilderElementMaskProps,
} from '@form-engine/builder-mask/components/mask';
import BuilderMaskGroup from '@form-engine/builder-mask/components/mask/group';
import BuilderMaskGroupAppearance from '@form-engine/builder-mask/components/groups/appearance';
import BuilderMaskGroupConditions from '@form-engine/builder-mask/components/groups/conditions';
import BuilderMaskGroupValidation from '@form-engine/builder-mask/components/groups/validation';
import BuilderBooleanProperty from '@form-engine/builder-mask/components/properties/boolean';
import BuilderEnumProperty from '@form-engine/builder-mask/components/properties/enum';
import BuilderLabelProperty from '@form-engine/builder-mask/components/properties/label';
import BuilderStringProperty from '@form-engine/builder-mask/components/properties/string';
import {
    AttachmentOriginEnum,
    AttachmentCategoryEnum,
} from '@form-engine/bso-cxp-attachments-client/dist/index';
import { useMaskContext } from '@form-engine/builder-mask/hooks/use-mask';
import { Element } from '.';
import { i18n } from '@form-engine/builder-application';
const { FormHandler: DefaultOrigin, ...Origins } = AttachmentOriginEnum;
const { Other: DefaultCateogry, ...Categories } = AttachmentCategoryEnum;

export default function BuilderInputElementMask(
    props: BuilderElementMaskProps
) {
    const { state: mask } = useMaskContext();

    const { c } = i18n.prefix(`mask.${Element.apiName}.`);

    return (
        <BuilderElementMask id={Element.apiName} {...props}>
            <BuilderMaskGroupAppearance />
            <BuilderMaskGroup label={c('groupTitle')}>
                <BuilderLabelProperty />
                <BuilderBooleanProperty
                    propertyName="showError"
                    label={c('showError')}
                />
                <BuilderEnumProperty
                    propertyName="bsoOrigin"
                    label={c('bsoOrigin')}
                    options={[
                        ['', DefaultOrigin],
                        ...Object.values(Origins).map((value) => [value]),
                    ]}
                />
                <BuilderEnumProperty
                    propertyName="bsoCategory"
                    label={c('bsoCategory')}
                    options={[
                        ['', DefaultCateogry],
                        ...Object.values(Categories).map((value) => [value]),
                    ]}
                />
                <BuilderStringProperty
                    propertyName="accept"
                    label={c('accept')}
                    placeholder={c('acceptPlaceholder')}
                />
                <BuilderBooleanProperty
                    propertyName="multiple"
                    label={c('multiple')}
                />
            </BuilderMaskGroup>
            <BuilderMaskGroupValidation contentType={false} length={false}>
                <BuilderStringProperty
                    type="number"
                    step="1"
                    propertyName="validationMaxAttachmentSize"
                    label={c('validationMaxAttachmentSize')}
                />
                {mask.multiple && (
                    <BuilderStringProperty
                        type="number"
                        step="1"
                        propertyName="validationMaxAttachments"
                        label={c('validationMaxAttachments')}
                    />
                )}
            </BuilderMaskGroupValidation>
            <BuilderMaskGroupConditions />
        </BuilderElementMask>
    );
}
