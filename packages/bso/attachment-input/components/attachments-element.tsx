import React, { useCallback, useEffect, useState } from 'react';

import FormEngineInputElement, {
    FormEngineInputElementProps,
} from '@form-engine/element-input/components/input-element';
import {
    useFormEngineElement,
    useFormEngineElementController,
} from '@form-engine/core-application/hooks/use-element';
import { ElementDefinition } from '@form-engine/core-application/types/form';
import { Server } from '@form-engine/bso-cxp-attachments-client/dist';
import { useFormElementSession } from '@form-engine/core-application/hooks/use-formsession';

export interface BsoAttachmentsElementDef extends ElementDefinition {
    multiple?: boolean;
    accept?: string;
    bsoServer?: Server;
    validationMaxFileSize?: string;
    validationMinLength?: string;
    validationMaxLength?: string;
}

function createAttachmentIndices(
    controller: (data: any) => Promise<any>,
    files: FileList
) {
    const indices = [];

    for (const file of files) {
        indices.push(
            controller({
                action: 'createAttachmentIndex',
                fileName: file.name,
                fileSize: file.size,
            })
        );
    }

    return Promise.all(indices);
}

async function uploadFile(presignedUploadUrl: string, file: File) {
    await fetch(presignedUploadUrl, {
        method: 'PUT',
        headers: { 'Content-Type': file.type },
        body: file,
    });
}

export default Object.assign(
    function BsoAttachmentsElement(props: FormEngineInputElementProps) {
        const { state: element } = useFormEngineElement();
        if (!element)
            throw new Error('tried to render input without definition');
        const { multiple = false, accept } =
            element as BsoAttachmentsElementDef;
        const controller = useFormEngineElementController();
        const [session, setSession] = useFormElementSession();

        const handleChange = useCallback(
            (e: React.ChangeEvent) => {
                const { files } = e.target as HTMLInputElement;

                if (files && files?.length) {
                    setSession({
                        ...session,
                        loading: true,
                        isTouched: true,
                        value: undefined,
                        error: undefined,
                        currentFileList: files,
                    });
                } else {
                    setSession({
                        ...session,
                        loading: false,
                        isTouched: true,
                        value: undefined,
                        error: undefined,
                        currentFileList: undefined,
                    });
                }
            },
            [setSession, session]
        );

        useEffect(() => {
            // wait for validation to settle
            if (session?.error === undefined) return;

            const currentFiles: FileList | undefined =
                session?.currentFileList as any;

            // wait for valid file list
            if (!currentFiles?.length || session?.error !== null) {
                if (session?.loading) {
                    // mark element as idle if no upload takes place
                    setSession({ ...session, loading: false });
                }
                return;
            }

            let mounted = true;

            (async () => {
                const indices = await createAttachmentIndices(
                    controller,
                    currentFiles
                );
                const uploads = [];

                for (const [key, attachmentIndex] of indices.entries()) {
                    const file = currentFiles[key];
                    uploads.push(
                        uploadFile(attachmentIndex.uploadUrl as string, file)
                    );
                }

                await Promise.all(uploads);

                const value = JSON.stringify(
                    indices.map(({ attachmentId }) => attachmentId)
                );

                if (!mounted) return;

                setSession({
                    ...session,
                    loading: false,
                    currentFileList: undefined,
                    value,
                });
            })();
            return () => {
                mounted = false;
            };
        }, [session?.currentFileList, session?.error, setSession, controller]);

        return (
            <FormEngineInputElement
                {...props}
                value={undefined}
                disabled={Boolean(session?.loading)}
                type="file"
                multiple={multiple}
                accept={accept}
                onChange={handleChange as any}
            />
        );
    },
    { apiName: 'BsoAttachmentsElement', isField: true, enabled: true }
);
