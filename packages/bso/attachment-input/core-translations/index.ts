/**
 * Auto generated import statements, do not modify this file manually
 */

export { default as default } from './default.json';

export { default as de } from './de.json';

export { default as es } from './es.json';

export { default as it } from './it.json';

export { default as fr } from './fr.json';

export { default as no } from './no.json';

export { default as sv } from './sv.json';

export { default as da } from './da.json';

export { default as fi } from './fi.json';

export { default as nl } from './nl.json';

export { default as pl } from './pl.json';

export { default as cs } from './cs.json';

export { default as ja } from './ja.json';

export { default as ko } from './ko.json';
