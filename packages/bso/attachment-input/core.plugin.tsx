import { FormEnginePlugin } from '@form-engine/core-application';
import Element from './components/attachments-element';
import BsoAttachmentValidation from './validations/attachment-validation';
import * as translations from './core-translations';

export default {
    elements: {
        [Element.apiName]: Element,
    },
    validations: {
        BsoAttachmentValidation: BsoAttachmentValidation,
    },
    translations,
} as FormEnginePlugin;
