import {
    ElementControllerType,
    ElementControllerArgs,
} from '@form-engine/core-application/types/form';
import {
    createApi,
    AttachmentApi,
    BaseAPI,
    Server,
    AttachmentDTO,
    AttachmentOriginEnum,
    AttachmentCategoryEnum,
} from '@form-engine/bso-cxp-attachments-client/dist/index';
import { getFormSessionLocals } from '@form-engine/core-application/utilities/form-session';
import { compileAuthorizationInfo } from '@form-engine/core-util/request';
import convertResponseError from '@form-engine/bso-core/utilities/convert-response-error';

const DEFAULT_SERVER = Server.Sandbox;

class BsoAttachmentsElementController {
    static invoke: ElementControllerType = Object.assign(
        (args: ElementControllerArgs) => {
            const action = `${args?.data?.action ?? 'index'}Action`;
            const instance = new BsoAttachmentsElementController(args);
            if (!action || typeof (instance as any)[action] !== 'function') {
                throw new Error(`invalid action ${action}`);
            }
            return (instance as any)[action]() as Promise<any>;
        },
        {
            apiName: 'BsoAttachmentsElementController',
        }
    );
    private constructor(public args: ElementControllerArgs) {}

    get server() {
        const bsoEnvironment: keyof typeof Server | undefined = (
            this.args.form as any
        ).bsoEnvironment;
        return bsoEnvironment ? Server[bsoEnvironment] : DEFAULT_SERVER;
    }

    private createApi<T extends BaseAPI>(type: new (...args: any) => T) {
        const {
            bsoAuth: auth,
            bsoBasicAuthUsername: basicAuthUsername,
            bsoBasicAuthPassword: basicAuthPassword,
            bsoBearerToken: bearerToken,
        } = this.args.form as any;

        return createApi(type, {
            ...compileAuthorizationInfo({
                auth,
                basicAuthUsername,
                basicAuthPassword,
                bearerToken,
                locals: getFormSessionLocals(this.args.form, this.args.session),
            }),
            server: this.server,
        });
    }

    async createAttachmentIndexAction() {
        const { fileName } = this.args.data ?? {};

        const {
            bsoCategory: category,
            bsoOrigin: origin,
        }: {
            bsoCategory?: AttachmentCategoryEnum;
            bsoOrigin?: AttachmentOriginEnum;
        } = this.args.definition as any;

        const attachment: AttachmentDTO = {
            fileName,
            origin: origin || AttachmentOriginEnum.FormHandler,
            category: category || AttachmentCategoryEnum.Other,
        };

        try {
            const [response] = await this.createApi(
                AttachmentApi
            ).addAttachment({ body: [attachment] });
            return response?.response;
        } catch (error) {
            throw await convertResponseError(error as Error);
        }
    }
}

export default BsoAttachmentsElementController.invoke;
