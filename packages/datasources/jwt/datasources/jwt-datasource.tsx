import {
    DatasourceArgs,
    DatasourceType,
} from '@form-engine/core-application/types/form';
import { getFormSessionLocals } from '@form-engine/core-application/utilities/form-session';
import get from 'lodash.get';
import set from 'lodash.set';

export const DEFAULT_JWT_PATH = 'request.headers.authorization';
export const DEFAULT_OUTPUT_PATH = 'jwt';

async function FormEngineJwtDatasource(options: DatasourceArgs) {
    let data = {};
    let error;

    const locals = getFormSessionLocals(options.form, options?.session ?? {});

    const token = get(
        locals,
        options?.definition?.jwtSource || DEFAULT_JWT_PATH
    );

    if (token) {
        const [, payload] = token.split('.');
        if (payload) {
            try {
                data = JSON.parse(atob(payload));
            } catch (e) {
                error = e;
            }
        }
    }

    // console.log({options,locals,token,data});

    const constants = set(
        {},
        options?.definition?.jwtOutput || DEFAULT_OUTPUT_PATH,
        data
    );

    return Promise.resolve({
        data,
        error,
        options: [],
        constants,
    });
}

export default Object.assign(FormEngineJwtDatasource, {
    apiName: 'FormEngineJwtDatasource',
    isGlobal: true,
}) as DatasourceType;
