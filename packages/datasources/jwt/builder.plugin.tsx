import { default as Mask } from './components/jwt-datasource-mask';
import { default as Datasource } from './datasources/jwt-datasource';
import { FilePerson as Icon } from 'react-bootstrap-icons';
import { BuilderPlugin } from '@form-engine/builder-application';
import DefaultJwtReaderDatasourcePreset from './presets/default-jwt-reader';
import * as translations from './builder-translations';

export default {
    translations,
    datasources: {
        [Datasource.apiName]: {
            label: `${Datasource.apiName}.title`,
            description: `${Datasource.apiName}.description`,
            exposesOptions: false,
            Icon,
            Datasource,
            Mask,
        },
    },
    formPresets: {
        DefaultJwtReaderDatasourcePreset,
    },
} as BuilderPlugin;
