import React from 'react';
import { Col, Row } from 'react-bootstrap';
import BuilderMaskGroup from '@form-engine/builder-mask/components/mask/group';
import BuilderStringProperty from '@form-engine/builder-mask/components/properties/string';
import {
    DEFAULT_JWT_PATH,
    DEFAULT_OUTPUT_PATH,
} from '../datasources/jwt-datasource';
import BuilderNameProperty from '@form-engine/builder-mask/components/properties/name';
import Datasource from '../datasources/jwt-datasource';
import { i18n } from '@form-engine/builder-application';

export function BuilderJwtDatasourceSetup() {
    const { c } = i18n.prefix(`mask.${Datasource.apiName}.`);

    return (
        <>
            <BuilderMaskGroup>
                <BuilderNameProperty />
                <BuilderStringProperty
                    propertyName="description"
                    label={c('description')}
                />
            </BuilderMaskGroup>
            <BuilderMaskGroup label="JWT">
                <BuilderStringProperty
                    propertyName="jwtSource"
                    label={c('jwtSource')}
                    defaultValue={DEFAULT_JWT_PATH}
                />
                <BuilderStringProperty
                    propertyName="jwtOutput"
                    label={c('jwtOutput')}
                    defaultValue={DEFAULT_OUTPUT_PATH}
                />
            </BuilderMaskGroup>
        </>
    );
}

export default function BuilderJwtDatasourceMask() {
    return (
        <>
            <div className="feb-view">
                <div className="feb-view-inner">
                    <Row>
                        <Col xl={{ span: 8, offset: 2 }}>
                            <BuilderJwtDatasourceSetup />
                        </Col>
                    </Row>
                </div>
            </div>
        </>
    );
}
