import { FormPreset } from '@form-engine/builder-application/types/form-preset';
import uuidv4 from '@form-engine/core-util/uuid';
import FormEngineJwtDatasource from '../datasources/jwt-datasource';

export default {
    name: 'Default Jwt Reader',
    required: true,
    init(form) {
        form?.datasources?.push({
            id: uuidv4(),
            type: FormEngineJwtDatasource.apiName,
            name: 'Jwt Reader',
            description:
                'Provides given json webtoken contents inside of the forms local variables',
        });
    },
} as FormPreset;
