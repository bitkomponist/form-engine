import { default as Mask } from './components/static-datasource-mask';
import { default as Datasource } from './datasources/static-datasource';
import { FileSpreadsheet as Icon } from 'react-bootstrap-icons';
import { BuilderPlugin } from '@form-engine/builder-application';
import * as translations from './builder-translations';

export default {
    translations,
    datasources: {
        [Datasource.apiName]: {
            label: `${Datasource.apiName}.title`,
            description: `${Datasource.apiName}.description`,
            exposesOptions: true,
            Icon,
            Datasource,
            Mask,
        },
    },
} as BuilderPlugin;
