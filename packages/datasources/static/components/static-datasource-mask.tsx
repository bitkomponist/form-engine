import React from 'react';
import { useCallback } from 'react';
import { Col, Row } from 'react-bootstrap';
import { useFormEditorContext } from '@form-engine/builder-form-editor/hooks/use-form-editor';
import { useMaskContext } from '@form-engine/builder-mask/hooks/use-mask';
import BuilderDatasourceDataTable from '@form-engine/builder-application/components/datasource-data-table';
import BuilderDrawerTabs from '@form-engine/builder-application/components/drawer-tabs';
import BuilderMaskGroup from '@form-engine/builder-mask/components/mask/group';
import BuilderCodeProperty from '@form-engine/builder-mask/components/properties/code';
import BuilderEnumProperty from '@form-engine/builder-mask/components/properties/enum';
import BuilderStringProperty from '@form-engine/builder-mask/components/properties/string';
import BuilderNameProperty from '@form-engine/builder-mask/components/properties/name';
import Datasource from '../datasources/static-datasource';
import { i18n } from '@form-engine/builder-application';

export function BuilderStaticDatasourceSetup() {
    const { state } = useMaskContext();
    const { c } = i18n.prefix(`mask.${Datasource.apiName}.`);
    return (
        <>
            <BuilderMaskGroup>
                <BuilderNameProperty />
                <BuilderStringProperty
                    propertyName="description"
                    label={c('description')}
                />
            </BuilderMaskGroup>
            <BuilderMaskGroup label={c('contentGroupTitle')}>
                <BuilderEnumProperty
                    propertyName="sourceType"
                    label={c('sourceType')}
                    options={[
                        ['csv', 'CSV'],
                        ['json', 'JSON'],
                    ]}
                />
                {state.sourceType === 'json' && (
                    <BuilderStringProperty
                        propertyName="resultPath"
                        label={c('resultPath')}
                        placeholder={c('resultPathPlaceholder')}
                    />
                )}
                <BuilderStringProperty
                    propertyName="labelColumn"
                    label={c('labelColumn')}
                    placeholder={c('labelColumnPlaceholder')}
                    compilable
                    variableOptions={['$i.']}
                />
                <BuilderStringProperty
                    propertyName="valueColumn"
                    label={c('valueColumn')}
                    placeholder={c('valueColumnPlaceholder')}
                    compilable
                    variableOptions={['$i.']}
                />
                <BuilderCodeProperty
                    propertyName="source"
                    label={c('source')}
                    language={state.sourceType || 'csv'}
                />
            </BuilderMaskGroup>
        </>
    );
}

export default function BuilderStaticDatasourceMask({ id }) {
    const { c } = i18n.prefix(`mask.${Datasource.apiName}.`);
    const { state: editor = {}, setItem: setEditorItem } =
        useFormEditorContext();
    const { tab = 'setup' } = (editor.datasources || {})[id] || {};
    const setTab = useCallback(
        (tab) => {
            const { datasources = {} } = editor;
            const { [id]: datasource = {} } = datasources;
            setEditorItem('datasources', {
                ...datasources,
                [id]: { ...datasource, tab },
            });
        },
        [editor, setEditorItem, id]
    );

    return (
        <>
            <BuilderDrawerTabs
                activeKey={tab}
                items={{ setup: c('setup'), data: c('data') }}
                onChange={setTab}
            />
            <div className="feb-view">
                {tab === 'setup' && (
                    <div className="feb-view-inner">
                        <Row>
                            <Col xl={{ span: 8, offset: 2 }}>
                                <BuilderStaticDatasourceSetup />
                            </Col>
                        </Row>
                    </div>
                )}
                {tab === 'data' && (
                    <div className="feb-view-inner p-0">
                        <BuilderDatasourceDataTable id={id} />
                    </div>
                )}
            </div>
        </>
    );
}
