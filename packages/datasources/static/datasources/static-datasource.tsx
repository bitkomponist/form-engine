import papaparse, { ParseRemoteConfig, ParseResult } from 'papaparse';
import get from 'lodash.get';
import set from 'lodash.set';
import { compile } from '@form-engine/core-util/template';
import {
    DatasourceArgs,
    DatasourceDefinition,
    DatasourceResponse,
    DatasourceType,
} from '@form-engine/core-application/types/form';
import { getFormSessionLocals } from '@form-engine/core-application/utilities/form-session';

export enum SourceType {
    Csv = 'csv',
    Json = 'json',
}

export const DEFAULT_SOURCE_TYPE = SourceType.Csv;

export interface FormEngineStaticDatasourceArgs extends DatasourceArgs {
    definition: {
        source?: string;
        sourceType?: SourceType;
        labelColumn?: string;
        valueColumn?: string;
        csvOptions?: ParseRemoteConfig;
        resultPath?: string;
        previousError?: Error;
        constantsPath?: string;
    } & DatasourceDefinition;
}

function FormEngineStaticDatasource(
    options: FormEngineStaticDatasourceArgs
): Promise<DatasourceResponse> {
    const {
        source = '',
        sourceType = DEFAULT_SOURCE_TYPE,
        labelColumn = '',
        valueColumn = '',
        csvOptions = { header: true },
        resultPath,
        previousError = null,
        constantsPath,
    } = options?.definition ?? {};

    const locals = getFormSessionLocals(options.form, options?.session ?? {});

    let data = [];
    let error: Error | null = previousError as Error;
    let result: any;

    if (!error) {
        switch (sourceType) {
            case SourceType.Json:
                try {
                    result = JSON.parse(source);
                    data =
                        (resultPath ? get(result, resultPath) : result) || [];
                } catch (e) {
                    data = [];
                    error = e as Error;
                }
                break;
            case SourceType.Csv:
                try {
                    result = data =
                        (
                            papaparse.parse(
                                source as any,
                                csvOptions as any
                            ) as unknown as ParseResult<any>
                        ).data || [];
                } catch (e) {
                    result = data = [];
                    error = e as Error;
                }
                break;
            default:
                data = [];
                error = new Error(`unknown source type ${sourceType}`);
        }
    }

    return Promise.resolve({
        constants: constantsPath ? set({}, constantsPath, result) : undefined,
        data,
        error,
        options: Array.isArray(data)
            ? data.map((item) => {
                  const itemLocals = { ...locals, $i: item };
                  return [
                      compile(valueColumn, itemLocals, ''),
                      compile(labelColumn, itemLocals, ''),
                  ];
              })
            : [],
    });
}

FormEngineStaticDatasource.apiName = 'FormEngineStaticDatasource';

export default FormEngineStaticDatasource as DatasourceType;
