import { ServerApplicationPlugin } from '@form-engine/server-application/typings';
import Datasource from './datasources/server-http-request-datasource';
export default {
    datasources: {
        [Datasource.apiName]: Datasource,
    },
} as ServerApplicationPlugin;
