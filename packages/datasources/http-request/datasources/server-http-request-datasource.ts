import { compile } from '@form-engine/core-util/template';
import fetch from 'cross-fetch';
import {
    DatasourceArgs,
    DatasourceType,
} from '@form-engine/core-application/types/form';
import { HttpError } from '@form-engine/server-application/errors/http';
import { getFormSessionLocals } from '@form-engine/core-application/utilities/form-session';
import {
    AuthorizationInfoSettings,
    compileAuthorizationHeader,
    compileRequestBody,
    CompileRequestContentTypes,
} from '@form-engine/core-util/request';

async function FormEngineHttpRequestDatasource({
    form,
    definition,
    session,
}: DatasourceArgs) {
    const {
        auth = 'none',
        basicAuthUsername = '',
        basicAuthPassword = '',
        bearerToken = '',
        url = '',
        method = 'GET',
        contentType = 'text/plain',
        body: bodyProp = '',
    } = definition;

    const locals = getFormSessionLocals(form, session ?? {});

    const headers: any = {
        ...compileAuthorizationHeader({
            auth,
            basicAuthUsername,
            basicAuthPassword,
            bearerToken,
            locals,
        } as AuthorizationInfoSettings),
    };

    const hasBody = ['POST', 'PATCH', 'PUT'].includes(method);

    if (hasBody && contentType) {
        headers['content-type'] = contentType;
    }

    let result = '';
    if (url) {
        const fetchUrl = compile(url, locals, '');

        const fetchOptions = {
            method,
            body: hasBody
                ? compileRequestBody(
                      contentType as CompileRequestContentTypes,
                      bodyProp,
                      locals
                  )
                : undefined,
            headers,
        };

        const response = await fetch(fetchUrl, fetchOptions);

        if (response.status < 200 || response.status >= 400) {
            const message = await response.text();
            throw new HttpError(message, response.status);
        }

        result = await response.text();
    }

    return {
        data: result,
    };
}

FormEngineHttpRequestDatasource.apiName = 'FormEngineHttpRequestDatasource';

export default FormEngineHttpRequestDatasource as DatasourceType;
