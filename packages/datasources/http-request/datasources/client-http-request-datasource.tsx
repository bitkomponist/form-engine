import fetchDatasourceBackend from '@form-engine/core-application/datasources/fetch-backend';
import {
    DatasourceArgs,
    DatasourceType,
} from '@form-engine/core-application/types/form';
import { getFormSessionLocals } from '@form-engine/core-application/utilities/form-session';
import { compile } from '@form-engine/core-util/template';
import FormEngineStaticDatasource from '@form-engine/datasource-static/datasources/static-datasource';
async function FormEngineHttpRequestDatasource({
    definition,
    form,
    session,
}: DatasourceArgs) {
    const { $prefetchResult, ...staticOptions } = definition;

    if ($prefetchResult !== undefined) {
        return FormEngineStaticDatasource({
            definition: {
                ...staticOptions,
                source: $prefetchResult,
            },
            form,
            session,
        });
    }

    if (!form?.id)
        throw new Error(
            'tried to invoke datasource with incomplete form definition'
        );

    let source = '';

    // skip loading when editing
    if (!['translate', 'edit'].includes(session?.editMode)) {
        const { data } = await fetchDatasourceBackend(
            form.id,
            definition.id,
            session ?? {}
        );
        source = data;
    }

    return FormEngineStaticDatasource({
        definition: {
            ...staticOptions,
            source,
        },
        form,
        session,
    });
}

FormEngineHttpRequestDatasource.apiName = 'FormEngineHttpRequestDatasource';

export default FormEngineHttpRequestDatasource as DatasourceType;
