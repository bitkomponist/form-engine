import { useFormEditorContext } from '@form-engine/builder-form-editor/hooks/use-form-editor';
import { useMaskContext } from '@form-engine/builder-mask/hooks/use-mask';
import React from 'react';
import { useCallback } from 'react';
import { Col, Row } from 'react-bootstrap';
import BuilderDatasourceDataTable from '@form-engine/builder-application/components/datasource-data-table';
import BuilderDrawerTabs from '@form-engine/builder-application/components/drawer-tabs';
import BuilderMaskGroup from '@form-engine/builder-mask/components/mask/group';
import BuilderEnumProperty from '@form-engine/builder-mask/components/properties/enum';
import BuilderStringProperty from '@form-engine/builder-mask/components/properties/string';
import BuilderNameProperty from '@form-engine/builder-mask/components/properties/name';
import BuilderMaskGroupRequestPayload from '@form-engine/builder-mask/components/groups/request-payload';
import Datasource from '../datasources/client-http-request-datasource';
import { i18n } from '@form-engine/builder-application';
import BuilderAuthProperty from '@form-engine/builder-mask/components/properties/auth';

export function BuilderHttpRequestDatasourceSetup() {
    const { state } = useMaskContext();

    const { c } = i18n.prefix(`mask.${Datasource.apiName}.`);

    return (
        <>
            <BuilderMaskGroup>
                <BuilderNameProperty />
                <BuilderStringProperty
                    propertyName="description"
                    label={c('description')}
                />
                <BuilderStringProperty
                    compilable
                    propertyName="url"
                    label={c('url')}
                    type="url"
                />
                <BuilderEnumProperty
                    propertyName="method"
                    label={c('method')}
                    options={['GET', 'POST', 'PUT', 'PATCH', 'DELETE']}
                />
            </BuilderMaskGroup>
            <BuilderMaskGroup label={c('authGroupTitle')}>
                <BuilderAuthProperty />
            </BuilderMaskGroup>
            {['POST', 'PUT', 'PATCH'].includes(state.method) && (
                <BuilderMaskGroupRequestPayload />
            )}
            <BuilderMaskGroup label={c('responseGroupTitle')}>
                <BuilderEnumProperty
                    propertyName="sourceType"
                    label={c('sourceType')}
                    options={[
                        ['csv', 'CSV'],
                        ['json', 'JSON'],
                    ]}
                />
                {state.sourceType === 'json' && (
                    <BuilderStringProperty
                        compilable
                        propertyName="resultPath"
                        label={c('resultPath')}
                        placeholder={'eg. "entries"'}
                    />
                )}
                <BuilderStringProperty
                    propertyName="labelColumn"
                    label={c('labelColumn')}
                    placeholder={c('labelColumnPlaceholder')}
                    compilable
                    variableOptions={['$i.']}
                />
                <BuilderStringProperty
                    propertyName="valueColumn"
                    label={c('valueColumn')}
                    placeholder={c('valueColumnPlaceholder')}
                    compilable
                    variableOptions={['$i.']}
                />
            </BuilderMaskGroup>
            {/* <BuilderMaskGroup label="Behaviour">
                <BuilderBooleanProperty
                    propertyName="cacheDisabled"
                    label="Disable Caching"
                />
            </BuilderMaskGroup> */}
        </>
    );
}

export default function BuilderHttpRequestDatasourceMask({
    id,
}: {
    id: string;
}) {
    const { c } = i18n.prefix(`mask.${Datasource.apiName}.`);
    const { state: editor = {}, setItem: setEditorItem } =
        useFormEditorContext();
    const { tab = 'setup' } = (editor.datasources || {})[id] || {};
    const setTab = useCallback(
        (tab: string) => {
            const { datasources = {} } = editor;
            const { [id]: datasource = {} } = datasources;
            setEditorItem('datasources', {
                ...datasources,
                [id]: { ...datasource, tab },
            });
        },
        [editor, setEditorItem, id]
    );

    return (
        <>
            <BuilderDrawerTabs
                activeKey={tab}
                items={{ setup: c('setup'), data: c('data') }}
                onChange={setTab}
            />
            <div className="feb-view">
                {tab === 'setup' && (
                    <div className="feb-view-inner">
                        <Row>
                            <Col xl={{ span: 8, offset: 2 }}>
                                <BuilderHttpRequestDatasourceSetup />
                            </Col>
                        </Row>
                    </div>
                )}
                {tab === 'data' && (
                    <div className="feb-view-inner p-0">
                        <BuilderDatasourceDataTable id={id} />
                    </div>
                )}
            </div>
        </>
    );
}
