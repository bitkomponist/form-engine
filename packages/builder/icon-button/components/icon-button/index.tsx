import React from 'react';

export interface IconButtonProps extends React.HTMLAttributes<HTMLButtonElement> {
    as?: React.ElementType;
    color?: 'primary' | 'secondary';
}

const IconButton = React.forwardRef(
    (
        {
            as: Component = 'button',
            color,
            className = '',
            ...props
        }: IconButtonProps,
        ref
    ) => {
        return (
            <Component
                {...props}
                className={`feb-icon-btn ${
                    color ? `feb-icon-btn--${color}` : ''
                } ${className}`}
                ref={ref}
            />
        );
    }
);

IconButton.displayName = 'IconButton';

export default IconButton;
