import FormEngine from '@form-engine/core-application';
import React from 'react';
import { createRoot } from 'react-dom/client';
import * as plugins from './plugins';
import BuilderApp from './components/app';
// workaround to only import stylesheets with parcel, not with storybook
import './style/theme.scss';
import { registerImportedPluginModules } from '@form-engine/core-util/register-plugin';
import { BuilderElementDescriptor } from './types/element.js';
import { BuilderDatasourceDescriptor } from './types/datasource.js';
import { BuilderWorkflowLayerDescriptor } from './types/workflow-layer.js';
import { BuilderEmbedDescriptor } from './types/embed.js';
import { FormPreset } from './types/form-preset.js';
import { BuilderFormStudioViewDescriptor } from './types/form-studio-view';
import { BuilderMaskConfigurationMap } from '@form-engine/builder-mask/typings';
import { I18n, I18nState } from '@form-engine/core-application/utilities/i18n';
import * as translations from './builder-translations';
interface BuilderComponentExtensionDefinition {
    placement?: 'before' | 'after';
    component: React.ElementType;
}
interface BuilderComponentExtensionMap {
    [extensionName: string]: BuilderComponentExtensionDefinition;
}
interface IBuilder {
    apiBaseUrl: string;
    basename: string;
    renderFormRoute: string;
    selector: string;
    initDocument: (doc?: Document) => void;
    initUi: (root: HTMLElement) => void;
    mapItemEntries: (
        itemKey: 'elements' | 'datasources' | 'workflowLayers',
        callback: (entry: [string, unknown]) => any
    ) => any[];
    getItemSubtypeMap: (
        itemKey: 'elements' | 'datasources' | 'workflowLayers',
        subtypeKey: string
    ) => { [key: string]: any };
    keybindings?: { [name: string]: string };
    routes?: { [key: string]: JSX.Element };
    defaultView?: JSX.Element;
    elements: { [apiName: string]: BuilderElementDescriptor };
    datasources: { [apiName: string]: BuilderDatasourceDescriptor };
    workflowLayers: { [apiName: string]: BuilderWorkflowLayerDescriptor };
    formEmbeds: { [apiName: string]: BuilderEmbedDescriptor };
    formStudioViews: { [apiName: string]: BuilderFormStudioViewDescriptor };
    formPresets: { [apiName: string]: FormPreset };
    componentExtensions: {
        [componentName: string]: BuilderComponentExtensionMap;
    };
    maskConfigurations: {
        [maskId: string]: BuilderMaskConfigurationMap;
    };
    getComponentExtensions: (
        componentName: string,
        filter?: (def: BuilderComponentExtensionDefinition) => boolean
    ) => BuilderComponentExtensionMap;
    renderComponentExtensions: (
        componentName: string,
        filter?: (def: BuilderComponentExtensionDefinition) => boolean
    ) => React.ReactNode;
    translations: I18nState['data'];
    i18n: I18n;
    [prop: string]: any;
}

export type BuilderPlugin = Partial<IBuilder>;

/** @todo clean up the any's */

const Builder: IBuilder = {
    apiBaseUrl: location.origin,
    basename: '/',
    renderFormRoute: '/form/:formId',
    autoload: true,
    selector: '#form-builder',
    keybindings: {},
    initDocument(doc = document) {
        return Array.from(
            doc.querySelectorAll(FormEngine.Builder.selector)
        ).map(FormEngine.Builder.initUi);
    },
    initUi(rootElement) {
        const root = createRoot(rootElement);
        root.render(
            <React.StrictMode>
                <BuilderApp />
            </React.StrictMode>
        );
    },
    mapItemEntries(itemKey, callback) {
        return Object.entries(FormEngine.Builder?.[itemKey] || {}).map(
            callback
        );
    },
    getItemSubtypeMap(itemKey, subtypeKey) {
        return Object.fromEntries(
            FormEngine.Builder.mapItemEntries(
                itemKey,
                ([key, def]: [string, any]) => [key, def?.[subtypeKey]]
            )
        );
    },
    getComponentExtensions(componentName, filter) {
        let extensions = {
            ...(Builder?.componentExtensions?.[componentName] ?? {}),
        };

        if (filter) {
            extensions = Object.fromEntries(
                Object.entries(extensions).filter(([_key, def]) => filter(def))
            );
        }

        return extensions;
    },
    renderComponentExtensions(componentName, filter) {
        return (
            <>
                {Object.entries(
                    Builder.getComponentExtensions(componentName, filter)
                ).map(([key, def]) => {
                    const { component: Component } = def;
                    return <Component key={key} />;
                })}
            </>
        );
    },
    ...(FormEngine?.Builder || {}),
    elements: Object.fromEntries(
        Object.entries({
            ...(FormEngine?.Builder?.elements || {}),
        }).map(([key, def]: [string, any]) => [
            def?.Element?.apiName || key,
            def,
        ])
    ),
    datasources: Object.fromEntries(
        Object.entries({
            ...(FormEngine?.Builder?.datasources || {}),
        }).map(([key, def]: [string, any]) => [
            def?.Datasource?.apiName || key,
            def,
        ])
    ),
    workflowLayers: Object.fromEntries(
        Object.entries({
            ...(FormEngine?.Builder?.workflowLayers || {}),
        }).map(([key, def]: [string, any]) => [
            def?.WorkflowLayer?.apiName || key,
            def,
        ])
    ),
    translations: {
        ...translations,
        ...(FormEngine?.Builder?.translations ?? {}),
    },
    i18n: null as any,
    formEmbeds: {},
    formStudioViews: {},
    formPresets: {},
    componentExtensions: {},
};

registerImportedPluginModules(Builder, plugins);

FormEngine.Builder = Builder;
FormEngine.isBuilder = true;

Builder.i18n = new I18n({ data: Builder.translations });

export const i18n = Builder.i18n;

export default Builder;

window.addEventListener('DOMContentLoaded', () => {
    if (Builder.autoload) {
        Builder.initDocument();
    }
});