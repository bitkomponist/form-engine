/** @todo investigate why Builder is not importable here */
export default function getKeyBindings(): { [key: string]: string } {
    const Builder = (window as any)?.FormEngine?.Builder || {};

    if (!Builder.keybindings) {
        Builder.keybindings = {};
    }
    return Builder.keybindings;
}

const keySymbols = Object.entries({
    cmd: '⌘',
    option: '⌥',
    alt: '⌥',
    ctrl: '⌃',
    shift: '⇧',
    capslock: '⇪',
    left: '←',
    right: '→',
    up: '↑',
    down: '↓',
    tab: '⇥',
    backtab: '⇤',
    return: '↩',
    enter: '⌤',
    forwarddelete: '⌦',
    delete: '⌫',
    backspace: '⌫',
    pageup: '⇞',
    pagedown: '⇟',
    home: '↖',
    end: '↘',
    clear: '⌧',
    space: '␣',
    escape: '⎋',
    eject: '⏏',
    '+': ' ',
});

export function getHotkeyLabel(
    id: string,
    fallback = 'UNASSIGNED',
    symbols = true
) {
    const resolve = () => {
        const keybindings = getKeyBindings();

        if (!(id in keybindings)) {
            return fallback;
        }

        const shortcuts = keybindings[id].split(',');

        // only show OS appropriate shortcuts
        if (shortcuts.length > 1) {
            const isWindows = navigator.userAgent.includes('Windows');
            const cmd = shortcuts.find((s) => s.startsWith('cmd'));
            const ctrl = shortcuts.find((s) => s.startsWith('ctrl'));
            if (isWindows && ctrl) {
                return ctrl.trim();
            }
            if (!isWindows && cmd) {
                return cmd.trim();
            }
        }

        return keybindings[id];
    };

    const result = resolve();

    if (!symbols) {
        return result;
    }

    let withSymbols = result;

    for (const [key, symbol] of keySymbols) {
        withSymbols = withSymbols.split(key).join(symbol);
    }

    return withSymbols;
}

export function setHotkey(
    name: string | { [name: string]: string | string[] },
    mapping?: string | string[]
) {
    if (typeof name === 'object') {
        Object.entries(name).forEach(([key, value]) => setHotkey(key, value));
        return;
    }
    if (!mapping) {
        throw new Error('invalid mapping');
    }
    getKeyBindings()[name] = Array.isArray(mapping)
        ? mapping.join(',')
        : mapping;
}
