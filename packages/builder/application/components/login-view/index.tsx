import React from 'react';
import useIdentity from '../../hooks/use-identity';
import { useCallback } from 'react';
import {
    Button,
    Card,
    Col,
    Container,
    FloatingLabel,
    Form,
    FormControl,
    Row,
    Spinner,
} from 'react-bootstrap';
import { UiChecksGrid } from 'react-bootstrap-icons';
import { i18n } from '../..';

export function BuilderLogin() {
    const { auth, identityError, loading } = useIdentity();

    const handleSubmit = useCallback(
        (e: React.SyntheticEvent) => {
            if (loading) return;
            e.preventDefault();
            e.stopPropagation();
            const [username, password] =
                e.target as unknown as HTMLInputElement[];
            auth(
                { username: username.value, password: password.value },
                'basic'
            );
        },
        [auth, loading]
    );

    const { c, Node } = i18n.prefix('login.');

    return (
        <Form onSubmit={handleSubmit}>
            <FloatingLabel label={c('username')} className="mb-3">
                <FormControl
                    disabled={loading}
                    type="text"
                    placeholder={c('usernamePlaceholder')}
                    required
                />
            </FloatingLabel>
            <FloatingLabel label={c('password')} className="mb-3">
                <FormControl
                    type="password"
                    placeholder={c('passwordPlaceholder')}
                    required
                    disabled={loading}
                />
            </FloatingLabel>
            {identityError && (
                <p className="text-danger mb-3 small">
                    <Node id="identityError" />
                </p>
            )}
            <div className="d-grid gap-2">
                <Button
                    type="submit"
                    size="lg"
                    variant="primary"
                    disabled={loading}
                >
                    {loading ? (
                        <>
                            <Spinner
                                as="span"
                                size="sm"
                                animation="border"
                                role="status"
                                aria-hidden="true"
                            />
                            <span className="ms-2">
                                <Node id="signInLoading" />
                            </span>
                        </>
                    ) : (
                        <Node id="signInLabel" />
                    )}
                </Button>
            </div>
        </Form>
    );
}

export default function BuilderLoginView() {
    const { Node } = i18n.prefix('login.');

    return (
        <div className="flex-grow-1 w-100 bg-light">
            <Container style={{ marginTop: '25vh', maxWidth: '1024px' }}>
                <Row>
                    <Col md={8} sm={6} xs={12}>
                        <h1 className="text-primary mb-3">
                            <UiChecksGrid /> <Node id="heading" />
                        </h1>
                        <h3>
                            <Node id="lead" />
                        </h3>
                    </Col>
                    <Col>
                        <Card>
                            <Card.Body>
                                <BuilderLogin />
                                <p className="text-center small mt-3">
                                    <a href="mailto:support@die-interaktiven.de">
                                        <Node id="forgotLink" />
                                    </a>
                                </p>
                                <hr />
                                <div className="d-grid gap-2">
                                    <Button
                                        as="a"
                                        variant="secondary"
                                        href="mailto:support@die-interaktiven.de"
                                    >
                                        <Node id="registerLink" />
                                    </Button>
                                </div>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}
