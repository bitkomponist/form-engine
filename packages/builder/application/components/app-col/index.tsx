import React from 'react';
import { Col } from 'react-bootstrap';
import BuilderResizable, {
    BuilderResizableHandle,
    BuilderResizableProps,
} from '../resizable';

export type BuilderAppColProps = Omit<BuilderResizableProps, 'handles'> &
    React.ComponentProps<typeof Col> & {
        border?: BuilderResizableHandle;
    };

export default function BuilderAppCol({
    className = '',
    border = 'right',
    as: asProp = Col,
    minWidth = 200,
    maxWidth = '33vw',
    ...props
}: BuilderAppColProps) {
    return (
        <BuilderResizable
            as={asProp}
            {...props}
            handles={border}
            minWidth={minWidth}
            maxWidth={maxWidth}
            className={`feb-app-col ${
                border && `feb-app-col--border-${border}`
            } ${className}`}
        />
    );
}
