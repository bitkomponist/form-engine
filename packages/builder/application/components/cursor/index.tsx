import { useEffect, useState } from 'react';
import { createPortal } from 'react-dom';

export function useNativeCursorInfo() {
    const [position, setPosition] = useState<[number, number]>([0, 0]);
    const [available, setAvailable] = useState(true);

    useEffect(() => {
        const updateIsAvailable = () => setAvailable(true);
        const updateIsNotAvailable = () => setAvailable(false);
        document.addEventListener('mouseover', updateIsAvailable);
        document.addEventListener('mouseleave', updateIsNotAvailable);

        return () => {
            document.removeEventListener('mouseover', updateIsAvailable);
            document.removeEventListener('mouseleave', updateIsNotAvailable);
        };
    }, [setAvailable]);

    useEffect(() => {
        const updatePosition = ({ clientX, clientY }: MouseEvent) =>
            setPosition([clientX, clientY]);
        document.addEventListener('mousemove', updatePosition);
        document.addEventListener('dragover', updatePosition);

        return () => {
            document.removeEventListener('mousemove', updatePosition);
            document.removeEventListener('dragover', updatePosition);
        };
    }, [setAvailable]);

    return { x: position[0], y: position[1], available };
}

export interface BuilderCursorProps {
    children?: React.ReactNode;
    show?: boolean;
    hideNative?: boolean;
}

export default function BuilderCursor({
    children,
    show = false,
    ...props
}: BuilderCursorProps) {
    const hideNative = 'hideNative' in props && props.hideNative !== false;
    delete props.hideNative;
    const [container, setContainer] = useState<HTMLElement | null>(null);
    const { x, y, available } = useNativeCursorInfo();

    useEffect(() => {
        const element = document.createElement('div');
        element.className = 'feb-cursor';
        document.body.appendChild(element);
        setContainer(element);

        return () => {
            setContainer(null);
            document.body.removeChild(element);
        };
    }, [setContainer]);

    useEffect(() => {
        document.documentElement.classList.toggle(
            'feb-native-cursor-hidden',
            show && hideNative
        );
    }, [show, hideNative]);

    useEffect(() => {
        if (!container) return;
        container.style.setProperty('--mouse-x', String(x));
        container.style.setProperty('--mouse-y', String(y));
    }, [container, x, y]);

    useEffect(() => {
        if (!container) return;
        container.classList.toggle('show', show && available);
    }, [container, show, available]);

    return container && createPortal(children, container);
}
