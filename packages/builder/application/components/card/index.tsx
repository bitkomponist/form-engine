import React from 'react';
import { Card } from 'react-bootstrap';

export default function BuilderCard({
    className = '',
    ...props
}: React.ComponentProps<typeof Card>) {
    return <Card {...props} className={`${className} feb-card`} />;
}
