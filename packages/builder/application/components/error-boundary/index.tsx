import React from 'react';

export interface ErrorBoundaryProps {
    children?: React.ReactNode;
    errorComponent?: React.ElementType;
}

interface ErrorBoundaryState {
    error: any;
}

export default class ErrorBoundary extends React.Component<
    ErrorBoundaryProps,
    ErrorBoundaryState
> {
    constructor(props: ErrorBoundaryProps) {
        super(props);
        this.state = { error: null };
    }

    componentDidCatch(error: any) {
        this.setState({ error });
    }

    render() {
        const { error } = this.state;
        const { children, errorComponent: Error = 'div' } = this.props;
        if (error) {
            return <Error>{error}</Error>;
        } else {
            return children;
        }
    }
}
