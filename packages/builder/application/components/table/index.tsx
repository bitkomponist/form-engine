import useBounds from '../../hooks/use-bounds';
import React, { LegacyRef } from 'react';
import { useRef } from 'react';
import { useMemo } from 'react';
import { useCallback } from 'react';
import { GridOnScrollProps, VariableSizeGrid as Grid } from 'react-window';

export interface ColumnProps {
    dataKey: string;
    header?: React.ReactNode;
    cell: CellProps['children'];
}

/* eslint-disable-next-line no-unused-vars */
export function Column(_props: ColumnProps) {
    // purely virtual should not do anything
    return null;
}

export interface CellProps {
    as?: React.ElementType;
    dataKey: string;
    index: number;
    data: any;
    children?: (cellInfo: {
        content: React.ReactNode;
        data: any;
        dataKey: string;
        index: number;
        row: any;
    }) => React.ReactNode | React.ReactNode;
}

export function Cell({
    dataKey,
    index,
    data,
    children,
    as: Component = React.Fragment,
}: CellProps) {
    const row = data[index];
    const content = row[dataKey];
    return (
        <Component>
            {typeof children === 'function'
                ? children({ content, data, dataKey, index, row })
                : content}
        </Component>
    );
}

export interface HeaderCellProps {
    as?: React.ElementType;
    dataKey: string;
    children?: (cellInfo: { dataKey: string }) => React.ReactNode;
}

export function HeaderCell({
    dataKey,
    children,
    as: Component = React.Fragment,
}: HeaderCellProps) {
    return (
        <Component>
            {typeof children === 'function' ? children({ dataKey }) : dataKey}
        </Component>
    );
}

export interface BuilderTableProps
    extends Omit<
        React.ComponentProps<typeof Grid>,
        | 'children'
        | 'width'
        | 'height'
        | 'columnWidth'
        | 'rowHeight'
        | 'rowCount'
        | 'columnCount'
        | 'onScroll'
    > {
    data: any;
    cellContainer?: React.ElementType;
    cell?: React.ElementType;
    headerCell?: React.ElementType;
    columnMinWidth?: number;
    sticky?: boolean;
    children?: React.ReactNode;
    rowHeight?: (index: number) => number;
}

type Renderer = React.ComponentProps<typeof Grid>['children'];

export default function BuilderTable({
    className = '',
    data,
    children,
    cellContainer: CellContainerComp = 'div',
    cell: CellComp = Cell,
    headerCell: HeaderCellComp = HeaderCell,
    columnMinWidth = 96,
    ...props
}: BuilderTableProps) {
    const isSticky = 'sticky' in props && props.sticky !== false;
    delete props.sticky;

    const {
        bounds: { width, height },
        ref: refWrap,
    } = useBounds();

    const columns =
        useMemo(() => {
            return React.Children.map(children, (child) => {
                if (
                    !child ||
                    typeof child !== 'object' ||
                    !('type' in child) ||
                    child.type !== Column
                ) {
                    throw new Error(
                        `BuilderTable only accepts Columns as children`
                    );
                }
                return child.props;
            });
        }, [children]) || [];

    const renderer: Renderer = useMemo(() => {
        return function CellRenderer({ columnIndex, rowIndex, style }) {
            const column = columns[columnIndex];
            if (!column) return null;
            const {
                dataKey,
                header: headerContent,
                cell: cellContent,
            } = column;
            if (!dataKey) return null;
            let cell;
            let className;
            if (!isSticky && rowIndex === 0) {
                cell = (
                    <HeaderCellComp dataKey={dataKey}>
                        {headerContent}
                    </HeaderCellComp>
                );
                className = 'feb-th';
            } else {
                cell = (
                    <CellComp
                        dataKey={dataKey}
                        data={data}
                        index={isSticky ? rowIndex : rowIndex - 1}
                    >
                        {cellContent}
                    </CellComp>
                );
                className = 'feb-td';
            }
            return (
                <CellContainerComp
                    className={`feb-tc ${className}`}
                    style={style}
                >
                    {cell}
                </CellContainerComp>
            );
        };
    }, [columns, data, CellContainerComp, isSticky]);

    const [widthProvider, widthEstimate] = useMemo(() => {
        let rest = width;
        let auto: number[] = [];

        const sizes: number[] = [];
        columns.forEach((column, index) => {
            if ('width' in column) {
                sizes.push(column.width);
                rest -= column.width;
            } else {
                sizes.push(-1);
                auto.push(index);
            }
        });
        let autoWidth = rest / auto.length;
        if (columnMinWidth > autoWidth) autoWidth = columnMinWidth;
        auto.forEach((index) => {
            sizes[index] = autoWidth;
        });

        return [
            (index: number) => sizes[index],
            sizes.reduce((p, s) => s + p, 0) / sizes.length,
        ];
    }, [columns, width, columnMinWidth]);

    const rowHeight = props.rowHeight || (() => 30);
    const heightEstimate = rowHeight(0);

    const refHead = useRef<HTMLElement>();

    const scrollHead = useCallback(
        (e: GridOnScrollProps) => {
            if (!refHead.current || !isSticky) return;
            refHead.current.scrollTo({
                left: e.scrollLeft,
                top: 0,
            });
        },
        [isSticky]
    );

    const hasSize = Boolean(width && height);

    const grid = (
        <>
            {isSticky && (
                <Grid
                    ref={refHead as unknown as LegacyRef<Grid<any>>}
                    className={`feb-thead`}
                    width={width}
                    rowHeight={rowHeight}
                    height={rowHeight(0)}
                    columnCount={columns.length}
                    columnWidth={widthProvider}
                    estimatedColumnWidth={widthEstimate}
                    estimatedRowHeight={heightEstimate}
                    rowCount={1}
                >
                    {({ columnIndex, style }) => {
                        const { dataKey, header: headerContent } =
                            columns[columnIndex];
                        return (
                            <CellContainerComp
                                className={`feb-tc feb-th`}
                                style={style}
                            >
                                <HeaderCellComp dataKey={dataKey}>
                                    {headerContent}
                                </HeaderCellComp>
                            </CellContainerComp>
                        );
                    }}
                </Grid>
            )}
            <Grid
                {...props}
                onScroll={scrollHead}
                width={width}
                height={isSticky ? height - rowHeight(0) : height}
                columnWidth={widthProvider}
                rowHeight={rowHeight}
                rowCount={isSticky ? data.length : data.length + 1}
                columnCount={columns.length}
                className={`feb-tbody ${className}`}
            >
                {renderer}
            </Grid>
        </>
    );

    return (
        <div
            className="feb-table"
            ref={refWrap as unknown as LegacyRef<HTMLDivElement>}
        >
            {hasSize && grid}
        </div>
    );
}

Object.assign(BuilderTable, { Column, Cell, HeaderCell });
