import React, { Fragment } from 'react';
import { Button, Modal } from 'react-bootstrap';
import BuilderSpinnerBoundary from '../spinner-boundary';
import { i18n } from '../..';

export interface BuilderModalProps extends React.ComponentProps<typeof Modal> {
    title?: React.ReactNode;
    wrap?: React.ReactElement<any, string | React.JSXElementConstructor<any>>;
    footer?: React.ReactNode;
    dialogClassName?: string;
    loading?: boolean;
}

export default function BuilderModal({
    as: Component = Modal,
    title = null,
    wrap,
    children,
    size = 'sm',
    footer = null,
    dialogClassName = 'feb',
    loading = false,
    ...props
}: BuilderModalProps) {
    const bordered = 'bordered' in props && props.bordered !== false;
    delete props.bordered;
    delete props.onSuccess;
    delete props.onError;

    const modalContent = (
        <Fragment>
            {title && (
                <Modal.Header closeButton>
                    <Modal.Title as="h6">{title}</Modal.Title>
                </Modal.Header>
            )}
            {children && <Modal.Body>{children}</Modal.Body>}
            {footer && <Modal.Footer>{footer}</Modal.Footer>}
        </Fragment>
    );

    return (
        <Component
            backdrop="static"
            size={size}
            dialogClassName={dialogClassName}
            {...props}
            className={`feb-modal ${bordered ? 'bordered' : ''} ${
                loading ? 'loading' : ''
            } ${props.className || ''}`}
        >
            <BuilderSpinnerBoundary show={loading}>
                {wrap ? (
                    <wrap.type {...wrap.props}>{modalContent}</wrap.type>
                ) : (
                    modalContent
                )}
            </BuilderSpinnerBoundary>
        </Component>
    );
}

export interface BuilderConfirmModalProps
    extends React.ComponentProps<typeof BuilderModal> {
    confirm?: React.ReactElement<
        any,
        string | React.JSXElementConstructor<any>
    >;
    abort?: React.ReactElement<any, string | React.JSXElementConstructor<any>>;
    onConfirm?: () => void;
    onHide?: () => void;
}

export function BuilderConfirmModal({
    as: Component = BuilderModal,
    confirm = (
        <Button size="sm" variant="primary">
            <i18n.Node id="modal.confirm.ok" />
        </Button>
    ),
    abort = (
        <Button size="sm" variant="outline-secondary">
            <i18n.Node id="modal.confirm.cancel" />
        </Button>
    ),
    onConfirm,
    onHide,
    ...props
}: BuilderConfirmModalProps) {
    const { type: Confirm, props: confirmProps } = confirm;
    const { type: Abort, props: abortProps } = abort;
    const footer = (
        <>
            <Abort {...abortProps} onClick={onHide} />
            <Confirm {...confirmProps} onClick={onConfirm} />
        </>
    );

    return <Component footer={footer} {...props} />;
}

export interface BuilderFormModalProps extends BuilderConfirmModalProps {
    confirm?: React.ReactElement<
        any,
        string | React.JSXElementConstructor<any>
    >;
    formProps: React.HTMLAttributes<'form'>;
}

export function BuilderFormModal({
    confirm = (
        <Button size="sm" variant="primary">
            <i18n.Node id="modal.form.submit" />
        </Button>
    ),
    formProps = {},
    ...props
}: BuilderFormModalProps) {
    const formSubmit = {
        ...confirm,
        props: {
            ...confirm.props,
            onClick: null,
            type: 'submit',
        },
    };

    return (
        <BuilderConfirmModal
            {...props}
            confirm={formSubmit}
            wrap={<form {...formProps} />}
        />
    );
}
