import useRouteActive from '../../hooks/use-route-active';
import React from 'react';
import { CaretDown, CaretRight } from 'react-bootstrap-icons';
import { NavLink, To } from 'react-router-dom';
import { i18n } from '../..';

export interface BuilderDrawerNavItemProps
    extends React.HTMLAttributes<HTMLElement> {
    active?: boolean;
    as?: React.ElementType;
    button?: React.ElementType;
    buttonProps?: any;
    wrap?: React.ElementType | false;
    nav?: React.ElementType;
    children?: React.ReactNode;
    label?: React.ReactNode;
    i18nLabel?: string;
    icon?: React.ReactNode;
    actions?: React.ReactNode;
    collapse?: boolean;
    collapseable?: boolean;
    onToggleCollapse?: () => void;
}

export function BuilderDrawerNavItem({
    active = false,
    as: Component = 'div',
    button: ButtonComponent = 'button',
    buttonProps = {},
    wrap: WrapComponent = 'div',
    nav: NavComponent = BuilderDrawerNav,
    children,
    label = '',
    i18nLabel,
    icon = null,
    actions = null,
    collapse,
    onToggleCollapse,
    ...props
}: BuilderDrawerNavItemProps) {
    const collapseable =
        'collapseable' in props && props.collapseable !== false;
    delete props.collapseable;

    return (
        <Component
            {...props}
            className={`feb-drawer-nav-item ${active ? 'active' : ''} ${
                props.className || ''
            }`}
        >
            {WrapComponent !== false && (
                <WrapComponent className="feb-drawer-nav-button">
                    {collapseable && (
                        <button
                            className="feb-drawer-nav-collapse-button"
                            onClick={onToggleCollapse}
                        >
                            {collapse ? <CaretRight /> : <CaretDown />}
                        </button>
                    )}
                    <ButtonComponent
                        {...buttonProps}
                        className={`${buttonProps.className || ''} label`}
                    >
                        {icon && <span className="type-icon">{icon}</span>}
                        {i18nLabel ? i18n.c(i18nLabel) : label}
                    </ButtonComponent>
                    {actions && <span className="actions">{actions}</span>}
                </WrapComponent>
            )}
            {children && <NavComponent>{children}</NavComponent>}
        </Component>
    );
}

export interface BuilderDrawerNavLinkProps extends BuilderDrawerNavItemProps {
    to: To;
    caseSensitive?: boolean;
    end?: boolean;
}

export function BuilderDrawerNavLink({
    to,
    caseSensitive,
    end,
    ...props
}: BuilderDrawerNavLinkProps) {
    const active = useRouteActive(to, caseSensitive, end);

    return (
        <BuilderDrawerNavItem
            {...props}
            active={active}
            button={NavLink}
            buttonProps={{ to, caseSensitive, end }}
        />
    );
}

export interface BuilderDrawerNavProps
    extends React.HTMLAttributes<HTMLElement> {
    as?: React.ElementType;
}

export default function BuilderDrawerNav({
    as: Component = 'div',
    className = '',
    ...props
}: BuilderDrawerNavProps) {
    return <Component {...props} className={`feb-drawer-nav ${className}`} />;
}
