import React, { useEffect, useState } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import BuilderAppNavbar from '../app-navbar';
import { AppProvider, appStorage, useAppContext } from '../../hooks/use-app';
import ErrorBoundary from '../error-boundary';
import { Container, Row, Col } from 'react-bootstrap';
import { ExclamationSquareFill, XLg } from 'react-bootstrap-icons';
import useIdentity from '../../hooks/use-identity';
import BuilderLoginView from '../login-view';
import Builder from '../../index';

import { ToastContainer, CloseButtonProps } from 'react-toastify';
import { DialogContainer } from '../dialog';

function AppStateWriter() {
    const { state } = useAppContext();

    useEffect(() => {
        appStorage.setItem('default', state);
    }, [state]);

    return null;
}

export function BuilderAppError({ children }: { children?: React.ReactNode }) {
    return (
        <Container>
            <Row className="mt-5">
                <Col md={1}>
                    <ExclamationSquareFill
                        style={{
                            width: '100%',
                            height: 'auto',
                            color: 'rgba(0,0,0,.1)',
                        }}
                    />
                </Col>
                <Col md={11}>
                    <h1>Oops!</h1>
                    {children instanceof Error ? (
                        <>
                            <h5>{children.message}</h5>
                            <pre>{children.stack}</pre>
                        </>
                    ) : (
                        children || <h5>Unknown Error occured</h5>
                    )}
                </Col>
            </Row>
        </Container>
    );
}

function BuilderToastCloseBtn({ closeToast }: CloseButtonProps) {
    return (
        <button onClick={closeToast} className="feb-toast__close-btn">
            <XLg />
        </button>
    );
}

function useAppLocale() {
    const app = useAppContext();
    const [_forceUpdate, setForceUpdate] = useState(0);
    useEffect(() => {
        Builder.i18n.setItem('locale', app?.state?.locale);
        setForceUpdate((u) => u + 1);
    }, [app?.state?.locale, setForceUpdate]);
    return app?.state?.locale ?? 'default';
}

function BuilderApp({
    className = '',
    ...props
}: React.HTMLAttributes<HTMLDivElement>) {
    const { loggedIn } = useIdentity();

    useAppLocale();

    const content = (
        <>
            <BuilderAppNavbar />
            <Routes>
                {Builder.routes &&
                    Object.entries(Builder.routes).map(([key, route]) => (
                        <route.type key={key} {...route.props} />
                    ))}
                {Builder.defaultView && (
                    <Route path="*" element={Builder.defaultView} />
                )}
            </Routes>
        </>
    );

    const loggedOutContent = (
        <Routes>
            <Route path="*" element={<BuilderLoginView />} />
        </Routes>
    );

    return (
        <>
            <AppStateWriter />
            <BrowserRouter basename={Builder.basename}>
                <div {...props} className={`feb feb-app ${className}`}>
                    <ErrorBoundary errorComponent={BuilderAppError}>
                        {loggedIn ? content : loggedOutContent}
                    </ErrorBoundary>
                    <ToastContainer
                        position="bottom-center"
                        autoClose={2000}
                        progressStyle={{ backgroundColor: 'var(--bs-primary)' }}
                        closeButton={BuilderToastCloseBtn}
                    />
                    <DialogContainer />
                </div>
            </BrowserRouter>
        </>
    );
}

export default function BuilderAppWrap(
    props: React.ComponentProps<typeof BuilderApp>
) {
    return (
        <AppProvider value={appStorage.getItem('default', {})}>
            <BuilderApp {...props} />
        </AppProvider>
    );
}
