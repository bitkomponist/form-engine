import FormEngine from '@form-engine/core-application';
import { useFormContext } from '@form-engine/core-application/hooks/use-form';
import { FormDefinition } from '@form-engine/core-application/types/form';
import React, { useCallback, useEffect, useMemo, useRef } from 'react';
import {
    Button,
    ListGroup,
    OverlayTrigger,
    OverlayTriggerProps,
    Popover,
} from 'react-bootstrap';
import { Braces } from 'react-bootstrap-icons';
import { SelectInstance } from 'react-select';
import BuilderFormSelect from '../select';

export const STATIC_VARIABLE_OPTIONS = [
    'request.',
    'request.headers',
    'request.headers.authorization',
    'locale',
    'jwt.',
    'bso.',
];

function resolveVariableOptions(form?: FormDefinition): string[] {
    const options = [...STATIC_VARIABLE_OPTIONS];
    if (!form) return options;

    form?.elements?.forEach((element) => {
        const isField =
            element?.type && element?.type in FormEngine.getFieldElements();

        if (element.name && isField) {
            options.push(`values.${element.name}`);
        }
    });

    form?.constants?.forEach((constant) => {
        if (constant.id) {
            options.push(constant.id);
        }
    });

    return options;
}

export interface BuilderInsertVariableSelectorProps {
    options?: string[];
    onChange?: (value: string) => void;
    value?: string;
}

export function BuilderInsertVariableSelector({
    options: additionalOptions,
    onChange,
    value,
}: BuilderInsertVariableSelectorProps) {
    const { state: form } = useFormContext();
    const options = useMemo(
        () =>
            [...resolveVariableOptions(form), ...(additionalOptions ?? [])]
                .sort()
                .map((v) => ({ value: `{{${v}}}`, label: v })),
        [form, additionalOptions]
    );

    const handleChange = useCallback(
        (e: any) => {
            onChange?.(`${value ?? ''}${e?.target?.value ?? ''}`);
        },
        [value, onChange]
    );

    return (
        <div style={{ height: 250, width: 250, overflow: 'hidden' }}>
            <BuilderFormSelect.Advanced
                className="feb-variable-select"
                options={options}
                placeholder="Search…"
                menuIsOpen={true}
                minMenuHeight={222}
                maxMenuHeight={222}
                style={{ width: '100%' }}
                onChange={handleChange}
            />
        </div>
        // <ListGroup>
        //     {options.map((option, index) => (
        //         <ListGroup.Item
        //             action
        //             style={{ border: '0', padding: '5px' }}
        //             key={index}
        //             onClick={handleOptionClick}
        //         >
        //             {'{{'}
        //             {option}
        //             {'}}'}
        //         </ListGroup.Item>
        //     ))}
        // </ListGroup>
    );
}

const BuilderVariableSelectPopover = React.forwardRef(
    (
        {
            options,
            value,
            onChange,
            ...props
        }: Omit<React.ComponentProps<typeof Popover>, 'onChange'> &
            BuilderInsertVariableSelectorProps,
        ref
    ) => {
        return (
            <Popover ref={ref as React.Ref<HTMLDivElement>} body {...props}>
                <div
                    style={{
                        overflowY: 'auto',
                        margin: '-16px',
                        fontSize: '12px',
                    }}
                >
                    <BuilderInsertVariableSelector
                        value={value}
                        onChange={onChange}
                        options={options}
                    />
                </div>
            </Popover>
        );
    }
);
BuilderVariableSelectPopover.displayName = 'BuilderVariableSelectPopover';

export interface BuilderVariableSelectProps {
    options?: string[];
    disabled?: boolean;
    children?: OverlayTriggerProps['children'];
    value?: string;
    onChange?: (value: string) => void;
}

export default function BuilderVariableSelect({
    value,
    onChange,
    children,
    options,
    disabled,
}: BuilderVariableSelectProps) {
    const button = (
        <Button
            variant="outline-dark"
            size="sm"
            disabled={disabled}
            style={{ pointerEvents: disabled ? 'none' : 'initial' }}
        >
            <Braces />
        </Button>
    );

    return (
        <OverlayTrigger
            trigger="click"
            rootClose={true}
            transition={false}
            overlay={
                <BuilderVariableSelectPopover
                    value={value}
                    onChange={onChange}
                    options={options}
                />
            }
        >
            {children ?? button}
        </OverlayTrigger>
    );
}
