import React from 'react';

export interface DrawerProps extends React.HTMLAttributes<HTMLElement> {
    as?: React.ElementType;
}

export default function Drawer({
    as: Component = 'div',
    className = '',
    ...props
}: DrawerProps) {
    return <Component {...props} className={`feb-drawer ${className}`} />;
}
