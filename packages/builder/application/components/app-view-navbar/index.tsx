import React from 'react';
import { Nav, Navbar, Container } from 'react-bootstrap';

export default function BuilderAppViewNavbar({
    children,
    ...props
}: React.ComponentProps<typeof Navbar>) {
    return (
        <Navbar
            className="feb-app-view-navbar"
            bg="dark"
            variant="dark"
            {...props}
        >
            <Container fluid>
                <Nav className="w-100">{children}</Nav>
            </Container>
        </Navbar>
    );
}
