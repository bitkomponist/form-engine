import React from 'react';
import { Spinner } from 'react-bootstrap';

export function BuilderSpinner({
    children,
    ...props
}: React.ComponentProps<typeof Spinner>) {
    return (
        <Spinner
            role="status"
            variant="primary"
            {...props}
            animation={props.animation || 'border'}
        >
            {children}
            <span className="visually-hidden">Loading...</span>
        </Spinner>
    );
}
export function BuilderSpinnerBar({
    className,
    ...props
}: React.HTMLAttributes<HTMLElement>) {
    return <div className={`feb-spinner-bar ${className}`} {...props} />;
}

export interface BuilderSpinnerBoundaryProps
    extends React.HTMLAttributes<HTMLElement> {
    show?: boolean;
    spinner?: React.ElementType | false;
    bar?: React.ElementType | false;
    overlay?: boolean;
    children?: React.ReactNode;
    prepend?: React.ReactNode;
    append?: React.ReactNode;
}

export default function BuilderSpinnerBoundary({
    children,
    show = false,
    spinner: SpinnerComponent = BuilderSpinner,
    bar: BarComponent = BuilderSpinnerBar,
    className = '',
    prepend,
    append,
    ...props
}: BuilderSpinnerBoundaryProps) {
    const overlay = 'overlay' in props && props.overlay !== false;
    delete props.overlay;

    if (!show) return <>{children}</> || null;

    return (
        <>
            {prepend}
            <div
                className={`feb-spinner-boundary ${className} ${
                    overlay ? 'is-overlay' : ''
                }`}
                {...props}
            >
                {BarComponent && <BarComponent />}
                {SpinnerComponent && <SpinnerComponent />}
            </div>
            {append}
        </>
    );
}
