import React from 'react';
import { Col, Row } from 'react-bootstrap';
import BuilderDrawerNav, { BuilderDrawerNavItem } from '../drawer-nav';
import Drawer from '../drawer';
import { Plus, X } from 'react-bootstrap-icons';
import { useCallback } from 'react';
import BuilderAppCol from '../app-col';
import { i18n } from '../..';

function UnsetButton({
    id,
    onClick,
}: {
    id: string;
    onClick?: (id: string) => void;
}) {
    return (
        <button
            onClick={() => {
                onClick && onClick(id);
            }}
        >
            <X />
        </button>
    );
}

export interface BuilderExplorerItem {
    id: string;
    label?: React.ReactNode;
    icon?: React.ReactNode;
}

export interface BuilderExplorerProps
    extends Omit<React.ComponentProps<typeof Row>, 'onSelect'> {
    selectedId?: string | null;
    onSelect?: (id?: string | null) => void;
    items?: BuilderExplorerItem[];
    onUnset?: (id: string) => void;
    onAdd?: () => void;
}

export default function BuilderExplorer({
    as: Component = Row,
    selectedId = null,
    onSelect,
    items = [],
    onUnset,
    onAdd,
    children,
    ...props
}: BuilderExplorerProps) {
    const toggle = useCallback(
        (id: string) => {
            return (e: React.SyntheticEvent) => {
                e.stopPropagation();
                e.preventDefault();
                onSelect && onSelect(selectedId === id ? null : id);
            };
        },
        [selectedId, onSelect]
    );

    return (
        <Component {...props}>
            <BuilderAppCol md={2} border="right" id="appExplorerLeftCol">
                <Drawer>
                    <BuilderDrawerNav>
                        {onAdd && (
                            <>
                                <BuilderDrawerNavItem
                                    active={!selectedId}
                                    label={i18n.c('explorer.add')}
                                    icon={<Plus />}
                                    buttonProps={{ onClick: onAdd }}
                                />
                                <hr />
                            </>
                        )}
                        {items.map(({ id, label, icon }) => {
                            return (
                                <BuilderDrawerNavItem
                                    key={id}
                                    active={selectedId === id}
                                    buttonProps={{ onClick: toggle(id) }}
                                    label={label}
                                    icon={icon}
                                    actions={
                                        onUnset && (
                                            <UnsetButton
                                                id={id}
                                                onClick={onUnset}
                                            />
                                        )
                                    }
                                />
                            );
                        })}
                    </BuilderDrawerNav>
                </Drawer>
            </BuilderAppCol>
            <Col className="feb-main-col">{children}</Col>
        </Component>
    );
}
