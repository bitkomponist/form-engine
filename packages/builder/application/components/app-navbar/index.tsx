import React, { useCallback } from 'react';
import { NavLink, To, useNavigate } from 'react-router-dom';
import { Nav, Navbar, Container } from 'react-bootstrap';

import { HouseDoorFill, Plus, X } from 'react-bootstrap-icons';
import { useAppTabs } from '../../hooks/use-app';
import useRouteActive from '../../hooks/use-route-active';

interface CloseTabButtonProps extends React.HTMLAttributes<HTMLElement> {
    id: string;
    to: To;
}

function CloseTabButton({ id, to, ...props }: CloseTabButtonProps) {
    const navigate = useNavigate();
    const active = useRouteActive(to);
    const tabs = useAppTabs();
    const handleClick = useCallback(
        (e: React.MouseEvent) => {
            e.stopPropagation();
            e.preventDefault();
            tabs.toggle({ id }, false);
            if (active) {
                navigate('/');
            }
        },
        [id, active, navigate, tabs]
    );

    return (
        <button {...props} className="close-tab-btn" onClick={handleClick}>
            <X />
        </button>
    );
}

export default function BuilderAppNavbar() {
    const tabs = useAppTabs();
    return (
        <>
            <Navbar className="feb-app-navbar" bg="dark" variant="dark">
                <Container fluid>
                    <Nav className="me-auto">
                        <Nav.Link as={NavLink} to={'/'}>
                            <HouseDoorFill />
                        </Nav.Link>
                        {tabs.open
                            .filter((tab) => Boolean(tab?.path))
                            .map((tab, index) => (
                                <Nav.Link
                                    as={NavLink}
                                    to={tab.path as To}
                                    key={index}
                                >
                                    <span>{tab.name}</span>
                                    <CloseTabButton
                                        to={tab.path as To}
                                        id={tab.id}
                                    />
                                </Nav.Link>
                            ))}
                        <Nav.Link as={NavLink} to="/new">
                            <Plus />
                        </Nav.Link>
                    </Nav>
                </Container>
            </Navbar>
        </>
    );
}
