import React, { useCallback, useMemo } from 'react';
import { CaretDown, CaretRight } from 'react-bootstrap-icons';

export interface BuilderTreeNode {
    id: string;
    label: React.ReactNode;
    parent?: string;
    [key: string]: unknown;
}

export function useBuilderTreeNodes<T>(
    data: T,
    transformer?: (
        key: string | number,
        entry: any,
        id: string
    ) => Partial<BuilderTreeNode>
) {
    return useMemo(() => {
        const nodes: BuilderTreeNode[] = [];
        function traverse(state: any, parent?: string) {
            if (Array.isArray(state)) {
                state.forEach((item, index) => {
                    const id = `${parent ?? ''}[${index}]`;
                    const node = {
                        label: String(index),
                        ...(transformer ? transformer(index, item, id) : {}),
                        id,
                        parent,
                        $data: item,
                    };
                    nodes.push(node);
                    traverse(item, node.id);
                });
            } else if (typeof state === 'object' && state) {
                for (const [key, value] of Object.entries(state)) {
                    const id = `${parent ? `${parent}.` : ''}${key}`;
                    const node = {
                        label: key,
                        ...(transformer ? transformer(key, value, id) : {}),
                        id,
                        parent,
                        $data: value,
                    };
                    nodes.push(node);
                    traverse(value, node.id);
                }
            }
        }

        traverse(data);

        return nodes;
    }, [data, transformer]);
}

export interface BuilderTreeNodeProps {
    id: string;
    expanded?: boolean;
    label?: React.ReactNode;
    children?: React.ReactNode;
    as?: React.ElementType;
    onToggle?: (id: string, force?: boolean) => void;
}

export function BuilderTreeNode({
    as: Component = 'li',
    id,
    children,
    label,
    expanded,
    onToggle,
}: BuilderTreeNodeProps) {
    const handleToggle = useCallback(
        (e: React.MouseEvent) => {
            e.preventDefault();
            e.stopPropagation();
            onToggle?.(id, !expanded);
        },
        [onToggle, id, expanded]
    );

    let labelContent = label;

    if (children) {
        labelContent = (
            <button className="feb-tree-collapse-btn" onClick={handleToggle}>
                {expanded ? <CaretDown /> : <CaretRight />}
                {label}
            </button>
        );
    }

    return (
        <Component
            className={`feb-tree-item ${
                expanded ? 'feb-tree-item--expanded' : ''
            }`}
        >
            <div className="feb-tree-item__label">{labelContent}</div>
            <div className="feb-tree-item__children">{children}</div>
        </Component>
    );
}

export interface BuilderTreeProps {
    nodes?: BuilderTreeNode[];
    node?: React.ElementType;
    state?: string[];
    onToggle?: (id: string, force?: boolean) => void;
    as?: React.ElementType;
}

export function BuilderTree({
    as: Component = 'ul',
    nodes,
    node: NodeComponent = BuilderTreeNode,
    state,
    onToggle,
}: BuilderTreeProps) {
    const traverse = useCallback(
        (parent?: string) => {
            const children: BuilderTreeNode[] =
                nodes?.filter((node) => parent === node.parent) ?? [];

            return children.map((child) => {
                let children;
                const childListItems = traverse(child.id);

                if (childListItems.length) {
                    children = (
                        <Component className="feb-tree-level">
                            {childListItems}
                        </Component>
                    );
                }

                return (
                    <NodeComponent
                        key={child.id}
                        id={child.id}
                        label={child.label}
                        expanded={state?.includes(child.id)}
                        onToggle={onToggle}
                    >
                        {children}
                    </NodeComponent>
                );
            });
        },
        [nodes, NodeComponent, state, onToggle, Component]
    );

    if (!nodes) return null;

    return (
        <Component className="feb-tree feb-tree-level">{traverse()}</Component>
    );
}
