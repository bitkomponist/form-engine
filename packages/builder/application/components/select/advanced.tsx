import React, { CSSProperties, useCallback } from 'react';
import { useMemo } from 'react';
import { CaretDown, XLg, ExclamationSquareFill } from 'react-bootstrap-icons';
import Select, {
    DropdownIndicatorProps,
    ClearIndicatorProps,
} from 'react-select';
import CreatableSelect from 'react-select/creatable';

export function BuilderAdvancedFormSelectDropdownIndicator({
    innerProps,
}: DropdownIndicatorProps) {
    return (
        <span {...innerProps} className="feb-select__dropdown-indicator">
            <CaretDown />
        </span>
    );
}

export function BuilderAdvancedFormSelectClearIndicator({
    innerProps,
}: ClearIndicatorProps) {
    return (
        <span {...innerProps} className="feb-select__clear-indicator">
            <XLg />
        </span>
    );
}

export interface BuilderAdvancedFormSelectProps
    extends Omit<React.ComponentProps<typeof Select>, 'onChange'> {
    children?: React.ReactNode;
    onChange?: (newValue: any) => void;
    required?: boolean;
    style?: CSSProperties;
    creatable?: boolean;
}

export interface BuilderAdvancedFormSelectChild {
    type: React.ElementType;
    props: any;
    [prop: string]: any;
}

export const BuilderAdvancedFormSelect = React.forwardRef(
    (
        {
            children,
            options: optionsProp = [],
            value,
            className = '',
            onChange,
            ...props
        }: BuilderAdvancedFormSelectProps,
        ref
    ) => {
        const required = 'required' in props && props.required !== false;
        delete props.required;

        const creatable = 'creatable' in props && props.creatable !== false;
        delete props.creatable;

        const options = useMemo(() => {
            function traverse(items: BuilderAdvancedFormSelectChild[]) {
                const result: BuilderAdvancedFormSelectChild[] = [];
                for (const item of items) {
                    if (item.type === 'option') {
                        const option: any = {
                            ...item.props,
                            isOption: true,
                            label: String(
                                item.props.label || item.props.children
                            ),
                        };
                        delete option.children;
                        result.push(option);
                    } else if (item.type === 'optgroup') {
                        const group: any = {
                            ...item.props,
                            isGroup: true,
                            options: traverse(item.props.children),
                        };
                        delete group.children;
                        result.push(group);
                    } else if (typeof item === 'object') {
                        result.push({ ...item, isOption: true });
                    }
                }

                return result;
            }

            return traverse(
                (React.Children.count(children)
                    ? React.Children.toArray(children)
                    : optionsProp) as BuilderAdvancedFormSelectChild[]
            );
        }, [children, optionsProp]);

        const currentOption = useMemo(() => {
            if (props.isMulti) {
                return value;
            }

            function traverse(
                items: BuilderAdvancedFormSelectChild[]
            ): BuilderAdvancedFormSelectChild | undefined {
                for (const item of items) {
                    if (item.isOption && item.value === value) {
                        return item;
                    } else if (item.isGroup) {
                        const subItem = traverse(item.options || []);
                        if (subItem) {
                            return subItem;
                        }
                    }
                }
            }

            const result = traverse(options);

            // no value given or a viable option found for the value
            if (value === undefined || value === '' || result) {
                return result;
            }

            // given value does not exist in the options
            return {
                label: (
                    <>
                        <ExclamationSquareFill className="me-2" /> {value}
                    </>
                ),
            };
        }, [options, value, props.isMulti]);

        const handleChange = useCallback(
            (optionOrOptions: unknown, actionMeta: any) => {
                if (!onChange) return;

                if (props.isMulti) {
                    onChange({
                        target: { value: optionOrOptions ?? [] },
                        actionMeta,
                    });
                    return;
                }

                if (optionOrOptions) {
                    onChange({ target: optionOrOptions, actionMeta });
                } else {
                    onChange({ target: { value: '' }, actionMeta });
                }
            },
            [onChange, props]
        );

        const Component = creatable ? CreatableSelect : Select;

        return (
            <Component
                options={options}
                {...props}
                ref={ref}
                value={currentOption}
                className={`feb-select ${className}`}
                classNamePrefix={`feb-select`}
                onChange={handleChange}
                isClearable={!required}
                components={{
                    DropdownIndicator:
                        BuilderAdvancedFormSelectDropdownIndicator,
                    ClearIndicator: BuilderAdvancedFormSelectClearIndicator,
                }}
            />
        );
    }
);

BuilderAdvancedFormSelect.displayName = 'BuilderAdvancedFormSelect';
