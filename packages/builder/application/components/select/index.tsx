import React from 'react';
import { FormSelect } from 'react-bootstrap';
import {
    BuilderAdvancedFormSelect,
    BuilderAdvancedFormSelectProps,
} from './advanced';

export type BuilderFormSelectProps = React.ComponentProps<typeof FormSelect> &
    BuilderAdvancedFormSelectProps & {
        advanced?: boolean;
    };

export default function BuilderFormSelect({
    ...props
}: BuilderFormSelectProps) {
    if (props.options || props.advanced || props.searchable) {
        delete props.advanced;
        return <BuilderAdvancedFormSelect {...props} />;
    }

    return <FormSelect {...props} />;
}

BuilderFormSelect.Advanced = BuilderAdvancedFormSelect;
