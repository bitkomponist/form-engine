import uuidv4 from '@form-engine/core-util/uuid';
import React, { Fragment, useCallback, useEffect, useState } from 'react';
import BuilderModal, { BuilderConfirmModal, BuilderFormModal } from '../modal';

interface DialogDescriptor {
    props: DialogProps;
    id: string;
    promise?: Promise<any>;
}
type DialogListener = (id: string, node?: React.ReactNode) => void;
const dialogs: { [id: string]: DialogDescriptor } = {};
const listeners: DialogListener[] = [];

function subscribe(listener: DialogListener) {
    if (!listeners.includes(listener)) {
        listeners.push(listener);
    }
}

function unsubscribe(listener: DialogListener) {
    const index = listeners.indexOf(listener);
    if (index > -1) {
        listeners.splice(index, 1);
    }
}

function emit(...args: Parameters<DialogListener>) {
    for (const listener of listeners) {
        listener(...args);
    }
}

export function DialogContainer() {
    const [children, setChildren] = useState<{
        [key: string]: React.ReactNode;
    }>();

    useEffect(() => {
        let mounted = true;

        const listener: DialogListener = (id, child) => {
            if (!mounted) return;
            const copy = { ...children };
            if (!child) {
                delete copy[id];
            } else {
                copy[id] = child;
            }
            setChildren(copy);
        };

        subscribe(listener);

        return () => {
            unsubscribe(listener);
            mounted = false;
        };
    }, [children, setChildren]);

    if (!children) return null;

    return (
        <Fragment>
            {Object.entries(children).map(([key, child]) => (
                <Fragment key={key}>{child}</Fragment>
            ))}
        </Fragment>
    );
}

export interface DialogProps {
    as?: React.ElementType;
    onSuccess?: (result: any) => void;
    onError?: (error: any) => void;
    children?: React.ReactNode;
    [prop: string]: any;
}

function Dialog({
    as: Component = BuilderModal,
    onSuccess,
    onError,
    children,
    ...innerProps
}: DialogProps) {
    const [show, setShow] = useState(true);
    const [result, setResult] = useState<unknown>();
    const [error, setError] = useState<unknown>();
    const handleExited = useCallback(() => {
        if (error) {
            onError && onError(error);
        } else {
            onSuccess && onSuccess(result);
        }
    }, [result, error, onSuccess, onError]);

    return (
        <Component
            {...innerProps}
            show={show}
            onSuccess={(result: unknown) => {
                setResult(result);
                setShow(false);
            }}
            onError={(error: unknown) => {
                setError(error);
                setShow(false);
            }}
            onHide={() => setShow(false)}
            onExited={handleExited}
        >
            {children}
        </Component>
    );
}

export default function dialog(props: DialogProps = {}) {
    const id = uuidv4();

    dialogs[id] = {
        props: { ...props },
        id,
    };

    const promise = new Promise((resolve, reject) => {
        emit(
            id,
            <Dialog
                {...dialogs[id].props}
                onSuccess={resolve}
                onError={reject}
            />
        );
    });

    dialogs[id].promise = promise;

    function destroy() {
        emit(id, null);
        delete dialogs[id];
    }

    promise.then(destroy, destroy);

    return promise;
}

export function confirm(props: DialogProps = {}) {
    return dialog({ as: BuilderConfirmModal, ...props });
}

export function formDialog(props: DialogProps = {}) {
    return dialog({ as: BuilderFormModal, ...props });
}
