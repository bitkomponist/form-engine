import React from 'react';
import { Nav } from 'react-bootstrap';

export interface BuilderDrawerTabsProps
    extends Omit<React.ComponentProps<typeof Nav>, 'onChange'> {
    items?: { [key: string]: React.ReactNode };
    onChange?: (key: string) => void;
    prepend?: React.ReactNode;
    append?: React.ReactNode;
}

export default function BuilderDrawerTabs({
    items = {},
    onChange,
    prepend = null,
    append = null,
    children,
    ...props
}: BuilderDrawerTabsProps) {
    return (
        <Nav className="feb-drawer-tabs" variant="tabs" {...props}>
            {prepend}
            {Object.entries(items).map(([key, label]) => {
                return (
                    <Nav.Item key={key}>
                        <Nav.Link
                            eventKey={key}
                            onClick={() => {
                                onChange && onChange(key);
                            }}
                        >
                            {label || key}
                        </Nav.Link>
                    </Nav.Item>
                );
            })}
            {children}
            {append}
        </Nav>
    );
}
