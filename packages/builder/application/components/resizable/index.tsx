import React, {
    useCallback,
    useEffect,
    useMemo,
    useRef,
    useState,
} from 'react';
import { useAppContext } from '../../hooks/use-app';

function styleValueToNumber(value?: number | string, fallback = 0) {
    if (typeof value === 'number') return value;
    if (typeof value !== 'string') return fallback;
    const n = parseFloat(value);
    if (isNaN(n)) return fallback;
    return n;
}

function vwToNumber(vw?: number | string, fallback = 0) {
    if (typeof vw === 'number') return vw;
    const n = styleValueToNumber(vw, fallback);
    if (isNaN(n)) return fallback;
    return n * 0.01 * window.innerWidth;
}

function vhToNumber(vh?: number | string, fallback = 0) {
    if (typeof vh === 'number') return vh;
    const n = styleValueToNumber(vh, fallback);
    if (isNaN(n)) return fallback;
    return n * 0.01 * window.innerHeight;
}

function guardNumber(min: number, max: number, n: number) {
    return Math.min(max, Math.max(min, n));
}
export interface BuilderResizableState {
    width: number;
    height: number;
    vw: string;
    vh: string;
}

export type BuilderResizableHandle = 'top' | 'right' | 'bottom' | 'left';

export interface BuilderResizableProps
    extends Omit<React.HTMLAttributes<'div'>, 'onChange'> {
    id?: string;
    as?: React.ElementType;
    handles: BuilderResizableHandle[] | BuilderResizableHandle;
    width?: string | number;
    height?: string | number;
    minWidth?: string | number;
    maxWidth?: string | number;
    minHeight?: string | number;
    maxHeight?: string | number;
    onChange?: (state: BuilderResizableState) => void;
}

export default function BuilderResizable({
    id,
    as: Component = 'div',
    className = '',
    children,
    handles = [],
    width: initialWidth,
    height: initialHeight,
    minWidth: minWidthProp,
    maxWidth: maxWidthProp,
    minHeight: minHeightProp,
    maxHeight: maxHeightProp,
    onChange,
    ...props
}: BuilderResizableProps) {
    const { state: app, setItem: setAppItem } = useAppContext();

    const initialDimensions = useMemo(() => {
        const currentDimensions = id && app?.resizables?.[id];

        if (!currentDimensions) {
            return {
                width: undefined,
                height: undefined,
            };
        }

        return { ...currentDimensions };
    }, [id, id && app?.resizables?.[id]]);

    handles = typeof handles === 'string' ? [handles] : handles;
    const ref = useRef<HTMLElement>();
    const changeDimensions = useRef<(number | undefined)[]>();
    const [width, setWidth] = useState(
        vwToNumber(initialDimensions?.width ?? initialWidth) || undefined
    );
    useEffect(() => {
        let mounted = true;
        const update = () => {
            if (mounted && id && app?.resizables[id]?.width !== width) {
                setAppItem('resizables', {
                    ...(app?.resizables ?? {}),
                    [id]: {
                        ...(app?.resizables[id] ?? {}),
                        width,
                    },
                });
            }
        };
        const t = setTimeout(update, 300);
        return () => {
            clearTimeout(t);
            mounted = false;
        };
    }, [width, setAppItem, id, app?.resizables]);
    const [height, setHeight] = useState(
        vhToNumber(initialDimensions?.height ?? initialHeight) || undefined
    );
    useEffect(() => {
        let mounted = true;
        const update = () => {
            if (mounted && id && app?.resizables[id]?.height !== height) {
                setAppItem('resizables', {
                    ...(app?.resizables ?? {}),
                    [id]: {
                        ...(app?.resizables[id] ?? {}),
                        height,
                    },
                });
            }
        };
        const t = setTimeout(update, 300);
        return () => {
            clearTimeout(t);
            mounted = false;
        };
    }, [height, setAppItem, id, app?.resizables]);
    const [guards, setGuards] = useState({
        minWidth: 0,
        maxWidth: Number.MAX_VALUE,
        minHeight: 0,
        maxHeight: Number.MAX_VALUE,
    });
    const [initialState, setInitialState] = useState<{
        mouseX: number;
        mouseY: number;
        rect: DOMRect;
        handles: string[];
    } | null>(null);
    const handleStartDrag = useCallback(
        (e: React.MouseEvent) => {
            if (!ref.current || !e.target) return;
            setInitialState({
                mouseX: e.clientX,
                mouseY: e.clientY,
                rect: ref.current.getBoundingClientRect(),
                handles:
                    (e.target as HTMLElement)?.dataset?.handles?.split(',') ||
                    [],
            });
        },
        [setInitialState]
    );

    const handleDragging = useCallback(
        (e: MouseEvent) => {
            if (!initialState || !ref.current) return;
            const { clientX: x, clientY: y } = e;
            const { mouseX, mouseY, rect, handles } = initialState;
            const dimensions = [x - mouseX, y - mouseY];

            handles.forEach((handle) => {
                switch (handle) {
                    case 'top':
                        setHeight(
                            guardNumber(
                                guards.minHeight,
                                guards.maxHeight,
                                rect.height - dimensions[1]
                            )
                        );
                        break;
                    case 'right':
                        setWidth(
                            guardNumber(
                                guards.minWidth,
                                guards.maxWidth,
                                rect.width + dimensions[0]
                            )
                        );
                        break;
                    case 'bottom':
                        setHeight(
                            guardNumber(
                                guards.minHeight,
                                guards.maxHeight,
                                rect.height + dimensions[1]
                            )
                        );
                        break;
                    case 'left':
                        setWidth(
                            guardNumber(
                                guards.minWidth,
                                guards.maxWidth,
                                rect.width - dimensions[0]
                            )
                        );
                        break;
                    default:
                        throw new Error(`unsupported handle ${handle}`);
                }
            });
        },
        [initialState, setWidth, setHeight, guards]
    );

    useEffect(() => {
        changeDimensions.current = [width, height];
    }, [width, height]);

    useEffect(() => {
        let mounted = true;
        function handleResize() {
            if (!mounted) return;
            const guards = {
                minWidth: vwToNumber(minWidthProp),
                maxWidth: vwToNumber(maxWidthProp, Number.MAX_VALUE),
                minHeight: vhToNumber(minHeightProp),
                maxHeight: vhToNumber(maxHeightProp, Number.MAX_VALUE),
            };
            setGuards(guards);
            if (typeof width === 'number' && !isNaN(width)) {
                const guardedWidth = guardNumber(
                    guards.minWidth,
                    guards.maxWidth,
                    width
                );
                if (guardedWidth !== width) setWidth(guardedWidth);
            }

            if (typeof height === 'number' && !isNaN(height)) {
                const guardedHeight = guardNumber(
                    guards.minHeight,
                    guards.maxHeight,
                    height
                );
                if (guardedHeight !== height) setHeight(guardedHeight);
            }
        }

        document.addEventListener('resize', handleResize);
        handleResize();

        return () => {
            mounted = false;
            document.removeEventListener('resize', handleResize);
        };
    }, [
        setGuards,
        width,
        setWidth,
        height,
        setHeight,
        minWidthProp,
        maxWidthProp,
        minHeightProp,
        maxHeightProp,
    ]);
    useEffect(() => {
        let mounted = true;
        function stopDrag() {
            if (!mounted) return;
            setInitialState(null);
            const [changeWidth = 0, changeHeight = 0] =
                changeDimensions.current || [];
            const changeVw = `${
                !isNaN(changeWidth)
                    ? (changeWidth / window.innerWidth) * 100
                    : 0
            }vw`;
            const changeVh = `${
                !isNaN(changeHeight)
                    ? (changeHeight / window.innerHeight) * 100
                    : 0
            }vw`;
            onChange &&
                onChange({
                    width: changeWidth,
                    height: changeHeight,
                    vw: changeVw,
                    vh: changeVh,
                });
        }

        document.addEventListener('mouseup', stopDrag);
        document.addEventListener('blur', stopDrag);
        document.addEventListener('mousemove', handleDragging);

        return () => {
            mounted = false;
            document.removeEventListener('mouseup', stopDrag);
            document.removeEventListener('blur', stopDrag);
            document.removeEventListener('mousemove', handleDragging);
        };
    }, [handleDragging, setInitialState, onChange]);

    return (
        <Component
            ref={ref}
            {...props}
            className={`feb-resizable ${className}`}
            style={{ width, height }}
        >
            {children}
            {handles.includes('top') && (
                <div
                    className="feb-resizable__top"
                    data-handles="top"
                    onMouseDown={handleStartDrag}
                />
            )}
            {handles.includes('right') && (
                <div
                    className="feb-resizable__right"
                    data-handles="right"
                    onMouseDown={handleStartDrag}
                />
            )}
            {handles.includes('bottom') && (
                <div
                    className="feb-resizable__bottom"
                    data-handles="bottom"
                    onMouseDown={handleStartDrag}
                />
            )}
            {handles.includes('left') && (
                <div
                    className="feb-resizable__left"
                    data-handles="left"
                    onMouseDown={handleStartDrag}
                />
            )}
        </Component>
    );
}
