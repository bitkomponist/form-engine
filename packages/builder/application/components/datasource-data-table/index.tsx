import { useDatasourceData } from '@form-engine/core-application/hooks/use-datasource';
import React, { useState } from 'react';
import BuilderSpinnerBoundary from '../spinner-boundary';
import { useMemo } from 'react';
import BuilderTable, { Column } from '../table';
import { FormSessionProvider } from '@form-engine/core-application/hooks/use-formsession';
import { FormSession } from '@form-engine/core-application/types/form';

interface BuilderBoundDatasourceDataTableProps {
    id?: string;
}

function BuilderBoundDatasourceDataTable({
    id,
}: BuilderBoundDatasourceDataTableProps) {
    const { data: datasourceData, loading } = useDatasourceData(id);
    const hasData =
        datasourceData &&
        Array.isArray(datasourceData) &&
        datasourceData.length > 0;
    const tableData = useMemo(() => {
        if (!hasData) return null;

        return {
            columns: Object.keys(datasourceData[0]),
            rows: datasourceData,
        };
    }, [datasourceData, hasData]);

    return (
        <BuilderSpinnerBoundary show={loading}>
            {hasData ? (
                <BuilderTable sticky data={tableData?.rows}>
                    {tableData?.columns.map((key) => (
                        <Column
                            key={key}
                            dataKey={key}
                            cell={({ content: value }: any) =>
                                typeof value === 'string'
                                    ? value
                                    : JSON.stringify(value)
                            }
                        />
                    ))}
                </BuilderTable>
            ) : (
                <p className="text-center p-5 text-muted">
                    Data not displayable
                </p>
            )}
        </BuilderSpinnerBoundary>
    );
}

export interface BuilderDatasourceDataTableProps
    extends BuilderBoundDatasourceDataTableProps {
    session?: FormSession;
}

export default function BuilderDatasourceDataTable({
    session: initialSession,
    ...props
}: BuilderDatasourceDataTableProps) {
    const [session] = useState(initialSession || {});

    return (
        <FormSessionProvider value={session}>
            <BuilderBoundDatasourceDataTable {...props} />
        </FormSessionProvider>
    );
}
