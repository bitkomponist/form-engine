import React, { forwardRef } from 'react';

export default forwardRef<HTMLInputElement>(function BuilderInlineInput(
    { className = '', ...props }: React.HTMLAttributes<HTMLInputElement>,
    ref
) {
    return (
        <input
            ref={ref}
            {...props}
            className={`feb-inline-input ${className}`}
        />
    );
});
