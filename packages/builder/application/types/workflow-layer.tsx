import { WorkflowLayerType } from '@form-engine/core-application/types/form';

export interface BuilderWorkflowLayerDescriptor {
    label: string;
    description?: string;
    Icon: React.ElementType;
    WorkflowLayer: WorkflowLayerType;
    Mask: React.ElementType;
    Node?: React.ElementType;
}
