export interface BuilderEmbedDescriptor {
    label: string;
    description?: string;
    Icon: React.ElementType;
    Mask: React.ElementType;
}
