export interface Identity {
    name?: string;
    email?: string;
    [prop: string]: any;
}

export interface IdentityState {
    identity?: Identity;
    identityType?: string;
    identityError?: string;
    authorization?: string;
}
