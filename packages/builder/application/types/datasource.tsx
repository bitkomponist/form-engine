import { DatasourceType } from '@form-engine/core-application/types/form';

export interface BuilderDatasourceDescriptor {
    label: string;
    description?: string;
    exposesOptions?: boolean;
    Icon: React.ElementType;
    Datasource: DatasourceType;
    Mask: React.ElementType;
}
