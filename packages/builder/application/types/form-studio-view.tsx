export interface BuilderFormStudioViewDescriptor {
    label: string;
    Icon: React.ElementType;
    Component: React.ElementType;
}
