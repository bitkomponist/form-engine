import { To } from 'react-router-dom';

export interface AppTab {
    id: string;
    path?: To;
    name?: string;
}
