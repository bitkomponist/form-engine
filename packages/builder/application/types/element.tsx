import {
    ElementDefinition,
    ElementType,
} from '@form-engine/core-application/types/form';

export type BuilderElementConstructor<
    T extends ElementDefinition = ElementDefinition
> = (overrides?: Partial<T>) => T | T;

export interface BuilderElementDescriptor {
    label: string;
    description?: string;
    Icon: React.ElementType;
    Element: ElementType;
    Mask: React.ElementType;
    DrawerButton: React.ElementType;
}
