import { FormDefinition } from '@form-engine/core-application/types/form';

export interface FormPreset {
    required?: boolean;
    init?: (form: FormDefinition) => void;
}
