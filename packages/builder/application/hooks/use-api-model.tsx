import useMounted from '@form-engine/core-application/hooks/use-mounted';
import Builder from '../index';
import { useEffect } from 'react';
import { useState } from 'react';
import { useCallback } from 'react';
import useIdentity from './use-identity';

export interface ApiListOptions {
    filter?: { [attr: string]: unknown };
    startAt?: string;
    attributes?: string[];
    limit?: number;
    verbose?: boolean;
    includeVersioningHistory?: boolean;
    sort?: { [attr: string]: 'asc' | 'desc' };
}

export type ApiListResult<T> = T[];
export interface VerboseApiListResult<T> {
    results: ApiListResult<T>;
    lastKey?: string;
    count: number;
}

export interface ApiListProgressiveResult<T> {
    state: ApiListResult<T>;
    next: () => Promise<any>;
    complete: boolean;
}

export interface ApiDto {
    id?: string;
    createdAt?: string;
    updatedAt?: string;
    [prop: string]: any;
}

export default function useApiModel<T extends ApiDto = ApiDto>(
    modelPath: string
) {
    const { authorization } = useIdentity();
    const exec = useCallback(
        (path: string, options: RequestInit = {}) => {
            const headers: { [header: string]: string } = {};

            if (authorization) {
                headers.authorization = authorization;
            }

            return fetch(`${Builder.apiBaseUrl}/${modelPath}/${path}`, {
                ...options,
                headers: {
                    ...headers,
                    ...(options.headers || {}),
                },
            });
        },
        [authorization, modelPath]
    );
    const execJson = useCallback(
        (path: string, options: RequestInit = {}) => {
            return exec(path, options).then((response) => response.json());
        },
        [exec]
    );
    const retrieve = useCallback(
        (id: string) => {
            // if (!id) throw new Error('invalid argument id');
            return execJson(id) as Promise<T>;
        },
        [execJson]
    );
    const list = useCallback(
        (options: ApiListOptions = {}) => {
            const params: ApiListOptions = {
                filter: undefined,
                startAt: undefined,
                attributes: undefined,
                limit: undefined,
                sort: undefined,
                includeVersioningHistory: undefined,
                verbose: false,
                ...options,
            };

            return execJson(
                `?params=${encodeURIComponent(JSON.stringify(params))}`
            ) as Promise<ApiListResult<T> | VerboseApiListResult<T>>;
        },
        [execJson]
    );
    const listProgressive = useCallback(
        (options: ApiListOptions = {}) => {
            const params: ApiListOptions = {
                filter: undefined,
                startAt: undefined,
                attributes: undefined,
                limit: undefined,
                sort: undefined,
                includeVersioningHistory: undefined,
                ...options,
                verbose: true,
            };

            let state: T[] = [];
            let nextKey: string | undefined;
            async function fetchNext() {
                const { results, lastKey } = (await list(
                    nextKey ? { ...params, startAt: nextKey } : params
                )) as VerboseApiListResult<T>;
                state = [...state, ...results];
                nextKey = lastKey;
                return {
                    state: [...state],
                    complete: !lastKey,
                    next: lastKey
                        ? fetchNext
                        : async () =>
                              Promise.reject(
                                  new Error(
                                      `cant refetch completed progressive list`
                                  )
                              ),
                };
            }

            return fetchNext() as Promise<ApiListProgressiveResult<T>>;
        },
        [list]
    );
    const _delete = useCallback(
        (id: string) => {
            return exec(id, { method: 'DELETE' });
        },
        [exec]
    );
    const create = useCallback(
        (dto: Partial<T>) => {
            if (!dto) throw new Error('invalid argument dto');
            return execJson('', {
                method: 'POST',
                body: JSON.stringify(dto),
                headers: { 'Content-Type': 'application/json' },
            }) as Promise<T>;
        },
        [execJson]
    );
    const update = useCallback(
        (dto: T) => {
            if (!dto.id) throw new Error('invalid argument dto.id');
            return execJson(dto.id, {
                method: 'PATCH',
                body: JSON.stringify(dto),
                headers: { 'Content-Type': 'application/json' },
            }) as Promise<T>;
        },
        [execJson]
    );

    return {
        exec,
        execJson,
        create,
        retrieve,
        list,
        listProgressive,
        update,
        delete: _delete,
    };
}

export type ApiInstance<T extends ApiDto = ApiDto> = ReturnType<
    typeof useApiModel<T>
>;

export function useEntity<T extends ApiDto = ApiDto>(
    apiModel: ApiInstance<T>,
    id?: string
) {
    const { retrieve, update } = apiModel;
    const [entity, setEntity] = useState<T | null>(null);
    const [loading, setLoading] = useState(Boolean(id));
    const mounted = useMounted();

    useEffect(() => {
        if (!id) {
            setLoading(false);
            setEntity(null);
            return;
        }

        setLoading(true);
        retrieve(id).then((result) => {
            if (!mounted.current) return;
            setLoading(false);
            setEntity(result);
        });
    }, [retrieve, id, setLoading, setEntity]);

    const updateEntity = useCallback(
        (dto: T) => {
            if (!id) {
                setLoading(false);
                setEntity(null);
                return;
            }
            setLoading(true);
            update({ ...dto, id }).then((result) => {
                if (!mounted.current) return;
                setLoading(false);
                setEntity(result);
            });
        },
        [update, setEntity, setLoading, id]
    );

    return { state: entity, setState: updateEntity, loading };
}

export function useList<T extends ApiDto = ApiDto>(
    apiModel: ApiInstance<T>,
    options?: ApiListOptions
) {
    const { list } = apiModel;
    const [callId, setCallId] = useState(0);
    const [state, setState] = useState<ApiListResult<T>>([]);
    const [loading, setLoading] = useState(false);
    const mounted = useMounted();

    const refresh = useCallback(() => {
        setCallId(callId + 1);
    }, [callId, setCallId]);

    useEffect(() => {
        setLoading(true);
        list({ ...(options || {}), verbose: false }).then((result) => {
            if (!mounted.current) return;
            setLoading(false);
            setState(result as ApiListResult<T>);
        });
    }, [setLoading, setState, callId, list, options]);

    return { state, refresh, loading };
}

export function useProgressiveList<T extends ApiDto = ApiDto>(
    apiModel: ApiInstance<T>,
    options?: ApiListOptions
) {
    const { listProgressive } = apiModel;
    const [callId, setCallId] = useState(0);
    const [state, setState] = useState<ApiListProgressiveResult<T> | null>(
        null
    );
    const [loading, setLoading] = useState(false);
    const mounted = useMounted();

    const refresh = useCallback(() => {
        setCallId(callId + 1);
    }, [callId, setCallId]);

    const next = useCallback(() => {
        state?.next();
    }, [state]);

    useEffect(() => {
        setLoading(true);
        listProgressive(options).then((result) => {
            if (!mounted.current) return;
            setLoading(false);
            setState(result);
        });
    }, [setLoading, setState, callId, listProgressive, options]);

    return {
        state: state?.state ?? [],
        refresh,
        next,
        complete: state?.complete ?? false,
        loading,
    };
}
