import { FormDefinition } from '@form-engine/core-application/types/form';
import { useMemo } from 'react';

export default function useFormTags(forms?: FormDefinition[]) {
    return useMemo(() => {
        const list: string[] = [];

        for (const { tags } of forms ?? []) {
            if (Array.isArray(tags) && tags.length) {
                for (const tag of tags) {
                    if (!list.includes(tag)) {
                        list.push(tag);
                    }
                }
            }
        }

        list.sort((a, b) => a.localeCompare(b));

        return list;
    }, [forms]);
}
