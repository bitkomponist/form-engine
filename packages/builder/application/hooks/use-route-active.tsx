import { useLocation, useResolvedPath, To } from 'react-router-dom';

export default function useRouteActive(
    to: To,
    caseSensitive = false,
    end = false
) {
    const location = useLocation();
    const path = useResolvedPath(to);
    let locationPathname = location.pathname;
    let toPathname = path.pathname;

    if (!caseSensitive) {
        locationPathname = locationPathname.toLowerCase();
        toPathname = toPathname.toLowerCase();
    }

    return (
        locationPathname === toPathname ||
        (!end &&
            locationPathname.startsWith(toPathname) &&
            locationPathname.charAt(toPathname.length) === '/')
    );
}
