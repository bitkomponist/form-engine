import { useHotkeys, Options } from 'react-hotkeys-hook';
import { useAppContext } from './use-app';
import getDefaultKeybindings from '../keybindings';
import { KeyHandler } from 'hotkeys-js';

export function useHotkey(
    hotkeyName: string,
    callback: KeyHandler,
    options?: Options
) {
    const {
        state: { keybindings = {} },
    } = useAppContext();

    const keys =
        keybindings[hotkeyName] || getDefaultKeybindings()?.[hotkeyName];

    if (!keys) throw new Error(`unknown hotkey ${hotkeyName}`);

    return useHotkeys(keys, callback, options);
}
