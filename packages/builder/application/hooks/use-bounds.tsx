import { useRef } from 'react';
import { useEffect } from 'react';
import { useState } from 'react';

export default function useBounds<T extends HTMLElement = HTMLElement>() {
    const [bounds, setBounds] = useState(new DOMRect());
    const ref = useRef<T>();
    useEffect(() => {
        if (!ref.current) return;

        const el = ref.current;

        let mounted = true;

        const update = () => {
            if (!ref || !mounted || !ref.current) return;
            setBounds(el.getBoundingClientRect());
        };
        const observer = new ResizeObserver(update);
        observer.observe(el);
        update();
        return () => {
            mounted = false;
            observer.unobserve(el);
        };
    }, [setBounds]);

    return { bounds, ref };
}
