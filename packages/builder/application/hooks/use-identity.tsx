import useMounted from '@form-engine/core-application/hooks/use-mounted';
import Builder from '../index';
import { useCallback } from 'react';
import { useState } from 'react';
import { useMemo } from 'react';
import { useAppContext } from './use-app';
import { IdentityState } from '../types/identity';

export interface AuthCredentials {
    token?: string;
    username?: string;
    password?: string;
}
export default function useIdentity() {
    const [loading, setLoading] = useState(false);
    const mounted = useMounted();
    const {
        state: { identity: currentIdentity = {} },
        setItem,
    } = useAppContext();
    const identity = useMemo(() => {
        return {
            identity: null,
            identityType: null,
            identityError: null,
            authorization: null,
            ...currentIdentity,
        };
    }, [currentIdentity]);

    const logout = useCallback(() => {
        setItem('identity', {});
        setLoading(false);
    }, [setItem, setLoading]);

    const auth = useCallback(
        async (
            credentials: AuthCredentials,
            method: 'basic' | 'bearer' = 'basic'
        ) => {
            logout();
            setItem('identity', {});
            setLoading(true);
            const headers: { authorization?: string } = {};

            if (method === 'basic') {
                headers.authorization = `Basic ${btoa(
                    `${credentials.username || ''}:${
                        credentials.password || ''
                    }`
                )}`;
            } else if (method === 'bearer') {
                headers.authorization = `Bearer ${credentials.token}`;
            }

            try {
                const result = await fetch(`${Builder.apiBaseUrl}/identity`, {
                    headers,
                });
                const {
                    identity = null,
                    identityType = null,
                    identityError = null,
                } = (await result.json()) as IdentityState;
                if (mounted.current) {
                    setLoading(false);
                    setItem('identity', {
                        identity,
                        identityType,
                        identityError,
                        authorization: headers['authorization'],
                    });
                }
            } catch (e) {
                if (mounted.current) {
                    setLoading(false);
                    setItem('identity', {
                        identityError:
                            e instanceof Error ? e.message : String(e),
                    });
                }
            }
        },
        [setItem, logout]
    );

    return {
        ...identity,
        loggedIn: identity.identity !== null,
        loading,
        auth,
        logout,
    };
}
