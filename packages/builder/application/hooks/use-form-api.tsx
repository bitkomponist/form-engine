import { FormDefinition } from '@form-engine/core-application/types/form';
import useApiModel, {
    ApiListOptions,
    useEntity,
    useList,
} from './use-api-model';

export default function useFormApi() {
    return useApiModel<FormDefinition>('form');
}

export function useFormEntity(id?: string) {
    return useEntity(useFormApi(), id);
}

export const DEFAULT_FORM_LIST_OPTIONS: ApiListOptions = {
    attributes: ['id', 'name', 'updatedAt', 'createdAt', 'tags', 'thumbnail'],
};

export function useFormList(
    options: ApiListOptions = DEFAULT_FORM_LIST_OPTIONS
) {
    return useList(useFormApi(), options);
}
