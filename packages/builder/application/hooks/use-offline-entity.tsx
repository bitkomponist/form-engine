import { LocalStorageNamespace } from '@form-engine/core-util/local-storage';
import { useEffect } from 'react';
import { useState } from 'react';
import { ApiDto } from './use-api-model';

export const DEFAULT_COMPARE_DIRTY = <T extends ApiDto = ApiDto>(
    offlineEntity: T,
    onlineEntity: T,
    field: keyof T = 'updatedAt'
) => {
    return (
        !offlineEntity ||
        !onlineEntity ||
        new Date(offlineEntity[field]).getTime() <
            new Date(onlineEntity[field]).getTime()
    );
};

export function useOfflineEntity<T extends ApiDto = ApiDto>(
    offlineStorage: LocalStorageNamespace,
    onlineEntity?: T | null | undefined,
    dirtyFn = DEFAULT_COMPARE_DIRTY<T>
) {
    const [state, setState] = useState<T | undefined>(undefined);
    const [dirty, setDirty] = useState<boolean>(false);

    useEffect(() => {
        if (!onlineEntity || !onlineEntity.id) {
            setState(undefined);
            return;
        }

        const offlineEntity = offlineStorage.getItem(onlineEntity.id);
        if (dirtyFn(offlineEntity, onlineEntity)) {
            offlineStorage.setItem(onlineEntity.id, onlineEntity);

            /** @todo attempt merge diverging states */

            setState({ ...onlineEntity });
            setDirty(true);
        } else if (offlineEntity) {
            setState({ ...offlineEntity });
            setDirty(false);
        }
    }, [offlineStorage, onlineEntity, dirtyFn, setState, setDirty]);

    return { state, dirty };
}
