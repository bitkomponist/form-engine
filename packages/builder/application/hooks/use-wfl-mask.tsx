import useWorkflowLayer from '@form-engine/core-application/hooks/use-wfl';
import { useMemo } from 'react';
import Builder from '../index';

export default function useWorkflowLayerMask(
    id?: string
): React.FunctionComponent | null {
    const { state: workflowLayer } = useWorkflowLayer(id);
    return useMemo(() => {
        if (!workflowLayer || !workflowLayer.type) return null;
        const type = Builder?.workflowLayers?.[workflowLayer.type];
        if (!type || !type.Mask) return null;
        return type.Mask;
    }, [workflowLayer]);
}
