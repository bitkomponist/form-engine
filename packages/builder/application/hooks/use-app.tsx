import {
    StoreReference,
    useStore,
} from '@form-engine/core-application/hooks/use-store';
import { useObjectDefaults } from '@form-engine/core-util/create-object';
import { getStorageNamespace } from '@form-engine/core-util/local-storage';
import { Optional } from '@form-engine/core-util/typings';
import React, { createContext, useContext, useMemo } from 'react';
import { useCallback } from 'react';
import { AppTab } from '../types/app-tab';
import { IdentityState } from '../types/identity';

const DefaultAppState = {
    locale: 'default' as 'default' | 'de',
    appTabs: [] as AppTab[],
    identity: undefined as Optional<IdentityState>,
    keybindings: {} as { [key: string]: string },
    resizables: {} as { [key: string]: { width?: number; height?: number } },
};

export type AppState = typeof DefaultAppState;

export type AppContextValue = StoreReference<AppState> | null;

export const AppContext = createContext<AppContextValue>(null);

export const appStorage = getStorageNamespace('febApp');

export interface AppProviderProps {
    value: AppState;
    children?:
        | React.ReactNode
        | ((store: StoreReference<AppState>) => JSX.Element);
}

export function AppProvider({
    value: initialState,
    children,
}: AppProviderProps) {
    const store = useStore(useObjectDefaults(DefaultAppState, initialState));
    if (!store) return null;
    return (
        <AppContext.Provider value={store}>
            {typeof children === 'function' ? children(store) : children}
        </AppContext.Provider>
    );
}

export function useAppContext() {
    const context = useContext(AppContext);

    if (!context) {
        throw new Error('tried to use app context without provider');
    }

    return context;
}

export function useAppTabs() {
    const { state, setItem } = useAppContext();

    const tabs = useMemo(() => state?.appTabs || [], [state?.appTabs]);

    const toggle = useCallback(
        (tab: AppTab, force?: boolean) => {
            const index = tabs.findIndex((t) => t.id === tab.id);
            const copy = [...tabs];

            // potentially update tab data
            if (index > -1) {
                copy[index] = { ...copy[index], ...tab };
            }

            if (force !== false && index < 0) {
                copy.push({ ...tab });
            } else if (force !== true && index > -1) {
                copy.splice(index, 1);
            }

            setItem('appTabs', copy);
        },
        [tabs, setItem]
    );

    return {
        open: tabs,
        toggle,
    };
}
