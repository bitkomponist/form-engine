import Builder from '../index';
import useDatasource from '@form-engine/core-application/hooks/use-datasource';
import { useMemo } from 'react';

export default function useDatasourceMask(
    id?: string
): React.FunctionComponent | null {
    const { state: datasource } = useDatasource(id);
    return useMemo(() => {
        if (!datasource || !datasource.type) return null;
        const type = Builder?.datasources?.[datasource.type];
        if (!type || !type.Mask) return null;
        return type.Mask;
    }, [datasource]);
}
