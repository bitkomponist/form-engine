import BuilderVariableSelect from '@form-engine/builder-application/components/variable-select';
import IconButton from '@form-engine/builder-icon-button';
import React from 'react';
import { Braces } from 'react-bootstrap-icons';
import { CSSNS } from '.';
import BuilderArrayFormControlCell, {
    BuilderArrayFormControlCellProps,
} from './cell';

export interface BuilderArrayFormControlEditableCellProps
    extends BuilderArrayFormControlCellProps {
    value?: string;
    onChange: React.FormEventHandler<HTMLInputElement>;
    innerProps: {
        as?: React.ElementType;
        [key: string]: unknown;
    } & React.HTMLAttributes<HTMLInputElement>;
}

export default function BuilderArrayFormControlEditableCell({
    as: Component = BuilderArrayFormControlCell,
    className = '',
    innerProps: innerPropsProp = {},
    value,
    onChange,
    compilable = false,
    ...props
}: BuilderArrayFormControlEditableCellProps) {
    const { as: ControlComponent = 'input', ...innerProps } = innerPropsProp;

    return (
        <Component
            {...props}
            className={`${CSSNS}__editable-cell ${className} ${
                compilable ? 'd-flex flex-row' : ''
            }`}
        >
            <ControlComponent
                {...innerProps}
                value={value || ''}
                onChange={onChange}
                className="flex-grow"
            />
            {compilable && (
                <BuilderVariableSelect
                    value={value}
                    onChange={(value: string) =>
                        onChange?.({ target: { value } } as any)
                    }
                >
                    <IconButton color="secondary" className="ms-1">
                        <Braces />
                    </IconButton>
                </BuilderVariableSelect>
            )}
        </Component>
    );
}
