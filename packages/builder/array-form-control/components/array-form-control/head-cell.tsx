import React from 'react';
import { CSSNS } from '.';
import BuilderArrayFormControlCell, {
    BuilderArrayFormControlCellProps,
} from './cell';

export default function BuilderArrayFormControlHeadCell({
    as: Component = BuilderArrayFormControlCell,
    className = '',
    ...props
}: BuilderArrayFormControlCellProps) {
    const label =
        typeof props.col?.label === 'function' ? (
            <props.col.label />
        ) : (
            props.col?.label
        );

    return (
        <Component className={`${CSSNS}__headcell ${className}`} {...props}>
            {label}
        </Component>
    );
}
