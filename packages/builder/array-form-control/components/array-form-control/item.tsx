import React, { useCallback, useMemo, useState } from 'react';
import { GripHorizontal, X } from 'react-bootstrap-icons';
import IconButton from '@form-engine/builder-icon-button';
import {
    CSSNS,
    ARRAY_SCHEMA,
    OBJECT_SCHEMA,
    BuilderArrayFormControlColConfig,
} from '.';
import BuilderArrayFormControlEditableCell from './editable-cell';

export interface BuilderArrayFormControlItemProps<T = any>
    extends Omit<React.HTMLAttributes<HTMLElement>, 'onChange'> {
    as?: React.ElementType;
    cell?: React.ElementType;
    cols: BuilderArrayFormControlColConfig[];
    value: T;
    onChange?: (newData: T | null) => void;
    schema?: unknown;
    dnd?: boolean;
    index: number;
    onMove?: (sourceIndex: number, itemIndex: number) => void;
}

export function BuilderArrayFormControlItem<T = any>({
    as: Component = 'div',
    cell: CellComponent = BuilderArrayFormControlEditableCell,
    className = '',
    cols,
    value,
    onChange,
    schema,
    dnd = false,
    index: itemIndex,
    onMove,
    ...props
}: BuilderArrayFormControlItemProps<T>) {
    const [draggable, setDraggable] = useState(false);

    const cells: React.ReactNode[] = useMemo(() => {
        return cols.map((col, index) => {
            if (schema === ARRAY_SCHEMA) {
                return (value as unknown[])[index];
            } else if (col.key) {
                return (value as any)?.[col.key];
            }
        });
    }, [value, cols, schema]);

    const handleCellChange = useCallback(
        (index: number) => {
            return (event: React.ChangeEvent) => {
                if (!onChange) return;
                const cellValue =
                    (event.target as HTMLInputElement)?.value || '';

                let copy;
                if (schema === ARRAY_SCHEMA) {
                    copy = cols.map((_col, colIndex) => {
                        if (index === colIndex) return cellValue;
                        return (value as unknown[])[colIndex];
                    });
                } else if (schema === OBJECT_SCHEMA) {
                    copy = Object.fromEntries(
                        cols.map((col, colIndex) => {
                            if (!col.key)
                                throw new Error('invalid key configuration');
                            return [
                                col.key,
                                colIndex === index
                                    ? col.formatter
                                        ? col.formatter(cellValue)
                                        : cellValue
                                    : (value as any)?.[col.key],
                            ];
                        })
                    );
                } else if (typeof schema === 'function') {
                    const { key } = cols[index];
                    if (!key) throw new Error('invalid key configuration');
                    copy = schema({ ...value, [key]: cellValue });
                }

                onChange(copy);
            };
        },
        [value, onChange]
    );

    const handleDeleteItem = useCallback(() => {
        onChange && onChange(null);
    }, [onChange]);

    return (
        <Component
            {...props}
            className={`${CSSNS}__row ${className}`}
            draggable={draggable}
            onDragStart={(e: React.DragEvent) => {
                e.dataTransfer.dropEffect = 'move';
                e.dataTransfer.setData('sourceIndex', String(itemIndex));
            }}
            onDragOver={(e: React.DragEvent) => {
                e.preventDefault();
            }}
            onDrop={(e: React.DragEvent) => {
                const sourceIndex = Number(
                    e.dataTransfer.getData('sourceIndex')
                );
                if (!onMove || sourceIndex === itemIndex) return;
                onMove(sourceIndex, itemIndex);
            }}
        >
            {dnd && (
                <div className={`${CSSNS}__cell ${CSSNS}__prefix-cell`}>
                    <IconButton
                        className={`${CSSNS}__dnd-btn`}
                        onMouseOver={() => setDraggable(true)}
                        onMouseOut={() => setDraggable(false)}
                    >
                        <GripHorizontal />
                    </IconButton>
                </div>
            )}
            {cols.map((col, index) => {
                const {
                    cellComponent: Component = CellComponent,
                    key,
                    asModel,
                    props = {},
                    compilable,
                } = col;
                return (
                    <Component
                        key={key}
                        {...props}
                        compilable={compilable}
                        col={col}
                        value={asModel ? value : cells[index]}
                        onChange={asModel ? onChange : handleCellChange(index)}
                    />
                );
            })}
            <div className={`${CSSNS}__cell ${CSSNS}__suffix-cell`}>
                <IconButton
                    className={`${CSSNS}__remove-btn`}
                    onClick={handleDeleteItem}
                >
                    <X />
                </IconButton>
            </div>
        </Component>
    );
}
