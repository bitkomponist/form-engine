import React, { useCallback, useMemo } from 'react';
import { PlusCircleFill } from 'react-bootstrap-icons';
import IconButton from '@form-engine/builder-icon-button';
import BuilderArrayFormControlHeadCell from './head-cell';
import { BuilderArrayFormControlItem } from './item';
import { BuilderArrayFormControlEditableCellProps } from './editable-cell';
import { BuilderArrayFormControlCellProps } from './cell';

export const ARRAY_SCHEMA = 'array';
export const OBJECT_SCHEMA = 'object';
export const CSSNS = 'feb-array-form-control';

export interface BuilderArrayFormControlColConfig {
    key?: string;
    label?: React.ReactNode | React.ElementType;
    size?: number | string;
    defaultValue?: React.ReactNode;
    headCellComponent?: React.ElementType;
    cellComponent?: React.ElementType;
    asModel?: boolean;
    formatter?: (source: string) => string;
    compilable?: boolean;
    props?: Partial<
        | BuilderArrayFormControlEditableCellProps
        | BuilderArrayFormControlCellProps
        | any
    >;
}

export interface BuilderArrayFormControlProps<T = any>
    extends Omit<React.HTMLAttributes<HTMLElement>, 'onChange'> {
    as?: React.ElementType;
    onChange?: (newValue: T[]) => void;
    value?: T[];
    schema?: 'array' | 'object' | ((overrides?: T) => T);
    head?: boolean;
    cols?:
        | { [P in keyof T]: BuilderArrayFormControlColConfig | string }
        | [string, BuilderArrayFormControlColConfig][]
        | [string, string][];
    headCell?: React.ElementType;
    item?: React.ElementType;
    dnd?: boolean;
    disabled?: boolean;
}

export default function BuilderArrayFormControl<T = any>({
    schema = OBJECT_SCHEMA,
    cols: colsProp,
    value: valueProp,
    onChange,
    className,
    as: Component = 'div',
    head = true,
    headCell: HeadCellComponent = BuilderArrayFormControlHeadCell,
    item: ItemComponent = BuilderArrayFormControlItem,
    disabled = false,
    ...props
}: BuilderArrayFormControlProps<T>) {
    const dnd = props.dnd !== false;
    delete props.dnd;

    const value = useMemo(() => valueProp || [], [valueProp]);

    const cols = useMemo(() => {
        if (!colsProp) return [];
        let cols: [string, BuilderArrayFormControlColConfig | string][];
        if (!Array.isArray(colsProp)) {
            cols = Object.entries(colsProp);
        } else {
            cols = colsProp;
        }

        return cols.map(([key, config]): BuilderArrayFormControlColConfig => {
            if (!config) {
                return { key, label: key };
            }
            if (typeof config === 'string') {
                return { key, label: config };
            }
            return { ...config, key };
        });
    }, [colsProp]);

    const handleItemChange = useCallback(
        (index: number) => {
            return (data: T) => {
                const copy = [...value];
                if (!data) {
                    copy.splice(index, 1);
                    onChange && onChange(copy);
                    return;
                }

                copy[index] = data;
                onChange && onChange(copy);
            };
        },
        [value, onChange]
    );

    const handleAddItem = useCallback(() => {
        if (!onChange) return;
        let item: T | null = null;
        if (schema === ARRAY_SCHEMA) {
            item = cols.map(({ defaultValue = '' }) => defaultValue) as T;
        } else if (schema === OBJECT_SCHEMA) {
            item = Object.fromEntries(
                cols.map(({ defaultValue = '', key }) => [key, defaultValue])
            ) as T;
        } else if (typeof schema === 'function') {
            item = schema();
        }

        item && onChange([...value, item]);
    }, [value, onChange, schema, cols]);

    const handleMoveItem = useCallback(
        (sourceIndex: number, targetIndex: number) => {
            const copy = [...value];
            copy.splice(sourceIndex, 1);
            let insertIndex = copy.indexOf(value[targetIndex]);
            if (sourceIndex < targetIndex) {
                insertIndex += 1;
            }
            copy.splice(insertIndex, 0, value[sourceIndex]);
            onChange && onChange(copy);
        },
        [value, onChange]
    );

    return (
        <Component
            className={`${CSSNS} ${
                disabled ? `${CSSNS}--disabled` : ''
            } ${className}`}
            {...props}
        >
            {head && (
                <div className={`${CSSNS}__head`}>
                    {dnd && (
                        <HeadCellComponent
                            col={{ label: '' }}
                            className={`${CSSNS}__prefix-headcell`}
                        />
                    )}
                    {cols &&
                        cols.map((col, index) => {
                            const {
                                headCellComponent:
                                    Component = HeadCellComponent,
                            } = col;
                            return <Component col={col} key={index} />;
                        })}
                    <HeadCellComponent
                        col={{ label: '' }}
                        className={`${CSSNS}__suffix-headcell`}
                    />
                </div>
            )}
            <div className={`${CSSNS}__body`}>
                {value.map((item, index) => (
                    <ItemComponent
                        key={index}
                        cols={cols}
                        value={item}
                        schema={schema}
                        onChange={handleItemChange(index)}
                        dnd={dnd}
                        index={index}
                        onMove={handleMoveItem}
                    />
                ))}
            </div>
            <div className="text-end">
                <IconButton onClick={handleAddItem} title="Add item">
                    <PlusCircleFill />
                </IconButton>
            </div>
        </Component>
    );
}
