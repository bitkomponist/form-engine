import React, { useMemo } from 'react';
import { CSSNS, BuilderArrayFormControlColConfig } from '.';

export interface BuilderArrayFormControlCellProps
    extends React.HTMLAttributes<HTMLElement> {
    as?: React.ElementType;
    col?: BuilderArrayFormControlColConfig;
    compilable?: boolean;
}

export default function BuilderArrayFormControlCell({
    as: Component = 'div',
    className = '',
    col = {},
    ...props
}: BuilderArrayFormControlCellProps) {
    const { size } = col;

    const style: React.CSSProperties = useMemo(() => {
        const sizeStyle: React.CSSProperties = {};

        if (size === 'auto') {
            sizeStyle.flex = `1 0 auto`;
        } else if (typeof size === 'number') {
            sizeStyle.flex = `0 0 ${size}px`;
        } else {
            sizeStyle.flex = `0 0 ${size}`;
        }

        return {
            ...(props.style || {}),
            ...sizeStyle,
        };
    }, [size, props.style]);

    return (
        <Component
            {...props}
            style={style}
            className={`${CSSNS}__cell ${className}`}
        ></Component>
    );
}
