import React from 'react';
import useFormApi from '@form-engine/builder-application/hooks/use-form-api';
import useHotkey from '../hooks/use-hotkey';
import { setHotkey } from '@form-engine/builder-application/keybindings';
import { useFormContext } from '@form-engine/core-application/hooks/use-form';
import { useCallback } from 'react';
import { CheckCircleFill } from 'react-bootstrap-icons';
import { toast } from 'react-toastify';
import useMounted from '@form-engine/core-application/hooks/use-mounted';

export function useTogglePublishFormAction(force?: boolean) {
    const { state: form, setItem } = useFormContext();
    const { update } = useFormApi();
    const mounted = useMounted();

    return useCallback(() => {
        let published = !form.published;
        if (force !== undefined) {
            published = Boolean(force);
        }

        const toastId = toast(published ? 'publishing...' : 'unpublishing...');
        update({ ...form, published }).then((result) => {
            if (!mounted) return;
            setItem('published', result.published);
            toast.update(toastId, {
                render: (
                    <>
                        <CheckCircleFill className="me-2" />{' '}
                        {result.published
                            ? 'Form published'
                            : 'Form unpublished'}
                    </>
                ),
            });
        });
    }, [update, form, toast, setItem, force]);
}

export default function usePublishFormAction() {
    return useTogglePublishFormAction(true);
}

export const ACTION_NAME = 'formEditor.form.publish';

setHotkey(ACTION_NAME, 'ctrl+alt+p,cmd+alt+p');

export const useAction = usePublishFormAction;

export function useActionHotkey() {
    useHotkey(ACTION_NAME, useAction());
}
