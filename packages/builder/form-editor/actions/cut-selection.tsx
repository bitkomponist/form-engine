import { useFormEditorContext } from '../hooks/use-form-editor';
import useHotkey from '../hooks/use-hotkey';
import { setHotkey } from '@form-engine/builder-application/keybindings';
import { useFormCollection } from '@form-engine/core-application/hooks/use-form';
import { getItemIndexById } from '@form-engine/core-util/element-list';
import { useCallback } from 'react';
import { clipboard } from './copy-selection';

export default function useCutSelectionAction() {
    const { state: elements } = useFormCollection('elements');
    const { state: editor } = useFormEditorContext();

    return useCallback(() => {
        if (!editor?.selectedId) return;
        const index = getItemIndexById(elements, editor?.selectedId);
        if (index > -1) {
            clipboard.targetId = editor.selectedId;
            clipboard.value = JSON.stringify(elements[index]);
            clipboard.isCut = true;
        }
    }, [editor?.selectedId, elements]);
}

export const ACTION_NAME = 'formEditor.selection.cut';

setHotkey(ACTION_NAME, 'ctrl+x,cmd+x');

export const useAction = useCutSelectionAction;

export function useActionHotkey() {
    useHotkey(ACTION_NAME, useAction(), { editorFilter: 'editOnly' });
}
