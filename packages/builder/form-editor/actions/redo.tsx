import useHotkey from '../hooks/use-hotkey';
import { setHotkey } from '@form-engine/builder-application/keybindings';
import useFormHistory from '../hooks/use-form-history';

export default function useRedoAction() {
    return useFormHistory().redo;
}

export const ACTION_NAME = 'formEditor.form.redo';

setHotkey(ACTION_NAME, 'ctrl+shift+z,cmd+shift+z');

export const useAction = useRedoAction;

export function useActionHotkey() {
    useHotkey(ACTION_NAME, useAction());
}
