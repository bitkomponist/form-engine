import useHotkey from '../hooks/use-hotkey';
import { setHotkey } from '@form-engine/builder-application/keybindings';
import { useTogglePublishFormAction } from './publish-form';

export default function useUnpublishFormAction() {
    return useTogglePublishFormAction(false);
}

export const ACTION_NAME = 'formEditor.form.unpublish';

setHotkey(ACTION_NAME, 'ctrl+alt+u,cmd+alt+u');

export const useAction = useUnpublishFormAction;

export function useActionHotkey() {
    useHotkey(ACTION_NAME, useAction());
}
