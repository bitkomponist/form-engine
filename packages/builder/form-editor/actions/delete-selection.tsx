import { useFormEditorContext } from '../hooks/use-form-editor';
import useHotkey from '../hooks/use-hotkey';
import { setHotkey } from '@form-engine/builder-application/keybindings';
import { useFormCollection } from '@form-engine/core-application/hooks/use-form';
import { getItemById, removeItem } from '@form-engine/core-util/element-list';
import { useCallback } from 'react';

export default function useDeleteSelectionAction() {
    const { state: elements, set } = useFormCollection('elements');
    const { state: editor = {} } = useFormEditorContext();

    return useCallback(() => {
        if (!editor?.selectedId) return;
        const target = getItemById(elements, editor?.selectedId);
        if (target) {
            set(removeItem(elements, target));
        }
    }, [editor?.selectedId, elements, set]);
}

export const ACTION_NAME = 'formEditor.selection.delete';

setHotkey(ACTION_NAME, 'delete,backspace');

export const useAction = useDeleteSelectionAction;

export function useActionHotkey() {
    useHotkey(ACTION_NAME, useAction(), { editorFilter: 'editOnly' });
}
