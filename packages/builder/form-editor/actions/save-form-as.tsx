import React from 'react';
import useFormApi from '@form-engine/builder-application/hooks/use-form-api';
import useHotkey from '../hooks/use-hotkey';
import { setHotkey } from '@form-engine/builder-application/keybindings';
import { useFormContext } from '@form-engine/core-application/hooks/use-form';
import { useCallback } from 'react';
import { CheckCircleFill } from 'react-bootstrap-icons';
import { toast } from 'react-toastify';
import { useAppTabs } from '@form-engine/builder-application/hooks/use-app';
import useMounted from '@form-engine/core-application/hooks/use-mounted';
import { FormControl } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import { i18n } from '@form-engine/builder-application';
import {
    BuilderFormModal,
    BuilderFormModalProps,
} from '@form-engine/builder-application/components/modal';
import dialog from '@form-engine/builder-application/components/dialog';
import { FormDefinition } from '@form-engine/core-application/types/form';

export function SaveFormAsDialog({
    title = <i18n.Node id="form-editor.saveFormAsDialog.title" />,
    onSuccess,
    onError,
    state,
    ...props
}: BuilderFormModalProps & {
    state: FormDefinition;
    onSuccess?: (result: FormDefinition) => void;
    onError?: (error: any) => any;
}) {
    const { create } = useFormApi();
    const mounted = useMounted();
    const handleSubmit = useCallback(
        (e: React.FormEvent) => {
            e.preventDefault();
            e.stopPropagation();
            const data = new FormData(e.target as HTMLFormElement);
            const name = (data.get('name') as string)?.trim();
            const copy = { ...state, name };
            delete copy.id;
            const toastId = toast(<>Saving...</>);
            create(copy).then((duplicate) => {
                if (!mounted.current) return;
                onSuccess && onSuccess(duplicate);
                toast.update(toastId, {
                    render: (
                        <>
                            <CheckCircleFill className="me-2" />{' '}
                            <i18n.Node id="form-editor.saveFormAsDialog.successToast" />
                        </>
                    ),
                });
            }, onError);
        },
        [state, onSuccess, onError]
    );

    return (
        <BuilderFormModal {...props} onSubmit={handleSubmit} title={title}>
            <FormControl
                placeholder={i18n.c(
                    'form-editor.saveFormAsDialog.namePlaceholder'
                )}
                name="name"
                defaultValue={`${state?.name} Copy`}
            />
        </BuilderFormModal>
    );
}

export default function useSaveFormAsAction() {
    const { state } = useFormContext();
    const navigate = useNavigate();
    const { toggle: toggleTab } = useAppTabs();
    return useCallback(async () => {
        let duplicate: FormDefinition;
        try {
            duplicate = (await dialog({
                as: SaveFormAsDialog,
                state,
            })) as FormDefinition;
        } catch (e) {
            console.error(e);
            return;
        }
        if (duplicate && duplicate?.id) {
            const { id, name } = duplicate;
            const path = `/forms/${id}`;
            toggleTab({ id, name, path }, true);
            navigate(path);
        }
    }, [state, navigate, toggleTab]);
}

export const ACTION_NAME = 'formEditor.form.saveAs';

setHotkey(ACTION_NAME, 'ctrl+shift+s,cmd+shift+s');

export const useAction = useSaveFormAsAction;

export function useActionHotkey() {
    useHotkey(ACTION_NAME, useAction());
}
