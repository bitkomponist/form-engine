import React, { useEffect } from 'react';
import useFormApi from '@form-engine/builder-application/hooks/use-form-api';
import useHotkey from '../hooks/use-hotkey';
import { setHotkey } from '@form-engine/builder-application/keybindings';
import { useFormContext } from '@form-engine/core-application/hooks/use-form';
import { useCallback } from 'react';
import { CheckCircleFill } from 'react-bootstrap-icons';
import { toast } from 'react-toastify';
import { removeOrphans } from '@form-engine/core-util/element-list';
import { FormDefinition } from '@form-engine/core-application/types/form';

/** @todo remove this, its only for cleaning legacy forms */
function sanitizeForm(form: FormDefinition) {
    const copy = { ...form };
    if (copy.elements) {
        copy.elements = removeOrphans(copy.elements);
    }
    return copy;
}

let hasSaveListener = false;

export default function useSaveFormAction() {
    const { state: form } = useFormContext();
    const { update } = useFormApi();

    const callback = useCallback(() => {
        const toastId = toast(<>Saving...</>);

        update(sanitizeForm(form)).then(() => {
            toast.update(toastId, {
                render: (
                    <>
                        <CheckCircleFill className="me-2" /> Saved
                    </>
                ),
            });
        });
    }, [update, form, toast]);

    useEffect(() => {
        if (hasSaveListener) return;

        hasSaveListener = true;

        const disableNative = (e: KeyboardEvent) => {
            if (
                (e.ctrlKey || e.metaKey) &&
                !e.altKey &&
                !e.shiftKey &&
                e.key === 's'
            ) {
                e.preventDefault();
                e.stopPropagation();
                callback();
            }
        };

        document.body.addEventListener('keydown', disableNative);
        return () => {
            hasSaveListener = false;
            document.body.removeEventListener('keydown', disableNative);
        };
    }, [callback]);

    return callback;
}

export const ACTION_NAME = 'formEditor.form.save';

setHotkey(ACTION_NAME, 'ctrl+s,cmd+s');

export const useAction = useSaveFormAction;

export function useActionHotkey() {
    useHotkey(ACTION_NAME, useAction());
}
