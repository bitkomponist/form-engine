import { useFormEditorContext } from '../hooks/use-form-editor';
import useHotkey from '../hooks/use-hotkey';
import { setHotkey } from '@form-engine/builder-application/keybindings';
import { useCallback } from 'react';

export default function useUnselectAction() {
    const { state: editor = {}, setItem } = useFormEditorContext();

    return useCallback(() => {
        if (!editor?.selectedId) return;
        setItem('selectedId', null);
    }, [editor?.selectedId, setItem]);
}

export const ACTION_NAME = 'formEditor.selection.unselect';

setHotkey(ACTION_NAME, 'esc');

export const useAction = useUnselectAction;

export function useActionHotkey() {
    useHotkey(ACTION_NAME, useAction(), { editorFilter: 'editOnly' });
}
