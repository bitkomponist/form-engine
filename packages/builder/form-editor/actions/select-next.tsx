import { useFormEditorContext } from '../hooks/use-form-editor';
import useHotkey from '../hooks/use-hotkey';
import { setHotkey } from '@form-engine/builder-application/keybindings';
import { useFormCollection } from '@form-engine/core-application/hooks/use-form';
import { getItemIndexById } from '@form-engine/core-util/element-list';
import { useCallback } from 'react';

export default function useSelectNextAction() {
    const { state: elements } = useFormCollection('elements');
    const { state: editor = {}, setItem } = useFormEditorContext();

    return useCallback(() => {
        if (!editor?.selectedId) return;
        const index = getItemIndexById(elements, editor?.selectedId);
        const nextIndex = index + 1;
        if (index > -1 && nextIndex < elements.length) {
            setItem('selectedId', elements[nextIndex].id);
        }
    }, [editor?.selectedId, elements, setItem]);
}

export const ACTION_NAME = 'formEditor.selection.next';

setHotkey(ACTION_NAME, 'down,tab');

export const useAction = useSelectNextAction;

export function useActionHotkey() {
    useHotkey(ACTION_NAME, useAction(), { editorFilter: 'editOnly' });
}
