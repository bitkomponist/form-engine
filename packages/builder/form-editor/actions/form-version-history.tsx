import React from 'react';
import useHotkey from '../hooks/use-hotkey';
import { setHotkey } from '@form-engine/builder-application/keybindings';
import { useFormContext } from '@form-engine/core-application/hooks/use-form';
import { useCallback } from 'react';
import BuilderModal, {
    BuilderModalProps,
} from '@form-engine/builder-application/components/modal';
import dialog from '@form-engine/builder-application/components/dialog';
import BuilderFormVersionHistory from '../components/form-version-history';
import { FormDefinition } from '@form-engine/core-application/types/form';
import { StoreReference } from '@form-engine/core-application/hooks/use-store';
import { i18n } from '@form-engine/builder-application';

export function FormVersionHistoryDialog({
    title = <i18n.Node id="form-editor.formVersionHistoryDialog.title" />,
    formStore,
    onSuccess,
    ...props
}: BuilderModalProps & { formStore: StoreReference<FormDefinition> }) {
    return (
        <BuilderModal size="xl" {...props} title={title}>
            <BuilderFormVersionHistory
                formStore={formStore}
                onActionComplete={onSuccess}
            />
        </BuilderModal>
    );
}

export default function useFormVersionAction() {
    const storeReference = useFormContext();
    return useCallback(async () => {
        try {
            await dialog({
                as: FormVersionHistoryDialog,
                formStore: storeReference,
            });
        } catch (e) {
            console.error(e);
            return;
        }
    }, [storeReference]);
}

export const ACTION_NAME = 'formEditor.form.versionHistory';

setHotkey(ACTION_NAME, 'ctrl+h,cmd+shift+h');

export const useAction = useFormVersionAction;

export function useActionHotkey() {
    useHotkey(ACTION_NAME, useAction());
}
