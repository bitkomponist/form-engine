import React from 'react';
import useHotkey from '../hooks/use-hotkey';
import { setHotkey } from '@form-engine/builder-application/keybindings';
import { useFormContext } from '@form-engine/core-application/hooks/use-form';
import { useCallback } from 'react';
import BuilderModal, {
    BuilderModalProps,
} from '@form-engine/builder-application/components/modal';
import dialog from '@form-engine/builder-application/components/dialog';
import BuilderFormEmbedListing from '../components/form-embed-listing';
import { FormDefinition } from '@form-engine/core-application/types/form';
import { i18n } from '@form-engine/builder-application';

export function EmbedFormDialog({
    title = <i18n.Node id="form-editor.formEmbedDialog.title" />,
    state,
    ...props
}: BuilderModalProps & { state: FormDefinition }) {
    return (
        <BuilderModal size="lg" {...props} title={title}>
            <BuilderFormEmbedListing form={state} />
        </BuilderModal>
    );
}

export default function useEmbedFormAction() {
    const { state } = useFormContext();
    return useCallback(async () => {
        try {
            await dialog({ as: EmbedFormDialog, state });
        } catch (e) {
            console.error(e);
            return;
        }
    }, [state]);
}

export const ACTION_NAME = 'formEditor.form.embed';

setHotkey(ACTION_NAME, 'ctrl+shift+e,cmd+shift+e');

export const useAction = useEmbedFormAction;

export function useActionHotkey() {
    useHotkey(ACTION_NAME, useAction());
}
