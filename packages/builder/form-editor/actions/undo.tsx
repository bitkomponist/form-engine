import useHotkey from '../hooks/use-hotkey';
import { setHotkey } from '@form-engine/builder-application/keybindings';
import useFormHistory from '../hooks/use-form-history';

export default function useUndoAction() {
    return useFormHistory().undo;
}

export const ACTION_NAME = 'formEditor.form.undo';

setHotkey(ACTION_NAME, 'ctrl+z,cmd+z');

export const useAction = useUndoAction;

export function useActionHotkey() {
    useHotkey(ACTION_NAME, useAction());
}
