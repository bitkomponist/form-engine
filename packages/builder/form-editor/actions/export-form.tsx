import useHotkey from '../hooks/use-hotkey';
import { setHotkey } from '@form-engine/builder-application/keybindings';
import { useFormContext } from '@form-engine/core-application/hooks/use-form';
import { useCallback } from 'react';
import { FormDefinition } from '@form-engine/core-application/types/form';

function exportAsFile(
    source: FormDefinition,
    filename = 'export.xfe',
    type = 'application/x-xfe'
) {
    const data = new Blob([JSON.stringify(source, undefined, 2)], { type });
    const toggle = document.createElement('a');
    toggle.download = filename;
    toggle.href = URL.createObjectURL(data);
    toggle.click();
}

export default function useExportFormAction() {
    const { state } = useFormContext();
    return useCallback(async () => {
        exportAsFile(state, `${state.name || 'untitled'}.xfe`);
    }, [state]);
}

export const ACTION_NAME = 'formEditor.form.export';

setHotkey(ACTION_NAME, 'ctrl+alt+e,cmd+alt+e');

export const useAction = useExportFormAction;

export function useActionHotkey() {
    useHotkey(ACTION_NAME, useAction());
}
