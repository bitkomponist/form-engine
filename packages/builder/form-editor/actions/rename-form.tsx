import React from 'react';
import useHotkey from '../hooks/use-hotkey';
import { setHotkey } from '@form-engine/builder-application/keybindings';
import { useFormContext } from '@form-engine/core-application/hooks/use-form';
import { useCallback } from 'react';
import { FormControl } from 'react-bootstrap';
import {
    BuilderFormModal,
    BuilderFormModalProps,
} from '@form-engine/builder-application/components/modal';
import dialog from '@form-engine/builder-application/components/dialog';
import { i18n } from '@form-engine/builder-application';

export function RenameFormDialog({
    title = <i18n.Node id="form-editor.renameFormDialog.title" />,
    value = '',
    onSuccess,
    ...props
}: BuilderFormModalProps & {
    value?: string;
    onSuccess: (value: string) => void;
}) {
    const handleSubmit = useCallback(
        (e: React.FormEvent) => {
            e.preventDefault();
            e.stopPropagation();
            const data = new FormData(e.target as HTMLFormElement);
            const name = (data?.get('name') as string)?.trim();
            onSuccess && onSuccess(name);
        },
        [onSuccess]
    );

    return (
        <BuilderFormModal {...props} onSubmit={handleSubmit} title={title}>
            <FormControl
                placeholder={i18n.c(
                    'form-editor.renameFormDialog.namePlaceholder'
                )}
                name="name"
                defaultValue={value}
            />
        </BuilderFormModal>
    );
}

export default function useRenameFormAction() {
    const { state, setItem } = useFormContext();
    return useCallback(async () => {
        let name;
        try {
            name = await dialog({
                as: RenameFormDialog,
                value: state?.name || '',
            });
        } catch (e) {
            console.error(e);
            return;
        }
        if (typeof name === 'string') setItem('name', name);
    }, [state, setItem]);
}

export const ACTION_NAME = 'formEditor.form.rename';

setHotkey(ACTION_NAME, 'ctrl+alt+r,cmd+alt+r');

export const useAction = useRenameFormAction;

export function useActionHotkey() {
    useHotkey(ACTION_NAME, useAction());
}
