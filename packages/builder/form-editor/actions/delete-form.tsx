import React from 'react';
import useFormApi from '@form-engine/builder-application/hooks/use-form-api';
import useHotkey from '../hooks/use-hotkey';
import { setHotkey } from '@form-engine/builder-application/keybindings';
import { useFormContext } from '@form-engine/core-application/hooks/use-form';
import { useCallback } from 'react';
import { toast } from 'react-toastify';
import { useAppTabs } from '@form-engine/builder-application/hooks/use-app';
import useMounted from '@form-engine/core-application/hooks/use-mounted';
import { useNavigate } from 'react-router-dom';
import {
    BuilderConfirmModal,
    BuilderConfirmModalProps,
} from '@form-engine/builder-application/components/modal';
import dialog from '@form-engine/builder-application/components/dialog';
import { FormDefinition } from '@form-engine/core-application/types/form';
import { i18n } from '@form-engine/builder-application';

function DeleteFormDialog({
    state,
    onSuccess,
    ...props
}: BuilderConfirmModalProps & {
    state: FormDefinition;
    onSuccess?: (success: boolean) => void;
}) {
    return (
        <BuilderConfirmModal
            {...props}
            onConfirm={() => onSuccess && onSuccess(true)}
            onHide={() => onSuccess && onSuccess(false)}
        >
            <i18n.Node
                id="form-editor.deleteFormDialog.message"
                name={state?.name}
            />
        </BuilderConfirmModal>
    );
}

export default function useDeleteFormAction() {
    const { state } = useFormContext();
    const navigate = useNavigate();
    const { toggle: toggleTab } = useAppTabs();
    const { delete: deleteForm } = useFormApi();
    const mounted = useMounted();
    return useCallback(async () => {
        if (!state?.id) return;
        let confirm = false;
        try {
            confirm = (await dialog({
                as: DeleteFormDialog,
                state,
            })) as boolean;
        } catch (e) {
            console.error(e);
            return;
        }
        if (confirm) {
            const toastId = toast(
                i18n.c('form-editor.deleteFormDialog.busyToast', {
                    name: state?.name,
                })
            );
            toggleTab({ id: state.id }, false);
            deleteForm(state.id).then(() => {
                toast.update(toastId, {
                    render: i18n.c('form-editor.deleteFormDialog.doneToast', {
                        name: state?.name,
                    }),
                });
                if (mounted.current) navigate('/');
            });
        }
    }, [state, navigate, toggleTab, deleteForm]);
}

export const ACTION_NAME = 'formEditor.form.delete';

setHotkey(ACTION_NAME, 'ctrl+alt+backspace,cmd+alt+backspace');

export const useAction = useDeleteFormAction;

export function useActionHotkey() {
    useHotkey(ACTION_NAME, useAction());
}
