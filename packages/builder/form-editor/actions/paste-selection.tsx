import { useFormEditorContext } from '../hooks/use-form-editor';
import useHotkey from '../hooks/use-hotkey';
import { setHotkey } from '@form-engine/builder-application/keybindings';
import { useFormElements } from '@form-engine/core-application/hooks/use-form';
import {
    cloneItemBranch,
    getItemIndexById,
    insertItemAfter,
    relocateItemAfter,
} from '@form-engine/core-util/element-list';
import { useCallback } from 'react';
import { clipboard } from './copy-selection';
import FormEngine from '@form-engine/core-application';

export default function usePasteSelectionAction() {
    const { state: elements, set } = useFormElements();
    const { state: editor, setItem: setEditorItem } =
        useFormEditorContext();

    return useCallback(() => {
        if (!clipboard.value) return;
        const destination = editor?.selectedId
            ? elements[getItemIndexById(elements, editor?.selectedId as string)]
            : undefined;

        const { id, parent: destParent, type } = destination || {};

        const parent =
            type && type in FormEngine.getContainerElements() ? id : destParent;

        const after = parent !== id ? id : null;

        const value = JSON.parse(clipboard.value);

        const source = { ...value, parent };

        if (clipboard.isCut) {
            set(relocateItemAfter(elements, source, after));
            clipboard.targetId = undefined;
            clipboard.value = undefined;
            clipboard.isCut = false;
        } else {
            // clone nested
            const [branchRoot, ...children] = cloneItemBranch(elements, source);
            if (branchRoot) {
                branchRoot.parent = parent;
                // insert clone root
                const updatedItems = insertItemAfter(
                    elements,
                    branchRoot,
                    after
                );
                const index = getItemIndexById(updatedItems, branchRoot.id);
                // insert clone children after root
                updatedItems.splice(index, 0, ...children);
                set(updatedItems);
            }
        }
        setEditorItem('selectedId', source.id);
    }, [editor?.selectedId, elements, set, setEditorItem]);
}

export const ACTION_NAME = 'formEditor.selection.paste';

setHotkey(ACTION_NAME, 'ctrl+v,cmd+v');

export const useAction = usePasteSelectionAction;

export function useActionHotkey() {
    useHotkey(ACTION_NAME, useAction(), { editorFilter: 'editOnly' });
}
