import React from 'react';
import { Route } from 'react-router-dom';
import BuilderFormNewView from './components/form-new-view';
import BuilderFormEditorView from './components/form-studio';
import BuilderHyperlinkEmbed from './components/hyperlink-embed';
import BuilderInlineEmbed from './components/inline-embed';
import { BuilderPlugin } from '@form-engine/builder-application';
import DatasourcesEditor from './components/datasources-editor';
import LayoutEditor from './components/layout-editor';
import WorkflowEditor from './components/workflow-editor';
import ConstantsEditor from './components/constants-editor';
import BuilderFormEngine from './components/form-engine';
import BuilderOutlineDrawer from './components/outline-drawer';
import BuilderElementsDrawer from './components/elements-drawer';
import BuilderSettingsDrawer from './components/settings-drawer';
import BuilderSessionDrawer from './components/session-drawer';
import { BuilderFormLayoutEditorViewMap } from './components/layout-editor/view';
import { Nav } from 'react-bootstrap';
import { ArrowClockwise } from 'react-bootstrap-icons';
import { FormEngineRemote } from '@form-engine/core-application/components/engine';
import { BuilderFormEditorPreviewFormEngine } from './components/layout-editor/preview';
import { BuilderFormEditorLocaleSelector } from './components/layout-editor/locale-selector';
import { BuilderFormEditorPreviewSettings } from './components/layout-editor/preview-settings-drawer';
import * as translations from './builder-translations';

export default {
    // renderFormRoute: '/public/:formId',
    translations,
    routes: {
        formEditor: (
            <Route path="forms/:formId" element={<BuilderFormEditorView />} />
        ),
        formNew: <Route path="new" element={<BuilderFormNewView />} />,
    },
    formEmbeds: {
        BuilderHyperlinkEmbed,
        BuilderInlineEmbed,
    },
    formStudioViews: {
        form: LayoutEditor,
        datasources: DatasourcesEditor,
        workflow: WorkflowEditor,
        constants: ConstantsEditor,
    },
    formLayoutEditorViews: {
        edit: {
            title: 'form-editor.layout.edit.title',
            component: BuilderFormEngine,
            leftDrawerTabs: {
                outline: {
                    title: 'form-editor.layout.edit.outline',
                    component: BuilderOutlineDrawer,
                },
                library: {
                    title: 'form-editor.layout.edit.components',
                    component: BuilderElementsDrawer,
                },
            },
            rightDrawerTabs: {
                settings: {
                    title: 'form-editor.layout.edit.settings',
                    component: BuilderSettingsDrawer,
                },
            },
            defaultLeftDrawerTab: 'outline',
            defaultRightDrawerTab: 'settings',
        },
        translate: {
            title: 'form-editor.layout.translate.title',
            component: BuilderFormEngine,
            leftDrawerTabs: {
                outline: {
                    title: 'form-editor.layout.translate.outline',
                    component: BuilderOutlineDrawer,
                },
            },
            rightDrawerTabs: {
                settings: {
                    title: 'form-editor.layout.translate.settings',
                    component: BuilderSettingsDrawer,
                },
            },
            defaultLeftDrawerTab: 'outline',
            defaultRightDrawerTab: 'settings',
            navItemsComponent: () => {
                return (
                    <>
                        <Nav.Item>
                            <BuilderFormEditorLocaleSelector />
                        </Nav.Item>
                    </>
                );
            },
        },
        preview: {
            title: 'form-editor.layout.preview.title',
            component: BuilderFormEditorPreviewFormEngine,
            navItemsComponent: ({
                remote,
            }: {
                remote?: React.MutableRefObject<FormEngineRemote | undefined>;
            }) => {
                return (
                    <>
                        <Nav.Item>
                            <Nav.Link
                                as="button"
                                onClick={() => {
                                    remote?.current?.reset?.();
                                    // immediately set preview options
                                }}
                            >
                                <ArrowClockwise />
                            </Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                            <BuilderFormEditorLocaleSelector />
                        </Nav.Item>
                    </>
                );
            },
            leftDrawerTabs: {
                outline: {
                    title: 'form-editor.layout.preview.outline',
                    component: BuilderOutlineDrawer,
                },
            },
            rightDrawerTabs: {
                settings: {
                    title: 'form-editor.layout.preview.settings',
                    component: BuilderFormEditorPreviewSettings,
                },
                scope: {
                    title: 'form-editor.layout.preview.scope',
                    component: BuilderSessionDrawer,
                },
            },
            defaultLeftDrawerTab: 'outline',
            defaultRightDrawerTab: 'scope',
        },
    } as BuilderFormLayoutEditorViewMap,
    maskConfigurations: {
        BuilderFormMask: {
            edit: {
                permissions: {
                    name: 'write',
                    published: 'write',
                    slug: 'write',
                    tags: 'write',
                    showValidFeedback: 'write',
                    includeCoreResources: 'write',
                    includeBootstrapCdn: 'write',
                    bootstrapCdnUrl: 'write',
                    resources: 'write',
                    stylesheet: 'write',
                },
            },
            preview: {
                permissions: {
                    name: 'write',
                    published: 'write',
                    slug: 'write',
                    tags: 'write',
                    showValidFeedback: 'write',
                    includeCoreResources: 'write',
                    includeBootstrapCdn: 'write',
                    bootstrapCdnUrl: 'write',
                    resources: 'write',
                    stylesheet: 'write',
                },
            },
            translate: {
                permissions: {
                    name: 'read',
                    published: 'read',
                    slug: 'read',
                    tags: 'read',
                    showValidFeedback: 'read',
                    includeCoreResources: 'read',
                    includeBootstrapCdn: 'read',
                    bootstrapCdnUrl: 'read',
                    resources: 'read',
                    stylesheet: 'write',
                },
            },
        },
    },
} as BuilderPlugin;
