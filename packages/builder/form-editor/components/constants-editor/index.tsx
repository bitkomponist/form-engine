import { BuilderFormStudioViewDescriptor } from '@form-engine/builder-application/types/form-studio-view';
import { List as Icon } from 'react-bootstrap-icons';
import { BuilderFormEditorViewConstants as Component } from './editor';

export default {
    label: 'form-editor.constants.title',
    description: 'form-editor.constants.description',
    Component,
    Icon,
} as BuilderFormStudioViewDescriptor;
