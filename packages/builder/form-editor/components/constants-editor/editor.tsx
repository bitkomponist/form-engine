import React from 'react';
import { useFormConstants } from '@form-engine/core-application/hooks/use-form';
import { Col, Row } from 'react-bootstrap';
import BuilderArrayFormControl from '@form-engine/builder-array-form-control';
import { formatResourceNameCase } from '@form-engine/core-util/format-case';

export function BuilderFormEditorViewConstants() {
    const { state: constants = [], set } = useFormConstants();

    return (
        <Row>
            <Col className="feb-main-col p-0">
                <BuilderArrayFormControl
                    cols={{
                        id: {
                            label: 'Constant',
                            size: 200,
                            formatter: formatResourceNameCase,
                        },
                        value: { label: 'Value', size: '33%' },
                        notes: { label: 'Notes', size: 'auto' },
                    }}
                    value={constants}
                    onChange={set}
                />
            </Col>
        </Row>
    );
}
