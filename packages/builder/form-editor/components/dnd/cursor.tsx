import { useFormEditorContext } from '../../hooks/use-form-editor';
import React, { useMemo } from 'react';
import { Badge } from 'react-bootstrap';
import BuilderCursor from '@form-engine/builder-application/components/cursor';
import Builder from '@form-engine/builder-application';

export default function BuilderDragAndDropCursor() {
    const { state } = useFormEditorContext();

    const elementTypeIcons = useMemo(
        () => Builder.getItemSubtypeMap('elements', 'Icon'),
        []
    );

    const elementTypeLabels = useMemo(
        () => Builder.getItemSubtypeMap('elements', 'label'),
        []
    );

    let label = '';
    let icon = null;
    const currentElement = state?.source;

    if (currentElement) {
        const IconType =
            currentElement.type in elementTypeIcons
                ? elementTypeIcons[currentElement.type]
                : null;

        if (IconType) icon = <IconType />;

        if (currentElement.name) {
            label = currentElement.name;
        } else if (currentElement.type) {
            label =
                currentElement.type in elementTypeLabels
                    ? elementTypeLabels[currentElement.type]
                    : currentElement.type;
        } else {
            label = 'Unnamed element';
        }
    }

    return (
        <BuilderCursor show={Boolean(label && state?.isDragging)}>
            <Badge bg="primary">
                {icon} {label}
            </Badge>
        </BuilderCursor>
    );
}
