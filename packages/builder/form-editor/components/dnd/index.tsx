import React, {
    useEffect,
    useCallback,
    useState,
    useRef,
    DragEvent,
} from 'react';
import { useFormContext } from '@form-engine/core-application/hooks/use-form';
import { useFormEditorContext } from '../../hooks/use-form-editor';
import {
    relocateItemAfter,
    relocateItemBefore,
} from '@form-engine/core-util/element-list';
import { ElementDefinition } from '@form-engine/core-application/types/form';

export const PLACEMENT_BEFORE = 'before';
export const PLACEMENT_AFTER = 'after';

export interface BuilderDropTargetProps
    extends React.HTMLAttributes<HTMLElement> {
    as?: React.ElementType;
    id?: string;
    parent?: string;
    placement?: 'before' | 'after';
    onClick?: (e: React.MouseEvent) => void;
}

function useStopDrag() {
    const { setItem } = useFormEditorContext();
    return useCallback(() => {
        setItem('isDragging', false);
        setItem('source', null);
        document.documentElement.classList.toggle('is-dragging', false);
    }, [setItem]);
}

function useStartDrag() {
    const { setItem } = useFormEditorContext();
    return useCallback(
        (source?: ElementDefinition | null) => {
            setItem('isDragging', true);
            setItem('source', source ?? null);
            document.documentElement.classList.toggle('is-dragging', true);
        },
        [setItem]
    );
}

export function BuilderDropTarget({
    as: Component = 'div',
    id,
    parent = '',
    placement = PLACEMENT_BEFORE,
    onClick,
    ...props
}: BuilderDropTargetProps) {
    const { state: editor } = useFormEditorContext();
    const { state: form, setItem } = useFormContext();
    const ref = useRef<HTMLDivElement>(null);
    const stopDrag = useStopDrag();

    const relocate = useCallback(() => {
        if (!form?.elements || !editor?.source || id === editor.source.id)
            return;

        const update = (
            placement === PLACEMENT_AFTER
                ? relocateItemAfter
                : relocateItemBefore
        )(
            form.elements,
            {
                name: `Element${form.elements?.length ?? 0}`,
                ...editor.source,
                parent,
            },
            id
        );

        setItem('elements', update);
    }, [id, parent, placement, editor, form, setItem]);

    const handleDrop = useCallback(() => {
        relocate();
        stopDrag();
    }, [relocate, stopDrag]);
    const handleDragLeave = useCallback(() => {
        ref.current?.classList.toggle('is-dragging-over', false);
    }, []);
    const handleDragOver = useCallback((e: DragEvent) => {
        e.preventDefault();
        ref.current?.classList.toggle('is-dragging-over', true);
    }, []);

    useEffect(() => {
        function hide() {
            ref.current?.classList.toggle('is-dragging-over', false);
        }
        document.addEventListener('dragend', hide);
        document.addEventListener('drop', hide);
        return () => {
            document.removeEventListener('dragend', hide);
            document.removeEventListener('drop', hide);
        };
    }, []);

    return (
        <Component className="feb-drop-target" {...props}>
            <div
                ref={ref}
                className="feb-drop-target-inner"
                onDragOver={handleDragOver}
                onDragLeave={handleDragLeave}
                onDrop={handleDrop}
                onClick={onClick}
            ></div>
        </Component>
    );
}

export interface BuilderDragSourceProps
    extends React.HTMLAttributes<HTMLElement> {
    source?: ElementDefinition | (() => ElementDefinition);
    as?: React.ElementType;
    dragImage?: HTMLCanvasElement | HTMLImageElement;
}

export function BuilderDragSource({
    source,
    as: Component = 'div',
    dragImage,
    className = '',
    ...props
}: BuilderDragSourceProps) {
    const [isDragging, setIsDragging] = useState(false);
    const startDrag = useStartDrag();
    const stopDrag = useStopDrag();
    const ref = useRef<HTMLDivElement>(null);

    const handleDragStart = useCallback(
        (e: DragEvent) => {
            // console.log(e.currentTarget===e.target);
            if (e.target !== ref.current) {
                return;
            }
            const isConstructor = typeof source === 'function';
            setIsDragging(true);
            startDrag(isConstructor ? source() : source);

            e.dataTransfer.dropEffect = isConstructor ? 'copy' : 'move';
            if (dragImage) {
                e.dataTransfer.setDragImage(dragImage, 0, -16);
            }
        },
        [setIsDragging, startDrag, source, dragImage]
    );

    useEffect(() => {
        let mounted = true;
        function handleStop() {
            // only stop dragging, if this is the current drag source
            if (!mounted || !isDragging) return;
            stopDrag();
            setIsDragging(false);
        }
        document.addEventListener('dragend', handleStop);
        document.addEventListener('drop', handleStop);
        return () => {
            mounted = false;
            document.removeEventListener('dragend', handleStop);
            document.removeEventListener('drop', handleStop);
        };
    }, [isDragging, setIsDragging, stopDrag]);

    return (
        <Component
            {...props}
            ref={ref}
            className={`feb-drag-source ${className} ${
                isDragging ? 'is-dragging' : ''
            }`}
            onDragStart={handleDragStart}
            draggable={true}
        />
    );
}
