const cache = new Map<string, HTMLCanvasElement>();

export function getDragImageCanvas(text: string) {
    if (cache.has(text)) return cache.get(text) as HTMLCanvasElement;

    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d');
    if (!ctx) {
        throw new Error('could not create 2d canvas context');
    }

    const computedStyle = getComputedStyle(document.body);

    const fontSize = 12;
    const fontFamily = computedStyle.getPropertyValue('--bs-body-font-family');
    const fontColor = computedStyle.getPropertyValue('--bs-light');
    const bg = computedStyle.getPropertyValue('--bs-primary');
    const paddingX = 15;
    const paddingY = 8;
    const marginLeft = 10;
    const marginTop = 10;
    const radius = 3;

    const scale = window.devicePixelRatio;

    const applyFontStyle = () => {
        ctx.font = `${fontSize}px ${fontFamily}`;
        ctx.textBaseline = 'middle';
    };

    applyFontStyle();

    const metrics = ctx.measureText(text);
    const width = metrics.width + paddingX * 2 + marginLeft;
    const height = fontSize + paddingY * 2 + marginTop;
    canvas.width = width * scale;
    canvas.height = height * scale;
    canvas.style.width = `${width}px`;
    canvas.style.height = `${height}px`;

    ctx.scale(scale, scale);

    ctx.fillStyle = bg;
    ctx.beginPath();
    ctx.roundRect(
        marginLeft,
        marginTop,
        width - marginLeft,
        height - marginTop,
        [radius, radius, radius, radius]
    );
    ctx.fill();

    ctx.fillStyle = fontColor;
    ctx.beginPath();
    applyFontStyle();
    ctx.fillText(
        text,
        marginLeft + paddingX,
        marginTop + (height - marginTop) / 2
    );
    ctx.fill();
    canvas.style.top = '-100vh';
    canvas.style.pointerEvents = 'none';
    canvas.style.position = 'fixed';
    document.body.appendChild(canvas);
    cache.set(text, canvas);
    return canvas;
}
