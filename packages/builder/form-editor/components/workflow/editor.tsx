import React from 'react';
import { useFormEditorContext } from '../../hooks/use-form-editor';
import { Badge, Card, Col, Container, Row } from 'react-bootstrap';
import { useFormContext } from '@form-engine/core-application/hooks/use-form';
import { PlusLg } from 'react-bootstrap-icons';
import { useCallback } from 'react';
import uuidv4 from '@form-engine/core-util/uuid';
import BuilderExplorer, {
    BuilderExplorerItem,
} from '@form-engine/builder-application/components/explorer';
import BuilderCard from '@form-engine/builder-application/components/card';
import useWorkflowLayer from '@form-engine/core-application/hooks/use-wfl';
import useWorkflowLayerMask from '@form-engine/builder-application/hooks/use-wfl-mask';
import { MaskProvider } from '@form-engine/builder-mask/hooks/use-mask';
import Builder from '@form-engine/builder-application';

interface WorkflowLayerTypeItemProps
    extends React.ComponentProps<typeof BuilderCard> {
    label: React.ReactNode;
    description: React.ReactNode;
    icon?: React.ElementType;
}

function WorkflowLayerTypeItem({
    label,
    description,
    icon: Icon = PlusLg,
    ...props
}: WorkflowLayerTypeItemProps) {
    return (
        <Col md={6} lg={4} xl={3}>
            <BuilderCard as="button" {...props}>
                <Card.Body className="row d-flex">
                    <Col>
                        <Card.Title className="text-xs">{label}</Card.Title>
                        <Card.Text className="text-xs text-black-50">
                            {description}
                        </Card.Text>
                    </Col>
                    <Col xs={2} className="align-self-center">
                        <Icon />
                    </Col>
                </Card.Body>
            </BuilderCard>
        </Col>
    );
}

function AddWorkflowLayerMask({
    onCreate,
}: {
    onCreate?: (id: string) => void;
}) {
    const {
        state: { workflowLayers = [] },
        setItem,
    } = useFormContext();

    const addTypeInstance = useCallback(
        (type: string) => {
            return (e: React.MouseEvent) => {
                e.preventDefault();
                e.stopPropagation();
                const id = uuidv4();
                setItem('workflowLayers', [
                    ...workflowLayers,
                    { id, type, name: 'UntitledWorkflowLayer' },
                ]);
                onCreate && onCreate(id);
            };
        },
        [workflowLayers, setItem, onCreate]
    );

    return (
        <div className="feb-view">
            <div className="feb-view-inner p-0">
                <Container fluid>
                    <h6 className="mb-3 mt-4">Add Workflow Layer</h6>
                    <hr />
                    <Row style={{ [`--bs-gutter-y` as any]: '1rem' }}>
                        {Object.values(Builder?.workflowLayers || {}).map(
                            ({ label, description, Icon, WorkflowLayer }) => (
                                <WorkflowLayerTypeItem
                                    key={WorkflowLayer.apiName}
                                    {...{ label, description }}
                                    onClick={addTypeInstance(
                                        WorkflowLayer.apiName
                                    )}
                                    icon={Icon}
                                />
                            )
                        )}
                    </Row>
                </Container>
            </div>
        </div>
    );
}

export function WorkflowLayerMask({ id }: { id?: string }) {
    const workflowLayer = useWorkflowLayer(id);
    const Mask = useWorkflowLayerMask(id) as React.FunctionComponent<{
        id: string;
    }>;
    if (!Mask || !id) return null;

    const { type = '' } = workflowLayer?.state ?? {};

    const { label } = Builder?.workflowLayers?.[type] ?? {};

    return (
        <div className="feb-element-mask">
            <MaskProvider value={workflowLayer}>
                <div className="mt-3">
                    <Badge bg="light" text="dark">
                        {label}
                    </Badge>
                </div>

                <div className="feb-view">
                    <div className="feb-view-inner">
                        <Row>
                            <Col xl={{ span: 8, offset: 2 }}>
                                <Mask id={id} />
                            </Col>
                        </Row>
                    </div>
                </div>
            </MaskProvider>
        </div>
    );
}

export function BuilderWorkflowLayerEditor() {
    const {
        state: { selectedWorkflowLayerId = null },
        setItem,
    } = useFormEditorContext();

    if (!selectedWorkflowLayerId) {
        return (
            <AddWorkflowLayerMask
                onCreate={(id) => setItem('selectedWorkflowLayerId', id)}
            />
        );
    }
    return <WorkflowLayerMask id={selectedWorkflowLayerId} />;
}

export function BuilderFormEditorViewWorkflow() {
    const {
        state: { selectedWorkflowLayerId },
        setItem,
    } = useFormEditorContext();
    const {
        state: { workflowLayers = [] },
        setItem: setFormItem,
    } = useFormContext();

    const unset = useCallback(
        (id: string) => {
            const index = workflowLayers.findIndex((wfl) => wfl.id === id);
            if (index < 0) return;
            const copy = [...workflowLayers];
            copy.splice(index, 1);
            setFormItem('workflowLayers', copy);
        },
        [workflowLayers, setFormItem]
    );

    console.warn(
        'BuilderFormEditorViewWorkflow is deprecated, please use BuilderWorkflowEditorView'
    );

    return (
        <BuilderExplorer
            selectedId={selectedWorkflowLayerId}
            items={
                workflowLayers
                    .map(({ id, name, type }) => {
                        if (!Builder?.workflowLayers?.[type]) return null;
                        const { Icon } = Builder.workflowLayers[type];
                        return { id, type, label: name, icon: <Icon /> };
                    })
                    .filter((item) => Boolean(item)) as BuilderExplorerItem[]
            }
            onAdd={() => setItem('selectedWorkflowLayerId', null)}
            onUnset={unset}
            onSelect={(id) => setItem('selectedWorkflowLayerId', id)}
        >
            <BuilderWorkflowLayerEditor />
        </BuilderExplorer>
    );
}
