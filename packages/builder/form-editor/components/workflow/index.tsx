import { BuilderFormStudioViewDescriptor } from '@form-engine/builder-application/types/form-studio-view';
import { Lightning as Icon } from 'react-bootstrap-icons';
import { BuilderFormEditorViewWorkflow as Component } from './editor';

export default {
    label: 'Workflow (deprecated)',
    description: 'Configure the behaviour of your form',
    Component,
    Icon,
} as BuilderFormStudioViewDescriptor;
