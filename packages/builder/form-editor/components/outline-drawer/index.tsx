import { useFormEditorContext } from '../../hooks/use-form-editor';
import useFormEngineElementState from '@form-engine/core-application/hooks/use-element';
import { useFormContext } from '@form-engine/core-application/hooks/use-form';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { BuilderDragSource, BuilderDropTarget } from '../dnd';
import { X } from 'react-bootstrap-icons';
import BuilderDrawerNav, {
    BuilderDrawerNavItem,
    BuilderDrawerNavItemProps,
} from '@form-engine/builder-application/components/drawer-nav';
import FormEngine from '@form-engine/core-application';
import { getItemParents } from '@form-engine/core-util/element-list';
import Builder from '@form-engine/builder-application';
import { getDragImageCanvas } from '../dnd/drag-image';

function useAutoExpandSelected() {
    const {
        state: { elements = [] },
    } = useFormContext();
    const {
        state: { selectedId, outlineCollapseList = [] },
        setItem,
    } = useFormEditorContext();
    const [prevSelectedId, setPrevSelectedId] = useState(selectedId);

    const ensureExpanded = useCallback(() => {
        if (!selectedId) return;
        const parents = getItemParents(elements, selectedId).map(
            ({ id }) => id
        );

        const collapseListUpdate = [];
        for (const id of outlineCollapseList) {
            if (!parents.includes(id)) {
                collapseListUpdate.push(id);
            }
        }
        setItem('outlineCollapseList', collapseListUpdate);
    }, [selectedId, outlineCollapseList, setItem, elements]);

    useEffect(() => {
        if (!selectedId || prevSelectedId === selectedId) {
            return;
        }
        ensureExpanded();
        setPrevSelectedId(selectedId);
    }, [ensureExpanded, selectedId, prevSelectedId, setPrevSelectedId]);
}

function OutlineNode({
    id,
    ...props
}: { id?: string } & BuilderDrawerNavItemProps) {
    const elementTypeIcons: { [apiName: string]: React.ElementType } = useMemo(
        () => Builder.getItemSubtypeMap('elements', 'Icon'),
        []
    );
    const elementTypeLabels: { [apiName: string]: string } = useMemo(
        () => Builder.getItemSubtypeMap('elements', 'label'),
        []
    );
    const {
        state: { elements = [] },
    } = useFormContext();
    const { state: currentElement, unsetItem } = useFormEngineElementState(id);
    const {
        state: { selectedId, outlineCollapseList: collapseList = [] },
        setItem,
    } = useFormEditorContext();
    useAutoExpandSelected();

    const isSelected = id === selectedId;

    const toggle = useCallback(
        (e: React.MouseEvent) => {
            e.stopPropagation();
            e.preventDefault();
            setItem('selectedId', isSelected ? null : id);
        },
        [isSelected, setItem, id]
    );
    const toggleCollapse = useCallback(
        (force?: boolean) => {
            if (!id) return;
            const copy = [...collapseList];
            const index = copy.indexOf(id);
            if (index < 0) {
                if (force !== false) {
                    copy.push(id);
                    setItem('outlineCollapseList', copy);
                }
            } else if (force !== true) {
                copy.splice(index, 1);
                setItem('outlineCollapseList', copy);
            }
        },
        [collapseList, setItem, id]
    );
    const unset = useCallback(
        (e: React.MouseEvent) => {
            e.stopPropagation();
            e.preventDefault();
            unsetItem();
        },
        [unsetItem]
    );

    const children = useMemo(() => {
        if (!id) return elements.filter((element) => !element.parent);
        return elements.filter((element) => element.parent === id);
    }, [elements, id]);

    let label = '';
    let icon = null;
    if (currentElement) {
        const IconType =
            currentElement.type in elementTypeIcons
                ? elementTypeIcons[currentElement.type]
                : null;

        if (IconType) icon = <IconType />;

        if (currentElement.name) {
            label = currentElement.name;
        } else if (currentElement.type) {
            label =
                currentElement.type in elementTypeLabels
                    ? elementTypeLabels[currentElement.type]
                    : currentElement.type;
        } else {
            label = 'Unnamed element';
        }
    }

    const actions = (
        <button onClick={unset}>
            <X />
        </button>
    );

    const collapse = id ? collapseList.includes(id as string) : false;

    const isContainer =
        currentElement &&
        currentElement.type in FormEngine.getContainerElements();

    const containerContent = (
        <>
            {isContainer && <BuilderDropTarget placement="before" parent={id} />}
            {
                !collapse &&
                children.length > 0 &&
                children.map((child) => (
                    <OutlineNode key={child.id} id={child.id} />
                    ))
            }
        </>
    );

    return (
        <>
            <BuilderDrawerNavItem
                {...props}
                wrap={currentElement ? 'div' : false}
                label={label}
                icon={icon}
                button={BuilderDragSource}
                buttonProps={{
                    as: 'button',
                    source: currentElement,
                    onClick: toggle,
                    dragImage:
                        currentElement &&
                        getDragImageCanvas(currentElement?.name ?? 'Untitled'),
                }}
                active={isSelected}
                actions={actions}
                collapseable={children.length > 0}
                collapse={collapse}
                onToggleCollapse={toggleCollapse}
            >
                {!currentElement && (
                    <BuilderDropTarget
                        placement="before"
                    />
                )}
                {containerContent}
            </BuilderDrawerNavItem>
            {currentElement && (
                <BuilderDropTarget
                    id={id}
                    parent={currentElement?.parent}
                    placement="after"
                />
            )}
        </>
    );
}

export default function BuilderOutlineDrawer() {
    return (
        <BuilderDrawerNav className="feb-outline-nav">
            <OutlineNode />
        </BuilderDrawerNav>
    );
}
