import React from 'react';
import {
    BuilderFormModal,
    BuilderFormModalProps,
} from '@form-engine/builder-application/components/modal';
import useFormApi, {
    useFormEntity,
} from '@form-engine/builder-application/hooks/use-form-api';
import useMounted from '@form-engine/core-application/hooks/use-mounted';
import { FormDefinition } from '@form-engine/core-application/types/form';
import { useCallback } from 'react';
import { CheckCircleFill } from 'react-bootstrap-icons';
import { FormControl } from 'react-bootstrap';
import dialog from '@form-engine/builder-application/components/dialog';
import { useNavigate } from 'react-router-dom';
import { useAppTabs } from '@form-engine/builder-application/hooks/use-app';
import { toast } from 'react-toastify';
import BuilderSpinnerBoundary from '@form-engine/builder-application/components/spinner-boundary';

export function DuplicateFromVersionDialog({
    title = 'Duplicate version as new form',
    onSuccess,
    onError,
    sourceFormId,
    ...props
}: BuilderFormModalProps & {
    sourceFormId: string;
    onSuccess?: (result: FormDefinition) => void;
    onError?: (error: any) => any;
}) {
    const { create } = useFormApi();
    const mounted = useMounted();
    const { state, loading } = useFormEntity(sourceFormId);

    const handleSubmit = useCallback(
        (e: React.FormEvent) => {
            e.preventDefault();
            e.stopPropagation();
            const data = new FormData(e.target as HTMLFormElement);
            const name = (data.get('name') as string)?.trim();
            const copy = { ...state, name };
            delete copy.id;
            delete copy.versioningDiff;
            delete copy.versioningParentId;
            const toastId = toast(<>Saving...</>);
            create(copy).then((duplicate) => {
                if (!mounted.current) return;
                onSuccess && onSuccess(duplicate);
                toast.update(toastId, {
                    render: (
                        <>
                            <CheckCircleFill className="me-2" /> Saved
                        </>
                    ),
                });
            }, onError);
        },
        [state, onSuccess, onError]
    );

    return (
        <BuilderFormModal {...props} onSubmit={handleSubmit} title={title}>
            <BuilderSpinnerBoundary show={loading}>
                <FormControl
                    placeholder="Name"
                    name="name"
                    defaultValue={`${state?.name} Copy`}
                />
            </BuilderSpinnerBoundary>
        </BuilderFormModal>
    );
}

export default function useDuplicateFromVersionDialog() {
    const navigate = useNavigate();
    const { toggle: toggleTab } = useAppTabs();
    return useCallback(
        async (sourceFormId: string) => {
            let duplicate: FormDefinition;
            try {
                duplicate = (await dialog({
                    as: DuplicateFromVersionDialog,
                    sourceFormId,
                })) as FormDefinition;
            } catch (e) {
                console.error(e);
                return;
            }
            if (duplicate && duplicate?.id) {
                const { id, name } = duplicate;
                const path = `/forms/${id}`;
                toggleTab({ id, name, path }, true);
                navigate(path);
            }
        },
        [navigate, toggleTab]
    );
}
