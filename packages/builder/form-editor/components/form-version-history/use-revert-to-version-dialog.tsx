import React from 'react';
import {
    BuilderConfirmModal,
    BuilderConfirmModalProps,
} from '@form-engine/builder-application/components/modal';
import useFormApi, {
    useFormEntity,
} from '@form-engine/builder-application/hooks/use-form-api';
import useMounted from '@form-engine/core-application/hooks/use-mounted';
import { FormDefinition } from '@form-engine/core-application/types/form';
import { useCallback } from 'react';
import { CheckCircleFill } from 'react-bootstrap-icons';
import { FormControl } from 'react-bootstrap';
import dialog from '@form-engine/builder-application/components/dialog';
import { useNavigate } from 'react-router-dom';
import { useAppTabs } from '@form-engine/builder-application/hooks/use-app';
import { toast } from 'react-toastify';
import BuilderSpinnerBoundary from '@form-engine/builder-application/components/spinner-boundary';
import { useFormContext } from '@form-engine/core-application/hooks/use-form';

export function RevertToVersionDialog({
    title = 'Revert to previous version',
    onSuccess,
    onError,
    sourceFormId,
    targetFormId,
    ...props
}: BuilderConfirmModalProps & {
    sourceFormId: string;
    targetFormId: string;
    onSuccess?: (result: FormDefinition) => void;
    onError?: (error: any) => any;
}) {
    const { state, loading } = useFormEntity(sourceFormId);

    const handleConfirm = useCallback(() => {
        const copy = { ...state };
        delete copy.versioningParentId;
        delete copy.versioningDiff;
        copy.id = targetFormId;

        onSuccess?.(copy);
    }, [state, targetFormId, onSuccess, onError]);

    return (
        <BuilderConfirmModal
            {...props}
            loading={loading}
            onConfirm={handleConfirm}
            onHide={() => onError?.(new Error('aborted'))}
            title={title}
        >
            Are you sure you want to revert this form its the previous version?
        </BuilderConfirmModal>
    );
}

export default function useRevertToVersionDialog() {
    const navigate = useNavigate();
    const { toggle: toggleTab } = useAppTabs();
    const {
        state: { id },
        set,
    } = useFormContext();

    return useCallback(
        async (sourceFormId: string) => {
            if (!id) {
                throw new Error('invalid form id');
            }

            let revertedFormState: FormDefinition;
            try {
                revertedFormState = (await dialog({
                    as: RevertToVersionDialog,
                    sourceFormId,
                    targetFormId: id,
                })) as FormDefinition;
            } catch (e) {
                console.error(e);
                return;
            }

            set(revertedFormState);
        },
        [set, id, navigate, toggleTab]
    );
}
