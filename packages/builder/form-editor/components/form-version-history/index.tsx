import React, { useCallback } from 'react';
import { FormDefinition } from '@form-engine/core-application/types/form';
import BuilderSpinnerBoundary from '@form-engine/builder-application/components/spinner-boundary';
import { ListGroup } from 'react-bootstrap';
import formatRelative from 'date-fns/formatRelative';
import { ArrowCounterclockwise, FilePlus } from 'react-bootstrap-icons';
import IconButton from '@form-engine/builder-icon-button';
import BuilderFormVersionDiffs from './version-diffs';
import useFormVersionHistory from './use-form-version-history';
import useRevertToVersionDialog from './use-revert-to-version-dialog';
import useDuplicateFromVersionDialog from './use-duplicate-from-version-dialog';
import { FormStoreReferenceProvider } from '@form-engine/core-application/hooks/use-form';
import { StoreReference } from '@form-engine/core-application/hooks/use-store';
import { i18n } from '@form-engine/builder-application';

export default function BuilderFormVersionHistory({
    formStore,
    onActionComplete,
}: {
    formStore: StoreReference<FormDefinition>;
    onActionComplete?: (id: string) => void;
}) {
    if (!formStore?.state?.id) {
        throw new Error('invalid form');
    }

    const { state = [], loading } = useFormVersionHistory(formStore?.state?.id);

    return (
        <FormStoreReferenceProvider value={formStore}>
            <BuilderSpinnerBoundary show={loading}>
                <ListGroup>
                    {state.map((version) => (
                        <FormVersionItem
                            key={version.id}
                            version={version}
                            onActionComplete={onActionComplete}
                        />
                    ))}
                </ListGroup>
            </BuilderSpinnerBoundary>
        </FormStoreReferenceProvider>
    );
}

export function FormVersionItem({
    version,
    onActionComplete,
}: {
    version: Partial<FormDefinition>;
    onActionComplete?: (id: string) => void;
}) {
    const spawnRevertDialog = useRevertToVersionDialog();
    const handleRevert = useCallback(
        (e: React.MouseEvent) => {
            e.preventDefault();
            e.stopPropagation();
            if (!version?.id) return;
            spawnRevertDialog(version.id).then(() => {
                onActionComplete?.('revert');
            });
        },
        [onActionComplete, spawnRevertDialog, version?.id]
    );

    const spawnDuplicateDialog = useDuplicateFromVersionDialog();
    const handleDuplicate = useCallback(
        (e: React.MouseEvent) => {
            e.preventDefault();
            e.stopPropagation();
            if (!version?.id) return;
            spawnDuplicateDialog(version.id).then(() => {
                onActionComplete?.('duplicate');
            });
        },
        [onActionComplete, spawnDuplicateDialog, version?.id]
    );

    return (
        <ListGroup.Item className="d-flex justify-content-between align-items-start">
            <div className="ms-2 flex-grow-1">
                <h6 className="mb-2 mt-1 fw-bold">
                    {formatRelative(
                        new Date(version.createdAt ?? ''),
                        new Date()
                    )}
                </h6>
                <BuilderFormVersionDiffs diffs={version.versioningDiff} />
            </div>
            <IconButton
                title={i18n.c('form-editor.formVersionHistory.revert')}
                color="primary"
                onClick={handleRevert}
            >
                <ArrowCounterclockwise />
            </IconButton>
            <IconButton
                title={i18n.c('form-editor.formVersionHistory.duplicate')}
                color="primary"
                onClick={handleDuplicate}
            >
                <FilePlus />
            </IconButton>
        </ListGroup.Item>
    );
}
