import React from 'react';
import { FormDefinition } from '@form-engine/core-application/types/form';
import { Diff, DiffArray, DiffDeleted, DiffEdit, DiffNew } from 'deep-diff';
import {
    ArrowRight,
    DashCircleFill,
    PlusCircleFill,
    Shuffle,
} from 'react-bootstrap-icons';
import startCase from 'lodash.startcase';

export default function BuilderFormVersionDiffs({
    diffs = [],
}: {
    diffs?: Diff<FormDefinition>[];
}) {
    return (
        <ol>
            {diffs.map((diff, index) => (
                <li key={index}>
                    <BuilderFormVersionDiff diff={diff} />
                </li>
            ))}
        </ol>
    );
}

function pathSegmentToHuman(segment: string | number) {
    if (typeof segment === 'number') return `#${segment}`;
    return startCase(segment);
}

function pathToHuman(path: (string | number)[], seperator = ' / '): string {
    return path.map(pathSegmentToHuman).join(seperator);
}

function DiffText({
    children,
    color = 'body',
}: {
    color?: string;
    children: React.ReactNode;
}) {
    return (
        <span className={`feb-diff-text text-${color}`}>
            <span>{children}</span>
        </span>
    );
}

export function BuilderFormVersionDiff({
    diff,
}: {
    diff: Diff<FormDefinition>;
}) {
    switch (diff.kind) {
        case 'A':
            return (
                <BuilderFormVersionDiffArray
                    diff={diff as DiffArray<FormDefinition>}
                />
            );
        case 'D':
            return (
                <BuilderFormVersionDiffDeleted
                    diff={diff as DiffDeleted<FormDefinition>}
                />
            );
        case 'N':
            return (
                <BuilderFormVersionDiffNew
                    diff={diff as DiffNew<FormDefinition>}
                />
            );
        case 'E':
            return (
                <BuilderFormVersionDiffEdit
                    diff={diff as DiffEdit<FormDefinition>}
                />
            );
        default:
            return null;
    }
}

function parseDiffLabel(target: any): string {
    if (target && typeof target === 'object') {
        if (typeof target.name === 'string') return target.name;
        if (typeof target.label === 'string') return target.label;
        if (typeof target.title === 'string') return target.title;
        return 'Object';
    }
    return String(target);
}

export function BuilderFormVersionDiffArray({
    diff,
}: {
    diff: DiffArray<FormDefinition>;
}) {
    return (
        <>
            {diff.path && (
                <h6 className="mb-2">
                    {pathToHuman([...diff.path, diff.index])}
                </h6>
            )}
            <BuilderFormVersionDiff diff={diff.item} />
        </>
    );
}

export function BuilderFormVersionDiffDeleted({
    diff,
}: {
    diff: DiffDeleted<FormDefinition>;
}) {
    return (
        <>
            {diff.path && <h6 className="mb-2">{pathToHuman(diff.path)}</h6>}
            <div className="d-flex flex-row mb-2">
                <DashCircleFill className="text-danger me-2" />{' '}
                <DiffText>{parseDiffLabel(diff.lhs)}</DiffText>
            </div>
        </>
    );
}

export function BuilderFormVersionDiffNew({
    diff,
}: {
    diff: DiffNew<FormDefinition>;
}) {
    return (
        <>
            {diff.path && <h6 className="mb-2">{pathToHuman(diff.path)}</h6>}
            <div className="d-flex flex-row mb-2">
                <PlusCircleFill className="text-success me-2" />{' '}
                <DiffText>{parseDiffLabel(diff.rhs)}</DiffText>
            </div>
        </>
    );
}

export function BuilderFormVersionDiffEdit({
    diff,
}: {
    diff: DiffEdit<FormDefinition>;
}) {
    return (
        <>
            {diff.path && <h6 className="mb-2">{pathToHuman(diff.path)}</h6>}
            <div className="d-flex flex-row mb-2">
                <Shuffle className="text-info me-2" />{' '}
                <DiffText>{parseDiffLabel(diff.lhs)}</DiffText>{' '}
                <ArrowRight className="ms-2 me-2" />{' '}
                <DiffText>{parseDiffLabel(diff.rhs)}</DiffText>
            </div>
        </>
    );
}
