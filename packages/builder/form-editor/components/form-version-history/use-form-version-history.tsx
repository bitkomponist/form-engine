import { useMemo } from 'react';
import useFormApi from '@form-engine/builder-application/hooks/use-form-api';
import {
    useList,
    ApiListOptions,
} from '@form-engine/builder-application/hooks/use-api-model';
import { createObjectSort } from '@form-engine/core-util/sorting';
const FORM_HISTORY_ATTRIBUTES = [
    'id',
    'version',
    'createdAt',
    'updatedAt',
    'versioningParentId',
    'versioningDiff',
];

export default function useFormVersionHistory(formId: string) {
    const listOptions = useMemo(() => {
        return {
            attributes: FORM_HISTORY_ATTRIBUTES,
            filter: {
                versioningParentId: formId,
            },
            includeVersioningHistory: true,
        } as ApiListOptions;
    }, [formId]);

    const list = useList(useFormApi(), listOptions);

    const state = useMemo(() => {
        return list?.state?.sort(createObjectSort('Date', 'createdAt', 'desc'));
    }, [list.state]);

    return { ...list, state };
}
