import useBounds from '@form-engine/builder-application/hooks/use-bounds';
import React, {
    DragEventHandler,
    useCallback,
    useEffect,
    useMemo,
    useState,
} from 'react';

import ReactFlow, {
    addEdge,
    MiniMap,
    Controls,
    Background,
    useNodesState,
    useEdgesState,
    Node,
    OnSelectionChangeFunc,
    OnConnect,
    Edge,
    OnEdgesChange,
    Connection,
    OnNodesChange,
} from 'reactflow';
import 'reactflow/dist/style.css';
import { useWorkflowLayers } from '@form-engine/core-application/hooks/use-wfl';
import { useFormEditorContext } from '../../hooks/use-form-editor';
import { BuilderWorkflowEditorLayerNode } from './layer-node';
import Builder from '@form-engine/builder-application';
import {
    getItemById,
    removeItem,
    addItem,
} from '@form-engine/core-util/element-list';
import { LightningChargeFill, LockFill } from 'react-bootstrap-icons';
import { WorkflowLayerDefinition } from '@form-engine/core-application/types/form';
import uuidv4 from '@form-engine/core-util/uuid';
import { i18n } from '@form-engine/builder-application';

const minimapStyle = {
    height: 120,
};

const SIGNAL_TYPE_NAME = 'FormEngineSignalWfl';

const STATIC_NODES: Node[] = [
    {
        id: 'form:ready',
        type: SIGNAL_TYPE_NAME,
        draggable: false,
        deletable: false,
        selectable: false,
        data: {
            label: () => (
                <>
                    <LightningChargeFill />{' '}
                    <i18n.Node id="form-editor.workflow.formReadyNode" />{' '}
                    <LockFill />
                </>
            ),
        },
        position: { x: 0, y: 0 },
    } as Node,
    {
        id: 'form:submit',
        type: SIGNAL_TYPE_NAME,
        draggable: false,
        deletable: false,
        selectable: false,
        data: {
            label: () => (
                <>
                    <LightningChargeFill />{' '}
                    <i18n.Node id="form-editor.workflow.formSubmitNode" />{' '}
                    <LockFill />
                </>
            ),
        },
        position: { x: 250, y: 0 },
    } as Node,
];

function castLayerToNode(layer: WorkflowLayerDefinition) {
    if (!layer) throw new Error('invalid argument layer');

    const Icon = Builder.workflowLayers[layer.type]?.Icon ?? React.Fragment;

    return {
        id: layer.id,
        type: layer.type,
        draggable: true,
        deletable: true,
        data: {
            layer,
            label: (
                <>
                    <Icon /> {layer.name}
                </>
            ),
            body: layer.description,
        },
        position: {
            x: isNaN(layer.nodeX) ? 0 : layer.nodeX,
            y: isNaN(layer.nodeY) ? 0 : layer.nodeY,
        },
    } as Node;
}

function syncWorkflowLayersToNodes(
    workflowLayers: WorkflowLayerDefinition[],
    setter: React.Dispatch<React.SetStateAction<Node<any>[]>>
) {
    function updateNodeFromLayer(
        node: Node<any>,
        layer: WorkflowLayerDefinition
    ) {
        if (
            node &&
            (node.data?.layer?.name !== layer.name ||
                node.data?.layer?.description !== layer.description)
        ) {
            const Icon = Builder.workflowLayers[layer.type]?.Icon;
            node.data = {
                ...(node.data ?? {}),
                layer,
                label: (
                    <>
                        <Icon /> {layer.name}
                    </>
                ),
                body: layer.description,
            };
            return true;
        }

        return false;
    }

    setter((nodes) => {
        // ignore static nodes
        const staticNodes: Node<any>[] = [];
        const layerNodes: Node<any>[] = [];
        for (const node of nodes) {
            if (node.data?.layer) {
                layerNodes.push(node);
            } else {
                staticNodes.push(node);
            }
        }

        let isDirty = workflowLayers.length !== layerNodes.length;

        const nodesUpdate: Node<any>[] = [];
        for (const layer of workflowLayers) {
            const node = getItemById(layerNodes, layer.id);
            if (!node) {
                isDirty = true;
                nodesUpdate.push(castLayerToNode(layer));
            } else {
                if (updateNodeFromLayer(node, layer)) {
                    isDirty = true;
                }
                nodesUpdate.push(node);
            }
        }

        if (isDirty) return [...staticNodes, ...nodesUpdate];

        return nodes;
    });
}

function syncWorkflowLayersToEdges(
    layers: WorkflowLayerDefinition[],
    setter: React.Dispatch<React.SetStateAction<Edge<any>[]>>
) {
    setter((edges) => {
        const edgesUpdate: Edge[] = [];
        let isDirty = false;
        for (const layer of layers) {
            if (!Array.isArray(layer.events)) continue;
            for (const eventName of layer.events) {
                const id = getEdgeId({ target: layer.id, source: eventName });
                let edge = getItemById(edges, id);
                if (!edge) {
                    isDirty = true;
                    edge = {
                        id: getEdgeId({ target: layer.id, source: eventName }),
                        source: eventName.split('@')[0],
                        sourceHandle: eventName,
                        target: layer.id,
                        animated: true,
                    } as Edge;
                }
                edgesUpdate.push(edge);
            }
        }

        if (edgesUpdate.length !== edges.length) isDirty = true;

        if (isDirty) return edgesUpdate;

        return edges;
    });
}

function useWorkflowNodesState() {
    const { state, set: setLayers } = useWorkflowLayers();

    const initialNodes = useMemo(() => {
        // the filter part is needed to sanitize legacy forms (prior to the flow editor)
        return [...STATIC_NODES];
    }, []);

    const [nodes, setNodes, onNodesChangeInternal] =
        useNodesState(initialNodes);

    useEffect(() => {
        syncWorkflowLayersToNodes(state, setNodes);
    }, [state, setNodes]);

    const onNodesChange: typeof onNodesChangeInternal =
        useCallback<OnNodesChange>(
            (changes) => {
                let layersUpdate;
                for (const change of changes) {
                    if (change.type === 'position' && !change.dragging) {
                        const layer = getItemById(state, change.id);
                        const node = getItemById(nodes, change.id);

                        if (layer && node) {
                            if (!layersUpdate) layersUpdate = [...state];
                            layersUpdate[layersUpdate.indexOf(layer)] = {
                                ...layer,
                                nodeX: node.position.x,
                                nodeY: node.position.y,
                            };
                        }
                    }
                    if (change.type === 'remove') {
                        const layer = getItemById(state, change.id);

                        if (layer) {
                            if (!layersUpdate) layersUpdate = [...state];
                            layersUpdate = removeItem(layersUpdate, layer);
                        }
                    }
                }

                layersUpdate && setLayers(layersUpdate);

                return onNodesChangeInternal(changes);
            },
            [onNodesChangeInternal, setLayers, state, nodes]
        );

    const addNode = useCallback(
        (params: Partial<WorkflowLayerDefinition>) => {
            if (!params.type || !Builder.workflowLayers[params.type]) {
                throw new Error(`unknown layer type ${params.type}`);
            }

            const descriptor = Builder.workflowLayers[params.type];

            const layer: WorkflowLayerDefinition = {
                id: uuidv4(),
                nodeX: 0,
                nodeY: 0,
                name: i18n.c(descriptor.label),
                type: '',
                enabled: true,
                ...params,
            };

            if (!Builder.workflowLayers[layer.type]) {
                throw new Error(`unknown layer type ${layer.type}`);
            }

            const node = castLayerToNode(layer);

            setLayers(addItem(state, layer));
            setNodes(addItem(nodes, node));
        },
        [setLayers, state, nodes, setNodes]
    );

    return { nodes, setNodes, addNode, onNodesChange };
}

function getEdgeId(props: { target: string; source: string }) {
    return JSON.stringify(props);
}

function parseEdgeId(id: string) {
    try {
        const { target, source } = JSON.parse(id) as {
            target: string;
            source: string;
        };
        return { target, source };
    } catch (e) {
        throw new Error(`malformed edge id ${id}`);
    }
}

function useWorkflowEdgesState() {
    const { state, setItem: setLayer, set: setLayers } = useWorkflowLayers();

    const [edges, setEdges, onEdgesChangeInternal] = useEdgesState([]);

    useEffect(() => {
        syncWorkflowLayersToEdges(state, setEdges);
    }, [state, setEdges]);

    const onEdgesChange = useCallback<OnEdgesChange>(
        (changes) => {
            const removed = changes.filter(
                (change) => change.type === 'remove'
            );

            if (!removed.length) return onEdgesChangeInternal(changes);

            const layersUpdate = [...state];

            let layersDirty = false;

            for (const change of removed) {
                const id: string | undefined = (change as any)?.id;

                if (!id) throw new Error('invalid id');

                const { target: layerId, source: eventName } = parseEdgeId(id);

                const layer = getItemById(layersUpdate, layerId);

                if (!layer) throw new Error('edge target layer does not exist');

                const eventIndex = layer?.events?.indexOf(eventName) ?? -1;

                if (eventIndex > -1) {
                    layersDirty = true;
                    layersUpdate[layersUpdate.indexOf(layer)] = {
                        ...layer,
                        events: layer.events?.filter(
                            (currentEventName) => currentEventName !== eventName
                        ),
                    };
                }
            }
            layersDirty && setLayers(layersUpdate);
            return onEdgesChangeInternal(changes);
        },
        [onEdgesChangeInternal, setLayers, state]
    );

    const addEdgeAndFormState = useCallback(
        (edge: Connection) => {
            const { sourceHandle: eventName, target } = edge;
            if (target && eventName) {
                const layer = getItemById(state, target);
                if (!layer) {
                    throw new Error(`layer ${target} does not exist`);
                }
                const index = state.indexOf(layer);
                if (!layer?.events?.includes(eventName)) {
                    setLayer(index, {
                        ...layer,
                        events: [...(layer?.events ?? []), eventName],
                    });
                }
                setEdges((eds) => {
                    return addEdge(
                        {
                            ...edge,
                            id: getEdgeId({ target, source: eventName }),
                            animated: true,
                        },
                        eds
                    );
                });
            }
        },
        [setEdges, state, setLayer]
    );

    return { edges, addEdge: addEdgeAndFormState, setEdges, onEdgesChange };
}

export function BuilderWorkflowEditorFlow() {
    const [reactFlowInstance, setReactFlowInstance] = useState<any>(null);

    const { bounds, ref } = useBounds<HTMLDivElement>();

    const { nodes, addNode, onNodesChange } = useWorkflowNodesState();

    const { edges, addEdge, onEdgesChange } = useWorkflowEdgesState();

    const nodeTypes = useMemo(
        () =>
            Object.fromEntries(
                Object.entries(Builder.workflowLayers).map(([type, def]) => [
                    type,
                    def.Node ?? BuilderWorkflowEditorLayerNode,
                ])
            ),
        []
    );

    const onConnect = useCallback<OnConnect>(
        (connection) => {
            addEdge(connection);
        },
        [addEdge]
    );
    const { setItem: setFormEditorItem } = useFormEditorContext();

    const handleSelectionChange = useCallback<OnSelectionChangeFunc>(
        (data) => {
            const [node] = data.nodes;
            if (node?.id) {
                setFormEditorItem('selectedWorkflowLayerId', node.id);
            } else {
                setFormEditorItem('selectedWorkflowLayerId', undefined);
            }
        },
        [setFormEditorItem]
    );

    const onDragOver = useCallback<DragEventHandler<HTMLDivElement>>(
        (event) => {
            event.preventDefault();
            event.dataTransfer.dropEffect = 'move';
        },
        []
    );

    const onDrop = useCallback<DragEventHandler<HTMLDivElement>>(
        (event) => {
            event.preventDefault();

            const type = event.dataTransfer.getData('application/reactflow');

            // check if the dropped element is valid
            if (typeof type === 'undefined' || !type) {
                return;
            }

            const position = reactFlowInstance.project({
                x: event.clientX - bounds.left,
                y: event.clientY - bounds.top,
            });

            addNode({ type, nodeX: position.x, nodeY: position.y });
        },
        [reactFlowInstance, bounds, addNode]
    );

    const flow = (
        <ReactFlow
            nodes={nodes}
            edges={edges}
            onNodesChange={onNodesChange}
            onSelectionChange={handleSelectionChange}
            onEdgesChange={onEdgesChange}
            onConnect={onConnect}
            onDragOver={onDragOver}
            onDrop={onDrop}
            onInit={setReactFlowInstance}
            fitView
            nodeTypes={nodeTypes as any}
        >
            <MiniMap style={minimapStyle} zoomable pannable />
            <Controls />
            <Background color="#aaa" gap={16} />
        </ReactFlow>
    );

    return (
        <div
            style={{
                display: 'flex',
                height: '100%',
                width: '100%',
                position: 'relative',
            }}
            ref={ref as React.LegacyRef<HTMLDivElement>}
        >
            {bounds.width && bounds.height && (
                <div
                    style={{
                        width: bounds.width,
                        height: bounds.height,
                        position: 'absolute',
                        top: '0',
                        left: '0',
                        overflow: 'hidden',
                    }}
                >
                    {flow}
                </div>
            )}
        </div>
    );
}
