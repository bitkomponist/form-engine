import React from 'react';
import { Badge } from 'react-bootstrap';
import useWorkflowLayer from '@form-engine/core-application/hooks/use-wfl';
import useWorkflowLayerMask from '@form-engine/builder-application/hooks/use-wfl-mask';
import { MaskProvider } from '@form-engine/builder-mask/hooks/use-mask';
import Builder from '@form-engine/builder-application';
import { i18n } from '@form-engine/builder-application';

export function BuilderWorkflowEditorSettingsDrawer({ id }: { id?: string }) {
    const workflowLayer = useWorkflowLayer(id);
    const Mask = useWorkflowLayerMask(id) as React.FunctionComponent<{
        id: string;
    }>;

    if (!Mask || !id)
        return (
            <p className="text-muted text-center mt-3">
                <i18n.Node id="form-editor.workflow.nothingSelected" />
            </p>
        );

    const { type = '' } = workflowLayer?.state ?? {};

    const { label } = Builder?.workflowLayers?.[type] ?? {};

    return (
        <div className="feb-view m-0">
            <div className="feb-view-inner p-0">
                <div className="feb-properties-drawer feb-element-mask">
                    <MaskProvider value={workflowLayer}>
                        <Badge bg="light" text="dark">
                            <i18n.Node id={label} />
                        </Badge>
                        <Mask id={id} />
                    </MaskProvider>
                </div>
            </div>
        </div>
    );
}
