import React from 'react';
import 'reactflow/dist/style.css';
import { useFormEditorContext } from '../../hooks/use-form-editor';
import { Col, Row } from 'react-bootstrap';
import BuilderAppCol from '@form-engine/builder-application/components/app-col';
import BuilderDrawerTabs from '@form-engine/builder-application/components/drawer-tabs';
import Drawer from '@form-engine/builder-application/components/drawer';
import BuilderWorkflowEditorNodesDrawer from './nodes-drawer';
import { BuilderWorkflowEditorFlow } from './flow';
import { BuilderWorkflowEditorSettingsDrawer } from './settings';
import { i18n } from '@form-engine/builder-application';

export function BuilderWorkflowEditorView() {
    const {
        state: {
            workflowLeftDrawerTab = 'library',
            workflowCanvasTab = 'flow',
            workflowRightDrawerTab = 'settings',
            selectedWorkflowLayerId,
        },
        setItem,
    } = useFormEditorContext();

    const { c } = i18n.prefix('form-editor.workflow.');

    return (
        <Row>
            <BuilderAppCol md={2} border="right" id="workflowEditorLeftCol">
                <BuilderDrawerTabs
                    activeKey={workflowLeftDrawerTab}
                    items={{ library: c('library') }}
                    onChange={(key) => setItem('workflowLeftDrawerTab', key)}
                />
                <div className="feb-view m-0">
                    <div className="feb-view-inner p-0">
                        <Drawer>
                            {workflowLeftDrawerTab === 'library' && (
                                <BuilderWorkflowEditorNodesDrawer />
                            )}
                        </Drawer>
                    </div>
                </div>
            </BuilderAppCol>
            <Col className="feb-main-col">
                <BuilderDrawerTabs
                    activeKey={workflowCanvasTab}
                    items={{ flow: c('flow') }}
                ></BuilderDrawerTabs>
                <div className="feb-view">
                    {workflowCanvasTab === 'flow' && (
                        <BuilderWorkflowEditorFlow />
                    )}
                </div>
            </Col>
            <BuilderAppCol
                md={3}
                maxWidth="50vw"
                border="left"
                id="workflowEditorRightCol"
            >
                <BuilderDrawerTabs
                    activeKey={workflowRightDrawerTab}
                    items={{ settings: c('settings') }}
                    onChange={(key) => setItem('rightDrawerTab', key)}
                />
                {workflowRightDrawerTab === 'settings' && (
                    <BuilderWorkflowEditorSettingsDrawer
                        id={selectedWorkflowLayerId}
                    />
                )}
            </BuilderAppCol>
        </Row>
    );
}
