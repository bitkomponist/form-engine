import { BuilderFormStudioViewDescriptor } from '@form-engine/builder-application/types/form-studio-view';
import { Diagram2Fill as Icon } from 'react-bootstrap-icons';
import { BuilderWorkflowEditorView as Component } from './editor';

export default {
    label: 'form-editor.workflow.title',
    description: 'form-editor.workflow.description',
    Component,
    Icon,
} as BuilderFormStudioViewDescriptor;
