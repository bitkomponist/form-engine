import React, { memo } from 'react';
import { Handle, NodeProps, Position } from 'reactflow';

interface handleProps {
    icon?: React.ElementType;
    title?: string;
    wrapProps?: React.HTMLAttributes<HTMLDivElement>;
    handle?: Partial<React.ComponentProps<typeof Handle>>;
}
export interface BuilderWorkflowEditorLayerNodeProps extends NodeProps {
    targets?: { [key: string]: handleProps } | null | false;
    sources?: { [key: string]: handleProps } | null | false;
}

const DEFAULT_TARGETS: BuilderWorkflowEditorLayerNodeProps['targets'] = {
    target: {
        handle: {
            position: Position.Top,
        },
    },
};

const DEFAULT_SOURCES: BuilderWorkflowEditorLayerNodeProps['sources'] = {
    success: {
        handle: {
            position: Position.Bottom,
        },
    },
};

function LayerNode({
    id,
    data,
    selected,
    targets = DEFAULT_TARGETS,
    sources = DEFAULT_SOURCES,
}: BuilderWorkflowEditorLayerNodeProps) {
    const targetEntries = Object.entries(targets ?? {});
    const sourceEntries = Object.entries(sources ?? {});

    const label =
        typeof data.label === 'function' ? <data.label /> : data.label;
    const body = typeof data.body === 'function' ? <data.body /> : data.body;

    return (
        <>
            <div
                className={`react-flow__node-default react-flow__node-default nopan ${
                    selected ? 'selected' : ''
                } selectable`}
            >
                <div className="layer-node__header">{label}</div>
                <div className="layer-node__body">
                    {body && <div className="mb-2">{body}</div>}

                    <div
                        className="d-flex flex-row"
                        style={{ margin: '0 -11px 0 -11px' }}
                    >
                        {targetEntries.map(([key, def], index) => {
                            const { icon: Icon, title } = def;
                            const colWidth = 100 / targetEntries.length;

                            return (
                                <div
                                    key={key}
                                    className="text-center"
                                    style={{ width: `${colWidth}%` }}
                                    {...(def.wrapProps ?? {})}
                                >
                                    {Icon && <Icon title={title} />}
                                    <Handle
                                        position={Position.Top}
                                        {...(def.handle ?? {})}
                                        type="target"
                                        id={`${id}@${key}`}
                                        style={{
                                            left: `${
                                                colWidth * index +
                                                colWidth * 0.5
                                            }%`,
                                        }}
                                    />
                                </div>
                            );
                        })}
                    </div>

                    <div
                        className="d-flex flex-row"
                        style={{ margin: '0 -11px 0 -11px' }}
                    >
                        {sourceEntries.map(([key, def], index) => {
                            const { icon: Icon, title } = def;
                            const colWidth = 100 / sourceEntries.length;
                            return (
                                <div
                                    key={key}
                                    className="text-center"
                                    style={{ width: `${colWidth}%` }}
                                    {...(def.wrapProps ?? {})}
                                >
                                    {Icon && <Icon title={title} />}
                                    <Handle
                                        position={Position.Bottom}
                                        {...(def.handle ?? {})}
                                        type="source"
                                        id={`${id}@${key}`}
                                        style={{
                                            left: `${
                                                colWidth * index +
                                                colWidth * 0.5
                                            }%`,
                                        }}
                                    />
                                </div>
                            );
                        })}
                    </div>
                </div>
            </div>
        </>
    );
}

export const BuilderWorkflowEditorLayerNode = memo(LayerNode);
