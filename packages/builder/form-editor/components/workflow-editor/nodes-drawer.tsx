import React, { useCallback, useMemo } from 'react';
import Builder from '@form-engine/builder-application';
import BuilderDrawerNav, {
    BuilderDrawerNavItem,
} from '@form-engine/builder-application/components/drawer-nav';
import { i18n } from '@form-engine/builder-application';
import { getDragImageCanvas } from '../dnd/drag-image';

export default function BuilderWorkflowEditorNodesDrawer() {
    const workflowLayerTypes = useMemo(
        () => Object.entries(Builder.workflowLayers),
        []
    );

    const handleDragStart = useCallback<(e: DragEvent) => void>((e) => {
        const type = (e.currentTarget as HTMLElement)?.getAttribute(
            'data-type'
        );
        const label = (e.currentTarget as HTMLElement)?.getAttribute(
            'data-label'
        );
        if (type && e.dataTransfer) {
            e.dataTransfer.setData('application/reactflow', type);
            e.dataTransfer.effectAllowed = 'move';
            label &&
                e.dataTransfer.setDragImage(getDragImageCanvas(label), 0, 0);
        }
    }, []);

    return (
        <BuilderDrawerNav>
            {workflowLayerTypes.map(([type, { Icon, label }], index) => {
                const i18nLabel = i18n.c(label);
                return (
                    <BuilderDrawerNavItem
                        key={index}
                        icon={<Icon />}
                        label={i18nLabel}
                        buttonProps={{
                            draggable: true,
                            onDragStart: handleDragStart,
                            'data-type': type,
                            'data-label': i18nLabel,
                        }}
                    />
                );
            })}
        </BuilderDrawerNav>
    );
}
