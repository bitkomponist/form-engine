import React, { useMemo } from 'react';
import Builder from '@form-engine/builder-application';
import BuilderDrawerNav, {
    BuilderDrawerNavItem,
} from '@form-engine/builder-application/components/drawer-nav';

export default function BuilderElementsDrawer() {
    const drawerButtons = useMemo(
        () =>
            Object.values(Builder.elements)
                .filter((ns) => ns.Element.enabled)
                .map((ns) => ns.DrawerButton)
                .filter((Comp) => Boolean(Comp)),
        []
    );

    return (
        <BuilderDrawerNav>
            {drawerButtons.map((Comp, index) => (
                <BuilderDrawerNavItem key={index} button={Comp} />
            ))}
        </BuilderDrawerNav>
    );
}
