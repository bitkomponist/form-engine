import React, { useCallback } from 'react';
import uuidv4 from '@form-engine/core-util/uuid';
import { BuilderDragSource, BuilderDragSourceProps } from '../dnd';
import { BuilderElementConstructor } from '@form-engine/builder-application/types/element';
import { ElementDefinition } from '@form-engine/core-application/types/form';
import { getDragImageCanvas } from '../dnd/drag-image';

export interface BuilderElementsDrawerButtonProps
    extends BuilderDragSourceProps {
    constructor?: BuilderElementConstructor;
    as?: React.ElementType;
    dragLabel?: string;
}

export default function BuilderElementsDrawerButton({
    constructor,
    as: Component = BuilderDragSource,
    dragLabel,
    ...props
}: BuilderElementsDrawerButtonProps) {
    const createSource = useCallback(() => {
        const sourceData: Partial<ElementDefinition> = {};

        if (typeof constructor === 'function') {
            Object.assign(sourceData, constructor());
        } else if (typeof constructor === 'object') {
            Object.assign(sourceData, constructor);
        }

        if (!sourceData.id) {
            sourceData.id = uuidv4();
        }

        return sourceData;
    }, [constructor]);

    return (
        <Component
            as="button"
            source={createSource}
            dragImage={dragLabel && getDragImageCanvas(dragLabel)}
            {...props}
        />
    );
}
