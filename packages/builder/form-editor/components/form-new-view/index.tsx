import useFormApi from '@form-engine/builder-application/hooks/use-form-api';
import useMounted from '@form-engine/core-application/hooks/use-mounted';
import React, { useCallback } from 'react';
import {
    Button,
    Card,
    FloatingLabel,
    Form,
    FormControl,
    FormGroup,
} from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Builder from '@form-engine/builder-application';

function FormStruct(overrides = {}) {
    const form = {
        name: 'Untitled Form',
        tags: [],
        elements: [],
        constants: [],
        datasources: [],
        workflowLayers: [],
        ...overrides,
    };

    Object.values(Builder?.formPresets ?? {}).forEach((preset) => {
        if (preset.required && preset.init) {
            preset.init(form);
        }
    });

    return form;
}

export default function BuilderFormNewView() {
    const navigate = useNavigate();
    const { create } = useFormApi();
    const mounted = useMounted();

    const handleSubmit = useCallback(
        (e: React.FormEvent) => {
            e.preventDefault();
            e.stopPropagation();
            const name = String(
                ((e.target as HTMLFormElement)[0] as HTMLInputElement)?.value
            ).trim();

            create(FormStruct({ name })).then(({ id }) => {
                if (!mounted.current) return;
                navigate(`/forms/${id}`);
            });
        },
        [navigate, create]
    );

    return (
        <div className="flex-grow-1 d-flex justify-content-center align-items-center">
            <div>
                <Card>
                    <Card.Body>
                        <Card.Title>Create new Form</Card.Title>
                        <Form className="mt-5" onSubmit={handleSubmit}>
                            <FormGroup>
                                <FloatingLabel label="Name">
                                    <FormControl
                                        name="formName"
                                        placeholder="Name"
                                        required
                                    />
                                </FloatingLabel>
                            </FormGroup>
                            <Button className="mt-3" type="submit">
                                Create
                            </Button>
                        </Form>
                    </Card.Body>
                </Card>
            </div>
        </div>
    );
}
