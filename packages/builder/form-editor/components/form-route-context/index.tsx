import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import {
    FormOfflineStorage,
    FormProvider,
    useFormContext,
} from '@form-engine/core-application/hooks/use-form';
import {
    FormEditorProvider,
    formEditorStorage,
} from '../../hooks/use-form-editor';
import BuilderSpinnerBoundary from '@form-engine/builder-application/components/spinner-boundary';
import { useFormEntity } from '@form-engine/builder-application/hooks/use-form-api';
import { useOfflineEntity } from '@form-engine/builder-application/hooks/use-offline-entity';
import { useStoreEffect } from '@form-engine/core-application/hooks/use-store';
import { Container } from 'react-bootstrap';
import { CloudLightningRainFill } from 'react-bootstrap-icons';
import FormEngineAlert from '@form-engine/core-application/components/alert';
import useFormHistory, {
    useWriteFormHistory,
} from '../../hooks/use-form-history';

function OfflineFormUpdater() {
    useStoreEffect(useFormContext(), ({ state }) => {
        if (state && state.id) {
            FormOfflineStorage.setItem(state.id, state);
        }
    });

    return null;
}

function FormHistoryUpdater({ clearOnMount }: { clearOnMount?: boolean }) {
    useWriteFormHistory();
    const { clear } = useFormHistory();

    useEffect(() => {
        if (clearOnMount) {
            clear();
        }
    }, [clearOnMount]);

    return null;
}

export default function BuilderFormRouteContextProvider({
    children,
    spinnerBoundaryProps = {},
}: {
    children?: React.ReactNode;
    spinnerBoundaryProps?: React.ComponentProps<typeof BuilderSpinnerBoundary>;
}) {
    const { formId } = useParams();
    const { state: onlineEntity, loading } = useFormEntity(formId);
    const { state, dirty } = useOfflineEntity(FormOfflineStorage, onlineEntity);

    /** @todo clear history if server entity newer than local */

    let content: React.ReactNode | undefined;

    if (formId && state) {
        content = (
            <FormEditorProvider value={formEditorStorage.getItem(formId, {})}>
                <FormProvider value={state}>
                    <>
                        <OfflineFormUpdater />
                        <FormHistoryUpdater clearOnMount={dirty} />
                        {children}
                    </>
                </FormProvider>
            </FormEditorProvider>
        );
    } else {
        content = (
            <Container className="my-5">
                <FormEngineAlert
                    variant="danger-outline"
                    title="Form not found"
                    aside={<CloudLightningRainFill />}
                >
                    This may happen if the Form was deleted by somebody else
                </FormEngineAlert>
            </Container>
        );
    }

    return (
        <>
            <BuilderSpinnerBoundary {...spinnerBoundaryProps} show={loading}>
                {content}
            </BuilderSpinnerBoundary>
        </>
    );
}
