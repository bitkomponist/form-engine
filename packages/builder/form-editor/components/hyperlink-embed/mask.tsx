import React from 'react';
import Builder from '@form-engine/builder-application';
import { Button, FormControl, InputGroup } from 'react-bootstrap';
import { Clipboard } from 'react-bootstrap-icons';
import { useCallback } from 'react';
import { FormDefinition } from '@form-engine/core-application/types/form';
import BuilderProperty from '@form-engine/builder-mask/components/mask/property';
import { toast } from 'react-toastify';

function ClipboardButton({ value }: { value: string }) {
    const handleCopy = useCallback(
        (e: React.MouseEvent) => {
            e.preventDefault();
            e.stopPropagation();
            navigator.clipboard.writeText(value);
            toast(<>copied to clipboard</>);
        },
        [value]
    );

    return (
        <Button size="sm" variant="dark" onClick={handleCopy}>
            <Clipboard />
        </Button>
    );
}

export default function BuilderHyperlinkEmbedMask({
    form,
}: {
    form: FormDefinition;
}) {
    const publicUrl =
        Builder.renderFormRoute.replace(':formId', form.slug || form.id || '') +
        '?language=default';

    const a = `<a href="${publicUrl}" target="_self">${form.name}</a>`;

    return (
        <div className="feb-element-mask">
            <div className={`feb-mask-group`}>
                <div className="feb-mask-group-body">
                    <BuilderProperty label="Url">
                        <InputGroup>
                            <FormControl size="sm" defaultValue={publicUrl} />
                            <ClipboardButton value={publicUrl} />
                        </InputGroup>
                    </BuilderProperty>
                    <BuilderProperty label="Hyperlink">
                        <InputGroup>
                            <FormControl size="sm" defaultValue={a} />
                            <ClipboardButton value={a} />
                        </InputGroup>
                    </BuilderProperty>
                </div>
            </div>
        </div>
    );
}
