import { BuilderEmbedDescriptor } from '@form-engine/builder-application/types/embed';
import { Link45deg as Icon } from 'react-bootstrap-icons';
import Mask from './mask';
export default {
    label: 'form-editor.hyperlinkEmbed.title',
    description: 'form-editor.hyperlinkEmbed.description',
    Icon,
    Mask,
} as BuilderEmbedDescriptor;
