import IconButton from '@form-engine/builder-icon-button'
import React, { MouseEvent, useCallback } from 'react'
import { TrashFill, ClipboardFill, ClipboardPlusFill, ArrowBarDown, ClipboardCheckFill, ClipboardXFill, Scissors } from 'react-bootstrap-icons'
import useDeleteSelectionAction from '../../actions/delete-selection'
import useCopySelectionAction, { clipboard } from '../../actions/copy-selection';
import { i18n } from '@form-engine/builder-application';
import { useFormElement } from '@form-engine/core-application/hooks/use-form';
import usePasteSelectionAction from '../../actions/paste-selection';
import useCutSelectionAction from '../../actions/cut-selection';

export default function BuilderFormEngineElementControls({id}:{id:string}){
  const deleteSelection = useDeleteSelectionAction();
  const copySelection = useCopySelectionAction();
  const cutSelection = useCutSelectionAction();
  const pasteAfter = usePasteSelectionAction();
  const useAction = (action:()=>void)=>(
    (e:MouseEvent) => {
      e.preventDefault();
      e.stopPropagation();
      action();
    }
  );
  const {c} = i18n.prefix('elementControls.');
  const { state } = useFormElement(id);

  const clipboardHasContents = Boolean(clipboard.targetId);
  const isInClipboard = clipboard.targetId === state?.id;

  return (
    <div className="feb-element-controls d-flex flex-row">
      { !isInClipboard && clipboardHasContents && <IconButton onClick={useAction(pasteAfter)} title={c('pasteElement')}><ClipboardCheckFill /></IconButton>}
      <IconButton onClick={useAction(copySelection)} title={c('copyElement')}><ClipboardFill /></IconButton>
      <IconButton onClick={useAction(cutSelection)} title={c('cutElement')}><Scissors /></IconButton>
      {/* <IconButton><ClipboardPlusFill /></IconButton> */}
      <IconButton onClick={useAction(deleteSelection)} title={c('deleteElement')}><TrashFill /></IconButton>
    </div>
  )
}