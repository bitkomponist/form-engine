import React, { useCallback, useEffect, useMemo } from 'react';
import FormEngineElement from '@form-engine/core-application/components/element';
import FormEngine from '@form-engine/core-application/components/engine';
import { BuilderDropTarget, BuilderDragSource } from '../dnd';
import { useFormEditorContext } from '../../hooks/use-form-editor';
import useFormEngineElementState, {
    useFormEngineElement,
} from '@form-engine/core-application/hooks/use-element';
import FormEngineContainer, {
    FormEngineContainerProps,
} from '@form-engine/core-application/components/container';
import { useFormSessionContext } from '@form-engine/core-application/hooks/use-formsession';
import { useInvalidateDatasourceData } from '@form-engine/core-application/hooks/use-datasource';
import { getDragImageCanvas } from '../dnd/drag-image';
import { useFormContext } from '@form-engine/core-application/hooks/use-form';
import BuilderFormEngineElementControls from './element-controls';

function BuilderFormEngineElement(
    props: React.ComponentProps<typeof FormEngineElement> & {
        id?: string;
        className?: string;
        parent?: string;
    }
) {
    const { state } = useFormEngineElementState(props.id);
    const { state: editor, setItem } = useFormEditorContext();
    const isSelected = editor.selectedId === props.id;
    const toggle = useCallback(
        (e: React.MouseEvent) => {
            e.stopPropagation();
            e.preventDefault();
            setItem('selectedId', isSelected ? null : props.id);
        },
        [isSelected, setItem, props.id]
    );

    return (
        <FormEngineElement
            {...props}
            as={BuilderDragSource}
            source={state}
            dragImage={getDragImageCanvas(state?.name ?? '')}
            onClick={toggle}
            editMode
            className={`${isSelected ? 'is-selected' : ''} ${
                props.className || ''
            }`}
            prepend={
                <BuilderDropTarget
                    id={props.id}
                    parent={props.parent}
                    placement="before"
                />
            }
            append={
                (<>
                    <BuilderDropTarget
                        id={props.id}
                        parent={props.parent}
                        placement="after"
                    />
                    <BuilderFormEngineElementControls id={props.id} />
                </>)
            }
        />
    );
}

function BuilderFormEngineContainer(props: FormEngineContainerProps) {
    const { state: element } = useFormEngineElement();

    return (
        <FormEngineContainer
            {...props}
            className={`feb-fe-container ${props.className || ''}`}
            prepend={
                !props?.childElementIds?.length ? (
                    <BuilderDropTarget
                        parent={element?.id}
                        placement="before"
                    />
                ) : undefined
            }
        />
    );
}

function LocaleUpdater() {
    const { state: editor } = useFormEditorContext();
    const { setItem: setSessionItem, state: session } = useFormSessionContext();
    const resetDatasources = useInvalidateDatasourceData();

    useEffect(() => {
        const locale =
            !editor?.translationLocale || editor?.canvasTab === 'edit'
                ? 'default'
                : editor?.translationLocale;
        if (session?.locals?.locale !== locale) {
            setSessionItem('locals', { ...(session?.locals ?? {}), locale });
            resetDatasources();
        }
    }, [
        editor?.translationLocale,
        editor?.canvasTab,
        setSessionItem,
        session,
        resetDatasources,
    ]);

    return null;
}

export default function BuilderFormEngine({
    editMode = 'edit',
}: {
    editMode?: string;
}) {
    const { state: editor } = useFormEditorContext();
    const value = useMemo(() => {
        if (editMode === 'edit')
            return { locals: { locale: 'default' }, editMode };
        return {
            locals: { locale: editor?.translationLocale ?? 'default' },
            editMode,
        };
    }, [editor?.translationLocale, editMode]);

    const { state } = useFormContext();
    const hasRootElements = useMemo(() => {
        return state?.elements?.find((element) => !element?.parent);
    }, [state?.elements]);

    return (
        <FormEngine
            className={`feb-form-engine ${
                editor.isDragging ? 'is-dragging' : ''
            }`}
            elementComponent={BuilderFormEngineElement}
            containerComponent={BuilderFormEngineContainer}
            editMode={editMode}
            value={value}
        >
            <>
                <LocaleUpdater />
                {!hasRootElements && <BuilderDropTarget placement="after" />}
            </>
        </FormEngine>
    );
}
