import { BuilderFormStudioViewDescriptor } from '@form-engine/builder-application/types/form-studio-view';

import { Cursor as Icon } from 'react-bootstrap-icons';
import { BuilderFormEditorViewForm as Component } from './editor';
export default {
    label: 'form-editor.layout.title',
    description: 'form-editor.layout.description',
    Component,
    Icon,
} as BuilderFormStudioViewDescriptor;
