import { toJpeg } from 'html-to-image';

export interface FormToImageOptions {
    domWidth?: number;
    domHeight?: number;
    width?: number;
    height?: number;
    quality?: number;
}

export async function formToImage(
    root: HTMLElement,
    options: FormToImageOptions = {}
) {
    const {
        domWidth = 1024,
        domHeight = (1024 / 16) * 9,
        width = 240,
        height = (240 / 16) * 9,
        quality = 0.6,
    } = options;

    const container = document.createElement('div');

    Object.assign(container.style, {
        position: 'fixed',
        top: 0,
        left: 0,
        height: 0,
        width: 0,
        overflow: 'hidden',
    });

    document.body.appendChild(container);

    const frame = document.createElement('div');

    Object.assign(frame.style, {
        width: `${domWidth}px`,
        height: `${domHeight}px`,
        background: 'var(--bs-gray-200)',
        padding: '30px',
        overflow: 'hidden',
    });

    const form = root.cloneNode(true) as HTMLFormElement;
    form.classList.toggle('is-initialized', true);
    frame.appendChild(form);
    container.appendChild(frame);

    const img = await toJpeg(frame, {
        canvasWidth: width,
        canvasHeight: height,
        quality,
    });

    // console.log('%c ', `font-size:240px; background:url('${img}') no-repeat; background-size: contain;`);

    // console.log(img);

    document.body.removeChild(container);

    return img;
}
