import BuilderElementMask from '@form-engine/builder-mask/components/mask';
import BuilderMaskGroup from '@form-engine/builder-mask/components/mask/group';
import BuilderArrayProperty from '@form-engine/builder-mask/components/properties/array';
import BuilderEnumProperty from '@form-engine/builder-mask/components/properties/enum';
import BuilderStringProperty from '@form-engine/builder-mask/components/properties/string';
import {
    MaskProvider,
    useMaskContext,
} from '@form-engine/builder-mask/hooks/use-mask';
import { formatVariableCase } from '@form-engine/core-util/format-case';
import React, { useMemo } from 'react';
import { useFormEditorContext } from '../../hooks/use-form-editor';
import { i18n } from '@form-engine/builder-application';
import BuilderAuthProperty from '@form-engine/builder-mask/components/properties/auth';

export function BuilderPreviewSettingsMask() {
    const { state } = useMaskContext();
    const { c } = i18n.prefix('form-editor.previewSettings.');

    return (
        <BuilderElementMask id="BuilderPreviewSettingsMask" general={false}>
            <BuilderMaskGroup label={c('authGroupTitle')}>
                <BuilderAuthProperty />
            </BuilderMaskGroup>
            <BuilderMaskGroup>
                <BuilderArrayProperty
                    propertyName="customVariables"
                    label={c('customLocals')}
                    cols={{
                        key: {
                            label: c('customLocalsVariable'),
                            formatter: formatVariableCase,
                        },
                        value: c('customLocalsValue'),
                    }}
                />
            </BuilderMaskGroup>
        </BuilderElementMask>
    );
}

export function BuilderFormEditorPreviewSettings() {
    const { state: editor, setItem } = useFormEditorContext();

    const api = useMemo(() => {
        const state = editor?.previewSettings ?? {};

        return {
            state,
            setItem: (value: any) => {
                setItem('previewSettings', value);
            },
            setItemProperty: (key: keyof any, value: any) => {
                setItem('previewSettings', { ...state, [key]: value });
            },
        };
    }, [editor, setItem]);

    return (
        <MaskProvider value={api}>
            <BuilderPreviewSettingsMask />
        </MaskProvider>
    );
}
