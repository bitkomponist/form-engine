import React, { useEffect, useMemo, useState } from 'react';
import FormEngine, {
    FormEngineProps,
} from '@form-engine/core-application/components/engine';
import { useFormEditorContext } from '../../hooks/use-form-editor';
import { useRef } from 'react';
import { FormSession } from '@form-engine/core-application/types/form';
import { useFormSessionContext } from '@form-engine/core-application/hooks/use-formsession';
import { useInvalidateDatasourceData } from '@form-engine/core-application/hooks/use-datasource';
import { useFormContext } from '@form-engine/core-application/hooks/use-form';
import { formToImage } from './form-to-image';

function PreviewSessionUpdater({
    dirty: dirtyProp = false,
    onWrite,
}: {
    dirty?: boolean;
    onWrite?: () => void;
}) {
    const { state: session, setItem: setSessionItem } = useFormSessionContext();
    const {
        state: { previewSettings = {} },
    } = useFormEditorContext();

    const resetDatasources = useInvalidateDatasourceData();

    useEffect(() => {
        const {
            auth,
            basicAuthUsername,
            basicAuthPassword,
            bearerToken,
            customVariables,
        } = previewSettings as any;

        const request = {
            ...(session?.request || {}),
            headers: {
                ...(session?.request?.headers || {}),
            },
        };

        if (auth === 'basic') {
            request.headers.authorization = `Basic ${btoa(
                `${basicAuthUsername}:${basicAuthPassword}`
            )}`;
        } else if (auth === 'bearer') {
            request.headers.authorization = `Bearer ${bearerToken}`;
        }

        let dirty = dirtyProp;

        if (
            session?.request?.headers?.authorization !==
            request?.headers?.authorization
        ) {
            setSessionItem('request', request);
            dirty = true;
        }

        const localsUpdate: any = {};
        let hasDirtyLocals = dirtyProp;

        if (Array.isArray(customVariables)) {
            for (const { key, value } of customVariables) {
                if (key && value && session?.locals?.[key] !== value) {
                    localsUpdate[key] = value;
                    hasDirtyLocals = true;
                }
            }
        }

        if (hasDirtyLocals) {
            dirty = true;
            setSessionItem('locals', {
                ...(session?.locals ?? {}),
                ...localsUpdate,
            });
        }

        if (dirty) {
            resetDatasources();
        }

        if (dirty || hasDirtyLocals) onWrite?.();
    }, [
        previewSettings,
        session,
        setSessionItem,
        resetDatasources,
        dirtyProp,
        onWrite,
    ]);

    return null;
}

function FormThumbnailUpdater() {
    const ref = useRef<HTMLDivElement>(null);
    const { state, setItem } = useFormContext();
    const currentTimeoutRef = useRef<NodeJS.Timeout | undefined>();

    useEffect(() => {
        let mounted = true;
        const target = ref?.current?.closest('form.fe');
        if (!target) return;

        if (currentTimeoutRef.current) {
            clearTimeout(currentTimeoutRef.current);
        }

        currentTimeoutRef.current = setTimeout(() => {
            formToImage(target as HTMLElement).then((dataUrl) => {
                if (!mounted) {
                    return;
                }
                setItem('thumbnail', dataUrl);
            });
            currentTimeoutRef.current = undefined;
        }, 500);

        return () => {
            mounted = false;
        };
    }, [state?.elements, setItem]);
    return <div style={{ visibility: 'hidden' }} ref={ref} />;
}

function LocaleUpdater() {
    const { state: editor } = useFormEditorContext();
    const { setItem: setSessionItem, state: session } = useFormSessionContext();
    const resetDatasources = useInvalidateDatasourceData();

    useEffect(() => {
        const locale = editor?.translationLocale || 'default';
        if (locale !== session?.locals?.locale) {
            setSessionItem('locals', { ...(session?.locals ?? {}), locale });
            resetDatasources();
        }
    }, [editor?.translationLocale, setSessionItem, session, resetDatasources]);

    return null;
}

export function BuilderFormEditorPreviewFormEngine({
    value: initialValue,
    children,
    ...props
}: Omit<FormEngineProps, 'value'> & { value?: FormSession }) {
    const { state: editor } = useFormEditorContext();
    const [dirty, setDirty] = useState(false);

    const value = useMemo(() => {
        return {
            ...(initialValue ?? {}),
            locals: {
                ...(initialValue?.locals ?? {}),
                locale: editor?.translationLocale ?? 'default',
            },
            editMode: 'preview',
        };
    }, [editor?.translationLocale, initialValue]);

    return (
        <FormEngine {...props} value={value} onReset={() => setDirty(true)}>
            <>
                <PreviewSessionUpdater
                    dirty={dirty}
                    onWrite={() => setDirty(false)}
                />
                <LocaleUpdater />
                <FormThumbnailUpdater />
                {children}
            </>
        </FormEngine>
    );
}
