import React, { useMemo } from 'react';
import { Col, Row } from 'react-bootstrap';
import { FormEngineRemote } from '@form-engine/core-application/components/engine';
import { useFormEditorContext } from '../../hooks/use-form-editor';
import BuilderDrawerTabs from '@form-engine/builder-application/components/drawer-tabs';
import Drawer from '@form-engine/builder-application/components/drawer';
import { useRef } from 'react';
import BuilderAppCol from '@form-engine/builder-application/components/app-col';
import Builder, { i18n } from '@form-engine/builder-application';
import {
    BuilderFormLayoutEditorViewDefaults,
    BuilderFormLayoutEditorViewMap,
} from './view';
import { useObjectDefaults } from '@form-engine/core-util/create-object';

export function BuilderFormEditorViewForm() {
    const {
        state: { leftDrawerTab, canvasTab, rightDrawerTab },
        setItem,
    } = useFormEditorContext();

    const formRemote = useRef<FormEngineRemote>();

    const editorViewDef = useObjectDefaults(
        BuilderFormLayoutEditorViewDefaults,
        useMemo(() => {
            const defs = (Builder?.formLayoutEditorViews ??
                {}) as BuilderFormLayoutEditorViewMap;

            return defs[canvasTab] ?? {};
        }, [canvasTab])
    );

    const leftDrawer = useMemo(() => {
        const items = Object.fromEntries(
            Object.entries(editorViewDef.leftDrawerTabs).map(([key, tab]) => [
                key,
                i18n.c(tab.title),
            ])
        );
        const activeKey =
            leftDrawerTab in items
                ? leftDrawerTab
                : editorViewDef.defaultLeftDrawerTab;
        return {
            items,
            activeKey,
            component: activeKey
                ? editorViewDef.leftDrawerTabs[activeKey]?.component
                : undefined,
        };
    }, [editorViewDef.leftDrawerTabs, leftDrawerTab]);

    const rightDrawer = useMemo(() => {
        const items = Object.fromEntries(
            Object.entries(editorViewDef.rightDrawerTabs).map(([key, tab]) => [
                key,
                i18n.c(tab.title),
            ])
        );
        const activeKey =
            rightDrawerTab in items
                ? rightDrawerTab
                : editorViewDef.defaultRightDrawerTab;
        return {
            items,
            activeKey,
            component: activeKey
                ? editorViewDef.rightDrawerTabs[activeKey]?.component
                : undefined,
        };
    }, [editorViewDef.rightDrawerTabs, rightDrawerTab]);

    const viewDrawer = useMemo(() => {
        const defs = (Builder?.formLayoutEditorViews ??
            {}) as BuilderFormLayoutEditorViewMap;

        const items = Object.fromEntries(
            Object.entries(defs).map(([key, view]) => [key, i18n.c(view.title)])
        );
        const component = defs[canvasTab]?.navItemsComponent;

        return { items, component };
    }, [canvasTab]);

    return (
        <Row>
            <BuilderAppCol md={2} border="right" id="layoutEditorLeftCol">
                <BuilderDrawerTabs
                    activeKey={leftDrawer.activeKey}
                    items={leftDrawer.items}
                    onChange={(key) => setItem('leftDrawerTab', key)}
                />
                <div className="feb-view m-0">
                    <div className="feb-view-inner p-0">
                        <Drawer>
                            {leftDrawer.component && (
                                <leftDrawer.component
                                    remote={formRemote}
                                    editMode={canvasTab}
                                />
                            )}
                        </Drawer>
                    </div>
                </div>
            </BuilderAppCol>
            <Col className="feb-main-col">
                <BuilderDrawerTabs
                    activeKey={canvasTab}
                    items={viewDrawer.items}
                    onChange={(key) => {
                        setItem('canvasTab', key);
                    }}
                >
                    <div className="ms-auto d-flex">
                        {viewDrawer.component && (
                            <viewDrawer.component
                                remote={formRemote}
                                editMode={canvasTab}
                            />
                        )}
                    </div>
                </BuilderDrawerTabs>
                <div className="feb-view">
                    <div className="feb-view-inner">
                        {editorViewDef.component && (
                            <editorViewDef.component
                                remote={formRemote}
                                editMode={canvasTab}
                            />
                        )}
                    </div>
                </div>
            </Col>
            <BuilderAppCol md={2} border="left" id="layoutEditorRightCol">
                <BuilderDrawerTabs
                    activeKey={rightDrawer.activeKey}
                    items={rightDrawer.items}
                    onChange={(key) => setItem('rightDrawerTab', key)}
                />

                {rightDrawer.component && (
                    <rightDrawer.component
                        remote={formRemote}
                        editMode={canvasTab}
                    />
                )}
            </BuilderAppCol>
        </Row>
    );
}
