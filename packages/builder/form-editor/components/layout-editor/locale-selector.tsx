import { BuilderAdvancedFormSelect } from '@form-engine/builder-application/components/select/advanced';
import React, { useCallback, useMemo } from 'react';
import { Translate } from 'react-bootstrap-icons';
import { useFormEditorContext } from '../../hooks/use-form-editor';
import locales from '@form-engine/builder-application/data/locales';
import { useFormElements } from '@form-engine/core-application/hooks/use-form';

const options = [['default', 'Default'], ...locales].map(([value, label]) => ({
    label: `${label} (${value})`,
    value,
}));

function useTouchedLocales() {
    const { state } = useFormElements();
    return useMemo(() => {
        const locales: string[] = [];
        for (const element of state) {
            if (element.$locales) {
                for (const locale of Object.keys(element.$locales)) {
                    if (!locales.includes(locale)) {
                        locales.push(locale);
                    }
                }
            }
        }
        return locales;
    }, [state]);
}

export function BuilderFormEditorLocaleSelector() {
    const { state: editor, setItem } = useFormEditorContext();
    const touchedLocales = useTouchedLocales();

    const currentValue = useMemo(() => {
        const locale = editor?.translationLocale || 'default';
        return (
            options.find(({ value }) => value === locale)?.value ?? 'default'
        );
    }, [editor?.translationLocale]);

    const handleChange = useCallback(
        (e: any) => {
            setItem('translationLocale', e?.target?.value ?? 'default');
        },
        [setItem]
    );

    const highlightedOptions = useMemo(() => {
        return options.map((option) => {
            return {
                ...option,
                label: touchedLocales.includes(option.value)
                    ? `${option.label} *`
                    : option.label,
            };
        });
    }, [touchedLocales]);

    return (
        <div
            className="feb-formeditor-locale-selector mt-1 me-1 ms-2 d-flex flex-row align-items-center"
            style={{ minWidth: '300px' }}
        >
            <Translate />
            <BuilderAdvancedFormSelect
                className="ms-1 w-100"
                defaultValue={{ label: 'Default', value: 'default' }}
                options={highlightedOptions}
                value={currentValue}
                onChange={handleChange}
            />
        </div>
    );
}
