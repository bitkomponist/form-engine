import { Optional } from '@form-engine/core-util/typings';
import { Fragment } from 'react';

interface BuilderFormLayoutEditorDrawerTabDefinition {
    title: string;
    component?: React.ElementType;
}

export const BuilderFormLayoutEditorViewDefaults = {
    title: '',
    component: Fragment as React.ElementType,
    leftDrawerTabs: {} as {
        [key: string]: BuilderFormLayoutEditorDrawerTabDefinition;
    },
    rightDrawerTabs: {} as {
        [key: string]: BuilderFormLayoutEditorDrawerTabDefinition;
    },
    defaultLeftDrawerTab: undefined as Optional<string>,
    defaultRightDrawerTab: undefined as Optional<string>,
};

export type BuilderFormLayoutEditorView =
    typeof BuilderFormLayoutEditorViewDefaults & {
        navItemsComponent?: React.ElementType;
    };

export type BuilderFormLayoutEditorViewMap = {
    [key: string]: BuilderFormLayoutEditorView;
};
