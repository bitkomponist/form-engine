import React, { useCallback, useEffect, useMemo } from 'react';
import { Col, Container, Nav, Row } from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import BuilderFormRouteContextProvider from '../form-route-context';
import { useFormContext } from '@form-engine/core-application/hooks/use-form';
import {
    formEditorStorage,
    useFormEditorContext,
} from '../../hooks/use-form-editor';

import { useAppTabs } from '@form-engine/builder-application/hooks/use-app';
import BuilderAppViewNavbar from '@form-engine/builder-application/components/app-view-navbar';
import {
    BoxSeam,
    Eye,
    EyeSlash,
    Play,
    ThreeDots,
    ArrowCounterclockwise,
    ArrowClockwise,
} from 'react-bootstrap-icons';
import BuilderFormActionsDropdown from '../actions-dropdown';
import Builder, { i18n } from '@form-engine/builder-application';
import BuilderDragAndDropCursor from '../dnd/cursor';
import * as actions from '../../actions';
import useEmbedFormAction, {
    ACTION_NAME as EMBED_ACTION_NAME,
} from '../../actions/embed-form';
import useUndoAction, {
    ACTION_NAME as UNDO_ACTION_NAME,
} from '../../actions/undo';
import useRedoAction, {
    ACTION_NAME as REDO_ACTION_NAME,
} from '../../actions/redo';
import { BuilderFormStudioViewDescriptor } from '@form-engine/builder-application/types/form-studio-view';
import { getHotkeyLabel } from '@form-engine/builder-application/keybindings';

export function BuilderFormStudioHotkeys() {
    Object.values(actions).forEach(
        ({ useActionHotkey }) => useActionHotkey && useActionHotkey()
    );

    return null;
}

function BuilderFormStudioEditorStateWriter() {
    const { formId } = useParams();
    const { state: form } = useFormContext();
    const { state: editor } = useFormEditorContext();
    const { toggle } = useAppTabs();

    /** @todo autosave */
    /** @see https://www.npmjs.com/package/json-diff */
    // useEffect(() => {
    //   /** @todo autosave */
    //   /** @see https://www.npmjs.com/package/json-diff */
    // }, [formId, form]);

    useEffect(() => {
        if (form && form.id && formId === form.id) {
            formEditorStorage.setItem(formId, editor);
        }
    }, [form, formId, editor]);

    useEffect(() => {
        if (form && form.id && formId === form.id) {
            toggle(
                { id: formId, name: form.name, path: `/forms/${formId}` },
                true
            );
        }
    }, [form, formId]);

    return null;
}

export interface BuilderFormStudioNavLinkProps
    extends React.ComponentProps<typeof Nav.Link> {
    title: string;
    isActive?: boolean;
    activeClass?: string;
    children?: React.ReactNode;
}

export function BuilderFormStudioNavLink({
    title,
    isActive = false,
    activeClass = 'active',
    className = '',
    children,
    ...linkProps
}: BuilderFormStudioNavLinkProps) {
    return (
        <Nav.Link
            title={title}
            className={`${className} ${isActive ? activeClass : ''}`}
            {...linkProps}
        >
            {children}
        </Nav.Link>
    );
}

export function BuilderFormStudioNavViewToggle({
    id,
    ...props
}: { id: string } & BuilderFormStudioNavLinkProps) {
    const {
        state: { view = 'form' },
        setItem,
    } = useFormEditorContext();

    const handleClick = useCallback(() => {
        view !== id && setItem('view', id);
    }, [id, view, setItem]);

    return (
        <BuilderFormStudioNavLink
            {...props}
            isActive={view === id}
            onClick={handleClick}
        />
    );
}

function BuilderFormStudioLayout() {
    const {
        state: { view = 'form', formHistory = [], formRedoHistory = [] },
    } = useFormEditorContext();

    const { state: form } = useFormContext();

    const embedForm = useEmbedFormAction();

    const publicUrl = Builder.renderFormRoute.replace(
        ':formId',
        form.slug || form.id || ''
    );

    const undo = useUndoAction();
    const redo = useRedoAction();

    const views = useMemo(
        () => Object.entries(Builder.formStudioViews || {}),
        []
    );
    const CurrentViewComponent = (
        views
            .find(([key]) => key === view)
            ?.at(1) as BuilderFormStudioViewDescriptor
    )?.Component;

    const { c } = i18n.prefix('form-editor.nav.');

    return (
        <>
            <BuilderAppViewNavbar>
                <div className="w-100">
                    <Row>
                        <Col className="d-flex" sm={4}>
                            {views.map(([key, descriptor]) => {
                                const { label, Icon } = descriptor;
                                return (
                                    <BuilderFormStudioNavViewToggle
                                        key={key}
                                        id={key}
                                        title={i18n.c(label)}
                                    >
                                        <Icon />
                                    </BuilderFormStudioNavViewToggle>
                                );
                            })}
                            <BuilderFormStudioNavLink
                                title={c('undo', {
                                    hotkey: getHotkeyLabel(UNDO_ACTION_NAME),
                                })}
                                onClick={undo}
                                className={`ms-2 ${
                                    formHistory.length ? '' : 'disabled'
                                }`}
                            >
                                <ArrowCounterclockwise />
                            </BuilderFormStudioNavLink>
                            <BuilderFormStudioNavLink
                                title={c('redo', {
                                    hotkey: getHotkeyLabel(REDO_ACTION_NAME),
                                })}
                                onClick={redo}
                                className={`ms-2 ${
                                    formRedoHistory.length ? '' : 'disabled'
                                }`}
                            >
                                <ArrowClockwise />
                            </BuilderFormStudioNavLink>
                        </Col>
                        <Col className="d-flex" sm={4}>
                            <BuilderFormActionsDropdown
                                menuVariant="dark"
                                className="me-auto ms-auto"
                                title={
                                    <>
                                        {form.published ? (
                                            <Eye />
                                        ) : (
                                            <EyeSlash />
                                        )}
                                        {` ${form.name}`}
                                    </>
                                }
                            />
                        </Col>
                        <Col className="d-flex" sm={4}>
                            <BuilderFormStudioNavLink
                                title={c('embed', {
                                    hotkey: getHotkeyLabel(EMBED_ACTION_NAME),
                                })}
                                className="ms-auto"
                                onClick={embedForm}
                            >
                                <BoxSeam />
                            </BuilderFormStudioNavLink>
                            <BuilderFormStudioNavLink
                                title={c('open-in-browser')}
                                href={publicUrl}
                                target="_blank"
                            >
                                <Play />
                            </BuilderFormStudioNavLink>
                        </Col>
                    </Row>
                </div>
            </BuilderAppViewNavbar>
            <Container fluid className="feb-form-editor-layout">
                {CurrentViewComponent && <CurrentViewComponent />}
            </Container>
        </>
    );
}

export function BuilderFormStudioViewStencil() {
    return (
        <BuilderAppViewNavbar>
            <div className="w-100">
                <Row>
                    <Col className="d-flex me-auto ms-auto" sm={4}>
                        <div className="me-auto ms-auto nav-item">
                            <span className="nav-link">
                                <ThreeDots />
                            </span>
                        </div>
                    </Col>
                </Row>
            </div>
        </BuilderAppViewNavbar>
    );
}

export default function BuilderFormStudioView() {
    return (
        <BuilderFormRouteContextProvider
            spinnerBoundaryProps={{ prepend: <BuilderFormStudioViewStencil /> }}
        >
            <BuilderFormStudioHotkeys />
            <BuilderFormStudioEditorStateWriter />
            <BuilderFormStudioLayout />
            {/* <BuilderDragAndDropCursor /> */}
        </BuilderFormRouteContextProvider>
    );
}
