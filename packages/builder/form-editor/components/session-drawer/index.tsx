import React, { useCallback } from 'react';
import {
    FormEngineRemote,
    FormEngineStateProvider,
} from '@form-engine/core-application/components/engine';
import { useFormSessionLocals } from '@form-engine/core-application/hooks/use-formsession';
import useFormRemoteState from '@form-engine/core-application/hooks/use-form-remote-state';
import {
    BuilderTree,
    useBuilderTreeNodes,
} from '@form-engine/builder-application/components/tree';
import { useFormEditorContext } from '../../hooks/use-form-editor';
import IconButton from '@form-engine/builder-icon-button';
import { Clipboard } from 'react-bootstrap-icons';
import { toast } from 'react-toastify';

function ClipboardBtn({ value }: { value?: string }) {
    const handleClick = useCallback(
        (e) => {
            e.preventDefault();
            e.stopPropagation();
            value && navigator.clipboard.writeText(value);
            toast(<>copied to clipboard</>);
        },
        [value]
    );

    return (
        <IconButton as="span" title={value} onClick={handleClick}>
            <Clipboard />
        </IconButton>
    );
}

function generateScopeNode(key: string | number, value: any, path: string) {
    let label = (
        <strong>
            <ClipboardBtn value={`{{${path}}}`} /> {key}
        </strong>
    );

    if (typeof value !== 'object') {
        label = (
            <>
                <strong>
                    <ClipboardBtn value={`{{${path}}}`} /> {key}:
                </strong>{' '}
                <span className="feb-tree-node-value">
                    {JSON.stringify(value)}
                </span>
            </>
        );
    }

    return { label };
}

function SessionDrawer() {
    const locals = useFormSessionLocals();

    const { setItem, state } = useFormEditorContext();

    const handleToggleItem = useCallback(
        (id: string, force?: boolean) => {
            const copy = [...(state?.sessionDrawerState || [])];
            const index = copy.indexOf(id);
            if (force !== false && index < 0) {
                copy.push(id);
            } else if (force !== true && index > -1) {
                copy.splice(index, 1);
            }
            setItem('sessionDrawerState', copy);
        },
        [state?.sessionDrawerState, setItem]
    );

    const nodes = useBuilderTreeNodes(locals?.state ?? {}, generateScopeNode);

    return (
        <div className="feb-view m-0">
            <div className="feb-view-inner p-0">
                <div className="feb-properties-drawer">
                    <BuilderTree
                        nodes={nodes}
                        state={state?.sessionDrawerState}
                        onToggle={handleToggleItem}
                    />
                </div>
            </div>
        </div>
    );
}

export default function BuilderSessionDrawer({
    remote,
}: {
    remote?: React.MutableRefObject<FormEngineRemote | undefined>;
}) {
    const formRemoteState = useFormRemoteState(remote);

    if (!formRemoteState) return null;

    const { form, session, workflow } = formRemoteState;

    return (
        <FormEngineStateProvider
            form={form.state}
            session={session.state}
            workflow={workflow.state}
        >
            <SessionDrawer />
        </FormEngineStateProvider>
    );
}
