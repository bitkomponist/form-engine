import React, { useCallback, useEffect, useMemo } from 'react';
import { useFormEditorContext } from '../../hooks/use-form-editor';
import { useFormContext } from '@form-engine/core-application/hooks/use-form';
import useFormEngineElementState from '@form-engine/core-application/hooks/use-element';
import {
    MaskContextValue,
    MaskProvider,
} from '@form-engine/builder-mask/hooks/use-mask';
import { BoundBuilderFormMask } from '../form-mask';
import Builder from '@form-engine/builder-application';
import { ElementType } from '@form-engine/core-application/types/form';
import {
    FormSessionProvider,
    useFormSessionContext,
} from '@form-engine/core-application/hooks/use-formsession';

export function InnerBuilderSettingsDrawer({
    fallback: Fallback = BoundBuilderFormMask,
    editMode = 'edit',
}: {
    fallback?: React.ElementType;
    editMode?: string;
}) {
    const maskTypes = useMemo(
        () =>
            Object.values(Builder.elements).map(
                (def): [ElementType, React.ElementType] => [
                    def.Element,
                    def.Mask,
                ]
            ),
        []
    );
    const { state: form } = useFormContext();
    const { state: editor } = useFormEditorContext();
    const { selectedId: selectedElementId } = editor;
    const selectedElement = useMemo(() => {
        if (!selectedElementId) return null;
        return (form.elements || []).find(
            (element) => element.id === selectedElementId
        );
    }, [form.elements, selectedElementId]);
    const Mask = useMemo(() => {
        if (!selectedElement) return null;
        const entry = maskTypes.find(
            ([Element]) => Element.apiName === selectedElement.type
        );
        if (!entry) return null;
        return entry[1];
    }, [selectedElement]);

    const element = useFormEngineElementState(selectedElementId || undefined);

    const setItemPropertyTranslation = useCallback(
        (propertyName: string, value: any) => {
            const locale = editor?.translationLocale;

            if (editMode !== 'translate' || !locale || locale === 'default')
                return element.setItemProperty(propertyName, value);
            const $locales: any = element?.state?.$locales ?? {};

            return element.setItemProperty('$locales', {
                ...$locales,
                [locale]: {
                    ...($locales[locale as keyof typeof $locales] ?? {}),
                    [propertyName]: value,
                },
            });
        },
        [element, editMode, editor?.translationLocale]
    );

    const elementApi = useMemo(
        () =>
            ({
                ...element,
                setItemProperty: setItemPropertyTranslation,
            } as ReturnType<typeof useFormEngineElementState>),
        [element, setItemPropertyTranslation]
    );

    const { setItem: setSessionItem, state: session } = useFormSessionContext();

    useEffect(() => {
        const locale =
            editor?.canvasTab === 'edit' || !editor?.translationLocale
                ? 'default'
                : editor?.translationLocale;
        if (session?.locals?.locale !== locale) {
            setSessionItem('locals', { ...(session?.locals ?? {}), locale });
        }
    }, [
        editor?.translationLocale,
        editor?.canvasTab,
        setSessionItem,
        session?.locals,
    ]);

    return (
        <div className="feb-view m-0">
            <div className="feb-view-inner p-0">
                <div className="feb-properties-drawer">
                    {Mask ? (
                        <MaskProvider value={elementApi as MaskContextValue}>
                            <Mask editMode={editMode} />
                        </MaskProvider>
                    ) : (
                        Fallback && <Fallback editMode={editMode} />
                    )}
                </div>
            </div>
        </div>
    );
}

export default function BuilderSettingsDrawer(props: {
    fallback?: React.ElementType;
    editMode?: string;
}) {
    const { state: editor } = useFormEditorContext();
    const sessionData = useMemo(() => {
        return {
            locals: {
                locale:
                    editor?.canvasTab === 'edit' || !editor?.translationLocale
                        ? 'default'
                        : editor?.translationLocale,
            },
        };
    }, [editor?.translationLocale, editor?.canvasTab]);
    return (
        <FormSessionProvider value={sessionData}>
            <InnerBuilderSettingsDrawer {...props} />
        </FormSessionProvider>
    );
}
