import BuilderCard from '@form-engine/builder-application/components/card';
import React from 'react';
import { Card, Col, Row } from 'react-bootstrap';
import { ArrowLeftCircleFill } from 'react-bootstrap-icons';
import IconButton from '@form-engine/builder-icon-button';
import Builder from '@form-engine/builder-application';
import { useMemo } from 'react';
import { useState } from 'react';
import { MaskProvider } from '@form-engine/builder-mask/hooks/use-mask';
import { FormDefinition } from '@form-engine/core-application/types/form';
import { BuilderEmbedDescriptor } from '@form-engine/builder-application/types/embed';
import { i18n } from '@form-engine/builder-application';

export function BuilderFormEmbedList({
    types = [],
    onSelect,
}: {
    types: [string, BuilderEmbedDescriptor][];
    onSelect: (key: string) => void;
}) {
    return (
        <Row>
            {types.map(([key, def]) => {
                return (
                    <Col md={6} lg={4} xl={4} key={key}>
                        <BuilderCard
                            as="button"
                            onClick={() => {
                                onSelect && onSelect(key);
                            }}
                        >
                            <Card.Body className="row d-flex">
                                <Col>
                                    <Card.Title className="text-xs">
                                        <i18n.Node id={def.label} />
                                    </Card.Title>
                                    <Card.Text className="text-xs text-black-50">
                                        {def.description && (
                                            <i18n.Node id={def.description} />
                                        )}
                                    </Card.Text>
                                </Col>
                                <Col xs={2} className="align-self-center">
                                    <def.Icon />
                                </Col>
                            </Card.Body>
                        </BuilderCard>
                    </Col>
                );
            })}
        </Row>
    );
}

export default function BuilderFormEmbedListing({
    form,
}: {
    form: FormDefinition;
}) {
    const types = useMemo(() => Object.entries(Builder.formEmbeds || {}), []);
    const [selected, setSelected] = useState<string | null>(null);
    const Mask = useMemo(() => {
        if (!selected) return null;
        return Builder.formEmbeds?.[selected]?.Mask;
    }, [selected]) as React.FunctionComponent<{ form: FormDefinition }>;

    const maskApi = useMemo(() => ({ state: form, setItem: () => {} }), [form]);

    if (!selected) {
        return <BuilderFormEmbedList types={types} onSelect={setSelected} />;
    }

    if (!Mask) {
        return null;
    }

    return (
        <>
            <p>
                <IconButton onClick={() => setSelected(null)}>
                    <ArrowLeftCircleFill />
                </IconButton>
            </p>
            <MaskProvider value={maskApi}>
                <Mask form={form} />
            </MaskProvider>
        </>
    );
}
