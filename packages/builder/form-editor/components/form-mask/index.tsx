import {
    MaskProvider,
    useMaskContext,
} from '@form-engine/builder-mask/hooks/use-mask';
import { useFormContext } from '@form-engine/core-application/hooks/use-form';
import React, { useMemo } from 'react';
import BuilderElementMask, {
    BuilderElementMaskProps,
} from '@form-engine/builder-mask/components/mask';
import BuilderMaskGroup from '@form-engine/builder-mask/components/mask/group';
import BuilderArrayProperty from '@form-engine/builder-mask/components/properties/array';
import BuilderBooleanProperty from '@form-engine/builder-mask/components/properties/boolean';
import BuilderCodeProperty from '@form-engine/builder-mask/components/properties/code';
import BuilderSlugProperty from '@form-engine/builder-mask/components/properties/slug';
import BuilderStringProperty from '@form-engine/builder-mask/components/properties/string';
import { DEFAULT_BOOTSTRAP_CDN_URL } from '@form-engine/core-application/hooks/use-form-resources';
import BuilderTagsProperty from '@form-engine/builder-mask/components/properties/tags';
import { FormDefinition } from '@form-engine/core-application/types/form';
import { i18n } from '@form-engine/builder-application';

export default function BuilderFormMask(props: BuilderElementMaskProps) {
    const { state } = useMaskContext();

    const { c } = i18n.prefix('mask.BuilderFormMask.');

    return (
        <BuilderElementMask id="BuilderFormMask" general={false} {...props}>
            <BuilderMaskGroup>
                <BuilderStringProperty propertyName="name" label={c('name')} />
                <BuilderBooleanProperty
                    propertyName="published"
                    label={c('published')}
                />
                <BuilderSlugProperty defaultValue={state?.id} />
                <BuilderTagsProperty stacked />
            </BuilderMaskGroup>
            <BuilderMaskGroup label={c('behaviourGroupTitle')}>
                <BuilderBooleanProperty
                    propertyName="showValidFeedback"
                    label={c('showValidFeedback')}
                    defaultValue={true}
                />
            </BuilderMaskGroup>
            <BuilderMaskGroup label={c('customizationGroupTitle')}>
                <BuilderBooleanProperty
                    propertyName="includeCoreResources"
                    label={c('includeCoreResources')}
                    defaultValue={true}
                />
                <BuilderBooleanProperty
                    propertyName="includeBootstrapCdn"
                    label={c('includeBootstrapCdn')}
                    defaultValue={true}
                />
                {state.includeBootstrapCdn !== false && (
                    <BuilderStringProperty
                        propertyName="bootstrapCdnUrl"
                        label={c('bootstrapCdnUrl')}
                        placeholder={DEFAULT_BOOTSTRAP_CDN_URL}
                    />
                )}

                <BuilderArrayProperty
                    propertyName="resources"
                    label={c('resources')}
                    cols={{ url: c('resourcesUrl') }}
                />
                <BuilderCodeProperty
                    language="css"
                    propertyName="stylesheet"
                    label={c('stylesheet')}
                    offCanvas
                />
            </BuilderMaskGroup>
        </BuilderElementMask>
    );
}

export function BoundBuilderFormMask(
    props: React.ComponentProps<typeof BuilderFormMask>
) {
    const { state, set: setItem, setItem: setStateItem } = useFormContext();

    const setItemProperty = useMemo(() => {
        return (key: keyof any, value: any) => {
            setStateItem(key as keyof FormDefinition, value);
        };
    }, [setStateItem]);
    return (
        <MaskProvider value={{ state, setItem, setItemProperty }}>
            <BuilderFormMask {...props} />
        </MaskProvider>
    );
}
