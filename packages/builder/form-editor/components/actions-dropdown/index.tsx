import { useFormContext } from '@form-engine/core-application/hooks/use-form';
import React from 'react';
import { NavDropdown, NavDropdownProps } from 'react-bootstrap';
import * as actionDefs from '../../actions';
import { getHotkeyLabel } from '@form-engine/builder-application/keybindings';
import { i18n } from '@form-engine/builder-application';

function useActions() {
    return Object.fromEntries(
        Object.entries(actionDefs).map(([key, { useAction, ACTION_NAME }]) => [
            key,
            { action: useAction(), hotkey: getHotkeyLabel(ACTION_NAME) },
        ])
    );
}

function NavDropdownButton({
    suffix,
    children,
    ...props
}: React.ComponentProps<typeof NavDropdown.Item> & {
    suffix?: React.ReactNode;
}) {
    return (
        <NavDropdown.Item as="button" {...props}>
            <div className="d-flex flex-row">
                <div className="flex-grow-1">{children}</div>
                {suffix && <div className="ms-2 text-muted">{suffix}</div>}
            </div>
        </NavDropdown.Item>
    );
}

export interface BuilderFormActionsDropdownProps
    extends Omit<NavDropdownProps, 'children'> {
    children?: React.ReactNode;
}

export default function BuilderFormActionsDropdown(
    props: BuilderFormActionsDropdownProps
) {
    const { state: form } = useFormContext();
    const actions = useActions();
    const { Node } = i18n.prefix('form-editor.nav.');
    return (
        <NavDropdown {...props}>
            <NavDropdownButton
                onClick={actions.formVersionHistory.action}
                suffix={actions.formVersionHistory.hotkey}
            >
                <Node id="formVersionHistory" />
            </NavDropdownButton>
            <NavDropdownButton
                onClick={
                    form.published
                        ? actions.unpublishForm.action
                        : actions.publishForm.action
                }
                suffix={
                    form.published
                        ? actions.unpublishForm.hotkey
                        : actions.publishForm.hotkey
                }
            >
                <Node id={form.published ? 'unpublishForm' : 'publishForm'} />
            </NavDropdownButton>
            <NavDropdownButton
                onClick={actions.saveForm.action}
                suffix={actions.saveForm.hotkey}
            >
                <Node id="saveForm" />
            </NavDropdownButton>
            <NavDropdown.Divider />
            <NavDropdownButton
                onClick={actions.saveFormAs.action}
                suffix={actions.saveFormAs.hotkey}
            >
                <Node id="saveFormAs" />
            </NavDropdownButton>
            <NavDropdownButton
                onClick={actions.exportForm.action}
                suffix={actions.exportForm.hotkey}
            >
                <Node id="exportForm" />
            </NavDropdownButton>
            <NavDropdownButton
                onClick={actions.renameForm.action}
                suffix={actions.renameForm.hotkey}
            >
                <Node id="renameForm" />
            </NavDropdownButton>
            <NavDropdownButton
                onClick={actions.deleteForm.action}
                suffix={actions.deleteForm.hotkey}
            >
                <Node id="deleteForm" />
            </NavDropdownButton>
            {props.children}
        </NavDropdown>
    );
}
