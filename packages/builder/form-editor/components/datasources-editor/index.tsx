import { BuilderFormStudioViewDescriptor } from '@form-engine/builder-application/types/form-studio-view';

import { Plug as Icon } from 'react-bootstrap-icons';
import { BuilderFormEditorViewDatasources as Component } from './editor';
export default {
    label: 'form-editor.datasources.title',
    description: 'form-editor.datasources.description',
    Component,
    Icon,
} as BuilderFormStudioViewDescriptor;
