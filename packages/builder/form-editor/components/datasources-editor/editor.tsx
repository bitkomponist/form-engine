import React, { useMemo } from 'react';
import { useFormEditorContext } from '../../hooks/use-form-editor';
import { Badge, Card, Col, Container, Row } from 'react-bootstrap';
import { useFormContext } from '@form-engine/core-application/hooks/use-form';
import { PlusLg } from 'react-bootstrap-icons';
import { useCallback } from 'react';
import uuidv4 from '@form-engine/core-util/uuid';
import BuilderExplorer, {
    BuilderExplorerItem,
} from '@form-engine/builder-application/components/explorer';

import { MaskProvider } from '@form-engine/builder-mask/hooks/use-mask';
import useDatasource from '@form-engine/core-application/hooks/use-datasource';
import useDatasourceMask from '@form-engine/builder-application/hooks/use-datasource-mask';
import BuilderCard from '@form-engine/builder-application/components/card';
import Builder, { i18n } from '@form-engine/builder-application';

interface DatasourceTypeItemProps
    extends React.ComponentProps<typeof BuilderCard> {
    label: React.ReactNode;
    description: React.ReactNode;
    icon?: React.ElementType;
}

function DatasourceTypeItem({
    label,
    description,
    icon: Icon = PlusLg,
    ...props
}: DatasourceTypeItemProps) {
    return (
        <Col md={6} lg={4} xl={3}>
            <BuilderCard as="button" {...props}>
                <Card.Body className="row d-flex">
                    <Col>
                        <Card.Title className="text-xs">{label}</Card.Title>
                        <Card.Text className="text-xs text-black-50">
                            {description}
                        </Card.Text>
                    </Col>
                    <Col xs={2} className="align-self-center">
                        <Icon />
                    </Col>
                </Card.Body>
            </BuilderCard>
        </Col>
    );
}

function AddDatasourceMask({ onCreate }: { onCreate?: (id: string) => void }) {
    const datasourceTypes = useMemo(() => Builder.datasources, []);

    const {
        state: { datasources = [] },
        setItem,
    } = useFormContext();

    const addTypeInstance = useCallback(
        (type: string) => {
            return (e: React.MouseEvent) => {
                e.preventDefault();
                e.stopPropagation();
                const id = uuidv4();
                setItem('datasources', [
                    ...datasources,
                    { id, type, name: 'UntitledDatasource' },
                ]);
                onCreate && onCreate(id);
            };
        },
        [datasources, setItem, onCreate]
    );

    return (
        <div className="feb-view">
            <div className="feb-view-inner p-0">
                <Container fluid>
                    <h6 className="mb-3 mt-4">
                        <i18n.Node id="form-editor.datasources.title" />
                    </h6>
                    <hr />
                    <Row style={{ [`--bs-gutter-y` as any]: '1rem' }}>
                        {Object.values(datasourceTypes).map(
                            ({ label, description, Icon, Datasource }) => (
                                <DatasourceTypeItem
                                    key={Datasource.apiName}
                                    label={i18n.c(label)}
                                    description={
                                        description
                                            ? i18n.c(description)
                                            : undefined
                                    }
                                    onClick={addTypeInstance(
                                        Datasource.apiName
                                    )}
                                    icon={Icon}
                                />
                            )
                        )}
                    </Row>
                </Container>
            </div>
        </div>
    );
}

export function DatasourceMask({ id }: { id?: string }) {
    const datasource = useDatasource(id);
    const Mask = useDatasourceMask(id) as React.FunctionComponent<{
        id: string;
    }>;
    if (!Mask || !id) return null;

    const { type = '' } = datasource?.state ?? {};

    const { label } = Builder?.datasources?.[type] ?? {};

    return (
        <div className="feb-element-mask">
            <div className="mt-3">
                <Badge bg="light" text="dark">
                    <i18n.Node id={label} />
                </Badge>
            </div>
            <MaskProvider value={datasource}>
                <Mask id={id} />
            </MaskProvider>
        </div>
    );
}

export function BuilderDatasourceEditor() {
    const {
        state: { selectedDatasourceId = null },
        setItem,
    } = useFormEditorContext();

    if (!selectedDatasourceId) {
        return (
            <AddDatasourceMask
                onCreate={(id) => setItem('selectedDatasourceId', id)}
            />
        );
    }
    return <DatasourceMask id={selectedDatasourceId} />;
}

export function BuilderFormEditorViewDatasources() {
    const datasourceTypes = useMemo(() => Builder.datasources, []);

    const {
        state: { selectedDatasourceId },
        setItem,
    } = useFormEditorContext();
    const {
        state: { datasources = [] },
        setItem: setFormItem,
    } = useFormContext();

    const unset = useCallback(
        (id: string) => {
            const index = datasources.findIndex((ds) => ds.id === id);
            if (index < 0) return;
            const copy = [...datasources];
            copy.splice(index, 1);
            setFormItem('datasources', copy);
        },
        [datasources, setFormItem]
    );

    return (
        <BuilderExplorer
            selectedId={selectedDatasourceId}
            items={
                datasources
                    .map(({ id, name, type }) => {
                        if (!(type in datasourceTypes)) return null;
                        const { Icon } = datasourceTypes[type];
                        return { id, type, label: name, icon: <Icon /> };
                    })
                    .filter((item) => Boolean(item)) as BuilderExplorerItem[]
            }
            onAdd={() => setItem('selectedDatasourceId', null)}
            onUnset={unset}
            onSelect={(id) => setItem('selectedDatasourceId', id)}
        >
            <BuilderDatasourceEditor />
        </BuilderExplorer>
    );
}
