import { BuilderEmbedDescriptor } from '@form-engine/builder-application/types/embed';

import { BoxSeam as Icon } from 'react-bootstrap-icons';
import Mask from './mask';
export default {
    label: 'form-editor.inlineEmbed.title',
    description: 'form-editor.inlineEmbed.description',
    Mask,
    Icon,
} as BuilderEmbedDescriptor;
