import React from 'react';
import Builder from '@form-engine/builder-application';
import { Button, FormControl } from 'react-bootstrap';
import { Clipboard } from 'react-bootstrap-icons';
import { useCallback } from 'react';
import { useMemo } from 'react';
import BuilderProperty from '@form-engine/builder-mask/components/mask/property';
import { FormDefinition } from '@form-engine/core-application/types/form';
import { toast } from 'react-toastify';

function ClipboardButton({ value }: { value: string }) {
    const handleCopy = useCallback(
        (e: React.MouseEvent) => {
            e.preventDefault();
            e.stopPropagation();
            navigator.clipboard.writeText(value);
            toast(<>copied to clipboard</>);
        },
        [value]
    );

    return (
        <Button size="sm" variant="dark" onClick={handleCopy}>
            <Clipboard />
        </Button>
    );
}

export default function BuilderInlineEmbedMask({
    form,
}: {
    form: FormDefinition;
}) {
    const url = new URL(
        Builder.renderFormRoute.replace(':formId', form.slug || form.id || '')
    );

    url.searchParams.set('embed', 'true');

    const source = useMemo(
        () => `<!-- Form (put in content) -->
<script async src="${url}"></script>
<!-- /Form -->`,
        []
    );

    return (
        <div className="feb-element-mask">
            <div className={`feb-mask-group`}>
                <div className="feb-mask-group-body">
                    <BuilderProperty
                        label={
                            <>
                                Embed Code <ClipboardButton value={source} />
                            </>
                        }
                    >
                        <FormControl
                            type="textarea"
                            value={source}
                            readOnly={true}
                            style={{
                                fontSize: 12,
                                fontFamily:
                                    'ui-monospace,SFMono-Regular,SF Mono,Consolas,Liberation Mono,Menlo,monospace',
                            }}
                        />
                    </BuilderProperty>
                </div>
            </div>
        </div>
    );
}
