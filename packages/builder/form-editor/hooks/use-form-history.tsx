import { useFormContext } from '@form-engine/core-application/hooks/use-form';
import { FormDefinition } from '@form-engine/core-application/types/form';
import { useCallback, useEffect, useState } from 'react';
import { diff, applyChange, Diff } from 'deep-diff';
import { useFormEditorContext } from './use-form-editor';
import { filterIgnoredHistoryDiffs } from '@form-engine/core-util/diff-util';
export interface FormHistory {
    [id: string]: ReturnType<typeof diff<FormDefinition>>;
}

export const DEFAULT_HISTORY_SIZE = 1024;
export const DEFAULT_IGNORE_PATHS: string[][] = [
    ['thumbnail'],
    ['createdAt'],
    ['updatedAt'],
];

export function useWriteFormHistory(
    historySize = DEFAULT_HISTORY_SIZE,
    ignorePaths = DEFAULT_IGNORE_PATHS
) {
    const [currentDiff, setCurrentDiff] = useState<
        ReturnType<typeof diff<FormDefinition>> | undefined
    >();
    const editorContext = useFormEditorContext();
    const formContext = useFormContext();

    useEffect(() => {
        const {
            state,
            previousState,
            eventData: { ignoreHistory = false },
        } = formContext;

        if (ignoreHistory) {
            return;
        }

        if (previousState && state) {
            const result = filterIgnoredHistoryDiffs(
                diff(state, previousState),
                ignorePaths
            );
            if (result) {
                setCurrentDiff(result);
            }
        }
    }, [formContext?.state, formContext?.previousState, ignorePaths]);

    useEffect(() => {
        const { formHistory = [] } = editorContext?.state ?? {};
        if (
            !currentDiff ||
            formHistory.includes(currentDiff) ||
            editorContext?.eventData?.ignoreHistory
        )
            return;
        let result = [...formHistory, currentDiff];

        if (result.length > historySize) {
            result = result.slice(1);
        }
        editorContext?.setItem('formHistory', result);
        // remove redo history when branching off
        editorContext?.setItem('formRedoHistory', []);
    }, [
        currentDiff,
        editorContext?.state?.formHistory,
        editorContext?.setItem,
        historySize,
    ]);
}

export default function useFormHistory() {
    const { state: editor, setItem: setEditorItem } = useFormEditorContext();
    const { state: currentState, set: setForm } = useFormContext();

    const undo = useCallback(() => {
        const history = [
            ...(editor?.formHistory ?? []),
        ] as Diff<FormDefinition>[][];
        if (!history.length || !currentState) return;
        const undoDiffs = history.pop();
        if (!undoDiffs || !Array.isArray(undoDiffs) || !undoDiffs.length)
            return;
        const revertedState = structuredClone(currentState);

        for (const undoDiff of undoDiffs) {
            applyChange(revertedState, undefined, undoDiff);
        }

        const redoDiffs = diff(revertedState, currentState);

        setForm(revertedState, { ignoreHistory: true });
        setEditorItem('formHistory', [...history], { ignoreHistory: true });
        setEditorItem(
            'formRedoHistory',
            [...(editor?.formRedoHistory ?? []), redoDiffs],
            { ignoreHistory: true }
        );
    }, [editor, setEditorItem, setForm, currentState]);

    const redo = useCallback(() => {
        const history = [...(editor?.formRedoHistory ?? [])];
        if (!history.length || !currentState) return;
        const redoDiffs = history.pop() as any;
        if (!redoDiffs || !Array.isArray(redoDiffs) || !redoDiffs.length)
            return;
        const redoneState = structuredClone(currentState);
        for (const redoDiff of redoDiffs) {
            applyChange(redoneState, undefined, redoDiff);
        }

        const undoDiffs = diff(redoneState, currentState);

        setForm(redoneState, { ignoreHistory: true });
        setEditorItem(
            'formHistory',
            [...(editor?.formHistory ?? []), undoDiffs],
            { ignoreHistory: true }
        );
        setEditorItem('formRedoHistory', [...history], { ignoreHistory: true });
    }, [
        editor?.formHistory,
        editor?.formRedoHistory,
        setEditorItem,
        setForm,
        currentState,
    ]);

    const clear = useCallback(() => {
        setEditorItem('formHistory', [], { ignoreHistory: true });
        setEditorItem('formRedoHistory', [], { ignoreHistory: true });
    }, [setEditorItem]);

    return {
        state: editor?.formHistory,
        clear,
        undo,
        redo,
    };
}
