import { Options } from 'react-hotkeys-hook';
import { useHotkey as useAppHotkey } from '@form-engine/builder-application/hooks/use-hotkey';
import {
    FormEditorContextValue,
    useFormEditorContext,
} from './use-form-editor';
import { useCallback, useEffect, useRef } from 'react';
import { KeyHandler } from 'hotkeys-js';

export type HotKeyAction = () => void;

export type HotkeyEditorFilter = (
    editorState: FormEditorContextValue
) => boolean;

export const HotkeyEditorFilters: { [key: string]: HotkeyEditorFilter } = {
    editOnly(editorState) {
        return editorState.view === 'form' && editorState.canvasTab === 'edit';
    },
};

type UseHotkeyOptions = Options & {
    editorFilter?: HotkeyEditorFilter | keyof typeof HotkeyEditorFilters;
};

export default function useFormEditorHotkey(
    hotkeyName: string,
    callback: HotKeyAction,
    options?: UseHotkeyOptions
) {
    const { state: editorState } = useFormEditorContext();

    const innerOptions = { ...(options ?? {}) };
    delete innerOptions.editorFilter;

    const handler = useCallback<KeyHandler>(
        (e) => {
            let filter = options?.editorFilter;

            if (filter && typeof filter !== 'function') {
                filter = HotkeyEditorFilters[filter];
            }

            if (!filter || (filter as HotkeyEditorFilter)(editorState)) {
                e.stopPropagation();
                e.preventDefault();
                callback();
            }
        },
        [options, editorState, callback, hotkeyName]
    );

    const handlerRef = useRef(handler);

    useEffect(() => {
        handlerRef.current = handler;
    }, [handler]);

    return useAppHotkey(
        hotkeyName,
        (...args) => {
            handlerRef?.current?.(...args);
        },
        innerOptions
    );
}
