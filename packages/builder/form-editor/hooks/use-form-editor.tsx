import {
    StoreReference,
    useStore,
} from '@form-engine/core-application/hooks/use-store';
import { ElementDefinition } from '@form-engine/core-application/types/form';
import { useObjectDefaults } from '@form-engine/core-util/create-object';
import { getStorageNamespace } from '@form-engine/core-util/local-storage';
import { Optional } from '@form-engine/core-util/typings';
import React, { createContext, useContext } from 'react';

export type FormEditorEditMode = 'edit' | 'preview' | 'translate';

const DefaultFormEditorContextValue = {
    view: 'form',
    leftDrawerTab: 'outline',
    canvasTab: 'edit' as FormEditorEditMode,
    rightDrawerTab: 'settings',
    previewSettings: {
        locale: 'default' as Optional<string>,
        auth: 'basic' as 'basic' | 'bearer',
        basicAuthUsername: undefined as Optional<string>,
        basicAuthPassword: undefined as Optional<string>,
        bearerToken: undefined as Optional<string>,
        customVariables: [] as { key: string; value: string }[],
    },
    selectedWorkflowLayerId: undefined as Optional<string>,
    selectedDatasourceId: undefined as Optional<string>,
    isDragging: false,
    source: undefined as Optional<ElementDefinition>,
    selectedId: undefined as Optional<string>,
    outlineCollapseList: [] as string[],
};

export type FormEditorContextValue = typeof DefaultFormEditorContextValue & {
    [key: string]: any;
};
export type FormEditorPreviewSettings =
    FormEditorContextValue['previewSettings'];

export const FormEditorContext =
    createContext<StoreReference<FormEditorContextValue> | null>(null);

export const formEditorStorage = getStorageNamespace('febEditor');

export interface FormEditorProviderProps {
    value: FormEditorContextValue;
    children?:
        | React.ReactNode
        | ((store: StoreReference<FormEditorContextValue>) => React.ReactNode);
}

export function FormEditorProvider({
    value: initialState,
    children,
}: FormEditorProviderProps) {
    const store = useStore(
        useObjectDefaults(DefaultFormEditorContextValue, initialState)
    );
    if (!store) return null;
    return (
        <FormEditorContext.Provider value={store}>
            {typeof children === 'function' ? children(store) : children}
        </FormEditorContext.Provider>
    );
}

export function useFormEditorContext() {
    const context = useContext(FormEditorContext);
    if (!context) {
        throw new Error('tried to use form editor context without provider');
    }
    return context;
}
