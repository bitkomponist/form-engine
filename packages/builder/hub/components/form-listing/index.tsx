import React, { useCallback, useMemo, useState } from 'react';
import { Card, Container, Row, Col, Dropdown, Badge } from 'react-bootstrap';
import { FileArrowDown, PlusLg } from 'react-bootstrap-icons';
import { Link, To, useNavigate } from 'react-router-dom';
import fallbackImage from './fallback-image';
import { useFormList } from '@form-engine/builder-application/hooks/use-form-api';
import BuilderSpinnerBoundary from '@form-engine/builder-application/components/spinner-boundary';
import formatDistanceToNow from 'date-fns/formatDistanceToNow';
import BuilderCard from '@form-engine/builder-application/components/card';
import BuilderHubViewHeader from '../view-header';
import { BuilderAdvancedFormSelect } from '@form-engine/builder-application/components/select/advanced';
import useFormTags from '@form-engine/builder-application/hooks/use-form-tags';
import { useAppContext } from '@form-engine/builder-application/hooks/use-app';
import { useImportForm } from '../../hooks/import-form';
import useMounted from '@form-engine/core-application/hooks/use-mounted';
import { createObjectSort } from '@form-engine/core-util/sorting';
import { i18n } from '@form-engine/builder-application';

function BuilderFormListingItem({
    to: toProp,
    as: asProp = Link,
    img,
    children,
    onClick,
}: {
    to?: To;
    as?: React.ElementType;
    img?: string;
    children?: React.ReactNode;
    onClick?: (e: React.MouseEvent) => void;
}) {
    const props: any = { as: asProp };

    if (asProp === Link) {
        props.to = toProp;
    } else {
        props.onClick = onClick;
    }

    return (
        <Col md={6} lg={4} xl={3}>
            <BuilderCard {...props}>
                {img && <Card.Img variant="top" src={img} />}
                <Card.Body className="w-100">{children}</Card.Body>
            </BuilderCard>
        </Col>
    );
}

export interface BuilderFormListingSortControlProps {
    properties?: { [key: string]: { label: string; type: SortType } };
    value?: FormListingControlSettings;
    onChange: (update: Partial<FormListingControlSettings>) => void;
}

export function BuilderFormListingSortControl({
    properties = {},
    value = {},
    onChange,
}: BuilderFormListingSortControlProps) {
    return (
        <Dropdown className="feb-sort-control">
            <Dropdown.Toggle variant="link" size="sm">
                {value.sortProperty
                    ? properties[value.sortProperty]?.label
                    : ''}
            </Dropdown.Toggle>
            <Dropdown.Menu>
                <Dropdown.Header>
                    <i18n.Node id="form-listing.sort-by" />
                </Dropdown.Header>
                {Object.entries(properties).map(
                    ([sortProperty, { label, type: sortType }]) => (
                        <Dropdown.Item
                            disabled={value?.sortProperty === sortProperty}
                            key={sortProperty}
                            as="button"
                            onClick={() =>
                                onChange?.({ sortProperty, sortType })
                            }
                        >
                            {label}
                        </Dropdown.Item>
                    )
                )}
                <Dropdown.Divider />
                <Dropdown.Header>
                    <i18n.Node id="form-listing.order" />
                </Dropdown.Header>
                <Dropdown.Item
                    disabled={value?.sortOrder === 'desc'}
                    as="button"
                    onClick={() => onChange?.({ sortOrder: 'desc' })}
                >
                    {i18n.c(
                        value?.sortType === 'Date'
                            ? 'form-listing.newest-first'
                            : 'form-listing.descending'
                    )}
                </Dropdown.Item>
                <Dropdown.Item
                    disabled={value?.sortOrder === 'asc'}
                    as="button"
                    onClick={() => onChange?.({ sortOrder: 'asc' })}
                >
                    {i18n.c(
                        value?.sortType === 'Date'
                            ? 'form-listing.oldest-first'
                            : 'form-listing.ascending'
                    )}
                </Dropdown.Item>
            </Dropdown.Menu>
        </Dropdown>
    );
}

export interface BuilderFormListingTagsControlProps {
    tags?: string[];
    value?: string[];
    onChange: (update: string[]) => void;
}

export function BuilderFormListingTagsControl({
    tags = [],
    value = [],
    onChange,
}: BuilderFormListingTagsControlProps) {
    return (
        <BuilderAdvancedFormSelect
            className="text-xs ms-2"
            value={value.map((v) => ({ value: v, label: v }))}
            onChange={({ target }) => {
                onChange?.(target?.value?.map((o: any) => o.value));
            }}
            placeholder={i18n.c('form-listing.tags')}
            options={tags.map((v) => ({ value: v, label: v }))}
            isMulti={true}
        />
    );
}

export interface BuilderFormListingControlsProps {
    tags: string[];
    settings?: FormListingControlSettings;
    onChange: (update: Partial<FormListingControlSettings>) => void;
}

export function BuilderFormListingControls({
    tags,
    settings,
    onChange,
}: BuilderFormListingControlsProps) {
    return (
        <div className="feb-form-listing-controls text-xs pb-4 d-flex flex-row">
            <div className="flex-grow-1 d-flex flex-row">
                <span className="feb-formlisting-control-label">
                    <i18n.Node id="form-listing.filter" />
                </span>
                <BuilderFormListingTagsControl
                    tags={tags}
                    value={settings?.currentTags ?? []}
                    onChange={(currentTags: string[]) =>
                        onChange?.({ currentTags })
                    }
                />
            </div>
            <div className="ms-auto d-flex flex-row">
                <span className="feb-formlisting-control-label">
                    <i18n.Node id="form-listing.sort" />
                </span>
                <BuilderFormListingSortControl
                    properties={{
                        name: {
                            label: i18n.c('form-listing.filter-name'),
                            type: 'String',
                        },
                        createdAt: {
                            label: i18n.c('form-listing.filter-createdAt'),
                            type: 'Date',
                        },
                        updatedAt: {
                            label: i18n.c('form-listing.filter-updatedAt'),
                            type: 'Date',
                        },
                    }}
                    value={settings}
                    onChange={(
                        settingsUpdate: Partial<FormListingControlSettings>
                    ) => onChange?.(settingsUpdate)}
                />
            </div>
        </div>
    );
}

export interface BuilderFormListingProps
    extends React.HTMLAttributes<HTMLElement> {
    as?: React.ElementType;
}

type SortType = 'Date' | 'String';

interface FormListingControlSettings {
    currentTags?: string[];
    sortProperty?: string;
    sortOrder?: 'asc' | 'desc';
    sortType?: SortType;
}

function useFormListingControl() {
    const { state = [], loading } = useFormList();
    const tags = useFormTags(state);
    const { state: appState, setItem: setAppStateItem } = useAppContext();
    const controlSettings = (appState?.formListingControlSettings ?? {
        sortProperty: 'updatedAt',
        sortType: 'Date',
        sortOrder: 'asc',
        currentTags: [],
    }) as FormListingControlSettings;

    const setControlSettings = useCallback(
        (update: Partial<FormListingControlSettings>) => {
            setAppStateItem('formListingControlSettings', {
                ...(controlSettings ?? {}),
                ...update,
            });
        },
        [controlSettings, setAppStateItem]
    );

    const forms = useMemo(() => {
        const {
            currentTags = [],
            sortProperty = 'updatedAt',
            sortType = 'Date',
            sortOrder = 'asc',
        } = controlSettings ?? {};
        let filteredForms = [...state];

        if (currentTags && currentTags.length) {
            filteredForms = filteredForms.filter((form) => {
                for (const tag of form?.tags ?? []) {
                    if (currentTags?.includes(tag)) {
                        return true;
                    }
                }
                return false;
            });
        }

        if (sortProperty) {
            filteredForms.sort(
                createObjectSort(sortType, sortProperty, sortOrder)
            );
        }

        return filteredForms;
    }, [controlSettings, state]);

    return {
        tags,
        forms,
        loading,
        controlSettings,
        setControlSettings,
    };
}

export function BuilderFormListing({
    as: Component = 'div',
    ...props
}: BuilderFormListingProps) {
    const { forms, tags, controlSettings, setControlSettings, loading } =
        useFormListingControl();
    const importForm = useImportForm();
    const mounted = useMounted();
    const navigate = useNavigate();

    const handleImport = useCallback(
        (e: React.MouseEvent) => {
            e.preventDefault();
            e.stopPropagation();
            importForm().then((formDefinition) => {
                if (!formDefinition?.id || !mounted) return;
                navigate(`/forms/${formDefinition.id}`);
            });
        },
        [importForm, navigate]
    );

    return (
        <Component {...props}>
            <div className="feb-view">
                <div className="feb-view-inner p-0">
                    <Container fluid className="pb-4">
                        <BuilderHubViewHeader
                            title={i18n.c('form-listing.title')}
                        >
                            <Row>
                                <BuilderFormListingItem to="/new">
                                    <Row className="d-flex">
                                        <Col>
                                            <Card.Title className="text-xs">
                                                <i18n.Node id="form-listing.new-form" />
                                            </Card.Title>
                                            <Card.Text className="text-xs text-black-50">
                                                <i18n.Node id="form-listing.new-form-subtitle" />
                                            </Card.Text>
                                        </Col>
                                        <Col
                                            xs={2}
                                            className="align-self-center"
                                        >
                                            <PlusLg />
                                        </Col>
                                    </Row>
                                </BuilderFormListingItem>
                                <BuilderFormListingItem
                                    as="button"
                                    onClick={handleImport}
                                >
                                    <Row className="d-flex">
                                        <Col>
                                            <Card.Title className="text-xs">
                                                <i18n.Node id="form-listing.import-form" />
                                            </Card.Title>
                                            <Card.Text className="text-xs text-black-50">
                                                <i18n.Node id="form-listing.import-form-subtitle" />
                                            </Card.Text>
                                        </Col>
                                        <Col
                                            xs={2}
                                            className="align-self-center"
                                        >
                                            <FileArrowDown />
                                        </Col>
                                    </Row>
                                </BuilderFormListingItem>
                            </Row>
                        </BuilderHubViewHeader>
                        <BuilderSpinnerBoundary show={loading} bar={false}>
                            <BuilderFormListingControls
                                tags={tags}
                                settings={controlSettings}
                                onChange={(update) =>
                                    setControlSettings(update)
                                }
                            />
                            <Row style={{ ['--bs-gutter-y' as any]: '1.5rem' }}>
                                {forms.map(
                                    ({
                                        id,
                                        name,
                                        updatedAt,
                                        tags,
                                        thumbnail,
                                    }) => {
                                        return (
                                            <BuilderFormListingItem
                                                key={id}
                                                img={thumbnail ?? fallbackImage}
                                                to={`/forms/${id}`}
                                            >
                                                <Card.Title className="text-xs">
                                                    {name}
                                                </Card.Title>
                                                <Card.Text className="text-xs text-black-50">
                                                    <i18n.Node
                                                        id="form-listing.edited"
                                                        date={formatDistanceToNow(
                                                            new Date(
                                                                updatedAt ?? ''
                                                            )
                                                        )}
                                                    />{' '}
                                                    <br />
                                                    {(tags ?? []).map((tag) => (
                                                        <Badge
                                                            pill
                                                            bg="secondary"
                                                            className="me-1 mt-1"
                                                            key={tag}
                                                        >
                                                            {tag}
                                                        </Badge>
                                                    ))}
                                                </Card.Text>
                                            </BuilderFormListingItem>
                                        );
                                    }
                                )}
                            </Row>
                        </BuilderSpinnerBoundary>
                    </Container>
                </div>
            </div>
        </Component>
    );
}
