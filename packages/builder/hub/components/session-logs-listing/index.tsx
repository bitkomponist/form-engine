import React, { useState } from 'react';
import { Card, Container, Row, Col, Nav, Table, Button } from 'react-bootstrap';
import BuilderSpinnerBoundary from '@form-engine/builder-application/components/spinner-boundary';
import formatDistanceToNow from 'date-fns/formatDistanceToNow';
import BuilderCard from '@form-engine/builder-application/components/card';
import useApiModel, {
    ApiListOptions,
    useEntity,
    useProgressiveList,
} from '@form-engine/builder-application/hooks/use-api-model';
import { Link, Route, Routes, To, useParams } from 'react-router-dom';
import BuilderAppCol, {
    BuilderAppColProps,
} from '@form-engine/builder-application/components/app-col';
import Drawer from '@form-engine/builder-application/components/drawer';
import { useMemo } from 'react';
import { ArrowDownCircle, XLg } from 'react-bootstrap-icons';
import BuilderDrawerTabs from '@form-engine/builder-application/components/drawer-tabs';
import {
    useFormEntity,
    useFormList,
} from '@form-engine/builder-application/hooks/use-form-api';
import {
    FormProvider,
    useFormCollection,
    useFormElements,
} from '@form-engine/core-application/hooks/use-form';
import {
    FormSessionProvider,
    useFormSessionValues,
} from '@form-engine/core-application/hooks/use-formsession';
import FormEngine from '@form-engine/core-application';
import BuilderFormSelect from '@form-engine/builder-application/components/select';
import {
    FormDefinition,
    LogDefinition,
} from '@form-engine/core-application/types/form';
import BuilderHubViewHeader from '../view-header';
import { i18n } from '@form-engine/builder-application';

function BuilderSessionLogsListingItem({
    to: toProp,
    item,
    active,
}: {
    to: To;
    item: LogDefinition;
    active: boolean;
}) {
    return (
        <Col xs={12}>
            <BuilderCard
                as={Link}
                to={toProp}
                className={active ? 'active' : ''}
            >
                <Card.Body>
                    {item.form && (
                        <Card.Title className="text-xs">
                            {item.form.name}{' '}
                            <span className="text-muted">
                                #{item.id?.slice(0, 8)}
                            </span>
                        </Card.Title>
                    )}
                    <Card.Text className="text-xs text-black-50">
                        <i18n.Node
                            id="session-logs.item.submitted"
                            date={formatDistanceToNow(
                                new Date(item.updatedAt ?? '')
                            )}
                        />
                    </Card.Text>
                </Card.Body>
            </BuilderCard>
        </Col>
    );
}

function BuilderSessionLogDetailView({
    as: Component = BuilderAppCol,
    closePath,
    ...props
}: BuilderAppColProps & { closePath: To }) {
    const { id } = useParams();
    const { state: log = {}, loading } = useEntity(
        useApiModel<LogDefinition>('session-log'),
        id
    );
    const { state: form = {}, loading: loadingForm } = useFormEntity(
        log?.formId
    );

    return (
        <Component md={4} border="left" {...props}>
            <BuilderDrawerTabs
                items={{ session: i18n.c('session-logs.item.session-info') }}
                activeKey={'session'}
            >
                {closePath && (
                    <Nav.Item className="ms-auto">
                        <Nav.Link as={Link} to={closePath}>
                            <XLg />
                        </Nav.Link>
                    </Nav.Item>
                )}
            </BuilderDrawerTabs>

            <div className="feb-view m-0">
                <div className="feb-view-inner p-0">
                    <Drawer>
                        <BuilderSpinnerBoundary
                            show={loading || loadingForm}
                            bar={false}
                        >
                            <BuilderSessionLogDetailInfo
                                log={log}
                                form={form}
                            />
                        </BuilderSpinnerBoundary>
                    </Drawer>
                </div>
            </div>
        </Component>
    );
}

function BuilderFormSessionDataList({
    children,
}: {
    children: React.ReactNode;
}) {
    const { state: elements = [] } = useFormElements();
    const inputElements = useMemo(() => {
        const fields = FormEngine.getFieldElements();
        return elements.filter((element) => element.type in fields);
    }, [elements]);
    const values = useFormSessionValues();

    return (
        <Table striped className="mb-0 text-xs">
            <tbody>
                {children}
                {inputElements.map((element) => {
                    return (
                        <tr key={element.id}>
                            <th>{element.label || element.name}</th>
                            <td>
                                {element?.name &&
                                    (typeof values[element.name] === 'string'
                                        ? values[element.name]
                                        : JSON.stringify(
                                              values[element.name],
                                              null,
                                              2
                                          ))}
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </Table>
    );
}

function BuilderSessionLogDetailInfo({
    log = {},
    form = {},
}: {
    log?: LogDefinition | null;
    form?: FormDefinition | null;
}) {
    const info = useMemo(() => {
        try {
            return JSON.parse(log?.value ?? 'null');
        } catch (e) {
            return null;
        }
    }, [log?.value]);

    if (!form) return null;

    return (
        <>
            <FormProvider value={form}>
                <FormSessionProvider value={info?.session || {}}>
                    <BuilderFormSessionDataList>
                        <tr>
                            <th>Form</th>
                            <td>{form?.name}</td>
                        </tr>
                        <tr>
                            <th>Workflow ID</th>
                            <td>{info?.definition?.name}</td>
                        </tr>
                    </BuilderFormSessionDataList>
                </FormSessionProvider>
            </FormProvider>
        </>
    );
}

interface FilterOption {
    value: string;
    label: string;
}

function useLogList() {
    const [formFilter, setFormFilter] = useState<FilterOption[]>([]);
    const { state: forms = [], loading: formsLoading } = useFormList();

    const listParams = useMemo(() => {
        const params: ApiListOptions = {
            attributes: ['id', 'createdAt', 'updatedAt', 'formId'],
        };

        if (!formFilter.length) return params;

        params.filter = {
            formId: {
                in: formFilter.map((option) => option.value),
            },
        };

        return params;
    }, [formFilter]);

    const {
        state: logs = [],
        next,
        complete,
        loading: logsLoading,
    } = useProgressiveList(
        useApiModel<LogDefinition>('session-log'),
        listParams
    );

    const state = useMemo(() => {
        return logs
            .map((log) => ({
                ...log,
                form: forms.find((form) => form.id === log.formId),
            }))
            .filter((log) => Boolean(log.form))
            .sort(
                (a, b) =>
                    new Date(b.createdAt ?? '').getTime() -
                    new Date(a.createdAt ?? '').getTime()
            );
    }, [forms, logs]);

    return {
        loading: formsLoading || logsLoading,
        formFilter,
        setFormFilter,
        forms,
        logs: state,
        next,
        complete,
    };
}

/** @todo move to scss */
const headerStyle: React.CSSProperties = {
    position: 'sticky',
    top: '0',
    zIndex: 1,
    backgroundImage:
        'linear-gradient(0deg, rgba(var(--bs-light-rgb),0) 0%, var(--bs-gray-200) 18%, var(--bs-gray-200) 100%)',
    paddingLeft: '1em',
    paddingRight: '1em',
    marginLeft: '-1em',
    marginRight: '-1em',
};

export interface BuilderSessionLogsListingProps
    extends React.HTMLAttributes<HTMLElement> {
    as?: React.ElementType;
    path?: To;
    showDetail?: boolean;
    detailProps?: Omit<
        React.ComponentProps<typeof BuilderSessionLogDetailView>,
        'closePath'
    >;
}

export default function BuilderSessionLogsListing({
    as: Component = 'div',
    path = '/hub/session-logs',
    showDetail = true,
    detailProps = {},
    ...props
}: BuilderSessionLogsListingProps) {
    const {
        logs = [],
        forms = [],
        loading,
        complete,
        next,
        formFilter,
        setFormFilter,
    } = useLogList();
    const { '*': activeLogId } = useParams();

    return (
        <>
            <Component {...props}>
                <div className="feb-view">
                    <div className="feb-view-inner p-0">
                        <Container fluid>
                            <BuilderHubViewHeader
                                title={i18n.c('session-logs.title')}
                            >
                                <BuilderFormSelect.Advanced
                                    className="text-xs"
                                    style={{ position: 'sticky' }}
                                    value={formFilter}
                                    onChange={({ target }) =>
                                        setFormFilter(target.value)
                                    }
                                    placeholder={i18n.c('session-logs.filter')}
                                    options={forms.map((form) => ({
                                        value: form.id,
                                        label: form.name,
                                    }))}
                                    isMulti={true}
                                />
                            </BuilderHubViewHeader>
                            <BuilderSpinnerBoundary show={loading} bar={false}>
                                {logs.length > 0 ? (
                                    <Row
                                        style={{
                                            ['--bs-gutter-y' as any]: '.75rem',
                                        }}
                                    >
                                        {logs.map((item) => {
                                            return (
                                                <BuilderSessionLogsListingItem
                                                    key={item.id}
                                                    item={item}
                                                    active={
                                                        activeLogId === item.id
                                                    }
                                                    to={`${path}/${item.id}`}
                                                />
                                            );
                                        })}
                                    </Row>
                                ) : (
                                    <p className="text-center text-muted">
                                        <i18n.Node id="session-logs.no-logs" />
                                    </p>
                                )}
                                {!complete && (
                                    <div className="text-center my-3">
                                        <Button
                                            size="sm"
                                            onClick={next}
                                            variant="outline-dark"
                                        >
                                            <i18n.Node id="session-logs.more" />{' '}
                                            <ArrowDownCircle />
                                        </Button>
                                    </div>
                                )}
                            </BuilderSpinnerBoundary>
                        </Container>
                    </div>
                </div>
            </Component>
            {showDetail && (
                <Routes>
                    <Route
                        path=":id"
                        element={
                            <BuilderSessionLogDetailView
                                id="sessionLogDetailCol"
                                closePath={path}
                                {...detailProps}
                            />
                        }
                    />
                </Routes>
            )}
        </>
    );
}
