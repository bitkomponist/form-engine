import useIdentity from '@form-engine/builder-application/hooks/use-identity';
import React from 'react';
import { useCallback } from 'react';
import {
    Container,
    Row,
    Col,
    NavDropdown,
    FormControl,
    Form,
    InputGroup,
} from 'react-bootstrap';
import { Search } from 'react-bootstrap-icons';
import { Routes, useNavigate } from 'react-router-dom';
import BuilderAppViewNavbar from '@form-engine/builder-application/components/app-view-navbar';
import Drawer from '@form-engine/builder-application/components/drawer';
import BuilderDrawerNav from '@form-engine/builder-application/components/drawer-nav';
import BuilderAppCol from '@form-engine/builder-application/components/app-col';
import Builder, { i18n } from '@form-engine/builder-application';
import useAppSettingsAction from '../app-settings/action';

export interface HubNavItemsDefinition {
    [key: string]: React.ReactElement;
}

export interface HubRoutesDefinition {
    [key: string]: React.ReactElement;
}

export default function BuilderHubView() {
    const navigate = useNavigate();
    const { logout, identity = {} } = useIdentity();
    const handleLogout = useCallback(
        (e: React.MouseEvent) => {
            e.preventDefault();
            logout();
            navigate('/');
        },
        [navigate, logout]
    );

    const showSettings = useAppSettingsAction();

    return (
        <>
            <BuilderAppViewNavbar>
                <div className="w-100">
                    <Row>
                        <Col className="d-flex" sm={6}>
                            {/* <Form className="feb-app-view-navbar-search">
                                <InputGroup>
                                    <InputGroup.Text>
                                        <Search />
                                    </InputGroup.Text>
                                    <FormControl
                                        type="search"
                                        placeholder="Search"
                                        aria-label="Search"
                                        disabled
                                    />
                                </InputGroup>
                            </Form> */}
                        </Col>
                        <Col className="d-flex" sm={6}>
                            <NavDropdown
                                menuVariant="dark"
                                className="ms-auto navdropdown-end"
                                title={identity?.name || identity?.email}
                            >
                                <NavDropdown.Item
                                    as="button"
                                    onClick={handleLogout}
                                >
                                    <i18n.Node id="hub.navbar-logout" />
                                </NavDropdown.Item>
                                <NavDropdown.Item
                                    as="button"
                                    onClick={showSettings}
                                >
                                    <i18n.Node id="hub.navbar-settings" />
                                </NavDropdown.Item>
                            </NavDropdown>
                        </Col>
                    </Row>
                </div>
            </BuilderAppViewNavbar>
            <Container fluid className="feb-form-editor-layout">
                <Row>
                    {Builder.hubNavItems && (
                        <BuilderAppCol md={2} border="right" id="hubLeftCol">
                            <Drawer>
                                <BuilderDrawerNav>
                                    {Builder.hubNavItems &&
                                        Object.entries(
                                            Builder.hubNavItems as HubNavItemsDefinition
                                        ).map(([key, navItem]) => (
                                            <navItem.type
                                                key={key}
                                                {...navItem.props}
                                            />
                                        ))}
                                </BuilderDrawerNav>
                            </Drawer>
                        </BuilderAppCol>
                    )}
                    {Builder.hubRoutes && (
                        <Routes>
                            {Builder.hubRoutes &&
                                Object.entries(
                                    Builder.hubRoutes as HubRoutesDefinition
                                ).map(([key, route]) => (
                                    <route.type key={key} {...route.props} />
                                ))}
                        </Routes>
                    )}
                </Row>
            </Container>
        </>
    );
}
