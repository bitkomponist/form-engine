import React from 'react';
import { useHotkey } from '@form-engine/builder-application/hooks/use-hotkey';
import { setHotkey } from '@form-engine/builder-application/keybindings';
import { useCallback } from 'react';
import { i18n } from '@form-engine/builder-application';
import Modal, {
    BuilderModalProps,
} from '@form-engine/builder-application/components/modal';
import dialog from '@form-engine/builder-application/components/dialog';
import {
    AppState,
    useAppContext,
} from '@form-engine/builder-application/hooks/use-app';
import {
    MaskProvider,
    getMaskProviderValueFromStore,
} from '@form-engine/builder-mask/hooks/use-mask';
import BuilderEnumProperty from '@form-engine/builder-mask/components/properties/enum';

export function AppSettingsDialog({
    title = <i18n.Node id="app-settings.title" />,
    ...props
}: BuilderModalProps) {
    const maskValue = getMaskProviderValueFromStore<AppState>(useAppContext());

    return (
        <Modal {...props} title={title}>
            <MaskProvider value={maskValue}>
                <BuilderEnumProperty
                    propertyName="locale"
                    options={[
                        ['default', 'English'],
                        ['de', 'German'],
                    ]}
                    required
                />
            </MaskProvider>
        </Modal>
    );
}

export default function useAppSettingsAction() {
    return useCallback(() => {
        dialog({
            as: AppSettingsDialog,
        });
    }, []);
}

export const ACTION_NAME = 'app.settings.show';

setHotkey(ACTION_NAME, 'ctrl+shift+p,cmd+shift+p');

export const useAction = useAppSettingsAction;

export function useActionHotkey() {
    useHotkey(ACTION_NAME, useAction());
}
