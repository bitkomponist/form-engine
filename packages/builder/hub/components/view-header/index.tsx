import React from 'react';

export interface BuilderHubViewHeaderProps
    extends Omit<React.HTMLAttributes<HTMLDivElement>, 'title'> {
    className?: string;
    title?: React.ReactNode;
}

export default function BuilderHubViewHeader({
    children,
    title,
    ...props
}: BuilderHubViewHeaderProps) {
    return (
        <div {...props} className={`${props.className ?? ''} feb-view-header`}>
            {title && <h6 className="m-0 pb-3 pt-3">{title}</h6>}
            {children && <hr className="mb-3 text-muted" />}
            {children && <div className="pb-5">{children}</div>}
        </div>
    );
}
