import { BuilderPlugin } from '@form-engine/builder-application';
import { BuilderDrawerNavLink } from '@form-engine/builder-application/components/drawer-nav';
import React from 'react';
import { Col } from 'react-bootstrap';
import { Briefcase, Stack } from 'react-bootstrap-icons';
import { Route } from 'react-router-dom';
import { BuilderFormListing } from './components/form-listing';
import HubView from './components/hub-view';
import BuilderSessionLogsListing from './components/session-logs-listing';
import * as translations from './builder-translations';

export default {
    defaultView: <HubView />,
    translations,
    routes: {
        hub: <Route path="hub/*" element={<HubView />} />,
    },
    hubNavItems: {
        forms: (
            <BuilderDrawerNavLink
                to="/"
                i18nLabel="hub.navItems.forms"
                icon={<Briefcase />}
            />
        ),
        logs: (
            <BuilderDrawerNavLink
                to="/hub/session-logs"
                i18nLabel="hub.navItems.logs"
                icon={<Stack />}
            />
        ),
    },
    hubRoutes: {
        forms: (
            <Route
                path=""
                element={
                    <BuilderFormListing as={Col} className="feb-main-col" />
                }
            />
        ),
        logs: (
            <Route
                path="session-logs/*"
                element={
                    <BuilderSessionLogsListing
                        as={Col}
                        className="feb-main-col"
                    />
                }
            />
        ),
    },
} as BuilderPlugin;
