import uuidv4 from '@form-engine/core-util/uuid';
import { FormDefinition } from '@form-engine/core-application/types/form';
import useFormApi from '@form-engine/builder-application/hooks/use-form-api';
import useMounted from '@form-engine/core-application/hooks/use-mounted';
import { useCallback } from 'react';

export async function importFormFromFile() {
    const [fileHandle] = await (window as any).showOpenFilePicker({
        types: [
            {
                description: 'Form Projects',
                accept: {
                    'application/x-form-engine-project': ['.xfe'],
                },
            },
        ],
    });
    const file = await fileHandle?.getFile();
    const readPromise = new Promise((res, rej) => {
        const reader = new FileReader();
        reader.onerror = rej;
        reader.onabort = rej;
        reader.onload = () => {
            try {
                res(JSON.parse(reader.result as string));
            } catch (e) {
                rej(e);
            }
        };
        reader.readAsText(file);
    });
    const form: FormDefinition = (await readPromise) as FormDefinition;

    /** @todo validate form */

    form.id = uuidv4();
    form.name = `${form.name} (Imported)`;

    return form;
}

export function useImportForm() {
    const formApi = useFormApi();
    const mounted = useMounted();

    const callback = useCallback(async () => {
        let formDefinition;
        try {
            formDefinition = await importFormFromFile();
        } catch (e) {
            console.warn(e);
        }

        if (!mounted || !formDefinition) return;

        return await formApi.create(formDefinition);
    }, [formApi]);

    return callback;
}
