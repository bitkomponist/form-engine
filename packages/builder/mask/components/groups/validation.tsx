import React from 'react';
import BuilderMaskGroup, { BuilderMaskGroupProps } from '../mask/group';
import BuilderBooleanProperty from '../properties/boolean';
import BuilderStringProperty from '../properties/string';
import BuilderValidationContentTypeProperty from '../properties/validation-content-type';
import { i18n } from '@form-engine/builder-application';

export interface BuilderMaskGroupValidationProps extends BuilderMaskGroupProps {
    required?: boolean;
    contentType?: boolean;
    length?: boolean;
    children?: React.ReactNode;
}

export default function BuilderMaskGroupValidation({
    required,
    contentType,
    length,
    children,
    ...props
}: BuilderMaskGroupValidationProps) {
    return (
        <BuilderMaskGroup
            id="BuilderMaskGroupValidation"
            label={i18n.c('mask.validationGroup.title')}
            {...props}
        >
            {required !== false && (
                <BuilderBooleanProperty
                    propertyName="required"
                    label={i18n.c('mask.validationGroup.required')}
                />
            )}
            {contentType !== false && <BuilderValidationContentTypeProperty />}
            {length !== false && (
                <>
                    <BuilderStringProperty
                        propertyName="validationMinLength"
                        step="1"
                        type="number"
                        label={i18n.c(
                            'mask.validationGroup.validationMinLength'
                        )}
                    />
                    <BuilderStringProperty
                        propertyName="validationMaxLength"
                        step="1"
                        type="number"
                        label={i18n.c(
                            'mask.validationGroup.validationMaxLength'
                        )}
                    />
                </>
            )}
            {children}
        </BuilderMaskGroup>
    );
}
