import React from 'react';
import BuilderMaskGroup from '../mask/group';
import BuilderBooleanProperty from '../properties/boolean';
import BuilderNameProperty from '../properties/name';
import { i18n } from '@form-engine/builder-application';

export default function BuilderMaskGroupGeneral() {
    return (
        <BuilderMaskGroup id="BuilderMaskGroupGeneral">
            <BuilderNameProperty />
            <BuilderBooleanProperty
                propertyName="hidden"
                label={i18n.c('mask.generalGroup.hidden')}
            />
        </BuilderMaskGroup>
    );
}
