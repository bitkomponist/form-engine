import React from 'react';
import BuilderMaskGroup from '../mask/group';
import BuilderBooleanProperty from '../properties/boolean';
import BuilderStringProperty from '../properties/string';
import BuilderNameProperty from '../properties/name';
import { i18n } from '@form-engine/builder-application';

export default function BuilderMaskGroupWflBehaviour({
    ...props
}: {
    /** @deprecated */
    events?: boolean;
}) {
    return (
        <>
            <BuilderMaskGroup label={i18n.c('mask.wflBehaviourGroup.title')}>
                <BuilderNameProperty />
                <BuilderStringProperty
                    propertyName="description"
                    label={i18n.c('mask.wflBehaviourGroup.description')}
                />
                <BuilderBooleanProperty
                    propertyName="enabled"
                    label={i18n.c('mask.wflBehaviourGroup.enabled')}
                />
            </BuilderMaskGroup>
        </>
    );
}
