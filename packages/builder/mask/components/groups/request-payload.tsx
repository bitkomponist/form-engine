import { CompileRequestContentTypes } from '@form-engine/core-util/request';
import React from 'react';
import { useMaskContext } from '../../hooks/use-mask';
import BuilderMaskGroup, { BuilderMaskGroupProps } from '../mask/group';
import BuilderCodeProperty, {
    BuilderCodePropertyProps,
} from '../properties/code';
import BuilderEnumProperty from '../properties/enum';
import BuilderObjectProperty, {
    BuilderObjectPropertyProps,
} from '../properties/object';
import { i18n } from '@form-engine/builder-application';

export const BuilderMaskGroupRequestPayloadTypes: {
    [key in CompileRequestContentTypes]: {
        label: string;
        component: React.ElementType;
    };
} = {
    'text/plain': {
        label: 'mask.requestPayloadGroup.textPlain',
        component: (props: BuilderCodePropertyProps) => (
            <BuilderCodeProperty {...props} language="text" />
        ),
    },
    'text/csv': {
        label: 'mask.requestPayloadGroup.textCsv',
        component: (props: BuilderCodePropertyProps) => (
            <BuilderCodeProperty {...props} language="csv" />
        ),
    },
    'application/json': {
        label: 'mask.requestPayloadGroup.applicationJson',
        component: (props: BuilderObjectPropertyProps) => (
            <BuilderObjectProperty {...props} />
        ),
    },
    'application/x-www-form-urlencoded': {
        label: 'mask.requestPayloadGroup.applicationXWwwFormUrlencoded',
        component: (props: BuilderCodePropertyProps) => (
            <BuilderCodeProperty {...props} language="json" />
        ),
    },
};

export interface BuilderMaskGroupRequestPayloadProps
    extends BuilderMaskGroupProps {
    bodyPropertyName?: string;
    contentTypePropertyName?: string;
    types?: typeof BuilderMaskGroupRequestPayloadTypes;
    forceContentType?: CompileRequestContentTypes;
}

export default function BuilderMaskGroupRequestPayload({
    bodyPropertyName = 'body',
    contentTypePropertyName = 'contentType',
    types = BuilderMaskGroupRequestPayloadTypes,
    forceContentType,
    children,
    ...props
}: BuilderMaskGroupRequestPayloadProps) {
    const { state } = useMaskContext();

    const targetContentType = forceContentType
        ? forceContentType
        : state?.[contentTypePropertyName];

    const Component: React.ElementType | undefined =
        targetContentType &&
        targetContentType in types &&
        types[targetContentType as keyof typeof types]?.component;

    return (
        <BuilderMaskGroup
            id="BuilderMaskGroupRequestPayload"
            label={i18n.c('mask.requestPayloadGroup.title')}
            {...props}
        >
            {!forceContentType && (
                <BuilderEnumProperty
                    propertyName={contentTypePropertyName}
                    label={i18n.c('mask.requestPayloadGroup.contentType')}
                    options={Object.entries(types).map(([key, def]) => [
                        key,
                        i18n.c(def.label),
                    ])}
                />
            )}
            {Component && (
                <Component
                    label={i18n.c('mask.requestPayloadGroup.body')}
                    propertyName={bodyPropertyName}
                />
            )}
            {children}
        </BuilderMaskGroup>
    );
}
