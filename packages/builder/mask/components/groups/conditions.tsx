import { useMaskContext } from '../../hooks/use-mask';
import { CONDITION_TYPES } from '@form-engine/core-application/components/element';
import FormEngine from '@form-engine/core-application';
import React from 'react';
import BuilderMaskGroup from '../mask/group';
import { ElementDefinition } from '@form-engine/core-application/types/form';
import BuilderExpressionsProperty from '../properties/expressions';
import { i18n } from '@form-engine/builder-application';

export const CONDITION_TYPE_OPTIONS = Object.values(CONDITION_TYPES).map(
    (value) => [value, value]
);

export default function BuilderMaskGroupConditions() {
    const { state } = useMaskContext<ElementDefinition>();
    const isField = state?.type && state?.type in FormEngine.getFieldElements();

    return (
        <BuilderMaskGroup
            id="BuilderMaskGroupConditions"
            label={i18n.c('mask.conditionsGroup.title')}
        >
            {isField && !state?.required && (
                <BuilderExpressionsProperty
                    label={i18n.c('mask.conditionsGroup.requiredExpressions')}
                    propertyName="requiredExpressions"
                />
            )}
            {!state?.hidden && (
                <BuilderExpressionsProperty
                    label={i18n.c('mask.conditionsGroup.hiddenExpressions')}
                    propertyName="hiddenExpressions"
                />
            )}
        </BuilderMaskGroup>
    );
}
