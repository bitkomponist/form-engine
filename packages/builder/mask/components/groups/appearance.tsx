import React from 'react';
import BuilderMaskGroup from '../mask/group';
import BuilderBoundsProperty from '../properties/bounds';
import { i18n } from '@form-engine/builder-application';

export default function BuilderMaskGroupBounds() {
    return (
        <BuilderMaskGroup
            id="BuilderMaskGroupBounds"
            label={i18n.c('mask.boundsGroup.title')}
        >
            <BuilderBoundsProperty />
        </BuilderMaskGroup>
    );
}
