import BuilderVariableSelect, {
    BuilderVariableSelectProps,
} from '@form-engine/builder-application/components/variable-select';
import React from 'react';
import { useMaskProperty } from '../../../hooks/use-mask';

export interface BuilderInsertVariableWizardProps
    extends Omit<BuilderVariableSelectProps, 'onChange' | 'value'> {
    propertyName: string;
}

export default function BuilderInsertVariableWizard({
    propertyName,
    ...props
}: BuilderInsertVariableWizardProps) {
    const [value, setValue] = useMaskProperty<string>(propertyName);

    return (
        <BuilderVariableSelect {...props} value={value} onChange={setValue} />
    );
}
