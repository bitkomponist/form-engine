import { useMaskContext } from '../../hooks/use-mask';
import React from 'react';
import { Col, FormControl, InputGroup, Row } from 'react-bootstrap';
import {
    ArrowBarDown,
    ArrowBarLeft,
    ArrowBarRight,
    ArrowBarUp,
    ArrowLeftRight,
} from 'react-bootstrap-icons';
import { useMaskPropertyPermission } from '../../hooks/use-mask-config';
import { i18n } from '@form-engine/builder-application';

export default function BuilderBoundsProperty() {
    const { state, setItemProperty } = useMaskContext();
    const xlColSize = { span: 12 };
    const smColSize = { span: 12 };

    const widthPermission = useMaskPropertyPermission('width');
    const marginTopPermission = useMaskPropertyPermission('marginTop');
    const marginRightPermission = useMaskPropertyPermission('marginRight');
    const marginBottomPermission = useMaskPropertyPermission('marginBottom');
    const marginLeftPermission = useMaskPropertyPermission('marginLeft');

    return (
        <Row>
            <Col xl={xlColSize} sm={smColSize} className="mb-2">
                <InputGroup>
                    <FormControl
                        size="sm"
                        type="number"
                        step="1"
                        min="1"
                        max="12"
                        value={state.width || '12'}
                        disabled={widthPermission === 'read'}
                        onChange={(e: React.ChangeEvent) =>
                            setItemProperty(
                                'width',
                                (e.target as HTMLInputElement).value
                            )
                        }
                    />
                    <InputGroup.Text
                        title={i18n.c('mask.boundsProperty.width')}
                    >
                        <ArrowLeftRight />
                    </InputGroup.Text>
                </InputGroup>
            </Col>
            <Col xl={xlColSize} sm={smColSize}>
                <Row
                    style={{
                        ['--bs-gutter-x' as any]: '.5rem',
                        ['--bs-gutter-y' as any]: '.5rem',
                    }}
                >
                    <Col md={6}>
                        <InputGroup>
                            <FormControl
                                size="sm"
                                type="number"
                                step="1"
                                min="0"
                                max="11"
                                value={state.marginTop || '0'}
                                disabled={marginTopPermission === 'read'}
                                onChange={(e: React.ChangeEvent) =>
                                    setItemProperty(
                                        'marginTop',
                                        (e.target as HTMLInputElement).value
                                    )
                                }
                            />
                            <InputGroup.Text
                                title={i18n.c('mask.boundsProperty.marginTop')}
                            >
                                <ArrowBarUp />
                            </InputGroup.Text>
                        </InputGroup>
                    </Col>
                    <Col md={6}>
                        <InputGroup>
                            <FormControl
                                size="sm"
                                type="number"
                                step="1"
                                min="0"
                                max="11"
                                value={state.marginRight || '0'}
                                disabled={marginRightPermission === 'read'}
                                onChange={(e: React.ChangeEvent) =>
                                    setItemProperty(
                                        'marginRight',
                                        (e.target as HTMLInputElement).value
                                    )
                                }
                            />
                            <InputGroup.Text
                                title={i18n.c(
                                    'mask.boundsProperty.marginRight'
                                )}
                            >
                                <ArrowBarRight />
                            </InputGroup.Text>
                        </InputGroup>
                    </Col>
                    <Col md={6}>
                        <InputGroup>
                            <FormControl
                                size="sm"
                                type="number"
                                step="1"
                                min="0"
                                max="11"
                                value={state.marginBottom || '0'}
                                disabled={marginBottomPermission === 'read'}
                                onChange={(e: React.ChangeEvent) =>
                                    setItemProperty(
                                        'marginBottom',
                                        (e.target as HTMLInputElement).value
                                    )
                                }
                            />
                            <InputGroup.Text
                                title={i18n.c(
                                    'mask.boundsProperty.marginBottom'
                                )}
                            >
                                <ArrowBarDown />
                            </InputGroup.Text>
                        </InputGroup>
                    </Col>
                    <Col md={6}>
                        <InputGroup>
                            <FormControl
                                size="sm"
                                type="number"
                                step="1"
                                min="0"
                                max="11"
                                value={state.marginLeft || '0'}
                                disabled={marginLeftPermission === 'read'}
                                onChange={(e: React.ChangeEvent) =>
                                    setItemProperty(
                                        'marginLeft',
                                        (e.target as HTMLInputElement).value
                                    )
                                }
                            />
                            <InputGroup.Text
                                title={i18n.c('mask.boundsProperty.marginLeft')}
                            >
                                <ArrowBarLeft />
                            </InputGroup.Text>
                        </InputGroup>
                    </Col>
                </Row>
            </Col>
        </Row>
    );
}
