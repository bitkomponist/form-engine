import React from 'react';
import BuilderCodeProperty, { BuilderCodePropertyProps } from './code';

export default function BuilderMarkdownBodyProperty({
    propertyName = 'markdownBody',
    label = false,
    ...props
}: Omit<BuilderCodePropertyProps, 'propertyName'> & { propertyName?: string }) {
    return (
        <BuilderCodeProperty
            language="markdown"
            {...{ ...props, propertyName, label }}
        />
    );
}
