import React, {
    useCallback,
    useEffect,
    useMemo,
    useRef,
    useState,
} from 'react';
import Editor, { EditorProps } from '@monaco-editor/react';
import BuilderProperty, { BuilderPropertyProps } from '../mask/property';
import { useMaskContext } from '../../hooks/use-mask';
import { useMaskPropertyPermission } from '../../hooks/use-mask-config';
import Button from 'react-bootstrap/Button';
import Offcanvas, { OffcanvasPlacement } from 'react-bootstrap/Offcanvas';
import { PencilFill } from 'react-bootstrap-icons';

export interface BuilderCodePropertyProps
    extends Omit<BuilderPropertyProps, 'stacked'> {
    propertyName: string;
    language?: string;
    defaultValue?: string;
    offCanvas?: OffcanvasPlacement | boolean;
}

/** @see https://microsoft.github.io/monaco-editor/api/interfaces/monaco.editor.IStandaloneEditorConstructionOptions.html#scrollbar */
export const defaultInlineEditorOptions: EditorProps['options'] = {
    lineNumbers: false,
    fontSize: 12,
    minimap: { enabled: false },
    glyphMargin: false,
    folding: false,
    lineDecorationsWidth: 0,
    lineNumbersMinChars: 0,
    scrollBeyondLastLine: false,
};

export const defaultOffCanvasEditorOptions: EditorProps['options'] = {};

export function BuilderInlineCodeProperty({
    label,
    propertyName,
    defaultValue = '',
    language = 'plain',
    ...props
}: Omit<BuilderCodePropertyProps, 'offCanvas'>) {
    const { state, setItemProperty } = useMaskContext();

    const value =
        typeof state[propertyName] === 'string'
            ? state[propertyName]
            : defaultValue;

    const permission = useMaskPropertyPermission(propertyName);

    const editorOptions = useMemo(() => {
        return {
            ...defaultInlineEditorOptions,
            readOnly: permission === 'read',
        };
    }, [permission]);

    const handleChange = useCallback(
        (value: string | undefined) => {
            setItemProperty(propertyName, value);
        },
        [propertyName, setItemProperty]
    );

    const lines = Math.max((value ?? '').split(/\r\n|\r|\n/).length + 2, 4);

    return (
        <BuilderProperty className="feb-code-property" stacked label={label}>
            <div className="w-100 form-control" {...props}>
                <Editor
                    height={`${lines * 18}px`}
                    language={language}
                    value={value}
                    onChange={handleChange}
                    options={editorOptions}
                />
            </div>
        </BuilderProperty>
    );
}

export function BuilderOffCanvasCodeProperty({
    label,
    propertyName,
    defaultValue = '',
    language = 'plain',
    placement = 'end',
    ...props
}: Omit<BuilderCodePropertyProps, 'offCanvas'> & {
    placement?: OffcanvasPlacement;
}) {
    const { state, setItemProperty } = useMaskContext();
    const mounted = useRef<boolean>();

    const value =
        typeof state[propertyName] === 'string'
            ? state[propertyName]
            : defaultValue;

    const permission = useMaskPropertyPermission(propertyName);

    const editorOptions = useMemo(() => {
        return {
            ...defaultOffCanvasEditorOptions,
            readOnly: permission === 'read',
        };
    }, [permission]);

    // fixes state leak with the change listener beeing called when the selected element
    useEffect(() => {
        mounted.current = true;
        return () => {
            mounted.current = false;
        };
    }, [state]);

    const handleChange = useCallback(
        (newValue: string | undefined) => {
            if (mounted.current) {
                setItemProperty(propertyName, newValue);
            }
        },
        [propertyName, setItemProperty]
    );

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const toggleShow = () => setShow(!show);

    useEffect(() => {
        document.documentElement.classList.toggle(
            'feb-offcanvas-editor--show',
            show
        );
        document.documentElement.classList.toggle(
            `feb-offcanvas-editor--show-${placement}`,
            show
        );
    }, [show, placement]);

    useEffect(() => {
        return () => {
            document.documentElement.classList.toggle(
                'feb-offcanvas-editor--show',
                false
            );
            document.documentElement.classList.toggle(
                `feb-offcanvas-editor--show-${placement}`,
                false
            );
        };
    }, []);

    return (
        <>
            <BuilderProperty className="feb-code-property" label={label}>
                <Button
                    variant="outline-secondary"
                    active={show}
                    className="w-100"
                    style={{ maxWidth: '150px' }}
                    onClick={toggleShow}
                >
                    <PencilFill />
                </Button>
            </BuilderProperty>

            <Offcanvas
                className="feb-offcanvas-editor"
                show={show}
                onHide={handleClose}
                placement={placement}
                backdrop={false}
                enforceFocus={false}
            >
                <Offcanvas.Header closeButton closeVariant="white">
                    <Offcanvas.Title>{label}</Offcanvas.Title>
                </Offcanvas.Header>
                <Offcanvas.Body>
                    <Editor
                        height={`100%`}
                        language={language}
                        value={value}
                        onChange={handleChange}
                        options={editorOptions}
                        theme="vs-dark"
                    />
                </Offcanvas.Body>
            </Offcanvas>
        </>
    );
}

export default function BuilderCodeProperty({
    ...props
}: BuilderCodePropertyProps) {
    let offCanvas: OffcanvasPlacement | '';
    if (typeof props.offCanvas === 'string') {
        offCanvas = props.offCanvas;
    } else {
        offCanvas = props.offCanvas !== false ? 'end' : '';
    }
    delete props.offCanvas;

    return offCanvas ? (
        <BuilderOffCanvasCodeProperty {...props} placement={offCanvas} />
    ) : (
        <BuilderInlineCodeProperty {...props} />
    );
}
