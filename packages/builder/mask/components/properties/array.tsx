import React from 'react';

import BuilderProperty, { BuilderPropertyProps } from '../mask/property';
import { useMaskContext } from '../../hooks/use-mask';
import BuilderArrayFormControl, {
    BuilderArrayFormControlProps,
} from '@form-engine/builder-array-form-control';
import { useMaskPropertyPermission } from '../../hooks/use-mask-config';

export interface BuilderArrayPropertyProps
    extends BuilderPropertyProps,
        Pick<BuilderArrayFormControlProps, 'cols' | 'schema'> {
    defaultValue?: any[];
    propertyName: string;
}

export default function BuilderArrayProperty({
    label,
    propertyName,
    defaultValue = [],
    schema,
    cols,
    ...props
}: BuilderArrayPropertyProps) {
    const { state, setItemProperty } = useMaskContext();

    const value = Array.isArray(state?.[propertyName])
        ? state[propertyName]
        : defaultValue;

    const permission = useMaskPropertyPermission(propertyName);

    return (
        <BuilderProperty
            className="feb-array-property"
            stacked
            label={label}
            {...props}
        >
            <BuilderArrayFormControl
                schema={schema}
                cols={cols}
                value={value}
                disabled={permission === 'read'}
                onChange={(update) => setItemProperty(propertyName, update)}
            />
        </BuilderProperty>
    );
}
