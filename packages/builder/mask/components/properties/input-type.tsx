import React from 'react';
import BuilderEnumProperty, { BuilderEnumPropertyProps } from './enum';
import { i18n } from '@form-engine/builder-application';

const inputTypes = {
    text: 'Text',
    // button: 'button',
    // checkbox: 'checkbox',
    color: 'Color',
    date: 'Date',
    'datetime-local': 'Datetime Local',
    email: 'Email',
    // file: 'file',
    hidden: 'Hidden',
    // image: 'image',
    month: 'Month',
    number: 'Number',
    password: 'Password',
    // radio: 'radio',
    // range: 'range',
    // reset: 'reset',
    // search: 'search',
    // submit: 'submit',
    tel: 'Tel',
    time: 'Time',
    url: 'Url',
    week: 'Week',
};

export default function BuilderInputTypeProperty(
    props: Omit<BuilderEnumPropertyProps, 'propertyName'> & {
        propertyName?: string;
    }
) {
    return (
        <BuilderEnumProperty
            options={Object.entries(inputTypes)}
            label={i18n.c('mask.inputTypeProperty.title')}
            defaultValue="text"
            {...props}
            propertyName={props.propertyName ?? 'inputType'}
        />
    );
}
