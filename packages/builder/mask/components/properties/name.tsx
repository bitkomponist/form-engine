import { formatResourceNameCase } from '@form-engine/core-util/format-case';
import React from 'react';
import BuilderStringProperty, { BuilderStringPropertyProps } from './string';
import { i18n } from '@form-engine/builder-application';

interface BuilderNamePropertyProps
    extends Omit<BuilderStringPropertyProps, 'propertyName'> {
    propertyName?: string;
}

export default function BuilderNameProperty(props: BuilderNamePropertyProps) {
    return (
        <BuilderStringProperty
            label={i18n.c('mask.nameProperty.title')}
            {...props}
            formatter={formatResourceNameCase}
            propertyName={props.propertyName ?? 'name'}
        />
    );
}
