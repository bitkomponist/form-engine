import React from 'react';
import BuilderStringProperty from './string';
import * as contentTypeValidations from '@form-engine/core-application/validation/validations/content-types';
import { useMaskContext } from '../../hooks/use-mask';
import BuilderEnumProperty from './enum';
import { i18n } from '@form-engine/builder-application';

const validationOptions = Object.keys(contentTypeValidations).map((key) => [
    key,
    key.replace('FormEngineValidateContentType', ''),
]);

export default function BuilderValidationContentTypeProperty() {
    const { state } = useMaskContext();

    const { validationContentType: type = '' } = state;

    const { c } = i18n.prefix('mask.validationContentTypeProperty.');

    return (
        <>
            <BuilderEnumProperty
                propertyName="validationContentType"
                label={c('validationContentType')}
                options={[...validationOptions]}
            />
            {type === 'FormEngineValidateContentTypeRegex' && (
                <BuilderStringProperty
                    propertyName="validateContentTypeRegex"
                    label={c('validateContentTypeRegex')}
                    placeholder={c('validateContentTypeRegexPlaceholder')}
                    compilable
                />
            )}
            {[
                'FormEngineValidateContentTypeDate',
                'FormEngineValidateContentTypeDatetime',
            ].includes(type) && (
                <>
                    <BuilderStringProperty
                        propertyName="validateContentTypeDateBefore"
                        label={c('validateContentTypeDateBefore')}
                        placeholder={c(
                            'validateContentTypeDateBeforePlaceholder'
                        )}
                        compilable
                    />
                    <BuilderStringProperty
                        propertyName="validateContentTypeDateAfter"
                        label={c('validateContentTypeDateAfter')}
                        placeholder={c(
                            'validateContentTypeDateAfterPlaceholder'
                        )}
                        compilable
                    />
                </>
            )}
            {type && (
                <BuilderStringProperty
                    propertyName="validateContentTypeMessage"
                    label={c('validateContentTypeMessage')}
                    compilable
                />
            )}
        </>
    );
}
