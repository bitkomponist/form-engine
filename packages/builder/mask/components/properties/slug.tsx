import formatSlug from '@form-engine/core-util/format-slug';
import React from 'react';
import BuilderStringProperty, { BuilderStringPropertyProps } from './string';
import { i18n } from '@form-engine/builder-application';

export default function BuilderSlugProperty(
    props: Omit<BuilderStringPropertyProps, 'propertyName'> & {
        propertyName?: string;
    }
) {
    return (
        <BuilderStringProperty
            label={i18n.c('mask.slugProperty.title')}
            formatter={formatSlug}
            {...props}
            propertyName={props.propertyName ?? 'slug'}
        />
    );
}
