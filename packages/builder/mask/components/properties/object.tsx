import BuilderFormSelect from '@form-engine/builder-application/components/select';
import BuilderArrayFormControl, {
    BuilderArrayFormControlProps,
} from '@form-engine/builder-array-form-control';
import merge from 'lodash.merge';
import React, { useCallback, useMemo } from 'react';
import { useMaskContext } from '../../hooks/use-mask';
import { useMaskPropertyPermission } from '../../hooks/use-mask-config';
import BuilderProperty, { BuilderPropertyProps } from '../mask/property';
import { CompileableObjectDefinition } from '@form-engine/core-util/create-object';
import { i18n } from '@form-engine/builder-application';

const DEFAULT_PROPERTY_TYPE = 'string';

const builderObjectPropertyCols: BuilderArrayFormControlProps['cols'] = {
    key: {
        label: () => <i18n.Node id="mask.objectProperty.key" />,
        formatter: (source: any) => String(source ?? '').trim(),
        compilable: true,
    },
    type: {
        label: () => <i18n.Node id="mask.objectProperty.type" />,
        size: '80px',
        props: {
            innerProps: {
                as: ({
                    value,
                    onChange,
                }: {
                    value?: string;
                    onChange?: (value?: string) => void;
                }) => {
                    return (
                        <BuilderFormSelect.Advanced
                            value={value}
                            onChange={onChange}
                            options={[
                                {
                                    value: 'string',
                                    label: i18n.c(
                                        'mask.objectProperty.typeOptions.string'
                                    ),
                                },
                                {
                                    value: 'number',
                                    label: i18n.c(
                                        'mask.objectProperty.typeOptions.number'
                                    ),
                                },
                                {
                                    value: 'boolean',
                                    label: i18n.c(
                                        'mask.objectProperty.typeOptions.boolean'
                                    ),
                                },
                                {
                                    value: 'json',
                                    label: i18n.c(
                                        'mask.objectProperty.typeOptions.json'
                                    ),
                                },
                                {
                                    value: 'raw-string',
                                    label: i18n.c(
                                        'mask.objectProperty.typeOptions.raw-string'
                                    ),
                                },
                            ]}
                        />
                    );
                },
            },
        },
    },
    value: {
        label: () => <i18n.Node id="mask.objectProperty.value" />,
        formatter: (source: any) => String(source ?? '').trim(),
        compilable: true,
    },
};

export interface BuilderObjectPropertyProps extends BuilderPropertyProps {
    propertyName: string;
    defaultValue?: any;
    cols?: BuilderArrayFormControlProps['cols'];
}

export default function BuilderObjectProperty({
    cols: colsProp,
    propertyName,
    defaultValue,
    label,
    ...props
}: BuilderObjectPropertyProps) {
    const cols = useMemo(() => {
        if (!colsProp) return builderObjectPropertyCols;
        return merge({}, builderObjectPropertyCols, colsProp);
    }, [colsProp]);

    const { state, setItemProperty } = useMaskContext();

    const value = useMemo(() => {
        let entries: CompileableObjectDefinition = state?.[propertyName]
            ? state[propertyName]
            : defaultValue;

        // convert deprecated object values to array
        if (entries && !Array.isArray(entries)) {
            entries = Object.entries(entries as any).map(([key, def]) => ({
                ...(def ?? {}),
                key,
            })) as CompileableObjectDefinition;
        }

        return (entries ?? []).map(({ key, value, type }) => ({
            key,
            value: value ?? '',
            type: type || DEFAULT_PROPERTY_TYPE,
        }));
    }, [state?.[propertyName], defaultValue]);

    const handleChange = useCallback(
        (update: CompileableObjectDefinition) => {
            setItemProperty(
                propertyName,
                (update ?? []).map(({ key, value, type }) => ({
                    key,
                    value: value ?? '',
                    type: type || DEFAULT_PROPERTY_TYPE,
                }))
            );
        },
        [setItemProperty, propertyName]
    );

    const permission = useMaskPropertyPermission(propertyName);

    return (
        <BuilderProperty
            className="feb-array-property"
            stacked
            label={label}
            {...props}
        >
            <BuilderArrayFormControl
                schema={'object'}
                cols={cols}
                value={value}
                disabled={permission === 'read'}
                onChange={handleChange}
            />
        </BuilderProperty>
    );
}
