import { useFormElements } from '@form-engine/core-application/hooks/use-form';
import React from 'react';
import { useMemo } from 'react';
import BuilderEnumProperty, { BuilderEnumPropertyProps } from './enum';
import { i18n } from '@form-engine/builder-application';

/** @todo define 'FormEnginePageElement' as global constant */

export default function BuilderPageNameProperty(
    props: Omit<BuilderEnumPropertyProps, 'propertyName'> & {
        propertyName?: string;
    }
) {
    const { state: elements } = useFormElements();

    const options = useMemo(() => {
        return (elements || [])
            .filter(
                ({ type, name }) => name && type === 'FormEnginePageElement'
            )
            .map(({ name }) => [name as string]);
    }, [elements]);

    return (
        <BuilderEnumProperty
            label={i18n.c('mask.pageNameProperty.title')}
            {...props}
            propertyName={props.propertyName ?? 'pageName'}
            options={options}
        />
    );
}
