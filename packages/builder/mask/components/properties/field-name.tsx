import FormEngine from '@form-engine/core-application';
import { useFormElements } from '@form-engine/core-application/hooks/use-form';
import React from 'react';
import { useMemo } from 'react';
import BuilderEnumProperty, { BuilderEnumPropertyProps } from './enum';
import { i18n } from '@form-engine/builder-application';

export type BuilderFieldNamePropertyProps = BuilderEnumPropertyProps & {
    omit?: string[];
    propertyName?: string;
    required?: boolean;
};

export default function BuilderFieldNameProperty({
    omit = [],
    ...props
}: BuilderFieldNamePropertyProps) {
    const { state: elements } = useFormElements();
    const required = 'required' in props && props.required !== false;
    delete props.required;

    const options = useMemo(() => {
        const fields = FormEngine.getFieldElements();
        const items = (elements || [])
            .filter(
                ({ type, name }) =>
                    name && !omit.includes(name) && type in fields
            )
            .map(({ name }) => [name as string]);

        if (required) {
            return items;
        }

        return [...items];
    }, [elements, omit, required]);

    return (
        <BuilderEnumProperty
            label={i18n.c('mask.fieldNameProperty.title')}
            {...props}
            propertyName={props.propertyName ?? 'fieldName'}
            options={options}
        />
    );
}
