import React from 'react';
import { useMaskContext } from '../../hooks/use-mask';
import BuilderEnumProperty from './enum';
import BuilderStringProperty from './string';
import { i18n } from '@form-engine/builder-application';

export interface BuilderAuthPropertyProps {
    authPropertyName?: string;
    usernameProperyName?: string;
    passwordProperyName?: string;
    accessTokenProperyName?: string;
}

export default function BuilderAuthProperty({
    authPropertyName = 'auth',
    usernameProperyName = 'basicAuthUsername',
    passwordProperyName = 'basicAuthPassword',
    accessTokenProperyName = 'bearerToken',
}: BuilderAuthPropertyProps) {
    const { state } = useMaskContext();

    return (
        <>
            <BuilderEnumProperty
                propertyName={authPropertyName}
                label={i18n.c('mask.authProperty.method')}
                options={[
                    ['', i18n.c('mask.authProperty.methods.none')],
                    ['basic', i18n.c('mask.authProperty.methods.basic')],
                    ['bearer', i18n.c('mask.authProperty.methods.bearer')],
                ]}
            />
            {state.auth === 'basic' && (
                <>
                    <BuilderStringProperty
                        compilable
                        propertyName={usernameProperyName}
                        label={i18n.c('mask.authProperty.basicAuthUsername')}
                        defaultValue="{{defaultBasicAuthUsername}}"
                    />
                    <BuilderStringProperty
                        compilable
                        propertyName={passwordProperyName}
                        label={i18n.c('mask.authProperty.basicAuthPassword')}
                        defaultValue="{{defaultBasicAuthPassword}}"
                        type="password"
                    />
                </>
            )}
            {state.auth === 'bearer' && (
                <>
                    <BuilderStringProperty
                        compilable
                        propertyName={accessTokenProperyName}
                        defaultValue="{{request.headers.authorization}}"
                        label={i18n.c('mask.authProperty.authorization')}
                    />
                </>
            )}
        </>
    );
}
