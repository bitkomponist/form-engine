import { useMaskContext } from '../../hooks/use-mask';

import { useFormContext } from '@form-engine/core-application/hooks/use-form';
import React, { useCallback } from 'react';
import {
    Button,
    ButtonGroup,
    FormControl,
    Row,
    Col,
    Nav,
} from 'react-bootstrap';
import { DashCircleFill, PlusCircleFill } from 'react-bootstrap-icons';
import BuilderArrayFormControl, {
    ARRAY_SCHEMA,
} from '@form-engine/builder-array-form-control';
import BuilderFormSelect from '@form-engine/builder-application/components/select';
import BuilderProperty from '../mask/property';
import Builder from '@form-engine/builder-application';
import {
    DatasourceDefinition,
    ElementDatasourceConfiguration,
    ElementDefinition,
} from '@form-engine/core-application/types/form';
import { useMaskPropertyPermission } from '../../hooks/use-mask-config';
import { i18n } from '@form-engine/builder-application';

function isSelectableDatasource(config: DatasourceDefinition) {
    return Boolean(Builder?.datasources?.[config.type]?.exposesOptions);
}

export function DataSourceSelect({
    value,
    onChange,
    disabled,
}: {
    value: unknown;
    onChange: (newValue: unknown) => void;
    disabled?: boolean;
}) {
    const {
        state: { datasources = [] },
    } = useFormContext();

    const handleChange = useCallback(
        (e: React.ChangeEvent) => {
            onChange && onChange((e.target as HTMLInputElement).value);
        },
        [onChange]
    );

    return (
        <BuilderFormSelect.Advanced
            isDisabled={disabled}
            value={value}
            onChange={handleChange}
        >
            {datasources.filter(isSelectableDatasource).map(({ id, name }) => {
                return (
                    <option key={id} value={id}>
                        {name}
                    </option>
                );
            })}
        </BuilderFormSelect.Advanced>
    );
}

function StaticOptionsEditorOption({
    value = [],
    onChange,
}: {
    value: string[];
    onChange: (newValue: string[]) => void;
}) {
    const handleValueChange = useCallback(
        (e: React.ChangeEvent) =>
            onChange &&
            onChange([(e.target as HTMLInputElement).value, value[1]]),
        [onChange, value]
    );
    const handleLabelChange = useCallback(
        (e: React.ChangeEvent) =>
            onChange &&
            onChange([value[0], (e.target as HTMLInputElement).value]),
        [onChange, value]
    );

    return (
        <Row className="mb-2" style={{ ['--bs-gutter-x' as any]: '5px' }}>
            <Col xs={4}>
                <FormControl
                    size="sm"
                    type="text"
                    value={value[0] || ''}
                    onChange={handleValueChange}
                />
            </Col>
            <Col xs={8}>
                <FormControl
                    size="sm"
                    type="text"
                    value={value[1] || ''}
                    onChange={handleLabelChange}
                />
            </Col>
        </Row>
    );
}

export function StaticOptionsEditor({
    value = [['', '']],
    onChange,
}: {
    value?: string[][];
    onChange: (newValue: string[][]) => void;
}) {
    const add = useCallback(() => {
        onChange && onChange([...value, ['', '']]);
    }, [onChange, value]);
    const remove = useCallback(() => {
        if (!onChange) return;
        const copy = [...value];
        copy.pop();
        onChange && onChange(copy);
    }, [onChange, value]);
    const handleChange = useCallback(
        (index: number, option: string[]) => {
            if (!onChange) return;
            const copy = [...value];
            copy[index] = option;
            onChange && onChange(copy);
        },
        [onChange, value]
    );

    return (
        <>
            {value.map((option, index) => (
                <StaticOptionsEditorOption
                    key={index}
                    value={option}
                    onChange={(ov) => handleChange(index, ov)}
                />
            ))}
            <Row>
                <Col className="d-flex justify-content-end">
                    <ButtonGroup size="sm">
                        <Button variant="light" onClick={add}>
                            <PlusCircleFill />
                        </Button>
                        <Button variant="light" onClick={remove}>
                            <DashCircleFill />
                        </Button>
                    </ButtonGroup>
                </Col>
            </Row>
        </>
    );
}

export const DATASOURCE_PROPERTY_MODE_STATIC = 'static';
export const DATASOURCE_PROPERTY_MODE_SOURCE = 'source';

export interface BuilderDataSourcePropertyProps {
    label?: string;
    propertyName?: string;
    defaultValue?: ElementDatasourceConfiguration;
    modes?: ('static' | 'source')[];
}

export default function BuilderDataSourceProperty({
    label = i18n.c('mask.datasourceProperty.title'),
    propertyName = 'datasource',
    defaultValue = {},
    modes = [DATASOURCE_PROPERTY_MODE_STATIC, DATASOURCE_PROPERTY_MODE_SOURCE],
}: BuilderDataSourcePropertyProps) {
    const { state: element, setItemProperty } =
        useMaskContext<ElementDefinition>();

    if (!element) throw new Error('invalid DataSource Property Invokation');

    const set = useCallback(
        (value: Partial<ElementDefinition>) => {
            setItemProperty(propertyName, {
                ...(element[propertyName] || {}),
                ...value,
            });
        },
        [element, propertyName, setItemProperty]
    );

    const {
        mode = modes[0],
        staticOptions = [],
        datasourceId = '',
    } = (element[propertyName] ||
        defaultValue) as ElementDatasourceConfiguration;

    const permission = useMaskPropertyPermission(propertyName);

    const disabled = permission === 'read';

    return (
        <BuilderProperty
            stacked
            className="fe-data-source-property"
            label={label}
        >
            <div className="w-100">
                {modes.length > 1 && (
                    <Nav variant="tabs" activeKey={mode} className="mb-2">
                        {modes.includes(DATASOURCE_PROPERTY_MODE_STATIC) && (
                            <Nav.Item className="ms-auto">
                                <Nav.Link
                                    eventKey="static"
                                    as="button"
                                    onClick={() => {
                                        if (disabled) return;

                                        set({
                                            mode: DATASOURCE_PROPERTY_MODE_STATIC,
                                        });
                                    }}
                                >
                                    <i18n.Node id="mask.datasourceProperty.static" />
                                </Nav.Link>
                            </Nav.Item>
                        )}

                        {modes.includes(DATASOURCE_PROPERTY_MODE_SOURCE) && (
                            <Nav.Item>
                                <Nav.Link
                                    eventKey="source"
                                    as="button"
                                    onClick={() => {
                                        if (disabled) return;
                                        set({
                                            mode: DATASOURCE_PROPERTY_MODE_SOURCE,
                                        });
                                    }}
                                >
                                    <i18n.Node id="mask.datasourceProperty.source" />
                                </Nav.Link>
                            </Nav.Item>
                        )}
                    </Nav>
                )}

                {mode === DATASOURCE_PROPERTY_MODE_SOURCE && (
                    <DataSourceSelect
                        value={datasourceId}
                        onChange={(value) => {
                            set({
                                datasourceId: value,
                                mode: DATASOURCE_PROPERTY_MODE_SOURCE,
                            });
                        }}
                        disabled={disabled}
                    />
                )}
                {mode === DATASOURCE_PROPERTY_MODE_STATIC && (
                    <BuilderArrayFormControl
                        cols={[
                            [
                                'value',
                                i18n.c('mask.datasourceProperty.optionValue'),
                            ],
                            [
                                'label',
                                i18n.c('mask.datasourceProperty.optionLabel'),
                            ],
                        ]}
                        value={staticOptions}
                        schema={ARRAY_SCHEMA}
                        disabled={disabled}
                        onChange={(value) => {
                            set({
                                staticOptions: value,
                                mode: DATASOURCE_PROPERTY_MODE_STATIC,
                            });
                        }}
                    />
                )}
            </div>
        </BuilderProperty>
    );
}
