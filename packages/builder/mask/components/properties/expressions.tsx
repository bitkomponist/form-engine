import BuilderArrayFormControl, {
    BuilderArrayFormControlColConfig,
} from '@form-engine/builder-array-form-control/components/array-form-control';
import BuilderArrayFormControlCell, {
    BuilderArrayFormControlCellProps,
} from '@form-engine/builder-array-form-control/components/array-form-control/cell';
import { ExpressionDefinition } from '@form-engine/core-application/types/form';
import { formatTitleCase } from '@form-engine/core-util/format-case';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import {
    MaskContextValue,
    MaskProvider,
    useMaskProperty,
} from '../../hooks/use-mask';
import { useMaskPropertyPermission } from '../../hooks/use-mask-config';
import BuilderProperty from '../mask/property';
import BuilderEnumProperty from './enum';
import BuilderStringProperty, { BuilderStringPropertyProps } from './string';
import { i18n } from '@form-engine/builder-application';

export enum EXPRESSION_OPERATORS {
    EQUAL = 'equal',
    NOT_EQUAL = 'not-equal',
    GREATER = 'greater',
    GREATER_EQUAL = 'greater-equal',
    LESSER = 'lesser',
    LESSER_EQUAL = 'lesser-equal',
    // IN = 'in',
    // CONTAINS = 'contains',
}

export enum EXPRESSION_DISJUNCTIONS {
    AND = 'and',
    OR = 'or',
}

interface ExpressionInputProps {
    value?: ExpressionDefinition;
    onChange: (u: ExpressionDefinition) => void;
    forceLeftOperand?: ExpressionDefinition['leftOperand'];
    forceRightOperand?: ExpressionDefinition['rightOperand'];
    forceOperator?: ExpressionDefinition['operator'];
    forceDisjunctor?: ExpressionDefinition['disjunctor'];
    variableOptions?: string[];
}

function ExpressionInput({
    value,
    onChange,
    forceLeftOperand,
    forceRightOperand,
    forceOperator,
    forceDisjunctor,
    variableOptions,
}: ExpressionInputProps) {
    const [state, setState] = useState(value);

    useEffect(() => {
        setState(value);
    }, [value, setState]);

    const setItem = useCallback(
        (newValue: ExpressionDefinition) => {
            if (newValue) {
                if (forceLeftOperand !== undefined) {
                    newValue.leftOperand = forceLeftOperand;
                }
                if (forceRightOperand !== undefined) {
                    newValue.rightOperand = forceRightOperand;
                }
                if (forceOperator !== undefined) {
                    newValue.operator = forceOperator;
                }
                if (forceDisjunctor !== undefined) {
                    newValue.disjunctor = forceDisjunctor;
                }
            }
            setState(newValue);
            onChange?.(newValue);
        },
        [
            setState,
            onChange,
            forceLeftOperand,
            forceRightOperand,
            forceOperator,
            forceDisjunctor,
        ]
    );

    const setItemProperty = useCallback(
        (propertyName: keyof ExpressionDefinition, newValue: any) => {
            setItem({
                ...(value ?? {}),
                [propertyName]: newValue,
            } as ExpressionDefinition);
        },
        [setItem, value]
    );

    const api = useMemo(
        () => ({
            state,
            setItem,
            setItemProperty,
        }),
        [state, setItem]
    );

    return (
        <MaskProvider value={api as MaskContextValue}>
            {forceLeftOperand === undefined && (
                <BuilderStringProperty
                    compilable
                    label={i18n.c('mask.expressionsProperty.leftOperand')}
                    propertyName="leftOperand"
                    placeholder='""'
                    variableOptions={variableOptions}
                />
            )}
            {forceOperator === undefined && (
                <BuilderEnumProperty
                    label={i18n.c('mask.expressionsProperty.operator')}
                    propertyName="operator"
                    options={Object.values(EXPRESSION_OPERATORS).map(
                        (value) => [value, formatTitleCase(value)]
                    )}
                />
            )}
            {forceRightOperand === undefined && (
                <BuilderStringProperty
                    compilable
                    label={i18n.c('mask.expressionsProperty.rightOperand')}
                    propertyName="rightOperand"
                    placeholder='""'
                    variableOptions={variableOptions}
                />
            )}
            {forceDisjunctor === undefined && (
                <BuilderEnumProperty
                    label={i18n.c('mask.expressionsProperty.disjunctor')}
                    propertyName="disjunctor"
                    options={Object.values(EXPRESSION_DISJUNCTIONS).map(
                        (value) => [value, formatTitleCase(value)]
                    )}
                />
            )}
        </MaskProvider>
    );
}

function ExpressionInputCell({
    value,
    onChange,
    forceLeftOperand,
    forceOperator,
    forceRightOperand,
    forceDisjunctor,
    variableOptions,
    ...cellProps
}: ExpressionInputProps & BuilderArrayFormControlCellProps) {
    return (
        <BuilderArrayFormControlCell {...cellProps}>
            <ExpressionInput
                {...{
                    value,
                    onChange,
                    forceLeftOperand,
                    forceOperator,
                    forceRightOperand,
                    forceDisjunctor,
                    variableOptions,
                }}
            />
        </BuilderArrayFormControlCell>
    );
}

export interface BuilderExpressionsPropertyProps
    extends BuilderStringPropertyProps {
    forceLeftOperand?: ExpressionDefinition['leftOperand'];
    forceRightOperand?: ExpressionDefinition['rightOperand'];
    forceOperator?: ExpressionDefinition['operator'];
    forceDisjunctor?: ExpressionDefinition['disjunctor'];
    variableOptions?: string[];
    propertyName?: string;
}

export default function BuilderExpressionsProperty({
    label = i18n.c('mask.expressionsProperty.title'),
    propertyName = 'expressions',
    forceLeftOperand,
    forceRightOperand,
    forceOperator,
    forceDisjunctor,
    variableOptions,
    prefix: _prefix,
    ...props
}: BuilderExpressionsPropertyProps) {
    const [expressions, setExpressions] = useMaskProperty(propertyName, []);

    const cols = useMemo<{
        [key: string]: BuilderArrayFormControlColConfig;
    }>(() => {
        return {
            expressions: {
                asModel: true,
                cellComponent: ExpressionInputCell,
                props: {
                    forceLeftOperand,
                    forceRightOperand,
                    forceOperator,
                    forceDisjunctor,
                    variableOptions,
                },
            } as BuilderArrayFormControlColConfig,
        };
    }, [
        forceLeftOperand,
        forceRightOperand,
        forceOperator,
        forceDisjunctor,
        variableOptions,
    ]);

    const permission = useMaskPropertyPermission(propertyName);

    return (
        <BuilderProperty
            className="feb-expressions-property"
            stacked
            label={label}
            {...props}
        >
            <BuilderArrayFormControl
                head={false}
                schema={'object'}
                cols={cols}
                value={expressions}
                onChange={setExpressions}
                disabled={permission === 'read'}
            />
        </BuilderProperty>
    );
}
