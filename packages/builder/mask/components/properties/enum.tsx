import React from 'react';
import BuilderProperty, { BuilderPropertyProps } from '../mask/property';
import { useMaskContext } from '../../hooks/use-mask';
import BuilderFormSelect from '@form-engine/builder-application/components/select';
import { useMaskPropertyPermission } from '../../hooks/use-mask-config';

export interface BuilderEnumPropertyProps extends BuilderPropertyProps {
    defaultValue?: string;
    propertyName: string;
    required?: boolean;
    options?: string[] | string[][];
}

export default function BuilderEnumProperty({
    label,
    propertyName,
    options = [],
    defaultValue = '',
    ...props
}: BuilderEnumPropertyProps) {
    const stacked = 'stacked' in props && props.stacked !== false;
    delete props.stacked;

    const { state, setItemProperty } = useMaskContext();
    const required = 'required' in props && props.required !== false;
    delete props.required;

    const value =
        typeof state[propertyName] === 'string'
            ? state[propertyName]
            : defaultValue;

    const permission = useMaskPropertyPermission(propertyName);

    return (
        <BuilderProperty
            className="feb-enum-property"
            label={label}
            stacked={stacked}
        >
            <div className="flex-grow-1">
                <BuilderFormSelect.Advanced
                    {...props}
                    value={value}
                    required={required}
                    isDisabled={permission === 'read'}
                    onChange={(e) =>
                        setItemProperty(propertyName, e.target.value)
                    }
                >
                    {options.map((option, key) => {
                        let label, value;
                        if (Array.isArray(option)) {
                            [value, label] = option;
                        } else {
                            label = value = option;
                        }
                        return (
                            <option key={key} value={value}>
                                {label || value}
                            </option>
                        );
                    })}
                </BuilderFormSelect.Advanced>
            </div>
        </BuilderProperty>
    );
}
