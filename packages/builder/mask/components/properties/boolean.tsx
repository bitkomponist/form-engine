import { useMaskContext } from '../../hooks/use-mask';
import React from 'react';
import { FormCheck, FormCheckProps } from 'react-bootstrap';
import BuilderProperty from '../mask/property';
import { useMaskPropertyPermission } from '../../hooks/use-mask-config';

export interface BuilderBooleanPropertyProps
    extends Omit<FormCheckProps, 'defaultValue'> {
    defaultValue?: boolean;
    propertyName: string;
    stacked?: boolean;
    label?: React.ReactNode;
}

export default function BuilderBooleanProperty({
    label,
    propertyName,
    defaultValue = false,
    ...props
}: BuilderBooleanPropertyProps) {
    const stacked = 'stacked' in props && props.stacked !== false;
    delete props.stacked;

    const { state, setItemProperty } = useMaskContext();

    const value =
        propertyName in state ? Boolean(state[propertyName]) : defaultValue;

    const permission = useMaskPropertyPermission(propertyName);

    return (
        <BuilderProperty
            className="feb-boolean-property"
            label={label}
            stacked={stacked}
        >
            <FormCheck
                type="switch"
                {...props}
                disabled={permission === 'read'}
                checked={value}
                onChange={(e: React.ChangeEvent) =>
                    setItemProperty(
                        propertyName,
                        Boolean((e.target as HTMLInputElement).checked)
                    )
                }
            />
        </BuilderProperty>
    );
}
