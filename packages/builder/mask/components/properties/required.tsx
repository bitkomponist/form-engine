import React from 'react';
import BuilderBooleanProperty, { BuilderBooleanPropertyProps } from './boolean';
import { i18n } from '@form-engine/builder-application';

export default function BuilderRequiredProperty(
    props: BuilderBooleanPropertyProps & { propertyName?: string }
) {
    return (
        <BuilderBooleanProperty
            label={i18n.c('mask.requiredProperty.title')}
            {...props}
            propertyName={props.propertyName ?? 'required'}
        />
    );
}
