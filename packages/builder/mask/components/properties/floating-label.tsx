import React from 'react';
import BuilderBooleanProperty, { BuilderBooleanPropertyProps } from './boolean';
import { i18n } from '@form-engine/builder-application';

export default function BuilderFloatingLabelProperty(
    props: Omit<BuilderBooleanPropertyProps, 'propertyName'> & {
        propertyName?: string;
    }
) {
    return (
        <BuilderBooleanProperty
            label={i18n.c('mask.floatingLabelProperty.title')}
            {...props}
            propertyName={props.propertyName ?? 'floatingLabel'}
        />
    );
}
