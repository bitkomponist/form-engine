import React, { useCallback, useMemo } from 'react';
import BuilderProperty, { BuilderPropertyProps } from '../mask/property';
import { useMaskContext } from '../../hooks/use-mask';
import BuilderFormSelect from '@form-engine/builder-application/components/select';
import { useMaskPropertyPermission } from '../../hooks/use-mask-config';
import { i18n } from '@form-engine/builder-application';

export interface BuilderTagsPropertyProps extends BuilderPropertyProps {
    defaultValue?: string[];
    propertyName?: string;
    required?: boolean;
    options?: string[];
}

interface TagOption {
    value: string;
    label: string;
}

export default function BuilderTagsProperty({
    label = i18n.c('mask.tagsProperty.title'),
    propertyName = 'tags',
    options = [],
    defaultValue = [],
    ...props
}: BuilderTagsPropertyProps) {
    const stacked = 'stacked' in props && props.stacked !== false;
    delete props.stacked;

    const { state, setItemProperty } = useMaskContext();
    const required = 'required' in props && props.required !== false;
    delete props.required;

    const value: string[] = Array.isArray(state[propertyName])
        ? state[propertyName]
        : [...defaultValue];

    const tagOptions: TagOption[] = useMemo(
        () => (options ?? []).map((value) => ({ value, label: value })),
        [options]
    );

    const handleChange = useCallback(
        (event: any) => {
            const { action } = event?.actionMeta ?? {};
            if (action === 'create-option') {
                const createValue: string = event?.actionMeta?.option?.value;
                if (createValue && !value.includes(createValue)) {
                    setItemProperty(propertyName, [...value, createValue]);
                }
            } else if (action === 'remove-value') {
                const removedValue: string =
                    event?.actionMeta?.removedValue?.value;
                const index = value.indexOf(removedValue);
                if (index > -1) {
                    const copy = [...value];
                    copy.splice(index, 1);
                    setItemProperty(propertyName, copy);
                }
            } else if (action === 'clear') {
                setItemProperty(propertyName, []);
            }
        },
        [state, propertyName, setItemProperty, value]
    );

    const permission = useMaskPropertyPermission(propertyName);

    return (
        <BuilderProperty
            className="feb-enum-property"
            label={label}
            stacked={stacked}
        >
            <div className="flex-grow-1">
                <BuilderFormSelect.Advanced
                    creatable
                    className="text-xs"
                    value={value.map((value) => ({ value, label: value }))}
                    onChange={handleChange}
                    placeholder={i18n.c('mask.tagsProperty.placeholder')}
                    options={tagOptions}
                    isMulti={true}
                    required={required}
                    isDisabled={permission === 'read'}
                />
            </div>
        </BuilderProperty>
    );
}
