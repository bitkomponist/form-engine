import React from 'react';
import BuilderStringProperty, { BuilderStringPropertyProps } from './string';
import { i18n } from '@form-engine/builder-application';

export default function BuilderPlaceholderProperty(
    props: Omit<BuilderStringPropertyProps, 'propertyName'> & {
        propertyName?: string;
    }
) {
    return (
        <BuilderStringProperty
            label={i18n.c('mask.placeholderProperty.title')}
            {...props}
            propertyName={props.propertyName ?? 'placeholder'}
        />
    );
}
