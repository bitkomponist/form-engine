import React, { useCallback, useEffect, useState } from 'react';
import { FormControl, InputGroup } from 'react-bootstrap';

import BuilderProperty, { BuilderPropertyProps } from '../mask/property';
import { useMaskContext } from '../../hooks/use-mask';
import BuilderInsertVariableWizard from '../wizards/insert-variable';
import { useMaskPropertyPermission } from '../../hooks/use-mask-config';

type ControlProps = React.ComponentProps<typeof FormControl>;
export interface BuilderStringPropertyProps
    extends Omit<ControlProps, 'prefix'>,
        BuilderPropertyProps {
    propertyName: string;
    prefix?: React.ReactNode;
    suffix?: React.ReactNode;
    compilable?: boolean;
    variableOptions?: string[];
    formatter?: (newValue: any, propertyName: string, state: any) => any;
}

export default function BuilderStringProperty({
    label,
    propertyName,
    defaultValue = '',
    prefix,
    suffix,
    variableOptions,
    // avg word length = 5, avg type speed = 40 words per minute, = 60000 / 40 / 5
    formatter,
    ...props
}: BuilderStringPropertyProps) {
    const stacked = 'stacked' in props && props.stacked !== false;
    delete props.stacked;

    const compilable = 'compilable' in props && props.compilable !== false;
    delete props.compilable;

    const { state, setItemProperty } = useMaskContext();

    const value: string =
        typeof state[propertyName] === 'string'
            ? state[propertyName]
            : defaultValue;

    const [currentValue, setCurrentValue] = useState(value);

    useEffect(() => {
        setCurrentValue(value);
    }, [value, setCurrentValue]);

    const handleChange = useCallback(
        (newValue: string) => {
            if (typeof formatter === 'function') {
                newValue = formatter(newValue, propertyName, state);
            }
            if (newValue !== state[propertyName]) {
                setItemProperty(propertyName, newValue);
            }
        },
        [setItemProperty, state, propertyName, formatter]
    );

    const permission = useMaskPropertyPermission(propertyName);

    const control = (
        <FormControl
            size="sm"
            {...props}
            value={currentValue}
            disabled={permission === 'read'}
            onChange={(e: React.ChangeEvent) => {
                const { value } = e.target as HTMLInputElement;
                setCurrentValue(value);
                handleChange(value);
            }}
        />
    );

    return (
        <BuilderProperty
            className="feb-string-property"
            label={label}
            stacked={stacked}
        >
            {prefix || suffix || compilable ? (
                <InputGroup>
                    {prefix}
                    {control}
                    {suffix}
                    {compilable && (
                        <BuilderInsertVariableWizard
                            disabled={permission === 'read'}
                            propertyName={propertyName}
                            options={variableOptions}
                        />
                    )}
                </InputGroup>
            ) : (
                control
            )}
        </BuilderProperty>
    );
}
