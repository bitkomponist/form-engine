import Builder from '@form-engine/builder-application';
import { FormEditorEditMode } from '@form-engine/builder-form-editor/hooks/use-form-editor';
import { createObject } from '@form-engine/core-util/create-object';
import React, { useMemo } from 'react';
import {
    MaskConfigContextValueDefaults,
    MaskConfigProvider,
} from '../../hooks/use-mask-config';
import BuilderMaskGroupGeneral from '../groups/general';

export interface BuilderElementMaskProps
    extends React.HTMLAttributes<HTMLElement> {
    as?: React.ElementType;
    children?: React.ReactNode;
    general?: boolean;
    id?: string;
    editMode?: FormEditorEditMode;
}

export default function BuilderElementMask({
    as: Component = 'div',
    children,
    id,
    editMode = 'edit',
    ...props
}: BuilderElementMaskProps) {
    const general = props.general !== false;
    delete props.general;

    const config = useMemo(() => {
        const currentConfigMap = id && Builder?.maskConfigurations[id];
        let currentConfig;
        if (currentConfigMap) {
            currentConfig =
                currentConfigMap[editMode as keyof typeof currentConfigMap];
        }
        return createObject(
            MaskConfigContextValueDefaults,
            currentConfig ?? {}
        );
    }, [id, editMode]);

    return (
        <MaskConfigProvider value={config}>
            <Component
                {...props}
                className={`feb-element-mask feb-element-mask--${editMode} ${
                    props.className || ''
                }`}
            >
                {id &&
                    Builder.renderComponentExtensions(
                        id,
                        ({ placement }) => placement === 'before'
                    )}
                {general && <BuilderMaskGroupGeneral />}
                {children}
                {id &&
                    Builder.renderComponentExtensions(
                        id,
                        ({ placement }) => placement === 'after'
                    )}
            </Component>
        </MaskConfigProvider>
    );
}
