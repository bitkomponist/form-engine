import React from 'react';
import { Col, FormLabel, Row } from 'react-bootstrap';
export interface BuilderPropertyProps {
    label?: React.ReactNode;
    stacked?: boolean;
    className?: string;
}

export default function BuilderProperty({
    label,
    children,
    className = '',
    ...props
}: BuilderPropertyProps & React.ComponentProps<typeof Row>) {
    const stacked = 'stacked' in props && props.stacked !== false;
    delete props.stacked;

    return (
        <Row {...props} className={`fe-property ${className}`}>
            {label && (
                <FormLabel column sm={stacked ? 12 : 4}>
                    {label}
                </FormLabel>
            )}
            <Col className="d-flex align-items-center justify-content-end">
                {children}
            </Col>
        </Row>
    );
}
