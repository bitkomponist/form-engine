import Builder from '@form-engine/builder-application';
import React from 'react';

export interface BuilderMaskGroupProps {
    label?: React.ReactNode;
    children?: React.ReactNode;
    className?: string;
    id?: string;
}

export default function BuilderMaskGroup({
    label,
    children,
    className = '',
    id,
}: BuilderMaskGroupProps) {
    return (
        <div className={`feb-mask-group ${className}`}>
            {label && (
                <div className="feb-mask-group-head">
                    <h6>{label}</h6>
                </div>
            )}
            <div className="feb-mask-group-body">
                {id &&
                    Builder.renderComponentExtensions(
                        id,
                        ({ placement }) => placement === 'before'
                    )}
                {children}
                {id &&
                    Builder.renderComponentExtensions(
                        id,
                        ({ placement }) => placement === 'after'
                    )}
            </div>
        </div>
    );
}
