import { FormEditorEditMode } from '@form-engine/builder-form-editor/hooks/use-form-editor';

export interface BuilderMaskConfiguration<T = any> {
    permissions: { [key in keyof T]: 'read' | 'write' };
}

export interface BuilderMaskConfigurationMap<T = any> {
    [key: FormEditorEditMode]: BuilderMaskConfiguration<T>;
}
