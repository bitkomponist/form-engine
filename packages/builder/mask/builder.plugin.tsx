import { BuilderPlugin } from '@form-engine/builder-application';
import * as translations from './builder-translations';

export default {
    translations,
} as BuilderPlugin;
