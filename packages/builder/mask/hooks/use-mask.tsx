import {
    StoreData,
    StoreReference,
} from '@form-engine/core-application/hooks/use-store';
import React, { createContext, useCallback, useContext, useMemo } from 'react';

export interface MaskContextValue<T = any> {
    state?: T | null;
    setItem: (item: Partial<T> | null) => void;
    setItemProperty: (propertyName: keyof T, value: any) => void;
}

export const MaskContext = createContext<MaskContextValue | null>(null);

export interface MaskProviderProps<
    T extends MaskContextValue = MaskContextValue
> {
    value?: T;
    children?: React.ReactNode | ((value: T) => React.ReactNode);
}

export function getMaskProviderValueFromStore<
    S extends StoreData,
    T extends StoreReference<S> = StoreReference<any>,
    C extends MaskContextValue = MaskContextValue
>(ref: T | null) {
    const setItemProperty = useMemo(() => {
        return (key: keyof S, value: any) => {
            ref?.setItem?.(key as keyof S, value);
        };
    }, [ref?.setItem]);
    if (!ref) return;
    return {
        setItemProperty,
        setItem: ref.setItem,
        state: ref.state,
    } as unknown as C;
}

export function MaskProvider<T extends MaskContextValue = MaskContextValue>({
    value,
    children,
}: MaskProviderProps<T>) {
    if (!value) return null;
    return (
        <MaskContext.Provider value={value}>
            {typeof children === 'function' ? children(value) : children}
        </MaskContext.Provider>
    );
}

export function useMaskContext<T = any>() {
    const context = useContext(MaskContext);

    if (!context) throw new Error('tried to use mask context without provider');

    return context as MaskContextValue<T>;
}

export function useMaskProperty<T>(propertyName: string, fallback?: T) {
    const { state, setItemProperty } = useMaskContext();

    const value =
        state && propertyName in state ? state[propertyName] : fallback;

    const setProperty = useCallback<any>(
        (setterOrValue: T | ((current: T) => T)) => {
            let update;
            if (typeof setterOrValue === 'function') {
                update = (setterOrValue as any)(value) as T;
            } else {
                update = setterOrValue;
            }

            if (update !== value) {
                setItemProperty(propertyName, update);
            }
        },
        [state, value, propertyName, setItemProperty]
    );

    return [value, setProperty] as [typeof value, typeof setProperty];
}
