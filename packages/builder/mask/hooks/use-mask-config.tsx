import React, { createContext, useContext } from 'react';
import { BuilderMaskConfiguration } from '../typings';

export type MaskConfigContextValue<T = any> = BuilderMaskConfiguration<T>;

export const MaskConfigContextValueDefaults: MaskConfigContextValue = {
    permissions: {},
};

export const MaskContext = createContext<MaskConfigContextValue | null>(null);

export interface MaskConfigProviderProps<
    T extends MaskConfigContextValue = MaskConfigContextValue
> {
    value?: T;
    children?: React.ReactNode | ((value: T) => React.ReactNode);
}

export function MaskConfigProvider<
    T extends MaskConfigContextValue = MaskConfigContextValue
>({ value, children }: MaskConfigProviderProps<T>) {
    if (!value) return null;
    return (
        <MaskContext.Provider value={value}>
            {typeof children === 'function' ? children(value) : children}
        </MaskContext.Provider>
    );
}

export function useMaskConfigContext<T = any>() {
    const context = useContext(MaskContext);

    if (!context)
        throw new Error('tried to use mask context config without provider');

    return context as MaskConfigContextValue<T>;
}

export function useMaskPropertyPermission(
    propertyName: string,
    fallback: 'read' | 'write' | undefined = undefined
) {
    const context = useContext(MaskContext);
    return context?.permissions?.[propertyName] ?? fallback;
}
