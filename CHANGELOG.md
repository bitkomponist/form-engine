# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.53.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.52.0...v1.53.0) (2023-02-08)


### Features

* better outline dnd indicators ([93f9c99](https://bitbucket.org/bitkomponist/form-engine/commit/93f9c990dd253d21f07da71280ed996a433f5539))
* form element controls ([805f241](https://bitbucket.org/bitkomponist/form-engine/commit/805f241695c8be5a1b532b12fd01031bcef948cd))


### Bug Fixes

* outline nav button icon positioning ([f13d263](https://bitbucket.org/bitkomponist/form-engine/commit/f13d263c6251f5e7e8c9dbffdea1e0c9a8296dc9))

## [1.52.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.51.0...v1.52.0) (2023-02-03)


### Features

* attachments l10n ([89b70db](https://bitbucket.org/bitkomponist/form-engine/commit/89b70dbed99ac73bf81a45a94cec1edb64c3ced3))
* basic app settings ([e739358](https://bitbucket.org/bitkomponist/form-engine/commit/e7393581f566c833ec5d1809676ff04fba902393))
* builder de l10n ([35c7f51](https://bitbucket.org/bitkomponist/form-engine/commit/35c7f511ec4943e05c6a66c001d57aca7f285dd1))
* builder i18n, login l10n ([f46c49a](https://bitbucket.org/bitkomponist/form-engine/commit/f46c49a97d80702c7e421f90295b6f7802c3d47d))
* button l10n ([b4a399c](https://bitbucket.org/bitkomponist/form-engine/commit/b4a399c304938eaaf0c8644941f3cec38aebcabc))
* checkbox l10n ([32d436e](https://bitbucket.org/bitkomponist/form-engine/commit/32d436e3f04423c9ac1c6ca99fb2900d7fd9d31d))
* cleanup dropzone styling ([10e1238](https://bitbucket.org/bitkomponist/form-engine/commit/10e1238639c34dd20308433e7e4654614d3fe9c2))
* datasource mask l10n ([e1d5a2e](https://bitbucket.org/bitkomponist/form-engine/commit/e1d5a2e29f53e64e459041ef326bdf5a82bbc73f))
* datasources l10n ([0fdcaf6](https://bitbucket.org/bitkomponist/form-engine/commit/0fdcaf6dab74756d383c3e9ea70ed70e3cb82fbd))
* default select option translation ([93aa7d7](https://bitbucket.org/bitkomponist/form-engine/commit/93aa7d7391d2835dec61f152340915cf00cc03f8))
* dragImage renderer ([f238ccb](https://bitbucket.org/bitkomponist/form-engine/commit/f238ccb3005dab887eb482bb6ad0fe8628c3a2e7))
* drop zone container styling ([941ba39](https://bitbucket.org/bitkomponist/form-engine/commit/941ba39a87c9d32623f6cf74662e3e0803850ee0))
* element drawer button l10n ([9cb9c2f](https://bitbucket.org/bitkomponist/form-engine/commit/9cb9c2fd0d7bc78a708bcea79a9bc587e9e565ef))
* elements libary drag labels ([7ee9aeb](https://bitbucket.org/bitkomponist/form-engine/commit/7ee9aeb3b576650d048df83f609b808aa232a256))
* embed dialog l10n ([09c123e](https://bitbucket.org/bitkomponist/form-engine/commit/09c123edecebcc9e0a563a0a6732f7cbf1608e36))
* facette & container l10n ([b31da49](https://bitbucket.org/bitkomponist/form-engine/commit/b31da495c2598bf32ae58a8fb24218881a5c2a08))
* form editor l10n progress ([0dc8f38](https://bitbucket.org/bitkomponist/form-engine/commit/0dc8f38a98386e644a8381623d47d48c36e6e5b9))
* form mask l10n ([0fd906b](https://bitbucket.org/bitkomponist/form-engine/commit/0fd906b81e2348680e1c5cbd1e2c720eaaa2ca64))
* form-editor l10n ([f46d583](https://bitbucket.org/bitkomponist/form-engine/commit/f46d583e92e8879d9082f82c8ff2c209bfcd28b8))
* http request ds l10n ([2f984ef](https://bitbucket.org/bitkomponist/form-engine/commit/2f984efddc7313a6e0d42d6516cf3510d5ca05dc))
* hub l10n ([9ff49d2](https://bitbucket.org/bitkomponist/form-engine/commit/9ff49d26a0b070172f0fac59c230882a8524cbf1))
* input markdown navigation l10n ([e62ca43](https://bitbucket.org/bitkomponist/form-engine/commit/e62ca43d1e68ede9389c33da6df503b6b77f83c5))
* l10n prefixed login ([8af31e5](https://bitbucket.org/bitkomponist/form-engine/commit/8af31e59c4c5a6387260584da599de8b398f1bcd))
* mask l10n ([e30f18e](https://bitbucket.org/bitkomponist/form-engine/commit/e30f18e2a317ad5609090b8909d31a0ae3d062b1))
* mask l10n ([a3be08d](https://bitbucket.org/bitkomponist/form-engine/commit/a3be08d2da6b66c5f1dda2984520acba91753304))
* mask l10n progress ([d2fdea6](https://bitbucket.org/bitkomponist/form-engine/commit/d2fdea6cb8d99326fb0685a79ca0e691ef30110a))
* more prefixed l10n ([b6bcf0e](https://bitbucket.org/bitkomponist/form-engine/commit/b6bcf0e88c48e523b22ededb0123dbc745623c16))
* move builder-translations ([f58713f](https://bitbucket.org/bitkomponist/form-engine/commit/f58713fe9750c19d66fafbed11d8c381497f34ae))
* native dnd progress ([b443c48](https://bitbucket.org/bitkomponist/form-engine/commit/b443c4859973ca9feffafd720681c2ff57047145))
* page radio textarea select l10n ([c20b429](https://bitbucket.org/bitkomponist/form-engine/commit/c20b4297e37c7d75177ad348614c4bb645f5c55e))
* parcel update ([ad55d64](https://bitbucket.org/bitkomponist/form-engine/commit/ad55d64104ff531116ac1dd633f4e92d7522c952))
* prefixable i18n ([796061c](https://bitbucket.org/bitkomponist/form-engine/commit/796061cf617e22126fcfb124b7492ce5caedd571))
* preview settings l10n ([a2e1b9f](https://bitbucket.org/bitkomponist/form-engine/commit/a2e1b9f1383909a417d11aea6ac5d3b4883baaf4))
* refactor builder translations ([3f09def](https://bitbucket.org/bitkomponist/form-engine/commit/3f09def9ca65c1fd7cc6b52e567445d1e73c3105))
* remove lerna ([7e4ef62](https://bitbucket.org/bitkomponist/form-engine/commit/7e4ef62b42f1c094d26a2b74c45cb24ff2fc6fe6))
* remove nodemon ([db7dc47](https://bitbucket.org/bitkomponist/form-engine/commit/db7dc47ffd6915dd0aa4b4a795afcd0bda92628c))
* rename core translations ([3b33acf](https://bitbucket.org/bitkomponist/form-engine/commit/3b33acf74487f369285ae016fd5809960e14b006))
* translation generator nesting ([1d686b8](https://bitbucket.org/bitkomponist/form-engine/commit/1d686b8c1898419f7a761ab45051071f618273a9))
* translation updater script ([51d3d05](https://bitbucket.org/bitkomponist/form-engine/commit/51d3d0587a9c7a9cee6ccc4bf9c49d5cd843815e))
* update to bootstrap 5.2.3 ([d6c359f](https://bitbucket.org/bitkomponist/form-engine/commit/d6c359fc8595713cfbe28916a1c79ed82b9ab33b))
* use simple labels in editor dnd ([d4672c7](https://bitbucket.org/bitkomponist/form-engine/commit/d4672c79122e76828d040c8b397a89a9ee1786e5))
* wfl l10n ([1c60044](https://bitbucket.org/bitkomponist/form-engine/commit/1c600440661fc1f3c0c1803150632388f375ccaf))
* wfl mask l10n ([00a37f7](https://bitbucket.org/bitkomponist/form-engine/commit/00a37f78affee0807c76a623c02df5b750feff59))
* wfl nodes drawer drag labels ([3692095](https://bitbucket.org/bitkomponist/form-engine/commit/3692095bbf6b0896d3d7db320776f9dce91b7ae8))
* wfl request error debugability ([0124ad1](https://bitbucket.org/bitkomponist/form-engine/commit/0124ad173e610e0319f03d5dcb6b4c48f363f9bd))


### Bug Fixes

* bootstrap cdn ([05453e9](https://bitbucket.org/bitkomponist/form-engine/commit/05453e9190baaf7c6edeb01f5348ab404c4f939f))
* disable app search placeholder ([91fd9a6](https://bitbucket.org/bitkomponist/form-engine/commit/91fd9a6b7da59c1288316a79e778eba4101c81fb))
* dnd stop drag cleanup ([aafd09d](https://bitbucket.org/bitkomponist/form-engine/commit/aafd09d1baf2e8554f667b6f392e6193efda08e0))
* show spinner only when form isnt editable ([c68d9ff](https://bitbucket.org/bitkomponist/form-engine/commit/c68d9ff3a18c8e51786eab897ca7e1eb54de4b42))
* store CompileableObjectDefinition to allow sorting and duplicate keys ([01d05e9](https://bitbucket.org/bitkomponist/form-engine/commit/01d05e9ca627670fb7356afb970ae0083a1eb327))
* validation content type l10n ([db882ea](https://bitbucket.org/bitkomponist/form-engine/commit/db882ea62f7eeb9498bb15065653f5f6bae73d49))
* workflow preloader ([47db243](https://bitbucket.org/bitkomponist/form-engine/commit/47db24343ef0e0e1659d1cf0c1a7151f85097987))

## [1.51.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.50.0...v1.51.0) (2023-01-12)


### Features

* clientOptions server param ([aaaace3](https://bitbucket.org/bitkomponist/form-engine/commit/aaaace3a01400229065280216a1fa86567dc1e07))

## [1.50.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.49.0...v1.50.0) (2023-01-12)


### Features

* configurable mongodb storage adapter ([722cfeb](https://bitbucket.org/bitkomponist/form-engine/commit/722cfebc0c9fa8d49f4eababfc3f7582f0171def))
* mongodb storage adapter ([0697321](https://bitbucket.org/bitkomponist/form-engine/commit/0697321ba3e1d77eda918cf828a70f53c60f2801))


### Bug Fixes

* findargs validation ([aebfd39](https://bitbucket.org/bitkomponist/form-engine/commit/aebfd39e7658ccec063bceae584e3bf66db37328))

## [1.49.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.48.0...v1.49.0) (2023-01-06)


### Features

* raw-string type in create object ([4cc6b44](https://bitbucket.org/bitkomponist/form-engine/commit/4cc6b443749a7a856cf93330f9d401a617224722))


### Bug Fixes

* object-create boolean parsing ([99f910a](https://bitbucket.org/bitkomponist/form-engine/commit/99f910afd0b27eb731e14443f8f85e419a7f7021))
* session drawer error on empty locals ([b751403](https://bitbucket.org/bitkomponist/form-engine/commit/b7514031db9f6c1297412328f4686c7c60a9f5d0))

## [1.48.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.47.0...v1.48.0) (2023-01-06)


### Features

* frontend form blocking / loading indicators ([e8b4d7d](https://bitbucket.org/bitkomponist/form-engine/commit/e8b4d7dd1acafe08c1ebfe0e90888534b5a517a4))


### Bug Fixes

* add missing for on floating labels ([93e369a](https://bitbucket.org/bitkomponist/form-engine/commit/93e369a21a4fe97f02a8660b9c44e3c7e8c56577))
* clean element markup ([39141ec](https://bitbucket.org/bitkomponist/form-engine/commit/39141ecf97cf4970cd1153951bac0a84d1670088))
* controlId duplicate error ([dfd7280](https://bitbucket.org/bitkomponist/form-engine/commit/dfd72808db2813003bbfefe0a69802b6c42e0d09))
* date validation error message date ([61b9cb3](https://bitbucket.org/bitkomponist/form-engine/commit/61b9cb3a2c7544682648ba36a3bd3b2950276be1))
* editor preview scrollbar ([61fc764](https://bitbucket.org/bitkomponist/form-engine/commit/61fc7640d73dcf8e1256665565f384e7cf419c4b))
* markdown compilation ([d5cf66c](https://bitbucket.org/bitkomponist/form-engine/commit/d5cf66ce8b6fd04b361283f0cba7672098579cbd))
* markdown compilation ([d50fab4](https://bitbucket.org/bitkomponist/form-engine/commit/d50fab49c4d0a104436133d53cbd8562cbccadc9))

## [1.47.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.46.0...v1.47.0) (2023-01-05)


### Features

* attachment input error translation ([f3b7e94](https://bitbucket.org/bitkomponist/form-engine/commit/f3b7e940a2d9e7eec0f512acec183e4407b19525))
* attachment input error translation ([a2b7e9a](https://bitbucket.org/bitkomponist/form-engine/commit/a2b7e9a683503092637cc10162a6f2cd10373791))
* migrate validation errors to new utility ([a1e206d](https://bitbucket.org/bitkomponist/form-engine/commit/a1e206db0c80d64c58286e4cb00849ee02cfc28f))

## [1.46.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.45.0...v1.46.0) (2023-01-05)


### Features

* i18n utility ([92bfb4f](https://bitbucket.org/bitkomponist/form-engine/commit/92bfb4f6e4cd052450ef9695e68dd18e815b1d66))

## [1.45.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.44.0...v1.45.0) (2023-01-04)


### Features

* date validation ([0340789](https://bitbucket.org/bitkomponist/form-engine/commit/0340789911e1524f4fff0ef7f62f7092bba84fe7))
* enable error feedback for floating label inputs ([25fd964](https://bitbucket.org/bitkomponist/form-engine/commit/25fd96498cfec12d0589dd3e00f3abfbf2a9d7fd))


### Bug Fixes

* stale session values during validation ([659f765](https://bitbucket.org/bitkomponist/form-engine/commit/659f765963563388a66441c947ca23621141bb71))
* validation content type messages ([f1efc5d](https://bitbucket.org/bitkomponist/form-engine/commit/f1efc5db79ecf941745f2fed7097f0255b02d591))

## [1.44.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.43.0...v1.44.0) (2023-01-04)


### Features

* condition template helper ([a0b1b05](https://bitbucket.org/bitkomponist/form-engine/commit/a0b1b057a6b16728c62ce172ab9a6bcddb2aa388))

## [1.43.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.42.0...v1.43.0) (2023-01-04)


### Features

* expose current session value for debugging ([df941f1](https://bitbucket.org/bitkomponist/form-engine/commit/df941f1e4166d8c6bfb46b582cfc219c871a6121))

## [1.42.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.41.0...v1.42.0) (2023-01-04)


### Features

* language fallback support ([2da5349](https://bitbucket.org/bitkomponist/form-engine/commit/2da53498692f124bc87d3458a1cd02ee73911baf))

## [1.41.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.40.0...v1.41.0) (2023-01-04)


### Features

* $this in templates ([6276001](https://bitbucket.org/bitkomponist/form-engine/commit/627600190c6f594dfcdfcfc23f289f19b2feb447))
* set arbitrary query params as locals ([5fed7ee](https://bitbucket.org/bitkomponist/form-engine/commit/5fed7ee0df69f2b7762e731a687fd8c6b23f0e08))

## [1.40.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.39.0...v1.40.0) (2023-01-03)


### Features

* IETF BCP 47 locales ([7f5f4ee](https://bitbucket.org/bitkomponist/form-engine/commit/7f5f4eefc9231f4e45fe91231c82470cbfdd1458))

## [1.39.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.38.2...v1.39.0) (2023-01-02)


### Features

* highlight touched locales in translate view ([674db10](https://bitbucket.org/bitkomponist/form-engine/commit/674db10b4210f2e92ffb5872b7013ea21823ddc9))


### Bug Fixes

* dont enforce focus on code editor ([3f22178](https://bitbucket.org/bitkomponist/form-engine/commit/3f22178fa53a687dd12bb4cc9922f9becab5abd0))
* history item labels improvements ([7b54c0e](https://bitbucket.org/bitkomponist/form-engine/commit/7b54c0e26caa5e0d7d719c2bc26f9125d86011ba))
* preview spinner scrolling ([acaa980](https://bitbucket.org/bitkomponist/form-engine/commit/acaa980e5f167430d9c9e29b76d76a5392b0416b))
* selection in translate mode ([37e296d](https://bitbucket.org/bitkomponist/form-engine/commit/37e296dea56dd256d8adfaa105af2818e2622eb5))
* show login form regardless of route ([e076716](https://bitbucket.org/bitkomponist/form-engine/commit/e0767167c333d4391da3869d365bcf64c4ed4642))

### [1.38.2](https://bitbucket.org/bitkomponist/form-engine/compare/v1.38.1...v1.38.2) (2023-01-02)


### Bug Fixes

* only emit ready event post initialization ([940717e](https://bitbucket.org/bitkomponist/form-engine/commit/940717e57408d4e350022c9aed10efee531f8cd7))

### [1.38.1](https://bitbucket.org/bitkomponist/form-engine/compare/v1.38.0...v1.38.1) (2022-12-23)


### Bug Fixes

* embed locale ([c31aa82](https://bitbucket.org/bitkomponist/form-engine/commit/c31aa829dc8a939ae373bb2c29f22492bc3dd713))

## [1.38.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.37.0...v1.38.0) (2022-12-23)


### Features

* attachments input progress ([512a5db](https://bitbucket.org/bitkomponist/form-engine/commit/512a5dbe82b61e241a075faca72b5cb16bb803ce))
* bso mask permissions ([dcaecb3](https://bitbucket.org/bitkomponist/form-engine/commit/dcaecb3d5642294738693d676ab1a1f31a66f8dd))
* data source l10n, template helpers ([5233e23](https://bitbucket.org/bitkomponist/form-engine/commit/5233e23b981be749c8f82df02dada7eda49dfebc))
* default select option l10n ([23b3e1a](https://bitbucket.org/bitkomponist/form-engine/commit/23b3e1afbe87e734b95803732bd577fcb758b986))
* embed post data support ([cf74130](https://bitbucket.org/bitkomponist/form-engine/commit/cf74130291312b98f926d47d3e04d7d1bdef3269))
* form ready event ([2805d25](https://bitbucket.org/bitkomponist/form-engine/commit/2805d25f7c43356f8f0b6e58e5bba0123fd106b0))
* map session start node to ready event ([a612909](https://bitbucket.org/bitkomponist/form-engine/commit/a612909a0ddb9e8c674195489412e95de10cc6a1))
* mask property permissions ([b5e6bb8](https://bitbucket.org/bitkomponist/form-engine/commit/b5e6bb88eafdf7b2c5dd235021073caf5af52339))
* modularize form editor ([1eccfe7](https://bitbucket.org/bitkomponist/form-engine/commit/1eccfe71f08bbb408cdb945b2e33866db16e1ba7))
* monaco code editor ([46658bf](https://bitbucket.org/bitkomponist/form-engine/commit/46658bff67988e48f1c76cc3e2645102bc968b10))
* object input progress ([1f7b4a6](https://bitbucket.org/bitkomponist/form-engine/commit/1f7b4a625e1047e73cd447e2c6ef47b464be6455))
* object input progress ([3e1f326](https://bitbucket.org/bitkomponist/form-engine/commit/3e1f326e15dd5b1598be55b3a944f34230ed2e21))
* object input progress ([e7fe86d](https://bitbucket.org/bitkomponist/form-engine/commit/e7fe86d73949d9b04d05955ad4dde197d2e49fbb))
* off canvas code editor ([134695b](https://bitbucket.org/bitkomponist/form-engine/commit/134695bbbe406f1a1aef594849d37b2cb1120faa))
* persist app col state ([afda9eb](https://bitbucket.org/bitkomponist/form-engine/commit/afda9eb9acaa94e2bcb10543200e31d81c645d5c))
* pipable template strings ([870191c](https://bitbucket.org/bitkomponist/form-engine/commit/870191cc863c2a6edf7071e5f2887657e1c74278))
* preview settings progress ([1275314](https://bitbucket.org/bitkomponist/form-engine/commit/1275314e18b42c3d2b0aaec1f8ac099151b322f1))
* property level mask setters ([43859da](https://bitbucket.org/bitkomponist/form-engine/commit/43859da382333ecead2de1c7141bbbcb138c7571))
* read only var wizward ([c6d4afd](https://bitbucket.org/bitkomponist/form-engine/commit/c6d4afdc9a3cae01f16af82f315deefb5f9fb40e))
* session progress ([5718cb3](https://bitbucket.org/bitkomponist/form-engine/commit/5718cb3ac99c572285f721b652ef93be93ab3749))
* skip http datasources in edit mode ([bf06907](https://bitbucket.org/bitkomponist/form-engine/commit/bf0690708d30a3cacb80493c34dcca7ba464ef3c))
* suspend form until global datasources are loaded ([b42a637](https://bitbucket.org/bitkomponist/form-engine/commit/b42a6378c4d8e780f6a541ac66aace9d96fb1cbf))
* translation editor progress ([e54001f](https://bitbucket.org/bitkomponist/form-engine/commit/e54001f87068693e7dc63d820ef8c2b7d4084006))
* translation editor progress ([e5903e3](https://bitbucket.org/bitkomponist/form-engine/commit/e5903e388951df9df43920e9bfaa1594e2025c87))
* variable select, compilable table cells ([32965ce](https://bitbucket.org/bitkomponist/form-engine/commit/32965ceb110b35a4571c414679ffba16006c98eb))


### Bug Fixes

* add missing import ([d19d02f](https://bitbucket.org/bitkomponist/form-engine/commit/d19d02fd987f0e9144d54084716d15334924b2bc))
* allow $ in case formatters ([ad77a90](https://bitbucket.org/bitkomponist/form-engine/commit/ad77a9086bb1aeb20413685d277007fd2492b5d9))
* code editor state leak ([1f7f048](https://bitbucket.org/bitkomponist/form-engine/commit/1f7f048422270fdfef620038ed0819e0a942eedb))
* default Element name, layout tab reset ([98f0272](https://bitbucket.org/bitkomponist/form-engine/commit/98f0272f9c57e3a0ceaa0acfadbec490e37aacc9))
* fix api clients ([c902795](https://bitbucket.org/bitkomponist/form-engine/commit/c902795935ea016501b21a63ecb410c1bd88ca03))
* fix regular form handler ([8fc924f](https://bitbucket.org/bitkomponist/form-engine/commit/8fc924f596fb82cb46354514ef81415e75060d8f))
* form stylesheet editing ([7772445](https://bitbucket.org/bitkomponist/form-engine/commit/7772445830b950e11410d2ed4eed49d9ea904dd1))
* prevent native save dialog on input focussed ([1db4581](https://bitbucket.org/bitkomponist/form-engine/commit/1db4581c18812d45a203ebcf2b2cffe4845122f8))
* temporarily disable form local storage (quota error) ([6bbb440](https://bitbucket.org/bitkomponist/form-engine/commit/6bbb4400a64b245e0b20dd7c531c7ec651113708))
* use memory storage for offline forms ([0b99899](https://bitbucket.org/bitkomponist/form-engine/commit/0b998991c1f90716f1b4b632d6de4320159246e7))

## [1.37.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.36.1...v1.37.0) (2022-12-15)


### Features

* datasource filter & element expressions ([878baf9](https://bitbucket.org/bitkomponist/form-engine/commit/878baf9955e8ac4b25d786370c5f42dd12de8378))
* datasource filters ([077ac63](https://bitbucket.org/bitkomponist/form-engine/commit/077ac63fffbc523cc13a0ab2ea801008fe4b7745))
* enforce resource name formatting ([6730da1](https://bitbucket.org/bitkomponist/form-engine/commit/6730da1a0fc2ffdfbea180ff7821fa36a5f17821))
* enforce title case for selects ([fc70406](https://bitbucket.org/bitkomponist/form-engine/commit/fc704069526478791db6c457a16d3ab9fec29490))
* force name format ([bfb050f](https://bitbucket.org/bitkomponist/form-engine/commit/bfb050f70cd3125d19b6149cabc0e564f0001ad1))
* variable insertion wizard ([8ac5385](https://bitbucket.org/bitkomponist/form-engine/commit/8ac5385cdbfc627c2781e8dd7d034f0911ed6acb))


### Bug Fixes

* expressions value options prop ([b990618](https://bitbucket.org/bitkomponist/form-engine/commit/b990618df22fd694b001f6fcef4d6c15fc40855b))
* select lowercase labels, select zindex ([3b822d6](https://bitbucket.org/bitkomponist/form-engine/commit/3b822d620d4c4254714f0b34f719ad525ba2431b))

### [1.36.1](https://bitbucket.org/bitkomponist/form-engine/compare/v1.36.0...v1.36.1) (2022-12-13)


### Bug Fixes

* omit bso auth data on client ([1da2cb6](https://bitbucket.org/bitkomponist/form-engine/commit/1da2cb61cbcd135e3a1c20b1f2801bd6ed3e67c4))

## [1.36.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.35.2...v1.36.0) (2022-12-13)


### Features

* create object utility ([1e6ad5c](https://bitbucket.org/bitkomponist/form-engine/commit/1e6ad5c99bb597afd59778905eacf6c504bf711d))
* cxp collections ([46e571b](https://bitbucket.org/bitkomponist/form-engine/commit/46e571b9eef95e1f90fba09d3817ee27de5b0f50))
* form editor defaults ([e7172ca](https://bitbucket.org/bitkomponist/form-engine/commit/e7172ca2f4cf66af2ff2a35ba15c7014158d58b8))
* updated cxp-api-client ([46d95e0](https://bitbucket.org/bitkomponist/form-engine/commit/46d95e0e60bf1481b75514d9733d7301064ba3b9))
* visualize transparent elements in editor ([37ae4a7](https://bitbucket.org/bitkomponist/form-engine/commit/37ae4a7e75efcbc9a00a9fe0b1dde201882f2b00))
* workflow history state sync ([77ec83d](https://bitbucket.org/bitkomponist/form-engine/commit/77ec83d5a19fe1c54e57e4f68043c0b128d747f0))


### Bug Fixes

* app state and hotkeys ([3b78f01](https://bitbucket.org/bitkomponist/form-engine/commit/3b78f01072ec1bc21c2d4f5f481d4794df0464f5))
* app tabs state leak ([20ee620](https://bitbucket.org/bitkomponist/form-engine/commit/20ee620cda81c7079f46d03432fd3f05b4d16e6e))
* set loading by default in useEntity ([b528047](https://bitbucket.org/bitkomponist/form-engine/commit/b5280473bc4343e4e969885c91a3528df4ddab4e))

### [1.35.2](https://bitbucket.org/bitkomponist/form-engine/compare/v1.35.1...v1.35.2) (2022-12-06)


### Bug Fixes

* faulty build command ([b82619f](https://bitbucket.org/bitkomponist/form-engine/commit/b82619f4beb1693541ff9dcb23edecdd8b569cd1))
* flow legacy compat fix ([65af623](https://bitbucket.org/bitkomponist/form-engine/commit/65af6237f6aaf4e5d6a35ab60a08ab8df6d18dcb))

### [1.35.1](https://bitbucket.org/bitkomponist/form-engine/compare/v1.35.0...v1.35.1) (2022-12-05)


### Bug Fixes

* update client dist ([a33bc2e](https://bitbucket.org/bitkomponist/form-engine/commit/a33bc2e373f36c1956cfb2a6953e41809f21be4f))

## [1.35.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.34.1...v1.35.0) (2022-12-05)


### Features

* logical condition node ([0136cd9](https://bitbucket.org/bitkomponist/form-engine/commit/0136cd98b24330b06c374b74c6a76c7fa5a7889a))

### [1.34.1](https://bitbucket.org/bitkomponist/form-engine/compare/v1.34.0...v1.34.1) (2022-12-05)


### Bug Fixes

* rename wfl signal package ([a38b11b](https://bitbucket.org/bitkomponist/form-engine/commit/a38b11b01c03e995af4657b121ad3b3b46751d5b))

## [1.34.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.33.2...v1.34.0) (2022-12-05)


### Features

* build ([4842185](https://bitbucket.org/bitkomponist/form-engine/commit/48421857b84fbd7ff341dafe41780ad458786ef5))
* disable legacy workflow ([f5ad2e3](https://bitbucket.org/bitkomponist/form-engine/commit/f5ad2e39f226cc4229584079f4cceb29c2720da1))
* workflow editor 2 progress ([28572d5](https://bitbucket.org/bitkomponist/form-engine/commit/28572d59cc24603884f176c093cca0000907620e))
* workflow editor 2 progress, form studio refactor ([d6a6d88](https://bitbucket.org/bitkomponist/form-engine/commit/d6a6d88f02487ab9c5f720753b82ce43327eb4d0))
* workflow editor progress ([0ac33ec](https://bitbucket.org/bitkomponist/form-engine/commit/0ac33ec9edbddef8c8b4216972e19296fffb83e1))


### Bug Fixes

* workflow editor paneling ([7390c0d](https://bitbucket.org/bitkomponist/form-engine/commit/7390c0d9b93b59948039c16ca353a15a8ad338bd))

### [1.33.2](https://bitbucket.org/bitkomponist/form-engine/compare/v1.33.1...v1.33.2) (2022-09-08)


### Bug Fixes

* form listing sorting labels ([1f7649c](https://bitbucket.org/bitkomponist/form-engine/commit/1f7649c461742df91d158720cee764864ddc9a49))
* get-form handler refactor ([c39de22](https://bitbucket.org/bitkomponist/form-engine/commit/c39de2229b1ac81c51d6dda59da0a4ee32677d39))

### [1.33.1](https://bitbucket.org/bitkomponist/form-engine/compare/v1.33.0...v1.33.1) (2022-09-08)


### Bug Fixes

* api model list verbose param ([67314cb](https://bitbucket.org/bitkomponist/form-engine/commit/67314cb8c86511a94cc7895b56e09dc5da883e8b))
* form listing sorting ([4ddab5e](https://bitbucket.org/bitkomponist/form-engine/commit/4ddab5eedefc708b8c8cbe154151ed4420e8e592))
* form slug resolving ([c240865](https://bitbucket.org/bitkomponist/form-engine/commit/c240865093d02155e7caac39cba08dd0d600a885))

## [1.33.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.32.0...v1.33.0) (2022-09-07)


### Features

* form versioning backend ([df969f5](https://bitbucket.org/bitkomponist/form-engine/commit/df969f5811e7ce18c751480eed98ea42c0d4bd9c))
* version history dialoge ([d2fad34](https://bitbucket.org/bitkomponist/form-engine/commit/d2fad3405bad114a38112558229147242ee43a7d))

## [1.32.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.31.0...v1.32.0) (2022-09-06)


### Features

* clear history hook ([58eeb23](https://bitbucket.org/bitkomponist/form-engine/commit/58eeb237a769aec7e1f670050239427f02537523))
* history progress ([1d56361](https://bitbucket.org/bitkomponist/form-engine/commit/1d563617e77633986e4f55c3dc81a67f1a1e6bc9))
* history size limit, history ignore rules ([9fdab72](https://bitbucket.org/bitkomponist/form-engine/commit/9fdab72b3293d23c2bd7e98a748f3f51ded57fe0))
* show current mask type for wfl's and datasources ([fbcb34c](https://bitbucket.org/bitkomponist/form-engine/commit/fbcb34c845106fe4fd33f37e23fc587290c46050))
* undo redo history ([28c274e](https://bitbucket.org/bitkomponist/form-engine/commit/28c274e6efe6841225098953042fbf478d04e114))


### Bug Fixes

* CXP Api Datasource Metadata ([65bbe96](https://bitbucket.org/bitkomponist/form-engine/commit/65bbe96ff1850e889e8765c0518fad1e03b0b7f6))
* fix redo history clearing and redo diffs ([11cc926](https://bitbucket.org/bitkomponist/form-engine/commit/11cc926591e563cd478bbd1168b41a45b50fc652))
* use partNumber as value column for diagnostic report products ([34a1ab9](https://bitbucket.org/bitkomponist/form-engine/commit/34a1ab93e7949a06f08e636e7cb9fa19cbc8ad1b))

## [1.31.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.30.0...v1.31.0) (2022-09-02)


### Features

* button element improvements ([d7b20ed](https://bitbucket.org/bitkomponist/form-engine/commit/d7b20ed63224490acc6ffd27dd5115fae43d29d1))
* internal / hidden elements ([265f3ac](https://bitbucket.org/bitkomponist/form-engine/commit/265f3acc401fccb868386cd9f925ba84f414ce2f))


### Bug Fixes

* drawer button refresh on drop issue ([9971117](https://bitbucket.org/bitkomponist/form-engine/commit/99711178d96806d672ed1b200164638c71f21b98))

## [1.30.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.29.0...v1.30.0) (2022-08-31)


### Features

* custom alert, form 404 display ([f995cd1](https://bitbucket.org/bitkomponist/form-engine/commit/f995cd1a738a75af622c7f87420fbde40e56c311))
* improved alert styling ([ea0bbc1](https://bitbucket.org/bitkomponist/form-engine/commit/ea0bbc14121d134bfa6763778ffb3374689ec6ee))


### Bug Fixes

* decrease body parser to aws lambda limit ([ae8ac99](https://bitbucket.org/bitkomponist/form-engine/commit/ae8ac99f90a283f03b4ade49b6804b025932a933))
* workflow mount stability fix ([c29d063](https://bitbucket.org/bitkomponist/form-engine/commit/c29d06329177ccd0708b571de5534153107a7e4c))

## [1.29.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.28.2...v1.29.0) (2022-08-29)


### Features

* attachment input ([6d7c4ae](https://bitbucket.org/bitkomponist/form-engine/commit/6d7c4aea8ff5ecd8c9e94b2f999a030d459228e0))
* attachments progress & element controllers ([c2ef536](https://bitbucket.org/bitkomponist/form-engine/commit/c2ef536535bdd4e7f6b9fd9c36000419f576250e))
* bso cxp api client ([6480492](https://bitbucket.org/bitkomponist/form-engine/commit/6480492ba7c6d16511f3c39b7184b59df0712a0d))
* bso cxp attachments client ([0bd4d27](https://bitbucket.org/bitkomponist/form-engine/commit/0bd4d27e51f629f45a47b5bc43bd6b2cef183637))
* bso-core package ([a4e5b23](https://bitbucket.org/bitkomponist/form-engine/commit/a4e5b239c8b19745ad85c6ae97b66235a323c82d))
* component extension api ([93b238b](https://bitbucket.org/bitkomponist/form-engine/commit/93b238b1f22f700b49d170a71d7009e6a43468cd))
* cxp api wfl & datasource ([a165068](https://bitbucket.org/bitkomponist/form-engine/commit/a165068273741c278918e3862bf1e0692183cccb))
* generated api backends and server configurations ([f38c002](https://bitbucket.org/bitkomponist/form-engine/commit/f38c002d5751b03efb7e0f0e29a250b5958dbbbf))
* session error handling ([2b435df](https://bitbucket.org/bitkomponist/form-engine/commit/2b435df3948d9d03a53635ef9133e4669daf0bcc))
* update cxp api code ([27ad1dc](https://bitbucket.org/bitkomponist/form-engine/commit/27ad1dc2a857adda8c33ac2ed4ad401f6b02858f))

### [1.28.2](https://bitbucket.org/bitkomponist/form-engine/compare/v1.28.1...v1.28.2) (2022-08-23)


### Bug Fixes

* auth header compilation ([7ba4f26](https://bitbucket.org/bitkomponist/form-engine/commit/7ba4f26b67de57bc858e8a649321950f291bf532))

### [1.28.1](https://bitbucket.org/bitkomponist/form-engine/compare/v1.28.0...v1.28.1) (2022-08-23)


### Bug Fixes

* use correct datasourceBackendRoute ([8676f23](https://bitbucket.org/bitkomponist/form-engine/commit/8676f236a9c05048e412b6f41f8d323efc555c2a))

## [1.28.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.27.0...v1.28.0) (2022-08-23)


### Features

* bso diagnostic report datasource ([7e9dee9](https://bitbucket.org/bitkomponist/form-engine/commit/7e9dee9da9164137b8e98748b2005049b95d3749))
* page bounds mask ([bf427ae](https://bitbucket.org/bitkomponist/form-engine/commit/bf427ae121dad2f492e01340d0f6dc5191a6aa30))
* unified service side datasource fetching, unified session locals ([639a638](https://bitbucket.org/bitkomponist/form-engine/commit/639a6388723a86694681b55b89a5a8a36ec28c9d))

## [1.27.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.26.0...v1.27.0) (2022-08-22)


### Features

* form import ([46f16d3](https://bitbucket.org/bitkomponist/form-engine/commit/46f16d3188f0ea4b18a1068c9b55146e6eca2cd6))
* thumbnail generation ([af48b32](https://bitbucket.org/bitkomponist/form-engine/commit/af48b32aa0d5be55f19b5cfe90979f2dfa7ee9e2))

## [1.26.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.25.0...v1.26.0) (2022-08-21)


### Features

* form list controls ([63e0614](https://bitbucket.org/bitkomponist/form-engine/commit/63e0614582e6d03313cc6b718c19e43e995c75ad))
* hub header component ([8513bfe](https://bitbucket.org/bitkomponist/form-engine/commit/8513bfe4cca6204aeb3772c20467a5567f857d9d))
* mem-storage filtering, sorting and attribute picking ([4835d2a](https://bitbucket.org/bitkomponist/form-engine/commit/4835d2aa7c41d86a3d5e804ce4cecb96f1dcda7d))
* tags builder mask property ([6e6d44e](https://bitbucket.org/bitkomponist/form-engine/commit/6e6d44e23fd79554956e6252400422fc31dbc4e5))


### Bug Fixes

* unignore typings, add identity fields ([562b9b0](https://bitbucket.org/bitkomponist/form-engine/commit/562b9b0539aaaa3a3283a0bfc45abf4a95924cd7))

## [1.25.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.24.2...v1.25.0) (2022-08-19)


### Features

* mem storage schema enforcement ([3505a9f](https://bitbucket.org/bitkomponist/form-engine/commit/3505a9ffac81dc22df02512bf7c254562959f3c0))


### Bug Fixes

* add owner field to form definition ([6c7f0ea](https://bitbucket.org/bitkomponist/form-engine/commit/6c7f0ea6b123c508b1317e2a9ccda90a00a31734))
* build targets ([8b04000](https://bitbucket.org/bitkomponist/form-engine/commit/8b0400049e2d0d37124261dbe3e294984ed1554c))
* styling fixes ([95923d3](https://bitbucket.org/bitkomponist/form-engine/commit/95923d36a4c4f226a43575c2eed81be0b67fee56))

### [1.24.2](https://bitbucket.org/bitkomponist/form-engine/compare/v1.24.1...v1.24.2) (2022-08-19)

### [1.24.1](https://bitbucket.org/bitkomponist/form-engine/compare/v1.24.0...v1.24.1) (2022-08-19)


### Bug Fixes

* add missing form schema fields ([7349267](https://bitbucket.org/bitkomponist/form-engine/commit/73492673c93a2fd22952904b7ab8f145991da67d))

## [1.24.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.23.2...v1.24.0) (2022-08-19)


### Features

* trusted origins and public url override ([e34b803](https://bitbucket.org/bitkomponist/form-engine/commit/e34b8035d6bf498a832063568245bd06fe1994d2))

### [1.23.2](https://bitbucket.org/bitkomponist/form-engine/compare/v1.23.1...v1.23.2) (2022-08-19)


### Bug Fixes

* url joining and wfl cors ([9c8f224](https://bitbucket.org/bitkomponist/form-engine/commit/9c8f224f8ae9bde431150d41f5e33f0130f85c88))

### [1.23.1](https://bitbucket.org/bitkomponist/form-engine/compare/v1.23.0...v1.23.1) (2022-08-18)


### Bug Fixes

* wflBackendRoute ([6aff6a2](https://bitbucket.org/bitkomponist/form-engine/commit/6aff6a231662ce84095a1c4e6afe01b59bd38785))

## [1.23.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.22.5...v1.23.0) (2022-08-18)


### Features

* assetsBasePath for server ([8abd96e](https://bitbucket.org/bitkomponist/form-engine/commit/8abd96e3aefbad2d98a9360bfef601abf5c971a7))


### Bug Fixes

* use assetsPath for app icons ([9fca34f](https://bitbucket.org/bitkomponist/form-engine/commit/9fca34f27e06a3be19801c69161af556b162ebd0))

### [1.22.5](https://bitbucket.org/bitkomponist/form-engine/compare/v1.22.4...v1.22.5) (2022-08-18)

### [1.22.4](https://bitbucket.org/bitkomponist/form-engine/compare/v1.22.3...v1.22.4) (2022-08-18)


### Bug Fixes

* fix view engine ([853c022](https://bitbucket.org/bitkomponist/form-engine/commit/853c022fae0524708d9706870d14c2e77db7fd19))

### [1.22.3](https://bitbucket.org/bitkomponist/form-engine/compare/v1.22.2...v1.22.3) (2022-08-18)


### Bug Fixes

* include view engine in bundle ([7e4f0ae](https://bitbucket.org/bitkomponist/form-engine/commit/7e4f0aedae392f49c14ed7928a4afa3ee5e5dcbd))

### [1.22.2](https://bitbucket.org/bitkomponist/form-engine/compare/v1.22.1...v1.22.2) (2022-08-18)

### [1.22.1](https://bitbucket.org/bitkomponist/form-engine/compare/v1.22.0...v1.22.1) (2022-08-18)


### Bug Fixes

* export server as library ([af82859](https://bitbucket.org/bitkomponist/form-engine/commit/af828599f0559d1d160f99e35808e02ec513556c))

## [1.22.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.21.0...v1.22.0) (2022-08-18)


### Features

* add compilation targets ([b53ee00](https://bitbucket.org/bitkomponist/form-engine/commit/b53ee00d62da0f0516fc51cb0dd3e00a0be790ac))

## [1.21.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.20.0...v1.21.0) (2022-08-18)


### Features

* backend api progress ([e68e27e](https://bitbucket.org/bitkomponist/form-engine/commit/e68e27ead33affcf0ea8d20e3dbfc994f1fb3648))
* build targets ([cbdfba6](https://bitbucket.org/bitkomponist/form-engine/commit/cbdfba6d7bfeace5787d759aac755b6b83e9e6a5))
* form-embed ([176d261](https://bitbucket.org/bitkomponist/form-engine/commit/176d261f55fbbe0e4fa550af39f46f4c138cfa98))
* initial server package ([c1c1e50](https://bitbucket.org/bitkomponist/form-engine/commit/c1c1e5073fcb0e10c0c1a084dab2b6151811ade9))
* logging and error handling improvments ([bbc6a00](https://bitbucket.org/bitkomponist/form-engine/commit/bbc6a00a0c50e7589584a1f56d28d712148720af))
* mem-storage persistence ([e125ce4](https://bitbucket.org/bitkomponist/form-engine/commit/e125ce47d531d8389efe8c9aa00f0219b45f6892))
* refactor Backend => ServerApplication ([b6e9160](https://bitbucket.org/bitkomponist/form-engine/commit/b6e9160a7823af78bbe1feec4f9d6eb0baf3341b))
* server http request wfl ([b4eab58](https://bitbucket.org/bitkomponist/form-engine/commit/b4eab583f56a742d33d784750730678470b5c8fc))
* server plugins ([4597f2e](https://bitbucket.org/bitkomponist/form-engine/commit/4597f2e934ebf445a9e862ff90191dc036951444))
* session log ss-wfl ([5eba1c6](https://bitbucket.org/bitkomponist/form-engine/commit/5eba1c6ecfb71cc693343895c103db55e6cb40d0))
* set handlers on router ([86f7761](https://bitbucket.org/bitkomponist/form-engine/commit/86f7761d6d746fcb39d73c5a85c039f239f17449))
* storage adapter pattern & ss datasources ([7a9e48c](https://bitbucket.org/bitkomponist/form-engine/commit/7a9e48c4eed92c8fad013405068bebf01c7bd34b))


### Bug Fixes

* api slugs ([99b8c2a](https://bitbucket.org/bitkomponist/form-engine/commit/99b8c2a6c815d3d4ec1bce1a605443d7694e1572))
* datasource fetching ([bd86f7c](https://bitbucket.org/bitkomponist/form-engine/commit/bd86f7c73eac45099c483e0ce788b51757d4f8a9))
* max body size, default button action ([d1603e2](https://bitbucket.org/bitkomponist/form-engine/commit/d1603e2dc89a74d84397dff981f2fcd33eb2d61e))
* use correct interface for server plugins ([f5b4975](https://bitbucket.org/bitkomponist/form-engine/commit/f5b497534194fa96e57321c7322e95b61f8168ea))

## [1.20.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.19.0...v1.20.0) (2022-08-10)


### Features

* basic scope explorer ([65ecdb4](https://bitbucket.org/bitkomponist/form-engine/commit/65ecdb4d7227417273a2af3f292bddd566fdd973))
* custom preview variables ([9aa720f](https://bitbucket.org/bitkomponist/form-engine/commit/9aa720fd231f7479e43e30b76cceeb800ed9b130))
* datasource constants and jwt datasource ([728e0f9](https://bitbucket.org/bitkomponist/form-engine/commit/728e0f9de8bf60fca51e0e1228ffacf0228c13de))
* datasource invalidation ([b003085](https://bitbucket.org/bitkomponist/form-engine/commit/b00308531961cfa13efd61707b74b1357c00817a))
* generic tree component, scope tree ([2119e42](https://bitbucket.org/bitkomponist/form-engine/commit/2119e427f5a55110649992bcac3d0ceeb43b7f0f))
* initial bso diagnostic report datasource ([838420d](https://bitbucket.org/bitkomponist/form-engine/commit/838420d0b0c8dde8f7855d4945637268a9df2641))
* jwt reader preset ([c314688](https://bitbucket.org/bitkomponist/form-engine/commit/c314688ee80383e8ce79ae56e0d1c636393bac63))
* redirect wfl ([c4b067f](https://bitbucket.org/bitkomponist/form-engine/commit/c4b067fbdf26dbfe27b64aa04235a68805a7d875))
* unmount form remote handling ([7691e2a](https://bitbucket.org/bitkomponist/form-engine/commit/7691e2aae973e728e65a410a51a257e9de5d7211))


### Bug Fixes

* app tabbar jankiness ([70ac64e](https://bitbucket.org/bitkomponist/form-engine/commit/70ac64e9f30eb020e08bd8322cdd714431d5c32b))
* change redirect icon ([af3ba23](https://bitbucket.org/bitkomponist/form-engine/commit/af3ba2369cfcf2b7583cf5e4ac7537bd0c6b0828))
* hotkeys ([bea1f05](https://bitbucket.org/bitkomponist/form-engine/commit/bea1f05e12f86c47d8febd5b52f6b4502befb3fe))
* input element props leak ([8fed81e](https://bitbucket.org/bitkomponist/form-engine/commit/8fed81ef8460bd41f4f78d44a81b9668ca2c1577))
* l10n faulty response ([dcf0162](https://bitbucket.org/bitkomponist/form-engine/commit/dcf016258329e839c1ab1e8adce614b35cc9cc88))
* missing spinner on new form tab ([b811ad2](https://bitbucket.org/bitkomponist/form-engine/commit/b811ad26bfd5042d7619e654eaf8cc61b3ee97f7))
* preview settings debounce issue ([6a7a1a8](https://bitbucket.org/bitkomponist/form-engine/commit/6a7a1a818e77af787be4b4ac6bd676febdc2ed6f))
* rename scope to session drawer ([40dd644](https://bitbucket.org/bitkomponist/form-engine/commit/40dd64427b68050ed44b136d33be3fe86447999c))
* sanitize checkbox values ([52545b7](https://bitbucket.org/bitkomponist/form-engine/commit/52545b75f9adc7e8baff5b01e118e4b9c99e39d7))
* set default element name ([52b5e1b](https://bitbucket.org/bitkomponist/form-engine/commit/52b5e1b3942e894120d5d9c9dab28e338721dda8))
* SpinnerBoundary jankiness ([78afd0f](https://bitbucket.org/bitkomponist/form-engine/commit/78afd0f832a766910d369158b15ac9b8fa86a0ce))
* string property state leak ([e48f797](https://bitbucket.org/bitkomponist/form-engine/commit/e48f797e638376f1551516762c9c6d19c88dce6d))
* write editor state to correct id ([14f874b](https://bitbucket.org/bitkomponist/form-engine/commit/14f874b04a0b1baf66237bb257b7d2bc4be09f5a))
* y gap on explorer items ([9d171f6](https://bitbucket.org/bitkomponist/form-engine/commit/9d171f63b69b6be903d580381a412e91d14e96db))

## [1.19.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.18.1...v1.19.0) (2022-07-21)


### Features

* add prop-types as dependencies ([2366597](https://bitbucket.org/bitkomponist/form-engine/commit/23665973cb0bf178aeb1fa56892f52df1f28a818))
* app navbar, builder card migration ([91406bd](https://bitbucket.org/bitkomponist/form-engine/commit/91406bd1d881a0d631c70518afe1937bef1c0db0))
* array form control migration ([0169fda](https://bitbucket.org/bitkomponist/form-engine/commit/0169fda730a33c98e2cf0018ad9f723afa54aadf))
* builder app col and resizable migration ([19790cf](https://bitbucket.org/bitkomponist/form-engine/commit/19790cfe277d645126acc03596bb80e9a0529d41))
* builder components migration progress ([2e65ddf](https://bitbucket.org/bitkomponist/form-engine/commit/2e65ddf0b7ffacf9d868eacea1350fcc97afe74e))
* Builder ts migration progress ([d8e7888](https://bitbucket.org/bitkomponist/form-engine/commit/d8e7888ce0f7c7a0cebe669110e9dcb3d720cbd3))
* Container migration to ts ([d97a85c](https://bitbucket.org/bitkomponist/form-engine/commit/d97a85cebe5c74a7052d30cf99c42eaf4778f20f))
* core hooks migration progress ([2782344](https://bitbucket.org/bitkomponist/form-engine/commit/2782344ab2c110ac726f8d429c2f58c85912f63d))
* **editor:** delete selection with backspace ([3427a46](https://bitbucket.org/bitkomponist/form-engine/commit/3427a4632555a3f6f61c94c17650118ae1ddb27b))
* **editor:** form export ([11560e9](https://bitbucket.org/bitkomponist/form-engine/commit/11560e9e7744e4e2e4f546c5f2e8204e123836d4))
* elements migration ([5534dd0](https://bitbucket.org/bitkomponist/form-engine/commit/5534dd035a7016411d31e0f5b73e11f1556c75d5))
* form-editor components migration ([0e98098](https://bitbucket.org/bitkomponist/form-engine/commit/0e980988cf00a14391b4b7292e476b86ef640c0e))
* formatting ([c429f47](https://bitbucket.org/bitkomponist/form-engine/commit/c429f47d156b63552887ea5a3600f428a288263e))
* FormEngineElement ts migration ([a79685c](https://bitbucket.org/bitkomponist/form-engine/commit/a79685c95116368fd63b85439328932048f1c71d))
* globally use .tsx extension ([b95682a](https://bitbucket.org/bitkomponist/form-engine/commit/b95682abf655b5f4b19ad09663412711769ce71f))
* IconButton proptypes and story ([852471b](https://bitbucket.org/bitkomponist/form-engine/commit/852471b59a14058cf6508b21cd7057071a0c8b64))
* initial storybook setup ([4770d96](https://bitbucket.org/bitkomponist/form-engine/commit/4770d964e1fe98e8ba494dc4a0e4876eefe6b0b2))
* initial ts setup, migrate core application index to ts ([9b16825](https://bitbucket.org/bitkomponist/form-engine/commit/9b16825f24d807c446e6acce5666772ab5576df3))
* linting and new build ([c3e38d0](https://bitbucket.org/bitkomponist/form-engine/commit/c3e38d05a4ae7f5e218fb205deb67f9cc2fa79d8))
* mask migration progress ([dd17281](https://bitbucket.org/bitkomponist/form-engine/commit/dd1728117fc4a3007641ed153ccdc8c382ca6ccd))
* migrate builder app, dialog, modal ([4568b5a](https://bitbucket.org/bitkomponist/form-engine/commit/4568b5a89c7a3832126e9a1cfa99330e6a38126a))
* migrate builder cursor ([9e1fa21](https://bitbucket.org/bitkomponist/form-engine/commit/9e1fa214fa2ab2f2676a8659f93092ce395e61ef))
* migrate builder hub ([d657536](https://bitbucket.org/bitkomponist/form-engine/commit/d657536af911f0d6d55f01c7d32fecfd970711d8))
* migrate builder table ([9000b50](https://bitbucket.org/bitkomponist/form-engine/commit/9000b50d76385f6fb9fc55a041fe7c872827b842))
* migrate engine to ts ([b08bfdd](https://bitbucket.org/bitkomponist/form-engine/commit/b08bfdd8621cf1927af13a3565fba143644dafe8))
* migrate form editor actions ([df92174](https://bitbucket.org/bitkomponist/form-engine/commit/df921746974db9686a00e36e9aede0ba87992426))
* migrate icon button ([44d24cf](https://bitbucket.org/bitkomponist/form-engine/commit/44d24cf4fcd8cda9134e281fb5260a74e59fe442))
* migrate l10n datasource ([f515e4d](https://bitbucket.org/bitkomponist/form-engine/commit/f515e4d58079cf783476b3341b619e8eb8ff81e9))
* migrate l10n to ts ([7132dc5](https://bitbucket.org/bitkomponist/form-engine/commit/7132dc56241eff3b1d0e267fabcd5c3b5a7dc176))
* migrate plugin definitions ([3d32c42](https://bitbucket.org/bitkomponist/form-engine/commit/3d32c42ede65f81ff5835c7c4cea984e31bc8363))
* migrate plugin dictionaries to ts ([8f7beed](https://bitbucket.org/bitkomponist/form-engine/commit/8f7beed9ae277683097a6fc4053167138013a73f))
* migrate spinner boundary and data source table ([5d29b8b](https://bitbucket.org/bitkomponist/form-engine/commit/5d29b8b8403522490e14cb85553aa2b6003996db))
* migrate utils package to ts ([1e28bb9](https://bitbucket.org/bitkomponist/form-engine/commit/1e28bb9b0bf686d653379c13a4ef4c4ed676ec6b))
* migrate wfls and datasources ([a2f2436](https://bitbucket.org/bitkomponist/form-engine/commit/a2f2436198aa58b99cb5f22145a2f450fb96f98f))
* refactor core modules ([904a387](https://bitbucket.org/bitkomponist/form-engine/commit/904a387c3087b34b5919164b119921f5c3122e02))
* remove glob resolver dependency for plugins ([11dcb83](https://bitbucket.org/bitkomponist/form-engine/commit/11dcb8386b9ad997f55c5b9f7433ef911ef1830c))
* shadow-portal ts migration ([085e037](https://bitbucket.org/bitkomponist/form-engine/commit/085e0378de33f067d2fac27bece57af64e694853))
* skip scss bundling in storybook ([445cd49](https://bitbucket.org/bitkomponist/form-engine/commit/445cd495d2c1e49b327c5e1cc2512f47f2038a24))
* ts linter setup ([10a2e0f](https://bitbucket.org/bitkomponist/form-engine/commit/10a2e0f26f14c03ade8ac7351c7af2b0ebe4cd8b))
* update bootstrap@5.1.3 ([229bc83](https://bitbucket.org/bitkomponist/form-engine/commit/229bc8305acb41676a6bc71261bf063255e55fb1))
* update dev dependencies ([4bf43e4](https://bitbucket.org/bitkomponist/form-engine/commit/4bf43e43d2fab59ff09b783a4e2d3bbdd9cfd10d))
* update papaparse@5.3.2 ([e30f219](https://bitbucket.org/bitkomponist/form-engine/commit/e30f2196e20b4dbaed352e27ecb1bb1280e59d83))
* update react-bootstrap-icons@1.8.4 ([021cc24](https://bitbucket.org/bitkomponist/form-engine/commit/021cc24efffe25acb14317db7f1bcb9cce868e72))
* update react-bootstrap@2.4.0 ([344dd2d](https://bitbucket.org/bitkomponist/form-engine/commit/344dd2db6afbf7be671844464daedadc355baa49))
* update react-dom@18.2.0 ([12b4bca](https://bitbucket.org/bitkomponist/form-engine/commit/12b4bcae26d335daa5acac4d6a4983d5bb2485fe))
* update react-hotkeys-hook@3.4.6 ([ad7a024](https://bitbucket.org/bitkomponist/form-engine/commit/ad7a0243b02eb369c75dd4691593b09fbfc56e8f))
* update react-mde@11.5.0 ([81cfad7](https://bitbucket.org/bitkomponist/form-engine/commit/81cfad741714e1403f15ea48f50da82083728f1b))
* update react-router-dom@6.3.0 ([42d32ed](https://bitbucket.org/bitkomponist/form-engine/commit/42d32ed1de1e45fd5eed1a1ead84c1f09478ab38))
* update react-select@5.4.0 ([43ec066](https://bitbucket.org/bitkomponist/form-engine/commit/43ec066dd325902e975e1bc45d5c73299fe69fa9))
* update react-toastify@9.0.5 ([c4cb7e7](https://bitbucket.org/bitkomponist/form-engine/commit/c4cb7e7b9664150a26151f81e23409f94dcd6a54))
* update react-window@1.8.7 ([3cdb829](https://bitbucket.org/bitkomponist/form-engine/commit/3cdb829079fdd5531986a340fd45efeeb8d60a79))
* update react@18.2.0 ([329a738](https://bitbucket.org/bitkomponist/form-engine/commit/329a7386d20b7fb4a2be6a81f53954591b4f1101))
* update showdown@2.1.0 ([fca9651](https://bitbucket.org/bitkomponist/form-engine/commit/fca9651e9db3278e72c96c494aab66d71dc43cb5))
* upgrade lodash.get ([068a502](https://bitbucket.org/bitkomponist/form-engine/commit/068a50252670ba43ae000332c361374f2b59934c))
* use react 18 renderer ([7d5ca83](https://bitbucket.org/bitkomponist/form-engine/commit/7d5ca83a6868255881b358067a60a481f7bb843d))
* validations migration ([f1fd67a](https://bitbucket.org/bitkomponist/form-engine/commit/f1fd67a31c7f4348304250646647474bfc602820))
* validations ts migration ([ef82545](https://bitbucket.org/bitkomponist/form-engine/commit/ef8254512db6b7ac8062528ea5e667a189626694))
* wfl & ds plugin defs in ts ([54135d1](https://bitbucket.org/bitkomponist/form-engine/commit/54135d1193a2209542ee62f117f74ec96bb49b93))


### Bug Fixes

* data table props ([52300de](https://bitbucket.org/bitkomponist/form-engine/commit/52300de2d927f3b0ebe931169a5006dc4ae4b721))
* disable search ([8d0d6f3](https://bitbucket.org/bitkomponist/form-engine/commit/8d0d6f356efe9da662c4fd2573d546a6c4b43dcf))
* Fix dawer button import issues ([022fd44](https://bitbucket.org/bitkomponist/form-engine/commit/022fd443dc02fd074429a9067776e4546c8c199f))
* fix missing uuid on new elements ([2d1894e](https://bitbucket.org/bitkomponist/form-engine/commit/2d1894e20227c3910b51f309afa8e53232c09d6e))
* rename workflow layers folder ([7200c0d](https://bitbucket.org/bitkomponist/form-engine/commit/7200c0dd1a7d811a1652b6a7c6ce8469d2195bb5))
* rename workflow layers folder ([b4cefa6](https://bitbucket.org/bitkomponist/form-engine/commit/b4cefa61a34562c2730ab57fcf6d44906a5d5386))
* workflow infinite initial state leak ([1f69dbe](https://bitbucket.org/bitkomponist/form-engine/commit/1f69dbef84e98f919f4c74ce72f1d8227c614f97))

### [1.18.1](https://bitbucket.org/bitkomponist/form-engine/compare/v1.18.0...v1.18.1) (2022-05-11)


### Bug Fixes

* include bs variables in host stylesheet ([ac8388a](https://bitbucket.org/bitkomponist/form-engine/commit/ac8388adc6d6323106f6f466afc69192a821e30f))

## [1.18.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.17.0...v1.18.0) (2022-05-10)


### Features

* lint & format ([eaecea6](https://bitbucket.org/bitkomponist/form-engine/commit/eaecea625d1626d6b2a58cd7515ccab7a1be8168))


### Bug Fixes

* facette search styling ([10f48be](https://bitbucket.org/bitkomponist/form-engine/commit/10f48bea3ad69503b81ac1803e7c20209bf8551c))

## [1.17.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.16.0...v1.17.0) (2022-05-10)


### Features

* optional bootsrap resources inclusion ([27863ed](https://bitbucket.org/bitkomponist/form-engine/commit/27863ed1f3fd9358ef0d6dbfbcc38da02bcadab5))

## [1.16.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.15.0...v1.16.0) (2022-05-10)


### Features

* **embeds:** embed builder & inline embedding ([605601d](https://bitbucket.org/bitkomponist/form-engine/commit/605601dd8851acce551b78e63b651f6116613d20))

## [1.15.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.14.1...v1.15.0) (2022-05-09)


### Features

* builder hub package ([efbdfbd](https://bitbucket.org/bitkomponist/form-engine/commit/efbdfbdd21611b0ca5e8e7569b97235f217e2753))
* form-editor sub package ([7eaf51d](https://bitbucket.org/bitkomponist/form-engine/commit/7eaf51da242a94aca7bf8c447ac6d6ccd88ca209))
* lerna monorepo conversion ([ce190bb](https://bitbucket.org/bitkomponist/form-engine/commit/ce190bba0565940f0e6fd4a510c075980b74cb60))
* **logs:** basic session log viewer ([9a4fd65](https://bitbucket.org/bitkomponist/form-engine/commit/9a4fd65af4e786a28d6eaa67663088f8dab1f2d0))
* move elements to subpackages ([a0b933c](https://bitbucket.org/bitkomponist/form-engine/commit/a0b933c3ec176da5460eaf25604bf7738cc145b4))
* move masks to dedicated package ([0f19d39](https://bitbucket.org/bitkomponist/form-engine/commit/0f19d395efa03f0422863f2e68101e23f28e625e))
* plugin naming refactor ([b32831d](https://bitbucket.org/bitkomponist/form-engine/commit/b32831db9942e78431a02450654ea17122578834))
* session logs listing layout ([dd1a224](https://bitbucket.org/bitkomponist/form-engine/commit/dd1a2244a0b526a7dbaeea5860aec712f4d299c4))
* SSB-2190 - presets ([a3e89d2](https://bitbucket.org/bitkomponist/form-engine/commit/a3e89d2e605a1efed96afba917fc2a733902555f))
* SSB-2190 - SSR Datasources ([dfd07b2](https://bitbucket.org/bitkomponist/form-engine/commit/dfd07b2ae2b45a58ce36eb929f5aa8f487f3002a))
* sub packages ([d4269d8](https://bitbucket.org/bitkomponist/form-engine/commit/d4269d89b3e170a1d061db927c7052c5f618515f))
* workflow layer subpackages ([2a50776](https://bitbucket.org/bitkomponist/form-engine/commit/2a50776989032f7d8d437230431e0b219d0da40e))


### Bug Fixes

* builder field name prop fix ([ae08368](https://bitbucket.org/bitkomponist/form-engine/commit/ae083687b2aa8c27a9dbed8f37953f93e15aba58))
* remove deprecated imports ([d979d05](https://bitbucket.org/bitkomponist/form-engine/commit/d979d05a8ea47d10b173a80120a77d9461087d6b))
* remove orphan elements on delete ([80fd596](https://bitbucket.org/bitkomponist/form-engine/commit/80fd596b2f78908190c4afef1b88af95b3852bcd))
* SSB-2190 - fix missing log fields ([38b2a50](https://bitbucket.org/bitkomponist/form-engine/commit/38b2a509f299f27094d534e0d4088f4f48281d3c))

### [1.14.1](https://bitbucket.org/bitkomponist/form-engine/compare/v1.14.0...v1.14.1) (2022-03-04)


### Bug Fixes

* fix leaky init form effect hook ([5899c59](https://bitbucket.org/bitkomponist/form-engine/commit/5899c59e8e1402c259ff93b027e18a89cb4d47ac))
* workflow and session leaks, compilation bugs ([b665d58](https://bitbucket.org/bitkomponist/form-engine/commit/b665d5831bb078b2449593136b987d3b04d2c481))

## [1.14.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.13.0...v1.14.0) (2022-03-04)


### Features

* current page hook ([e732980](https://bitbucket.org/bitkomponist/form-engine/commit/e7329807677006d1c4518c3b81df2e526ff04cb0))


### Bug Fixes

* dont prevent hotkey events in preview mode ([a11a80c](https://bitbucket.org/bitkomponist/form-engine/commit/a11a80c279951c36c1870c2a5cc7f42302f3b8b2))
* exclude s3 datasource from build ([0ed8e11](https://bitbucket.org/bitkomponist/form-engine/commit/0ed8e117ebd02333c4379f2c4ba62f5653693745))
* remove glob imports for release ([271e7e4](https://bitbucket.org/bitkomponist/form-engine/commit/271e7e453146c973e26940abb0e931030c50d51a))
* support dynamicly registered wfls and datasources ([b7841e4](https://bitbucket.org/bitkomponist/form-engine/commit/b7841e43b758e091c4fa5ec27770b5a606642155))

## [1.13.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.12.0...v1.13.0) (2022-02-25)


### Features

* array form control styling ([1d59ac9](https://bitbucket.org/bitkomponist/form-engine/commit/1d59ac9f4324531dca57caad2b48e447b3929166))
* array form control, new datasource property ([9311674](https://bitbucket.org/bitkomponist/form-engine/commit/93116742b4acebd2fcefb73b1357f3cc46445bee))
* basic plugin loader ([701b311](https://bitbucket.org/bitkomponist/form-engine/commit/701b3113308f43171739301ab14c5511cacd737a))
* debounced text inputs ([5206ca7](https://bitbucket.org/bitkomponist/form-engine/commit/5206ca7a3bb4c3752afc8696cde6fcce5b34724b))
* facetted select ([277a591](https://bitbucket.org/bitkomponist/form-engine/commit/277a5918fcf7bf5545b6222fb682ca962d5da48f))
* facetted selection, page nav, wfl values ([c53412b](https://bitbucket.org/bitkomponist/form-engine/commit/c53412b1debda1d179b48887ea4606ba7072373d))
* form properties mask ([1a45cf4](https://bitbucket.org/bitkomponist/form-engine/commit/1a45cf4bd9f2cbcb0c5859abe62ecc89d871032b))
* form slug ([dae8775](https://bitbucket.org/bitkomponist/form-engine/commit/dae87751e8e46bcc42720d792e2a513910a3b555))
* list elements markdown, l10n and tpl ([a350e13](https://bitbucket.org/bitkomponist/form-engine/commit/a350e132389a7c62e8b622d67ab3e7610b7e8ec8))
* new constants input ([b93123c](https://bitbucket.org/bitkomponist/form-engine/commit/b93123cea2f655f6e6f843ca737b221dde6ea7ce))
* overhauled registry ([90e1a79](https://bitbucket.org/bitkomponist/form-engine/commit/90e1a793f603040e92622fed7cac63a9ae0df763))
* preview settings dialog ([f43e0b0](https://bitbucket.org/bitkomponist/form-engine/commit/f43e0b00f29dba23ee7499bf33d5dd784478a71e))
* request locals ([1a4d4a9](https://bitbucket.org/bitkomponist/form-engine/commit/1a4d4a9dc9fc41def39ef0251ad0248d25ae0137))
* show valid feedback flag ([35b8e92](https://bitbucket.org/bitkomponist/form-engine/commit/35b8e9279989bef51aba98f3fe790f32c509d100))
* simple theme ([10ebc05](https://bitbucket.org/bitkomponist/form-engine/commit/10ebc0563a520b63ed7bed1b6c3001184f3e484b))


### Bug Fixes

* debounce noop ([7339b08](https://bitbucket.org/bitkomponist/form-engine/commit/7339b08ab3137895fa4fecee6e3cca2b8769d549))
* element entry mapper ([242acbd](https://bitbucket.org/bitkomponist/form-engine/commit/242acbdbc6167facb9661610718bd2b1fe72ae6b))
* facetted select as field ([d3bd764](https://bitbucket.org/bitkomponist/form-engine/commit/d3bd764ccbfcc6fb93e40097015a6170553057a1))

## [1.12.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.11.0...v1.12.0) (2022-02-03)


### Features

* dialogs, dialog actions ([9ef28d5](https://bitbucket.org/bitkomponist/form-engine/commit/9ef28d576b0fac7e33df4b7f3089f5eb0bcc4d88))
* form editor actions ([5c5cdcd](https://bitbucket.org/bitkomponist/form-engine/commit/5c5cdcda8be31adc6c8ef2463988ec81d2272033))
* nested & paste ([833a0e3](https://bitbucket.org/bitkomponist/form-engine/commit/833a0e34b052a835e19d3bb4e530378e9c2a191c))
* toasts ([d980563](https://bitbucket.org/bitkomponist/form-engine/commit/d980563f98e141cfadb267d7f555ec77089934c7))

## [1.11.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.10.0...v1.11.0) (2022-02-02)


### Features

* auto expand selected element ([3833ea3](https://bitbucket.org/bitkomponist/form-engine/commit/3833ea3e5febbcc06631c56ce9c47c49af304d6a))
* basic hotkeys ([107d4cc](https://bitbucket.org/bitkomponist/form-engine/commit/107d4cc3f3b3e62df5faf710c8f8104eadd9c25d))
* code editor ([d2aaee8](https://bitbucket.org/bitkomponist/form-engine/commit/d2aaee836cea33083d5009f91047d9701b9632a0))
* dnd improvements / cleanup ([8c270be](https://bitbucket.org/bitkomponist/form-engine/commit/8c270be45b4608531653aabed70f08fadf2b1119))
* l10n ([92aaa00](https://bitbucket.org/bitkomponist/form-engine/commit/92aaa0078a86a144a29bd0619565835ff3a2993b))
* linter, linting, serve script ([4fdba0f](https://bitbucket.org/bitkomponist/form-engine/commit/4fdba0f69b884aba4b47cab3c11f479f92b06411))
* react-select migration ([37ec633](https://bitbucket.org/bitkomponist/form-engine/commit/37ec633bce6d8012a5cd04992767da3254fd84a3))
* resizable sidebars ([888fdc9](https://bitbucket.org/bitkomponist/form-engine/commit/888fdc9fc8f6ea612764a919c1a9c26bbd69f621))
* selection hotkeys ([94296ab](https://bitbucket.org/bitkomponist/form-engine/commit/94296abe869b74500242130b9a5283bea3cb7fb0))


### Bug Fixes

* datasource loader ([0e0b7cd](https://bitbucket.org/bitkomponist/form-engine/commit/0e0b7cdc99560cfaeb0185cdd6706e37c4ad84b9))
* datasource preview ([0350d07](https://bitbucket.org/bitkomponist/form-engine/commit/0350d073ca3cd4fb9117c9475a3486438302c628))
* drag&drop fixes ([c3fbe23](https://bitbucket.org/bitkomponist/form-engine/commit/c3fbe233c02265c47fe80712e9e6b8cfd3f5f2d9))
* dropping elements on relocate ([d095fe7](https://bitbucket.org/bitkomponist/form-engine/commit/d095fe77f9ccbd8a5b9136378c61fcc80670150d))
* element order ([fea3052](https://bitbucket.org/bitkomponist/form-engine/commit/fea3052ebe8fa8c6ee478b9a71ebdad776a1d3d0))
* getItemParents ([4069abb](https://bitbucket.org/bitkomponist/form-engine/commit/4069abbff8f632d6eb34f0e9c8d247e489385d4d))
* l10n empty label ([44735f9](https://bitbucket.org/bitkomponist/form-engine/commit/44735f9a5cdc24b5dc43e439269f5c64ac5a223f))
* workflow loading invariations ([aad9ce5](https://bitbucket.org/bitkomponist/form-engine/commit/aad9ce530c68b01db475f37e4e5958d190008403))

## [1.10.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.9.1...v1.10.0) (2022-01-14)


### Features

* element conditions ([5467afa](https://bitbucket.org/bitkomponist/form-engine/commit/5467afaebf6db3fd17f5911259377ecdfbbf927c))

### [1.9.1](https://bitbucket.org/bitkomponist/form-engine/compare/v1.9.0...v1.9.1) (2022-01-13)


### Bug Fixes

* fail http wfl on status > 399 ([b70b2ca](https://bitbucket.org/bitkomponist/form-engine/commit/b70b2ca7aedbcd4057ef2795b7bbde5e14dacff1))
* linter errors ([c13397e](https://bitbucket.org/bitkomponist/form-engine/commit/c13397e085d44fd8d1bbf26994d09680faa577ab))

## [1.9.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.8.0...v1.9.0) (2022-01-13)


### Features

* advanced select ([5a13751](https://bitbucket.org/bitkomponist/form-engine/commit/5a1375195909c8aacfffd5e14cf402a2f0cebf2c))
* autocomplete attribute ([6d56365](https://bitbucket.org/bitkomponist/form-engine/commit/6d56365f0568d1996d6c388416689b4266f44663))
* preview reset ([66e7a7e](https://bitbucket.org/bitkomponist/form-engine/commit/66e7a7e12f0d44a819d866434a3cc50dab0c9115))


### Bug Fixes

* revert button event name ([f93cf58](https://bitbucket.org/bitkomponist/form-engine/commit/f93cf585d40c8ec088d7349c2c9ef88f3f68c3ad))
* various workflow fixes ([68b6712](https://bitbucket.org/bitkomponist/form-engine/commit/68b671275e3f2c6b0872a5a9e05e03718e64616e))
* workflow orchestration ([a618bd6](https://bitbucket.org/bitkomponist/form-engine/commit/a618bd6002e7a81bcf30ebc7ebbaccb004d33530))

## [1.8.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.7.0...v1.8.0) (2022-01-12)


### Features

* basic wfl backend functionality ([d41cc7f](https://bitbucket.org/bitkomponist/form-engine/commit/d41cc7f0fbad302edce7adaad8a2fa3a88118b76))
* event button ([d3aaa76](https://bitbucket.org/bitkomponist/form-engine/commit/d3aaa76881d7cc9be44d00160553b5636cd7a9fc))
* WFL editor ([cbf4c3f](https://bitbucket.org/bitkomponist/form-engine/commit/cbf4c3f840e436e47dabfbdca5fcb762bc4f9cb0))
* workflow orchestration ([0303528](https://bitbucket.org/bitkomponist/form-engine/commit/03035281a4473febfb1ceb2b6179f843a10657c2))

## [1.7.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.6.2...v1.7.0) (2022-01-11)


### Features

* multi page rendering, single page validation ([0298c6f](https://bitbucket.org/bitkomponist/form-engine/commit/0298c6fcb159ed46c3e8891c95d9cfd7f16f2a1a))
* offline form storage ([5361e60](https://bitbucket.org/bitkomponist/form-engine/commit/5361e608c8e623eabac9f3a3b39d35f9b286d43d))


### Bug Fixes

* fix input attribute names ([90f2563](https://bitbucket.org/bitkomponist/form-engine/commit/90f2563a8ea3b9e276c31efa000e9a24398b7d45))

### [1.6.2](https://bitbucket.org/bitkomponist/form-engine/compare/v1.6.1...v1.6.2) (2022-01-10)


### Bug Fixes

* resolve ds mask by apiName ([879bfd7](https://bitbucket.org/bitkomponist/form-engine/commit/879bfd71b0aaa1790fa82dc6b55ab464a099fc04))

### [1.6.1](https://bitbucket.org/bitkomponist/form-engine/compare/v1.6.0...v1.6.1) (2022-01-10)


### Bug Fixes

* missing datasources in explorer ([4e96cdf](https://bitbucket.org/bitkomponist/form-engine/commit/4e96cdf6058dc37f7dcc2a57218f2ab28253bf34))

## [1.6.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.5.0...v1.6.0) (2022-01-10)


### Features

* only commit build if dirty ([390e374](https://bitbucket.org/bitkomponist/form-engine/commit/390e37409c676e8d85fb3165b663344880e4745d))


### Bug Fixes

* commit new build on release ([0411a0b](https://bitbucket.org/bitkomponist/form-engine/commit/0411a0b102af552473f0392ce7363066b8454167))
* prerelease script ([74e0301](https://bitbucket.org/bitkomponist/form-engine/commit/74e0301f56da061a6191a280ec26d9b70832c48f))

## [1.5.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.4.2...v1.5.0) (2022-01-10)


### Features

* datasources editor styling ([b564d64](https://bitbucket.org/bitkomponist/form-engine/commit/b564d64d9f47b7a32c9a819facc2033a74319607))
* element edit mode flag ([238f147](https://bitbucket.org/bitkomponist/form-engine/commit/238f1478a2af3ce323162f419609cdc3d4db1262))


### Bug Fixes

* create fresh build on release ([383a1f7](https://bitbucket.org/bitkomponist/form-engine/commit/383a1f7c42728e7c2d6022ebef751ba242c3dd33))
* disable browser autofill when editing ([c0e67da](https://bitbucket.org/bitkomponist/form-engine/commit/c0e67da6415e0162f32e2f27f20b80e92771a994))

### [1.4.2](https://bitbucket.org/bitkomponist/form-engine/compare/v1.4.1...v1.4.2) (2022-01-10)


### Bug Fixes

* new build and icon change ([a9e5b85](https://bitbucket.org/bitkomponist/form-engine/commit/a9e5b852d689c3f2c8b44b414b7c7631fbbf19ff))

### [1.4.1](https://bitbucket.org/bitkomponist/form-engine/compare/v1.4.0...v1.4.1) (2022-01-08)

## [1.4.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.3.1...v1.4.0) (2022-01-08)


### Features

* home layout ([d82484e](https://bitbucket.org/bitkomponist/form-engine/commit/d82484ec7e390e0bfa1f7932c4877e31bf12637f))
* home layout cards ([a3ed5d7](https://bitbucket.org/bitkomponist/form-engine/commit/a3ed5d7df7b81266493910f9b166e8d2ae6e0cdb))


### Bug Fixes

* home account dropdown alignment ([53c0052](https://bitbucket.org/bitkomponist/form-engine/commit/53c00527f79889d81db1cc7e1a40b596e2fa28bf))
* root element container flow ([c6e04e6](https://bitbucket.org/bitkomponist/form-engine/commit/c6e04e60ea6263050d682732b03f7ee32de334fd))

### [1.3.1](https://bitbucket.org/bitkomponist/form-engine/compare/v1.3.0...v1.3.1) (2022-01-08)


### Bug Fixes

* lint & prettier commands pattern ([920e083](https://bitbucket.org/bitkomponist/form-engine/commit/920e083d560192f266c7a1c75add78cf7b6ad9d4))
* linter errors ([d612ae9](https://bitbucket.org/bitkomponist/form-engine/commit/d612ae94527b5d00909ace210ce6e9972f7e4574))
* linter errors ([5c6f00d](https://bitbucket.org/bitkomponist/form-engine/commit/5c6f00db0d85f6ff68ad18ec07a6cf3be94a85df))
* missing outline names ([31fc227](https://bitbucket.org/bitkomponist/form-engine/commit/31fc2278fdfbc40a434d3052083388307af14024))

## [1.3.0](https://bitbucket.org/bitkomponist/form-engine/compare/v1.2.0...v1.3.0) (2022-01-08)


### Features

* formatter & linter ([481b3f7](https://bitbucket.org/bitkomponist/form-engine/commit/481b3f7f47cf0aa43b6e440d77a2663f340a5bc8))

## 1.2.0 (2022-01-08)


### Features

* 1.0.1 ([26cebb5](https://bitbucket.org/bitkomponist/form-engine/commit/26cebb535e3ea50570e5015d3052dc6be19a52e5))
* basic new wizard ([eb31789](https://bitbucket.org/bitkomponist/form-engine/commit/eb31789bf6a6757801ef924e13f0d9432bcac26a))
* basic outline drawer ([fe015f7](https://bitbucket.org/bitkomponist/form-engine/commit/fe015f7c09203fae26fb474c1b0f51d5f3e0c31e))
* container flow ([b08ff33](https://bitbucket.org/bitkomponist/form-engine/commit/b08ff339de5cb8969d93f3aa1df5faddf8c99651))
* datasource loading && loadqueue ([e5beb06](https://bitbucket.org/bitkomponist/form-engine/commit/e5beb06ce33048029befa1189680b952da37b58f))
* delete and duplicate modals ([f7ecaf1](https://bitbucket.org/bitkomponist/form-engine/commit/f7ecaf16a2a4769137fe7930b424e401af9e26d0))
* dnd styling ([28bfafb](https://bitbucket.org/bitkomponist/form-engine/commit/28bfafb46c57fecc9781eef2832fd793239840b5))
* drag drop select mechanics ([d0bd0cb](https://bitbucket.org/bitkomponist/form-engine/commit/d0bd0cbc2ed01ff6cdef866def0425aee84f017e))
* element hover ([5da4022](https://bitbucket.org/bitkomponist/form-engine/commit/5da4022143d7a823214d91cc8a220af2e69f9aad))
* element traversal ([e765da6](https://bitbucket.org/bitkomponist/form-engine/commit/e765da67898f1f29e266e5777b71b97b54b93a95))
* form constants ([fe74e7c](https://bitbucket.org/bitkomponist/form-engine/commit/fe74e7c15eff5db238290f9f27cdd644307d62bc))
* form publishing ([bddcf7a](https://bitbucket.org/bitkomponist/form-engine/commit/bddcf7ac894a94225b2b14fd36b09a4a99defd74))
* generalized mask properties ([3036690](https://bitbucket.org/bitkomponist/form-engine/commit/3036690153f0f1fe5437b307c0c4a947ca9c03b9))
* global settings ([a7f46b7](https://bitbucket.org/bitkomponist/form-engine/commit/a7f46b7c6e1d0ef73dd889fdabc2f2e0250f5956))
* home fallback image ([650f41e](https://bitbucket.org/bitkomponist/form-engine/commit/650f41e1df05258ea1654b2e0df7de9f798ac13c))
* identity & login ([edb389d](https://bitbucket.org/bitkomponist/form-engine/commit/edb389de0c6f182c1a91927f1ed33b2832b4a96c))
* layout cleanup ([690e4f5](https://bitbucket.org/bitkomponist/form-engine/commit/690e4f55ae7d130deee90bfb963d44fdbb507a1b))
* mde autoheight ([59d47c1](https://bitbucket.org/bitkomponist/form-engine/commit/59d47c10c7daf919ee91c68aedea94362d47e04c))
* modal utility & styling ([a89d41b](https://bitbucket.org/bitkomponist/form-engine/commit/a89d41bb840421332b9b1ce5590f131e0387ab01))
* new editor layout ([18f0c6e](https://bitbucket.org/bitkomponist/form-engine/commit/18f0c6ef8d42a6f2e059872cf910b99c32eacea8))
* outline actions ([ee8d1be](https://bitbucket.org/bitkomponist/form-engine/commit/ee8d1bec7425b3b9b861f59455b6620d3d29294a))
* parcel 2 conversion ([b747f77](https://bitbucket.org/bitkomponist/form-engine/commit/b747f77006a127f786aac8f062d6b5ca9dabd89d))
* preview ([d8bc57f](https://bitbucket.org/bitkomponist/form-engine/commit/d8bc57fbba7889a103852a9d7788a85bf3b53090))
* radios and checkbox elements ([fda32fa](https://bitbucket.org/bitkomponist/form-engine/commit/fda32fab36fdc0d918d9795236fae92d001e773f))
* rename modal ([0eb5a77](https://bitbucket.org/bitkomponist/form-engine/commit/0eb5a779535917941e84ffd5b6616b2cfedd249d))
* routing ([d528cb1](https://bitbucket.org/bitkomponist/form-engine/commit/d528cb1b1fff2046152e49890bbdf4a5fb3eb824))
* select element ([0e7529f](https://bitbucket.org/bitkomponist/form-engine/commit/0e7529fb86604722c5576c0371f8150e38d026c8))
* sidebar scrollbars ([4155a0e](https://bitbucket.org/bitkomponist/form-engine/commit/4155a0eb1a8f6c7ae4a96bfd222b92a9d81196c2))
* static datasource ([73d2cbb](https://bitbucket.org/bitkomponist/form-engine/commit/73d2cbb8eb01f6bf94585c552434c6da51e5ce3d))
* template strings ([de3eb99](https://bitbucket.org/bitkomponist/form-engine/commit/de3eb997a4059b7e162efc076728a92a1ed13937))


### Bug Fixes

* check base height ([2fc9751](https://bitbucket.org/bitkomponist/form-engine/commit/2fc9751a8f66966fb1529a02ae3363ad80ea4dc1))
* dom nesting ([c54b3a5](https://bitbucket.org/bitkomponist/form-engine/commit/c54b3a5ddd170347ce6452adf33f580c89e7e5bf))
* element deletion ([05af0c8](https://bitbucket.org/bitkomponist/form-engine/commit/05af0c8892833b34a43d6a17f1084bf3b2857812))
* grid layout ([a98e693](https://bitbucket.org/bitkomponist/form-engine/commit/a98e693c2000d6fb9a5f7ea2c32bc6fd100bbadf))
* production ready fixes ([bf0c81b](https://bitbucket.org/bitkomponist/form-engine/commit/bf0c81bea766533c44c409508863af3002125a26))
* tab navigation ([6041b12](https://bitbucket.org/bitkomponist/form-engine/commit/6041b12f865314d4b314c1dd63a4e00b73c4b3df))
* typos ([8360450](https://bitbucket.org/bitkomponist/form-engine/commit/8360450d85593602b245f54bab9710a75a86d2ff))
